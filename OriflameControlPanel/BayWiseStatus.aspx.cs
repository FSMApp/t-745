﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
namespace ControlPanel
{
    public partial class BayWiseStatus : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Id"])))
            {
                Response.Redirect("Login.aspx");
            }
            if (!IsPostBack)
            {
                BindGrid();
            }

        }
        private void BindGrid()
        {
            DataTable dtBay = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
                using (MySqlCommand mycmd = new MySqlCommand("sp_getBayCurrentStatus", mycon))
                {
                    mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                    using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                    {
                        dtBay = new DataTable();
                        myadp.Fill(dtBay);
                    }
                }
                if (dtBay != null && dtBay.Rows.Count > 0)
                {
                    gridBayList.DataSource = null;
                    gridBayList.DataSource = dtBay;
                    gridBayList.DataBind();
                }
            }
            catch (Exception ex)
            {
                clsPanelsLog.writelog(ex);
            }
        }
        protected void timer_Tick(object sender, EventArgs e)
        {
            BindGrid();
            //updateTimer.Update();
            updateBayInfo.Update();
        }
    }
}