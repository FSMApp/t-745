﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Tolerance.aspx.cs" Inherits="Tolerance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updateControl" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="content" class="span10">
                <ul class="breadcrumb" style="margin-bottom: 4px;">
                    <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
                    <li><a href="CreateOperator.aspx">Configurations</a></li>
                </ul>
                <div class="row-fluid">
                    <div class="box span12">
                        <div class="box-header">
                            <h2>
                                <i class="halflings-icon edit"></i><span class="break"></span>Weight Tolerance Configurations</h2>
                        </div>
                        <div class="box-content">
                            <div class="form-horizontal">
                                <fieldset>
                                    <p>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                                    </p>
                                    <%--<div class="box-content">
                                        <div class="control-group">
                                            <label class="control-label">
                                                Weiging Machine IP <span style="color: Red;">*</span>
                                            </label>
                                            <div class="controls">
                                                <asp:TextBox runat="server" ValidationGroup="Document" ID="txtIP"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Document"
                                                    runat="server" ControlToValidate="txtIP" ErrorMessage="IP Address is required."
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <label class="control-label">
                                                tolerance<span style="color: Red;">*</span>
                                            </label>
                                            <div class="controls">
                                                <asp:TextBox runat="server" ValidationGroup="Document" ID="txttolerance"></asp:TextBox>
                                                <asp:HiddenField runat="server" ID="hdntolerance" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Document"
                                                    runat="server" ControlToValidate="txttolerance" ErrorMessage="Weight tolerance is required."
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <asp:LinkButton runat="server" ID="btnCreate" ValidationGroup="Document" CausesValidation="true"
                                                CssClass="btn btn-primary" Text="Configure" OnClick="btnSave_Click">
                                            </asp:LinkButton>
                                            <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn"></asp:Button>
                                        </div>
                                    </div>--%>
                                    <div class="box-content">
                                        <asp:Panel ID="Panel2" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                            <table class="table table-striped table-condensed">
                                                <asp:GridView runat="server" ID="gridWeightTolerance" BackColor="White" AutoGenerateColumns="false"
                                                    AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                                    <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                                    <Columns>
                                                        <asp:BoundField DataField="From_Weight" HeaderText="From Weight" HeaderStyle-Width="200px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="To_Weight" HeaderText="To Weight" HeaderStyle-Width="200px" HeaderStyle-Font-Bold="false"
                                                            HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="Tolerance_per" HeaderText="Tolerance(%)" HeaderStyle-Width="200px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="Static_Tolerance" HeaderText="Static Tolerance(in gm)" HeaderStyle-Width="200px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="Black" />
                                                    <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                                </asp:GridView>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

