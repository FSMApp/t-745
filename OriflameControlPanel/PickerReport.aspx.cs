﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PickerReport : System.Web.UI.Page
{
    string _fromdatetime = string.Empty;
    string _enddatetime = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            this.inputStartFromTime.Text = "00:00:00";
            this.inputEndFromTime.Text = "00:00:00";
            this.inputStartToTime.Text = "00:00:00";
            this.inputEndToTime.Text = "00:00:00";
        }

    }
    void BindGrid(string _fromdatetime, string _todatetime)
    {
        DataTable data = null;
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            using (MySqlCommand mycmd = new MySqlCommand("sp_getPickerProductivity_Report", mycon))
            {
                mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                mycmd.Parameters.AddWithValue("_fromdatetime", _fromdatetime);
                mycmd.Parameters.AddWithValue("_todatetime", _todatetime);
                using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                {
                    data = new DataTable();
                    myadp.Fill(data);
                }
            }
            if (data != null && data.Rows.Count > 0)
            {
                Session["PickerData"] = data;
                gridOrderDetails.DataSource = data;
                gridOrderDetails.DataBind();
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportGridToExcel();
    }

    private void ExportGridToExcel()
    {
        GridView gridView = new GridView();
        gridView.DataSource = (object)(this.Session["PickerData"] as DataTable);
        gridView.DataBind();
        this.Response.Clear();
        this.Response.Buffer = true;
        this.Response.ClearContent();
        this.Response.ClearHeaders();
        this.Response.Charset = "";
        string str = "PickerReport-" + (object)DateTime.Now + ".xls";
        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter((TextWriter)stringWriter);
        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        this.Response.ContentType = "application/vnd.ms-excel";
        this.Response.AddHeader("Content-Disposition", "attachment;filename=" + str);
        gridView.GridLines = GridLines.Both;
        gridView.HeaderStyle.Font.Bold = true;
        gridView.RenderControl(writer);
        this.Response.Write(stringWriter.ToString());
        this.Response.End();
    }



    protected void inputEndToTime_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (this.inputFromDate.Text != string.Empty)
            this._fromdatetime = this.inputFromDate.Text;
        if (this.inputToDate.Text != string.Empty)
            this._enddatetime = this.inputToDate.Text;
        if (this.inputStartFromTime.Text != string.Empty)
        {
            _fromdatetime = _fromdatetime + " " + this.inputStartFromTime.Text;
        }
        if (this.inputStartToTime.Text != string.Empty)
            _enddatetime = _enddatetime + " " + this.inputStartToTime.Text;
        this._fromdatetime = Convert.ToDateTime(this._fromdatetime).ToString("yyyy/MM/dd HH:mm:ss");
        this._enddatetime = Convert.ToDateTime(this._enddatetime).ToString("yyyy/MM/dd HH:mm:ss");
        BindGrid(this._fromdatetime, this._enddatetime);
    }
}