﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
using System.ComponentModel;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.Collections;
using System.IO;

public partial class Home : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User_Id"])))
        {
            Response.Redirect("Login.aspx");
        }
        BindchartData();
    }
    private void BindchartData()
    {
        DataSet dataSet = (DataSet)null;
        try
        {
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                using (MySqlCommand selectCommand = new MySqlCommand("sp_getDashboardGraphData", connection))
                {
                    selectCommand.CommandType = CommandType.StoredProcedure;
                    if (this.rdodaily.Checked)
                    {
                        this.btnDownload.Visible = true;
                        selectCommand.Parameters.AddWithValue("_type", (object)"H");
                    }
                    else if (this.rdoweekly.Checked)
                    {
                        selectCommand.Parameters.AddWithValue("_type", (object)"W");
                        this.btnDownload.Visible = true;
                    }
                    else if (this.rdomonthly.Checked)
                    {
                        selectCommand.Parameters.AddWithValue("_type", (object)"M");
                        this.btnDownload.Visible = true;
                    }
                    else if (this.rdoDaywise.Checked)
                    {
                        selectCommand.Parameters.AddWithValue("_type", (object)"D");
                        this.btnDownload.Visible = true;
                    }
                    using (MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(selectCommand))
                    {
                        dataSet = new DataSet();
                        mySqlDataAdapter.Fill(dataSet);
                    }
                }
            }
            if (dataSet == null || dataSet.Tables.Count <= 0)
                return;
            string[] strArray1 = new string[dataSet.Tables[0].Rows.Count];
            int[] numArray1 = new int[dataSet.Tables[0].Rows.Count];
            string[] strArray2 = new string[dataSet.Tables[0].Rows.Count];
            for (int index = 0; index < dataSet.Tables[0].Rows.Count; ++index)
            {
                strArray2[index] = dataSet.Tables[0].Rows[index][1] == DBNull.Value ? "0" : Convert.ToString(dataSet.Tables[0].Rows[index][1]).Replace("00:00:00", "").Trim();
                strArray1[index] = Convert.ToString(dataSet.Tables[0].Rows[index][0]).Replace("00:00:00", "").Trim();
                numArray1[index] = dataSet.Tables[0].Rows[index][1] == DBNull.Value ? 0 : Convert.ToInt32(dataSet.Tables[0].Rows[index][1]);
            }
            this.CompleteCartons.Series[0]["PieLabelStyle"] = "Enabled";
            this.CompleteCartons.Series[0].IsValueShownAsLabel = true;
            this.CompleteCartons.ChartAreas[0].AxisX.LabelStyle.Interval = 1.0;
            this.CompleteCartons.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            this.CompleteCartons.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            this.CompleteCartons.ChartAreas[0].AxisY.Title = "Carton Count";
            if (this.rdodaily.Checked)
            {
                this.lblCartonTitle.Text = "Hour Wise Complete Cartons";
                this.CompleteCartons.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Cartons( Hours )";
            }
            else if (this.rdoweekly.Checked)
            {
                this.lblCartonTitle.Text = "Week Wise Complete Cartons";
                this.CompleteCartons.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Cartons( Week )";
            }
            else if (this.rdomonthly.Checked)
            {
                this.lblCartonTitle.Text = "Month Wise Complete Cartons";
                this.CompleteCartons.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Cartons( Month )";
            }
            else if (this.rdoDaywise.Checked)
            {
                this.lblCartonTitle.Text = "Day Wise Complete Cartons";
                this.CompleteCartons.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Cartons( Day )";
            }
            this.CompleteCartons.ChartAreas[0].AxisY.TitleFont = new Font("Trebuchet MS", 11f);
            this.CompleteCartons.ChartAreas[0].AxisX.TitleFont = new Font("Trebuchet MS", 11f);
            this.CompleteCartons.ChartAreas[0].AxisY.TitleForeColor = Color.Green;
            this.CompleteCartons.ChartAreas[0].AxisX.TitleForeColor = Color.Green;
            this.CompleteCartons.ChartAreas[0].AxisX.LabelStyle.Font = new Font("Trebuchet MS", 10f);
            this.CompleteCartons.ChartAreas[0].AxisY.LabelStyle.Font = new Font("Trebuchet MS", 10f);
            this.CompleteCartons.Series["Series1"].ChartType = SeriesChartType.Spline;
            this.CompleteCartons.Series["Series1"].BorderWidth = 2;
            this.CompleteCartons.Series["Series1"].Color = Color.CornflowerBlue;
            this.CompleteCartons.Series["Series1"].SmartLabelStyle.Enabled = true;
            this.CompleteCartons.Series["Series1"].Points.DataBindXY((IEnumerable)strArray1, (IEnumerable)numArray1);
            if (this.rdoDaywise.Checked)
            {
                this.CompleteCartons.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                this.CompleteCartons.ChartAreas[0].AxisX.LabelStyle.Angle = 90;
            }
            string str1 = string.Empty;
            int num1 = 0;
            int num2 = 1;
            int rowIndex1 = 1;
            string empty1 = string.Empty;
            if (this.rdomonthly.Checked)
            {
                foreach (DataRow row in (InternalDataCollectionBase)dataSet.Tables[0].Rows)
                {
                    if (str1.IndexOf(Convert.ToString(row[3]) + "_" + Convert.ToString(row[2])) == -1)
                    {
                        string str2 = Convert.ToString(row[2]);
                        str1 = str1 + Convert.ToString(row[3]) + "_" + Convert.ToString(row[2]);
                        int length = dataSet.Tables[0].Select("_Quarter in ('" + Convert.ToString(row[3]) + "') and CartonCompletionDate in ('" + str2 + "')").Length;
                        this.CompleteCartons.ChartAreas[0].AxisX.CustomLabels.Add((double)num2, (double)(num2 + length), Convert.ToString(row[3]).Trim(), rowIndex1, LabelMarkStyle.LineSideMark);
                        num2 += length;
                    }
                }
                ++rowIndex1;
            }
            num1 = 0;
            int num3 = 1;
            string empty2 = string.Empty;
            foreach (DataRow row in (InternalDataCollectionBase)dataSet.Tables[0].Rows)
            {
                if (empty2.IndexOf(Convert.ToString(row[2]).Replace("00:00:00", "").Trim()) == -1)
                {
                    empty2 += Convert.ToString(row[2]).Replace("00:00:00", "").Trim();
                    int length = dataSet.Tables[0].Select("CartonCompletionDate in ('" + Convert.ToString(row[2]) + "')").Length;
                    this.CompleteCartons.ChartAreas[0].AxisX.CustomLabels.Add((double)num3, (double)(num3 + length), Convert.ToString(row[2]).Replace("00:00:00", "").Trim(), rowIndex1, LabelMarkStyle.LineSideMark);
                    num3 += length;
                }
            }
            string[] strArray3 = new string[dataSet.Tables[1].Rows.Count];
            int[] numArray2 = new int[dataSet.Tables[1].Rows.Count];
            string[] strArray4 = new string[dataSet.Tables[1].Rows.Count];
            for (int index = 0; index < dataSet.Tables[1].Rows.Count; ++index)
            {
                strArray4[index] = dataSet.Tables[1].Rows[index][1] == DBNull.Value ? "0" : Convert.ToString(dataSet.Tables[1].Rows[index][1]).Replace("00:00:00", "").Trim();
                strArray3[index] = Convert.ToString(dataSet.Tables[1].Rows[index][0]).Replace("00:00:00", "").Trim();
                numArray2[index] = dataSet.Tables[1].Rows[index][1] == DBNull.Value ? 0 : Convert.ToInt32(dataSet.Tables[1].Rows[index][1]);
            }
            this.CompleteOrders.Series[0]["PieLabelStyle"] = "Enabled";
            this.CompleteOrders.Series[0].IsValueShownAsLabel = true;
            this.CompleteOrders.ChartAreas[0].AxisX.LabelStyle.Interval = 1.0;
            this.CompleteOrders.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            this.CompleteOrders.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            this.CompleteOrders.ChartAreas[0].AxisY.Title = "Orders Count";
            if (this.rdodaily.Checked)
            {
                this.lblTilteOrder.Text = "Hour Wise Complete Orders";
                this.CompleteOrders.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Orders( Hours )";
            }
            else if (this.rdoweekly.Checked)
            {
                this.lblTilteOrder.Text = "Week Wise Complete Orders";
                this.CompleteOrders.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Orders( Week )";
            }
            else if (this.rdomonthly.Checked)
            {
                this.lblTilteOrder.Text = "Month Wise Complete Orders";
                this.CompleteOrders.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Orders( Month )";
            }
            else if (this.rdoDaywise.Checked)
            {
                this.lblTilteOrder.Text = "Day Wise Complete Orders";
                this.CompleteOrders.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Orders( Day )";
            }
            this.CompleteOrders.ChartAreas[0].AxisY.TitleFont = new Font("Trebuchet MS", 11f);
            this.CompleteOrders.ChartAreas[0].AxisX.TitleFont = new Font("Trebuchet MS", 11f);
            this.CompleteOrders.ChartAreas[0].AxisY.TitleForeColor = Color.Green;
            this.CompleteOrders.ChartAreas[0].AxisX.TitleForeColor = Color.Green;
            this.CompleteOrders.ChartAreas[0].AxisX.LabelStyle.Font = new Font("Trebuchet MS", 10f);
            this.CompleteOrders.ChartAreas[0].AxisY.LabelStyle.Font = new Font("Trebuchet MS", 10f);
            this.CompleteOrders.Series[0].ChartType = SeriesChartType.Spline;
            this.CompleteOrders.Series["Series1"].BorderWidth = 2;
            this.CompleteOrders.Series["Series1"].Color = Color.CornflowerBlue;
            this.CompleteOrders.Series["Series1"].SmartLabelStyle.Enabled = true;
            this.CompleteOrders.Series[0].Points.DataBindXY((IEnumerable)strArray3, (IEnumerable)numArray2);
            if (this.rdoDaywise.Checked)
            {
                this.CompleteOrders.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                this.CompleteOrders.ChartAreas[0].AxisX.LabelStyle.Angle = 90;
            }
            string str3 = string.Empty;
            num1 = 0;
            int num4 = 1;
            int rowIndex2 = 1;
            if (this.rdomonthly.Checked)
            {
                foreach (DataRow row in (InternalDataCollectionBase)dataSet.Tables[1].Rows)
                {
                    if (str3.IndexOf(Convert.ToString(row[3]) + "_" + Convert.ToString(row[2])) == -1)
                    {
                        string str2 = Convert.ToString(row[2]);
                        str3 = str3 + Convert.ToString(row[3]) + "_" + Convert.ToString(row[2]);
                        int length = dataSet.Tables[1].Select("_Quarter in ('" + Convert.ToString(row[3]) + "') and OrderCompletionDate in ('" + str2 + "')").Length;
                        this.CompleteOrders.ChartAreas[0].AxisX.CustomLabels.Add((double)num4, (double)(num4 + length), Convert.ToString(row[3]).Trim(), rowIndex2, LabelMarkStyle.LineSideMark);
                        num4 += length;
                    }
                }
                ++rowIndex2;
            }
            num1 = 0;
            int num5 = 1;
            string empty3 = string.Empty;
            foreach (DataRow row in (InternalDataCollectionBase)dataSet.Tables[1].Rows)
            {
                if (empty3.IndexOf(Convert.ToString(row[2]).Replace("00:00:00", "").Trim()) == -1)
                {
                    empty3 += Convert.ToString(row[2]).Replace("00:00:00", "").Trim();
                    int length = dataSet.Tables[1].Select("OrderCompletionDate in ('" + Convert.ToString(row[2]) + "')").Length;
                    this.CompleteOrders.ChartAreas[0].AxisX.CustomLabels.Add((double)num5, (double)(num5 + length), Convert.ToString(row[2]).Replace("00:00:00", "").Trim(), rowIndex2, LabelMarkStyle.LineSideMark);
                    num5 += length;
                }
            }
            if (dataSet.Tables.Count != 4)
                return;
            string[] strArray5 = new string[dataSet.Tables[2].Rows.Count];
            int[] numArray3 = new int[dataSet.Tables[2].Rows.Count];
            string[] strArray6 = new string[dataSet.Tables[2].Rows.Count];
            for (int index = 0; index < dataSet.Tables[2].Rows.Count; ++index)
            {
                strArray6[index] = dataSet.Tables[2].Rows[index][1] == DBNull.Value ? "0" : Convert.ToString(dataSet.Tables[2].Rows[index][1]).Replace("00:00:00", "").Trim();
                strArray5[index] = Convert.ToString(dataSet.Tables[2].Rows[index][0]).Replace("00:00:00", "").Trim();
                numArray3[index] = dataSet.Tables[2].Rows[index][1] == DBNull.Value ? 0 : Convert.ToInt32(dataSet.Tables[2].Rows[index][1]);
            }
            this.CompleteEaches.Series[0]["PieLabelStyle"] = "Enabled";
            this.CompleteEaches.Series[0].IsValueShownAsLabel = true;
            this.CompleteEaches.ChartAreas[0].AxisX.LabelStyle.Interval = 1.0;
            this.CompleteEaches.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            this.CompleteEaches.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            this.CompleteEaches.ChartAreas[0].AxisY.Title = "Eaches Count";
            if (this.rdodaily.Checked)
            {
                this.lblEachesTitle.Text = "Hour Wise Complete Eaches";
                this.CompleteEaches.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Eaches( Hours )";
            }
            else if (this.rdoweekly.Checked)
            {
                this.lblEachesTitle.Text = "Week Wise Complete Eaches";
                this.CompleteEaches.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Eaches( Week )";
            }
            else if (this.rdomonthly.Checked)
            {
                this.lblEachesTitle.Text = "Month Wise Complete Eaches";
                this.CompleteEaches.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Eaches( Month )";
            }
            else if (this.rdoDaywise.Checked)
            {
                this.lblEachesTitle.Text = "Day Wise Complete Eaches";
                this.CompleteEaches.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Eaches( Day )";
            }
            this.CompleteEaches.ChartAreas[0].AxisY.TitleFont = new Font("Trebuchet MS", 11f);
            this.CompleteEaches.ChartAreas[0].AxisX.TitleFont = new Font("Trebuchet MS", 11f);
            this.CompleteEaches.ChartAreas[0].AxisY.TitleForeColor = Color.Green;
            this.CompleteEaches.ChartAreas[0].AxisX.TitleForeColor = Color.Green;
            this.CompleteEaches.ChartAreas[0].AxisX.LabelStyle.Font = new Font("Trebuchet MS", 10f);
            this.CompleteEaches.ChartAreas[0].AxisY.LabelStyle.Font = new Font("Trebuchet MS", 10f);
            this.CompleteEaches.Series[0].ChartType = SeriesChartType.Spline;
            this.CompleteEaches.Series["Series1"].BorderWidth = 2;
            this.CompleteEaches.Series["Series1"].Color = Color.CornflowerBlue;
            this.CompleteEaches.Series["Series1"].SmartLabelStyle.Enabled = true;
            this.CompleteEaches.Series[0].Points.DataBindXY((IEnumerable)strArray5, (IEnumerable)numArray3);
            if (this.rdoDaywise.Checked)
            {
                this.CompleteEaches.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                this.CompleteEaches.ChartAreas[0].AxisX.LabelStyle.Angle = 90;
            }
            string str4 = string.Empty;
            num1 = 0;
            int num6 = 1;
            int rowIndex3 = 1;
            if (this.rdomonthly.Checked)
            {
                foreach (DataRow row in (InternalDataCollectionBase)dataSet.Tables[2].Rows)
                {
                    if (str4.IndexOf(Convert.ToString(row[3]) + "_" + Convert.ToString(row[2])) == -1)
                    {
                        string str2 = Convert.ToString(row[2]);
                        str4 = str4 + Convert.ToString(row[3]) + "_" + Convert.ToString(row[2]);
                        int length = dataSet.Tables[2].Select("_Quarter in ('" + Convert.ToString(row[3]) + "') and EachesCompletionDate in ('" + str2 + "')").Length;
                        this.CompleteEaches.ChartAreas[0].AxisX.CustomLabels.Add((double)num6, (double)(num6 + length), Convert.ToString(row[3]).Trim(), rowIndex3, LabelMarkStyle.LineSideMark);
                        num6 += length;
                    }
                }
                ++rowIndex3;
            }
            num1 = 0;
            int num7 = 1;
            string empty4 = string.Empty;
            foreach (DataRow row in (InternalDataCollectionBase)dataSet.Tables[2].Rows)
            {
                if (empty4.IndexOf(Convert.ToString(row[2]).Replace("00:00:00", "").Trim()) == -1)
                {
                    empty4 += Convert.ToString(row[2]).Replace("00:00:00", "").Trim();
                    int length = dataSet.Tables[2].Select("EachesCompletionDate in ('" + Convert.ToString(row[2]) + "')").Length;
                    this.CompleteEaches.ChartAreas[0].AxisX.CustomLabels.Add((double)num7, (double)(num7 + length), Convert.ToString(row[2]).Replace("00:00:00", "").Trim(), rowIndex3, LabelMarkStyle.LineSideMark);
                    num7 += length;
                }
            }
            string[] strArray7 = new string[dataSet.Tables[3].Rows.Count];
            int[] numArray4 = new int[dataSet.Tables[3].Rows.Count];
            string[] strArray8 = new string[dataSet.Tables[3].Rows.Count];
            for (int index = 0; index < dataSet.Tables[3].Rows.Count; ++index)
            {
                strArray8[index] = dataSet.Tables[3].Rows[index][1] == DBNull.Value ? "0" : Convert.ToString(dataSet.Tables[3].Rows[index][1]).Replace("00:00:00", "").Trim();
                strArray7[index] = Convert.ToString(dataSet.Tables[3].Rows[index][0]).Replace("00:00:00", "").Trim();
                numArray4[index] = dataSet.Tables[3].Rows[index][1] == DBNull.Value ? 0 : Convert.ToInt32(dataSet.Tables[3].Rows[index][1]);
            }
            this.CompleteEcahcesPerBay.Series[0]["PieLabelStyle"] = "Enabled";
            this.CompleteEcahcesPerBay.Series[0].IsValueShownAsLabel = true;
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.LabelStyle.Interval = 1.0;
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisY.Title = "Eaches Count Per Bay";
            if (this.rdodaily.Checked)
            {
                this.lblBayTitle.Text = "Daily Complete Eaches Per Bay";
                this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Eaches( Daily )";
            }
            else if (this.rdoweekly.Checked)
            {
                this.lblBayTitle.Text = "DailyComplete Eaches Per Bay";
                this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Eaches( Daily )";
            }
            else if (this.rdomonthly.Checked)
            {
                this.lblBayTitle.Text = "Daily Complete Eaches Per Bay";
                this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Eaches( Daily )";
            }
            else if (this.rdoDaywise.Checked)
            {
                this.lblBayTitle.Text = "Daily Complete Eaches Per Bay";
                this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.Title = "Time Intervals for Complete Eaches( Daily )";
            }
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisY.TitleFont = new Font("Trebuchet MS", 11f);
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.TitleFont = new Font("Trebuchet MS", 11f);
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisY.TitleForeColor = Color.Green;
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.TitleForeColor = Color.Green;
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.LabelStyle.Font = new Font("Trebuchet MS", 10f);
            this.CompleteEcahcesPerBay.ChartAreas[0].AxisY.LabelStyle.Font = new Font("Trebuchet MS", 10f);
            this.CompleteEcahcesPerBay.Series[0].ChartType = SeriesChartType.StackedColumn;
            this.CompleteEcahcesPerBay.Series["Series1"].BorderWidth = 2;
            this.CompleteEcahcesPerBay.Series["Series1"].Color = Color.CornflowerBlue;
            this.CompleteEcahcesPerBay.Series["Series1"].SmartLabelStyle.Enabled = true;
            this.CompleteEcahcesPerBay.Series[0].Points.DataBindXY((IEnumerable)strArray7, (IEnumerable)numArray4);
            if (this.rdoDaywise.Checked)
            {
                this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.IsLabelAutoFit = false;
                this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.LabelStyle.Angle = 90;
            }
            string str5 = string.Empty;
            num1 = 0;
            int num8 = 1;
            int rowIndex4 = 1;
            if (!this.rdomonthly.Checked)
                return;
            foreach (DataRow row in (InternalDataCollectionBase)dataSet.Tables[3].Rows)
            {
                if (str5.IndexOf(Convert.ToString(row[3]) + "_" + Convert.ToString(row[2])) == -1)
                {
                    string str2 = Convert.ToString(row[2]);
                    str5 = str5 + Convert.ToString(row[3]) + "_" + Convert.ToString(row[2]);
                    int length = dataSet.Tables[3].Select("ScanTimeStamp in ('" + str2 + "')").Length;
                    this.CompleteEcahcesPerBay.ChartAreas[0].AxisX.CustomLabels.Add((double)num8, (double)(num8 + length), Convert.ToString(row[3]).Trim(), rowIndex4, LabelMarkStyle.LineSideMark);
                    num8 += length;
                }
            }
            int num9 = rowIndex4 + 1;
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }

    protected void timer_Tick(object sender, EventArgs e)
    {
        this.BindchartData();
        this.updateTimer.Update();
        this.updatePage.Update();
    }

    protected void btnDownload_Click(object sender, EventArgs e)
    {
        if (this.rdoweekly.Checked)
            this.Session["_dtReportExportData"] = (object)this.downloadReport("W");
        else if (this.rdomonthly.Checked)
            this.Session["_dtReportExportData"] = (object)this.downloadReport("Y");
        else if (this.rdoDaywise.Checked)
            this.Session["_dtReportExportData"] = (object)this.downloadReport("M");
        else if (this.rdodaily.Checked)
            this.Session["_dtReportExportData"] = (object)this.downloadReport("D");
        this.ExportGridToExcel();
    }

    private void ExportGridToExcel()
    {
        GridView gridView = new GridView();
        gridView.DataSource = (object)(this.Session["_dtReportExportData"] as DataTable);
        gridView.DataBind();
        this.Response.Clear();
        this.Response.Buffer = true;
        this.Response.ClearContent();
        this.Response.ClearHeaders();
        this.Response.Charset = "";
        string str = "SummeryReport-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";
        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter((TextWriter)stringWriter);
        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        this.Response.ContentType = "application/vnd.ms-excel";
        this.Response.AddHeader("Content-Disposition", "attachment;filename=" + str);
        gridView.GridLines = GridLines.Both;
        gridView.HeaderStyle.Font.Bold = true;
        gridView.RenderControl(writer);
        this.Response.Write(stringWriter.ToString());
        this.Response.End();
    }

    private DataTable downloadReport(string _frequency)
    {
        DataTable dataTable = (DataTable)null;
        try
        {
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                using (MySqlCommand selectCommand = new MySqlCommand("SP_DetailReport", connection))
                {
                    selectCommand.CommandType = CommandType.StoredProcedure;
                    selectCommand.Parameters.AddWithValue("_frequency", (object)_frequency);
                    using (MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(selectCommand))
                    {
                        dataTable = new DataTable();
                        mySqlDataAdapter.Fill(dataTable);
                    }
                }
            }
            return dataTable;
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
            return (DataTable)null;
        }
        finally
        {
        }
    }
}