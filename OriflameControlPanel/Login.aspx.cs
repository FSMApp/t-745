﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Net;
public partial class Login : System.Web.UI.Page
{
    public static List<string> ipaddresslist = new List<string>();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack == false)
        {
            StreamReader reader = new StreamReader(Server.MapPath("~/ConfigInfo.txt"));
            string content = reader.ReadToEnd();
            string[] getUserCredentials = content.Split(';');
            if (getUserCredentials[0] != null)
            {
                MySqlConnectionStringBuilder connectionstring = new MySqlConnectionStringBuilder();
                connectionstring.Server = "localhost";
                connectionstring.Database = "dwh_falcon";
                connectionstring.MinimumPoolSize = 50;
                connectionstring.MaximumPoolSize = 1000;
                connectionstring.ConnectionTimeout = 60;
                connectionstring.UserID = getUserCredentials[0];
                connectionstring.Password = getUserCredentials[1];
            }
        }
    }

    public static string GetMD5(string text)
    {
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
        byte[] result = md5.Hash;
        StringBuilder str = new StringBuilder();

        for (int i = 1; i < result.Length; i++)
        {
            str.Append(result[i].ToString("x2"));
        }

        return str.ToString();
    }

    public string GetIP()
    {
        string Str = "";
        Str = System.Net.Dns.GetHostName();
        IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(Str);
        IPAddress[] addr = ipEntry.AddressList;
        return addr[addr.Length - 1].ToString();

    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        MySqlConnection con = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString);
        try
        {
            con.Open();
            MySqlCommand cmd = new MySqlCommand("select * from tbl_users where User_Id=@UserId and Password=@Password and (user_category !='OPERATOR')", con);
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("@UserId", user.Text);
            cmd.Parameters.AddWithValue("@Password", GetMD5(password.Text));
            MySqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                Session["User_Id"] = Convert.ToString(reader["User_Id"]);
                Session["Role"] = Convert.ToString(reader["User_Category"]);
                Response.Redirect("Home.aspx");
            }
            else
            {
                lblMessage.Text = "Invalid User or Password";
                lblMessage.ForeColor = System.Drawing.Color.Red;
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
        finally
        {

            con.Close();
        }

    }
}
