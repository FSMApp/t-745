﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="BayWiseStatus.aspx.cs" Inherits="ControlPanel.BayWiseStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .table-bordered {
            border-color: none !important;
            border-left: none !important;
        }

        .table-responsive tbody tr {
            border: none !important;
            background: none !important;
        }

        tr {
            background: none !important;
        }

        .divbay {
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            border-radius: 8px;
            border: 1px solid #BAC6E2 !important;
            font-size: 11px;
            font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

            .divbay table > tbody > tr > td a {
                font-size: 13px;
                color: brown;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="content" class="span10" style="min-height: 400px;">
        <ul class="breadcrumb">
            <li><i class="icon-home"></i><a href="Dashboard.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="Dashboard.aspx">Bay Status</a></li>
        </ul>

        <div class="row-fluid sortable">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon align-justify"></i>Bay Shipment Status</h2>
                </div>
                <div class="box-content">
                    <asp:UpdatePanel runat="server" ID="updateBayInfo" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:DataList runat="server" ID="gridBayList" Width="100%" RepeatColumns="3" RepeatDirection="Horizontal" RepeatLayout="Table">
                                <ItemTemplate>
                                    <div class="divbay">
                                        <table class="table table-responsive active" cellpadding="2" cellspacing="5">
                                            <tr>
                                                <td colspan="2">
                                                    <center>
                                                        <h3><%# DataBinder.Eval(Container.DataItem, "Zone_Name")%></h3>
                                                        - <%# DataBinder.Eval(Container.DataItem, "Location")%></center>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><b>Current Invoice </b>: <%# DataBinder.Eval(Container.DataItem, "PrevCarton")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><b>Current Crate </b>: <%# DataBinder.Eval(Container.DataItem, "CurrentCarton")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2"><b>Login Operator </b>: <%# DataBinder.Eval(Container.DataItem, "NextCarton")%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <center><%# DataBinder.Eval(Container.DataItem, "InProgressSku")%></center>
                                                </td>
                                                <td>
                                                    <center><%# DataBinder.Eval(Container.DataItem, "ProcessedSku")%></center>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>SKU Count :<div style="float: right"><%# DataBinder.Eval(Container.DataItem, "inProgressitems")%></div>
                                                </td>
                                                <td>SKU Count :
                                                    <div style="float: right"><%# DataBinder.Eval(Container.DataItem, "Processeditems")%></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>SKU Qty :
                                                    <div style="float: right"><%# DataBinder.Eval(Container.DataItem, "inProgressQty")%></div>
                                                </td>
                                                <td>SKU Qty :
                                                    <div style="float: right"><%# DataBinder.Eval(Container.DataItem, "Processedqty")%></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ItemTemplate>
                            </asp:DataList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
    <asp:UpdatePanel runat="server" ID="updateTimer" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Timer runat="server" ID="timer" OnTick="timer_Tick" Interval="2000">
            </asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
