﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Text;
using System.IO;
using MySql.Data.MySqlClient;


    public partial class Site : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Convert.ToString(Session["User_Id"])))
            {
                Response.Redirect("Login.aspx");
            }
            else
            {
                if (Convert.ToString(Session["User_Id"]) == "falcon")
                {
                    //listIdealHourPage.Visible = true;
                }
                else
                {
                    //listIdealHourPage.Visible = false;
                }
                lblUserName.Text = Convert.ToString(Session["User_Id"]);
            }
        }

        protected void lnkLogout_OnClick(object sender, EventArgs e)
        {

            //if (LoginPage.ipaddresslist.Count >0)
            //{
            //    Application["OnlineUserCounter"] = Convert.ToInt32(Application["OnlineUserCounter"]) - 1;
            //}
            //Session.Abandon();
            Response.Redirect("Login.aspx");
        }

    }
