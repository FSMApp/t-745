﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeFile="WeightBalance.aspx.cs" Inherits="ControlPanel.WeightBalance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        #txtqty a {
            cursor: pointer;
            background: black;
            text-align: center;
            height: 25px;
            display: inline-block;
            margin: 0px;
            padding: 0px 10px;
            float: left;
            line-height: 25px;
            font-size: 16px;
            font-weight: bold;
            color: #fff;
            text-decoration: none;
        }

        #txtqty input[type="text"] {
            border: solid 1px #1dac2e;
            height: 21px;
            float: left;
            width: 30px;
            padding: 1px 5px;
            text-align: center;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content" class="span10">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="CreateOperator.aspx">Weight Tolerance Balance</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>Weight Tolerance Balance</h2>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                            </p>
                            <div class="control-group">
                                <label class="control-label">
                                    Invoice Number<span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:TextBox ID="txtCartonCode" runat="server"></asp:TextBox>
                                    <asp:LinkButton runat="server" ID="btnSearch" ValidationGroup="Document" CausesValidation="true"
                                        CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click">
                                    </asp:LinkButton>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Document"
                                        runat="server" ControlToValidate="txtCartonCode" ErrorMessage="Invoice Number is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="box-content">
                                Weiging Machine Information
                                <asp:Panel ID="Panel1" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridCartonDetails" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderStyle="None">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="Carton_Code" HeaderText="Crate Code" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Weighing_Machine_IP" HeaderText="Weighing Machine IP"
                                                    HeaderStyle-Width="400px" HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF"
                                                    HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Weight" HeaderText="Crate Weight" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="Panel2" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridCartonskuDetails" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="Invoice_Number" HeaderText="Invoice Number" HeaderStyle-Width="200px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Carton_Code" HeaderText="Crate Code" HeaderStyle-Width="200px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Sku" HeaderText="Sku" HeaderStyle-Width="200px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Sku_Type" HeaderText="Sku Type" HeaderStyle-Width="100px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Pick_Qty" HeaderText="Pick Quantity" HeaderStyle-Width="100px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                               <%-- <asp:TemplateField HeaderText="Quantity" HeaderStyle-Width="220px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White">
                                                    <ItemTemplate>
                                                        <asp:Label runat="server" ID="lblQty" Style="display: none;" Text='<%#Eval("Qty")%>kk'></asp:Label>
                                                        <div id="txtqty">
                                                            <a class="minus" href="#">-</a>
                                                            <asp:TextBox runat="server" Text='<%#Eval("Qty")%>' ID="txtQty"></asp:TextBox><a
                                                                class="plus" href="#">+</a>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="220px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" TextMode="MultiLine"   ID="txtRemarks"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                            </div>
                            <div class="form-actions" style="display:none;">
                                <asp:LinkButton runat="server" ID="btnCreate" ValidationGroup="Document" CausesValidation="true"
                                    CssClass="btn btn-primary" Text="Update" OnClick="btnCreate_Click">
                                </asp:LinkButton>
                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn"></asp:Button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#txtqty a.plus').click(function () {
                $val = parseInt($(this).parent('div#txtqty').find('input[type="text"]').val(), 10) + 1;
                $(this).parent('div#txtqty').find('input[type="text"]').val($val);
                $wps = parseFloat($(this).parent().parent().parent().find('td:eq(3)').text());
                $(this).parent().parent().parent().find('td:eq(2)').text(($wps * $val).toFixed(2));
            });
            $('#txtqty a.minus').click(function () {
                $val = parseInt($(this).parent('div#txtqty').find('input[type="text"]').val(), 10)
                if ($val > 1) {
                    $val = parseInt($(this).parent('div#txtqty').find('input[type="text"]').val(), 10) - 1;
                    $(this).parent('div#txtqty').find('input[type="text"]').val($val);
                    $wps = parseFloat($(this).parent().parent().parent().find('td:eq(3)').text());
                    $(this).parent().parent().parent().find('td:eq(2)').text(($wps * $val).toFixed(2));
                }
                else {
                    alert("Value can't be less then 0");
                }
            });
        });

    </script>
</asp:Content>
