﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="MappingZone.aspx.cs" Inherits="MappingZone" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content" class="span10">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="CreateOperator.aspx">Mapping Zone</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>
                        <asp:Label runat="server" ID="ReportTitle" />Crate & Invoice Mapping Zone
                    </h2>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <div class="box-content">
                                <div>
                                    <asp:Label runat="server" Style="font-size: 35px; font-weight: bold;" ID="lblMessage"></asp:Label>
                                </div>
                            </div>
                            <div class="box-content">
                                <div style="height: 36px; font-weight: bold; float: left; margin-right: 10px;">
                                    Crate Code
                                    <asp:TextBox runat="server" ID="txtCrateCode" ClientIDMode="Static" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div style="height: 36px; font-weight: bold; float: left; margin-right: 10px;">
                                    Invoice Number
                                    <asp:TextBox runat="server" ID="txtInvoiceNumber" ClientIDMode="Static" MaxLength="20" CssClass="form-control"></asp:TextBox>
                                </div>
                                <div style="height: 36px; font-weight: bold; float: left; padding-top: 3px;">
                                    <asp:Button runat="server" Text="Submit" ID="btnMap" ClientIDMode="Static" CssClass="btn btn-success" OnClick="btnMap_Click" />
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="myModal1" role="dialog" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header" style="padding-left: 10px;">
                    <div style="background-color: cadetblue; padding: 2px 2px 2px 2px;">
                        <input type="hidden" id="hdnControlName" />
                        <h4 class="modal-title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Information</h4>
                    </div>
                </div>
                <div class="modal-body">
                    <span id="messageCrate" style="font-size: 30px;"></span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success btnclose" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            setInterval(function () {
                $("#sessionframe").attr('src', 'test.aspx');
            }, 1000);
            setInterval(function () {
                if ($("span[id*=lblMessage]").html() != '') {
                    //$("#messageCrate").html($("span[id*=lblMessage]").html());
                    $("span[id*=lblMessage]").html('');
                    //$("#myModal1").show();
                }
            }, 10000);
            $(".btnclose").click(function () {
                $("#myModal1").hide();
            });
            $("#txtCrateCode").keypress(function (e) {
                if (e.which == 13 || e.which == 9) {
                    if ($(this).val().indexOf("PBIN") != -1 && $.trim($(this).val()).length > 6) {
                        $("#txtInvoiceNumber").focus();
                        //$("#messageCrate").html('');
                    }
                    else {
                        $("span[id*=lblMessage]").html('Invalid Crate Code,Please Scan a valid Crate Codes');
                        //$("#messageCrate").html('Invalid Crate Code,Please Scan a valid Crate Code');
                        // $("#myModal1").show();
                        $(this).val('');
                        $(this).focus();
                    }
                }
            });
            $("#txtInvoiceNumber").keypress(function (e) {
                if ((e.which == 13 || e.which == 9)) {
                    if ($.trim($(this).val()).length > 3) {
                        if ($(this).val().substring(0, 2) == "40") {
                            $("#btnMap").click();
                            $("#messageCrate").html('');
                        }
                        else {
                            //$("#messageCrate").html('Invalid Invoice Number,Please Scan a valid Invoice Number.');
                            // $("#myModal1").show();
                            $(this).val('');
                            $(this).focus();
                        }
                    }
                    else {
                        $(this).val('');
                        $(this).focus();
                    }
                }
            });
            $("#btnMap").click(function () {
                if ($("#txtInvoiceNumber").val() == '' || $("#txtCrateCode").val() == '') {
                    return false;
                }
            });
        });
    </script>
</asp:Content>

