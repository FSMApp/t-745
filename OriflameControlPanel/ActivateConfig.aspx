﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="ActivateConfig.aspx.cs" Inherits="ActivateConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updateControl" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="content" class="span10">
                <ul class="breadcrumb" style="margin-bottom: 4px;">
                    <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
                    <li><a href="CreateOperator.aspx">Pending Configurations</a></li>
                </ul>
                <div class="row-fluid">
                    <div class="box span12">
                        <div class="box-header">
                            <h2>
                                <i class="halflings-icon edit"></i><span class="break"></span>Pending SKU Configurations</h2>
                        </div>
                        <div class="box-content">
                            <div class="form-horizontal">
                                <fieldset>
                                    <p>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                                    </p>
                                    <div class="box-content">
                                        <asp:Panel ID="PnlNewBay" runat="server" CssClass="sku" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                            <table class="table table-bordered table-striped table-condensed">
                                                <asp:GridView runat="server" ID="gridConfigInfo" BackColor="White" AutoGenerateColumns="false"
                                                    AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" OnRowCommand="gridConfigInfo_RowCommand">
                                                    <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                                    <Columns>
                                                        <asp:BoundField DataField="configid" HeaderText="CONFIG ID" HeaderStyle-Width="1px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="FileName" HeaderText="File Name" HeaderStyle-Width="200px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="Upload_date" HeaderText="Upload_date"
                                                            HeaderStyle-Width="200px" HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF"
                                                            HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="Upload_by" HeaderText="Upload_by" HeaderStyle-Width="100px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="Config_RunBy" HeaderText="Approved/Rejected By" HeaderStyle-Width="200px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="STATUS" HeaderText="STATUS" HeaderStyle-Width="100px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:ButtonField ButtonType="Button" ControlStyle-CssClass="btn btn-success" Text="Approve" HeaderText="Action" CommandName="Approved" />
                                                        <asp:ButtonField ButtonType="Button" Text="Reject" ControlStyle-CssClass="btn btn-danger" HeaderText="Action" CommandName="Rejected" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="Black" />
                                                    <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                                </asp:GridView>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        window.onload = function () {
            $('table').find('tr').each(function () {
                $(this).children().eq(0).css('display', 'none');
            });
        }
    </script>
</asp:Content>

