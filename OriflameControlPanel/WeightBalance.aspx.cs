﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
namespace ControlPanel
{
    public partial class WeightBalance : System.Web.UI.Page
    {
        string constr = ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        void BindGrid()
        {
            DataSet dslist = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(this.constr))
                using (MySqlCommand mycmd = new MySqlCommand("sp_getCartonDataforBalanceWeight", mycon))
                {
                    mycmd.CommandType = CommandType.StoredProcedure;
                    mycmd.Parameters.AddWithValue("_Carton_Code", txtCartonCode.Text.Trim());
                    using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                    {
                        dslist = new DataSet();
                        myadp.Fill(dslist);
                    }
                }
                if (dslist != null && dslist.Tables.Count == 2)
                {
                    gridCartonDetails.DataSource = dslist.Tables[0];
                    gridCartonDetails.DataBind();
                    gridCartonskuDetails.DataSource = dslist.Tables[1];
                    gridCartonskuDetails.DataBind();
                }
            }
            catch (Exception ex)
            {
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGrid();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            string cartoncode = string.Empty;
            string sku = string.Empty;
            foreach (GridViewRow gr in gridCartonskuDetails.Rows)
            {
                if (gr.RowType == DataControlRowType.DataRow)
                {
                    Label lblqty = (Label)gr.FindControl("lblQty");

                    TextBox txtQty = (TextBox)gr.FindControl("txtQty");
                    TextBox _remarks = (TextBox)gr.FindControl("txtRemarks");
                    if (lblqty.Text != txtQty.Text)
                    {
                        cartoncode = gr.Cells[0].Text;
                        sku = gr.Cells[1].Text;
                        using (MySqlConnection mycon = new MySqlConnection(this.constr))
                        {
                            mycon.Open();
                            using (MySqlCommand mycmd = new MySqlCommand("sp_updateOrderStatusSkuQty", mycon))
                            {
                                mycmd.CommandType = CommandType.StoredProcedure;
                                mycmd.Parameters.AddWithValue("_carton_code", cartoncode);
                                mycmd.Parameters.AddWithValue("_skucode", sku);
                                mycmd.Parameters.AddWithValue("_qty", txtQty.Text);
                                mycmd.Parameters.AddWithValue("_remarks", _remarks.Text);
                                mycmd.ExecuteNonQuery();
                            }
                        }
                    }

                }
            }
            gridCartonDetails.DataSource = null;
            gridCartonDetails.DataBind();
            gridCartonskuDetails.DataSource = null;
            gridCartonskuDetails.DataBind();
            lblMessage.Text = "Weight has been updated for Carton " + txtCartonCode.Text;
            txtCartonCode.Text = string.Empty;

        }
    }
}