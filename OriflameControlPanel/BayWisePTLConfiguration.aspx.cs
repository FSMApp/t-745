﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BayWisePTLConfiguration : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User_Id"])))
        {
            Response.Redirect("Login.aspx");
        }
        if (string.IsNullOrEmpty(Convert.ToString(Session["Role"])) || Convert.ToString(Session["Role"]).ToUpper() != "ADMIN")
        {
            Response.Redirect("Home.aspx");
        }
        if (!IsPostBack)
        {
            BindDropDown();
        }
        lblMessage.Text = "";
    }
    private void BindDropDown()
    {
        DataTable dtBay = null;
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            using (MySqlCommand mycmd = new MySqlCommand("sp_getBayConfigurationDetails", mycon))
            {
                mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                mycmd.Parameters.AddWithValue("_bayname", "1");
                using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                {
                    dtBay = new DataTable();
                    myadp.Fill(dtBay);
                }
            }
            if (dtBay != null && dtBay.Rows.Count > 0)
            {
                ddlBayName.DataSource = dtBay;
                ddlBayName.DataTextField = "Zone_Name";
                ddlBayName.DataValueField = "Zone_Name";
                ddlBayName.DataBind();
                ddlBayName.Items.Insert(0, new ListItem("Select Bay"));
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }
    protected void ddlBayName_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtPtl = null;
        try
        {
            if (ddlBayName.SelectedIndex > 0)
            {
                using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
                using (MySqlCommand mycmd = new MySqlCommand("sp_getBaywisePTLConfigurations", mycon))
                {
                    mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                    mycmd.Parameters.AddWithValue("_bayname", ddlBayName.SelectedValue);
                    using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                    {
                        dtPtl = new DataTable();
                        myadp.Fill(dtPtl);
                    }
                }
                if (dtPtl != null && dtPtl.Rows.Count > 1)
                {
                    gridBayPTLDetails.DataSource = dtPtl;
                    gridBayPTLDetails.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }

    }

    protected void gridBayPTLDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        DataTable dtskulist = null;
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            using (MySqlCommand mycmd = new MySqlCommand("sp_getSkuMaster", mycon))
            {
                mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                {
                    dtskulist = new DataTable();
                    myadp.Fill(dtskulist);
                }
            }
            foreach (GridViewRow gr in gridBayPTLDetails.Rows)
            {
                if (gr.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hdnsku = (HiddenField)gr.FindControl("hdnactivesku");
                    DropDownList ddlsku = ((DropDownList)gr.FindControl("ddlskulist"));
                    ddlsku.DataSource = dtskulist;
                    ddlsku.DataTextField = "sku_code";
                    ddlsku.DataValueField = "skuvalue";
                    ddlsku.DataBind();
                    ddlsku.ClearSelection();
                    ddlsku.Items.FindByText(hdnsku.Value).Selected = true;
                }
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                mycon.Open();
                foreach (GridViewRow gr in gridBayPTLDetails.Rows)
                {
                    if (gr.RowType == DataControlRowType.DataRow)
                    {
                        HiddenField hdnsku = (HiddenField)gr.FindControl("hdnactivesku");
                        DropDownList ddlsku = ((DropDownList)gr.FindControl("ddlskulist"));
                        if (hdnsku.Value != ddlsku.SelectedItem.Text)
                        {
                            using (MySqlCommand mycmd = new MySqlCommand("sp_setPtlActiveSkuConfiguration", mycon))
                            {
                                mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                                mycmd.Parameters.AddWithValue("_PtlCode", gr.Cells[1].Text);
                                mycmd.Parameters.AddWithValue("_newsku", ddlsku.SelectedItem.Text);
                                mycmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }
}