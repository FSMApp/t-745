﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Net;
using System.Data;
using System.Drawing;

public partial class MappingZone : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
    }
    public string GetIPAddress()
    {
        string IPAddress=string.Empty;
        IPHostEntry Host = default(IPHostEntry);
        string Hostname = null;
        Hostname = System.Environment.MachineName;
        Host = Dns.GetHostEntry(Hostname);
        foreach (IPAddress IP in Host.AddressList)
        {

            if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
            {
                IPAddress = Convert.ToString(IP);
            }
        }
        return IPAddress;
    }
    protected void btnMap_Click(object sender, EventArgs e)
    {
        DataTable dtCrate = null;
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                mycon.Open();
                using (MySqlCommand mycmd = new MySqlCommand("SP_Map_CaratetoInvoiceandPrepareList", mycon))
                {
                    mycmd.CommandType = CommandType.StoredProcedure;
                    mycmd.Parameters.Clear();
                    mycmd.Parameters.AddWithValue("_Carate_code", txtCrateCode.Text.Trim());
                    mycmd.Parameters.AddWithValue("_invoice_no", txtInvoiceNumber.Text.Trim());
                    mycmd.Parameters.AddWithValue("_system_IP", GetIPAddress());
                    mycmd.Parameters.AddWithValue("_user", Convert.ToString(Session["User_Id"]));
                    using (MySqlDataAdapter myadp=new MySqlDataAdapter(mycmd))
                    {
                        dtCrate = new DataTable();
                        myadp.Fill(dtCrate);
                    }
                }
                if(dtCrate!=null && dtCrate.Rows.Count > 0)
                {
                    if (dtCrate.Columns.Contains("ErrorStatus"))
                    {
                        lblMessage.Text = Convert.ToString(dtCrate.Rows[0]["ErrorStatus"]);
                        lblMessage.ForeColor = Color.Red;
                    }
                    else
                    {
                        lblMessage.Text = Convert.ToString(dtCrate.Rows[0]["EmptyLocation"]);
                        lblMessage.ForeColor = Color.Green;

                    }
                }
                txtCrateCode.Text = "";
                txtInvoiceNumber.Text = "";
            }
        }
        catch(Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
       
    }
}