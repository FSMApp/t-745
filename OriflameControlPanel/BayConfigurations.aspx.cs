﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class BayConfigurations : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(Convert.ToString(Session["User_Id"])))
        {
            Response.Redirect("Login.aspx");
        }
        if (string.IsNullOrEmpty(Convert.ToString(Session["Role"])) || Convert.ToString(Session["Role"]).ToUpper() != "ADMIN")
        {
            Response.Redirect("Home.aspx");
        }
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["BayId"])))
            ViewState["bayname"] = Request.QueryString["BayId"].ToString();
        if (!IsPostBack)
        {
            BindGrid();
        }
        lblMessage.Text = "";
    }
    private void BindGrid()
    {
        DataTable dtBay = null;
        try
        {
            string _bayname = Convert.ToString(ViewState["bayname"]);
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            using (MySqlCommand mycmd = new MySqlCommand("sp_getBayConfigurationDetails", mycon))
            {
                mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                //if (!string.IsNullOrEmpty(_bayname))
                mycmd.Parameters.AddWithValue("_bayname", _bayname);
                using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                {
                    dtBay = new DataTable();
                    myadp.Fill(dtBay);
                }
            }
            if (dtBay != null && dtBay.Rows.Count > 0)
            {
                gridBayDetails.DataSource = dtBay;
                gridBayDetails.DataBind();
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        string _zoneIP = string.Empty;
        string _lcdIP = string.Empty;
        string _ScannerIP = string.Empty;
        string displayname = string.Empty;
        //sp_setBayConfigurations
        using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
        {
            foreach (GridViewRow gr in gridBayDetails.Rows)
            {
                _zoneIP = ((TextBox)gr.FindControl("ZoneIP")).Text;
                _lcdIP = ((TextBox)gr.FindControl("LCDIP")).Text;
                _ScannerIP = ((TextBox)gr.FindControl("ScannerIP")).Text;
                displayname = ((TextBox)gr.FindControl("DisplayName")).Text;
                if (gr.RowType == DataControlRowType.DataRow)
                {
                    using (MySqlCommand mycmd = new MySqlCommand("sp_setBayConfigurations", mycon))
                    {
                        mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("zone_ip", _zoneIP);
                        mycmd.Parameters.AddWithValue("lcd_ip", _lcdIP);
                        mycmd.Parameters.AddWithValue("scanner_ip", _ScannerIP);
                        mycmd.Parameters.AddWithValue("displayname", displayname);
                        mycmd.Parameters.AddWithValue("zonename", gr.Cells[0].Text);
                        mycmd.ExecuteNonQuery();
                    }
                }
            }
        }
        lblMessage.Text = "Configuration has updated successfully.";
    }
}