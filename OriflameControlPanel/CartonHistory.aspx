﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="CartonHistory.aspx.cs" Inherits="CartonHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        table.mylist input {
            width: 4%;
            display: block;
            float: left;
        }

        table.mylist label {
            width: 96%;
            display: block;
            float: left;
            padding-top: 4px;
        }

        span.checked {
            float: left !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updateControl" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="content" class="span10">
                <ul class="breadcrumb" style="margin-bottom: 4px;">
                    <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
                    <li><a href="CreateOperator.aspx">Configurations</a></li>
                </ul>
                <div class="row-fluid">
                    <div class="box span12">
                        <div class="box-header">
                            <h2>
                                <i class="halflings-icon edit"></i><span class="break"></span>Invoice /Crate Details</h2>
                        </div>
                        <div class="box-content">
                            <div class="form-horizontal">
                                <fieldset>
                                    <p>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                                    </p>
                                    <div class="box-content">
                                        <div class="box-content">
                                            <div class="control-group">
                                                <table class="mylist" style='width: 100%;'>
                                                    <tr style="background: none;">
                                                        <td style='width: 100px;'>Search Type<span style="color: Red;">*</span></td>
                                                        <td style='width: 150px;'>
                                                            <asp:RadioButton runat="server" GroupName="Filter" ID="rdoCrate" Checked="true" />Crate</td>
                                                        <td style='width: 150px;'>
                                                            <asp:RadioButton runat="server" GroupName="Filter" ID="rdoInvoice" Checked="false" />Invoice</td>
                                                        <td style='width: 150px;'>
                                                            <asp:RadioButton runat="server" GroupName="Filter" ID="rdoBatch" Checked="false" />Batch Number</td>
                                                        <td>
                                                            <asp:Label runat="server" ID="lblInvoiceWeight" Style="font-weight: bold; font-size: 15px;"></asp:Label></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <div class="control-group">
                                            <label class="control-label">
                                                Crate/Invoice Code<span style="color: Red;">*</span>
                                            </label>
                                            <div class="controls">
                                                <asp:TextBox ID="txtCartonCode" runat="server"></asp:TextBox>
                                                <asp:LinkButton runat="server" ID="btnSearch" ValidationGroup="Document" CausesValidation="true"
                                                    CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click">
                                                </asp:LinkButton>
                                                <asp:LinkButton runat="server" ID="btnDownlaod"
                                                    CssClass="btn btn-primary" Text="Export" OnClick="btnDownlaod_Click">
                                                </asp:LinkButton>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Document"
                                                    runat="server" ControlToValidate="txtCartonCode" ErrorMessage="Carton Code is required."
                                                    ForeColor="Red"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="box-content">
                                                <asp:Panel ID="Panel2" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                                    <table class="table table-striped table-condensed">
                                                        <asp:GridView runat="server" ID="gridCartonHistory" BackColor="White" AutoGenerateColumns="false"
                                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" Width="100%">
                                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                                            <Columns>
                                                                <asp:BoundField DataField="Carate_Barcode" HeaderText="Crate" HeaderStyle-Width="200px"
                                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                                <asp:BoundField DataField="Order_No" HeaderText="Order No" HeaderStyle-Width="200px"
                                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                                <asp:BoundField DataField="Invoice_Number" HeaderText="Invoice" HeaderStyle-Width="200px" HeaderStyle-Font-Bold="false"
                                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                                <asp:BoundField DataField="Sku" HeaderText="Product Code" HeaderStyle-Width="100px"
                                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                                <asp:BoundField DataField="Qty" HeaderText="Qty" HeaderStyle-Width="100px"
                                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                                <asp:BoundField DataField="PICK_LOCATION" HeaderText="Pick Location" HeaderStyle-Width="100px"
                                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                                <asp:BoundField DataField="Status" HeaderText="Picking Status" HeaderStyle-Width="100px"
                                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                                <asp:BoundField DataField="Operator_id" HeaderText="Pick By" HeaderStyle-Width="100px"
                                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                            </Columns>
                                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                                        </asp:GridView>
                                                    </table>
                                                </asp:Panel>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

