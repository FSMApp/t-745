﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="SkuConfiguration.aspx.cs" Inherits="SkuConfiguration" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="content" class="span10">
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="CreateOperator.aspx">Upload Sku Configuration</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>
                        Sku Configuration </h2>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                            </p>
                            <div class="control-group">
                                <label class="control-label">
                                    Upload Sku Configration
                                </label>
                                <div class="controls">
                                    <asp:UpdatePanel runat="server" ID="updateControl" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:FileUpload runat="server" ValidationGroup="Config" ID="inputUpload" accept="application/vnd.ms-excel" />
                                            <a href="Templete/SkuConfig.csv" class="btn-xs dst">Download Sku Master Templete</a>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br />
                                    <asp:LinkButton runat="server" ID="btnCreate" ValidationGroup="Document" CausesValidation="true" Style="display: inline-table"
                                        CssClass="btn btn-primary" Text="Upload" OnClick="btnCreate_Click">
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnSubmit" ValidationGroup="Document" CausesValidation="true" Style="display: inline-table"
                                        CssClass="btn btn-primary" Text="Submit" OnClick="btnSubmit_Click">
                                    </asp:LinkButton>
                                    <%--<asp:Button runat="server" ID="btnClear" OnClick="btnClear_Click" Text="Clear" CssClass="btn" Style="display: inline-table"></asp:Button>--%>
                                </div>
                            </div>
                            <br />
                            <div class="box-content">
                                <asp:Panel ID="Panel2" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridSkuDetails" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="Pick_Location" HeaderText="Pick_Location" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="QRCode" HeaderText="QR Code" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="UploadedBy" HeaderText="Uploaded By" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="UploadedDate" HeaderText="UploadedDate" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="PTL" HeaderText="PTL Location" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>


