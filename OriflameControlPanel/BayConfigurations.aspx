﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="BayConfigurations.aspx.cs" Inherits="BayConfigurations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content" class="span10">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="CreateOperator.aspx">Zone Configuration</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>Zone Configuration</h2>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                            </p>
                            <div class="box-content">
                                <asp:Panel ID="Panel2" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridBayDetails" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="Zone_name" HeaderText="Bay Name" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="CreatedBy" HeaderText="Created By" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Zone_IP_Address" HeaderText="Zone IP Address" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Scanner_Ip_Address" HeaderText="Scanner Ip Address" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Display_Ip_Address" HeaderText="LCD IP Addesss" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                 <asp:BoundField DataField="Display_Name" HeaderText="Display Name" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <%--<asp:TemplateField HeaderText="Zone IP Address" HeaderStyle-Width="150px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" Text='<%#Eval("Zone_IP_Address")%>' BorderStyle="None" Enabled="false" BackColor="White" ID="ZoneIP"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Scanner IP Address" HeaderStyle-Width="150px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" Text='<%#Eval("Scanner_Ip_Address")%>' ID="ScannerIP"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Display Ip Address" HeaderStyle-Width="150px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" Text='<%#Eval("Display_Ip_Address")%>' ID="LCDIP"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Display Name" HeaderStyle-Width="150px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White">
                                                    <ItemTemplate>
                                                        <asp:TextBox runat="server" Text='<%#Eval("Display_Name")%>' ID="DisplayName"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                            </div>
                            <div class="form-actions">
                                <asp:LinkButton runat="server" ID="btnUpdate" ValidationGroup="Document" Visible="false" CausesValidation="true"
                                    CssClass="btn btn-primary" Text="Update" OnClick="btnUpdate_Click">
                                </asp:LinkButton>
                                <asp:Button runat="server" ID="btnClear" Text="Clear" CssClass="btn"  Visible="false"></asp:Button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

