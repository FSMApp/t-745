﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;
using FileReader;

public partial class SkuConfiguration : System.Web.UI.Page
{
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            log4net.Config.XmlConfigurator.Configure();
            BindSku();
        }
    }
    void BindSku()
    {
        DataTable dtSku = null;
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                using (MySqlCommand mycmd = new MySqlCommand("SP_UploadQrCode", mycon))
                {
                    mycmd.CommandType = CommandType.StoredProcedure;
                    mycmd.Parameters.AddWithValue("_get", 1);
                    mycmd.Parameters.AddWithValue("_file_name", string.Empty);
                    mycmd.Parameters.AddWithValue("_user", string.Empty);
                    mycmd.Parameters.AddWithValue("_QrCode", string.Empty);
                    mycmd.Parameters.AddWithValue("_Pick_Location", string.Empty);
                    using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                    {
                        dtSku = new DataTable();
                        myadp.Fill(dtSku);
                    }
                }
            }
            if (dtSku != null && dtSku.Rows.Count > 0)
            {
                gridSkuDetails.DataSource = dtSku;
                gridSkuDetails.DataBind();
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }
        finally
        {
            dtSku = null;
        }

    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        DataTable result = null;
        string _filepath = Convert.ToString(ViewState["filePath"]);
        try
        {
            result = (DataTable)(Session["QRData"]);
            uploadskuConfiguration(Path.GetFileName(_filepath), "", "");
            foreach (DataRow dr in result.Rows)
            {
                uploadskuConfiguration(Path.GetFileName(_filepath), Convert.ToString(dr["QRCode"]), Convert.ToString(dr["Pick_Location"]));
            }
            BindSku();
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }
    void uploadskuConfiguration(string filename, string _QrCode, string _Pick_Location)
    {
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                mycon.Open();
                using (MySqlCommand mycmd = new MySqlCommand("SP_UploadQrCode", mycon))
                {
                    mycmd.CommandType = CommandType.StoredProcedure;
                    mycmd.Parameters.AddWithValue("_get", 0);
                    mycmd.Parameters.AddWithValue("_file_name", filename);
                    mycmd.Parameters.AddWithValue("_user", Convert.ToString(Session["User_Id"]));
                    mycmd.Parameters.AddWithValue("_QrCode", _QrCode);
                    mycmd.Parameters.AddWithValue("_Pick_Location", _Pick_Location);
                    mycmd.ExecuteNonQuery();
                }
            }
            lblMessage.Text = "Configuration has been uploaded to run successfully";
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
            lblMessage.Text = "There is error in file! Please contact to Administrator";
        }
    }
    protected void btnCreate_Click(object sender, EventArgs e)
    {
        if (inputUpload.HasFile)
        {

            string _filename = inputUpload.FileName;
            inputUpload.SaveAs(Path.Combine(Server.MapPath("~"), ConfigurationManager.AppSettings["ItemMasterLocation"].ToString(), _filename));
            ViewState["filePath"] = Path.Combine(Server.MapPath("~"), ConfigurationManager.AppSettings["ItemMasterLocation"].ToString(), _filename);
            try
            {
                string[] Skus = File.ReadAllLines(Path.Combine(Server.MapPath("~"), ConfigurationManager.AppSettings["ItemMasterLocation"].ToString(), _filename));
                DataTable dtsku = new DataTable();
                dtsku.Columns.Add("QRCode", typeof(System.String));
                dtsku.Columns.Add("Pick_Location", typeof(System.String));

                for (int l = 1; l < Skus.Length; l++)
                {
                    if (Skus[l].Split(',')[0].Trim() != string.Empty)
                    {
                        DataRow dr = dtsku.NewRow();
                        dr[0] = Skus[l].Split(',')[0];
                        dr[1] = Skus[l].Split(',')[1];
                        dtsku.Rows.Add(dr);
                        dr = null;
                    }
                }
                Session["QRData"] = dtsku;
                gridSkuDetails.DataSource = dtsku;
                gridSkuDetails.DataBind();
            }
            catch (Exception ex)
            {
                clsPanelsLog.writelog(ex);
            }
        }
    }
}