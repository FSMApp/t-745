﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class test : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string _userid = string.Empty;
        string _Role = string.Empty;
        _userid = Convert.ToString(Session["User_Id"]);
        _Role = Convert.ToString(Session["Role"]);
        Session["User_Id"] = _userid;
        Session["Role"] = _Role;
    }
}