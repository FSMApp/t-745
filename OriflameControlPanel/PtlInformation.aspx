﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="PtlInformation.aspx.cs" Inherits="PtlInformation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .table-bordered {
            border-color: none !important;
            border-left: none !important;
        }

        .table-responsive tbody tr {
            border: none !important;
            background: none !important;
        }

        tr {
            background: none !important;
        }

        .divbay {
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            border-radius: 8px;
            border: 1px solid #BAC6E2 !important;
            font-size: 9.5px;
            font-family:'Times New Roman', Times, serif;
        }

            .divbay table > tbody > tr:first-child {
                font-size: 13px;
                color: brown;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div id="content" class="span10" style="min-height: 400px;">
        <ul class="breadcrumb">
            <li><i class="icon-home"></i><a href="Dashboard.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li>Bay Wise PTL Status<i class="icon-angle-right"></i><b><asp:Label runat="server" ID="lblBayname"></asp:Label></b></li>
        </ul>
        <div class="row-fluid sortable">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon align-justify"></i>Bay PTLs Status</h2>
                </div>
                <div class="box-content">
                    <table>
                        <asp:UpdatePanel runat="server" ID="updateBayInfo" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:DataList runat="server" ID="gridBayPTLList" Width="100%" RepeatColumns="5" RepeatDirection="Horizontal" RepeatLayout="Table">
                                    <ItemTemplate>
                                        <div class="divbay">
                                            <table class="table table-responsive active" cellpadding="2" cellspacing="5">
                                                <tr>
                                                    <td colspan="3"><center><b><%# DataBinder.Eval(Container.DataItem, "PTL_Code")%></center>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <b>Active Sku : <%# DataBinder.Eval(Container.DataItem, "Sku_Code")%>[ <%# DataBinder.Eval(Container.DataItem, "Sku_type")%> ]<br />
                                                            <%# DataBinder.Eval(Container.DataItem, "Description")%></b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <b>Alternate Sku : <%# DataBinder.Eval(Container.DataItem, "Alternate_Sku_Code")%></b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3">
                                                        <b>Carton : <%# DataBinder.Eval(Container.DataItem, "Carton_Code")%><br />
                                                            Requested Qty : <%# DataBinder.Eval(Container.DataItem, "Request_Qty")%>
                                                        </b>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </table>
                </div>
                <asp:UpdatePanel runat="server" ID="updateTimer" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Timer runat="server" ID="timer" OnTick="timer_Tick" Interval="2000">
                        </asp:Timer>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <!--/span-->
        </div>
    </div>
</asp:Content>

