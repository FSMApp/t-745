﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="BayWisePTLConfiguration.aspx.cs" Inherits="BayWisePTLConfiguration" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content" class="span10">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="CreateOperator.aspx">Zone Wise PTL Configuration</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>Zone Wise PTL Configuration</h2>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                            </p>
                            <div class="control-group">
                                <label class="control-label">
                                    Carton Code<span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList ID="ddlBayName" runat="server" OnSelectedIndexChanged="ddlBayName_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Document"
                                        runat="server" ControlToValidate="ddlBayName" ErrorMessage="Bay Name is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="box-content">
                                <asp:Panel ID="Panel2" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridBayPTLDetails" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px" OnRowDataBound="gridBayPTLDetails_RowDataBound">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="Zone_name" HeaderText="Bay Name" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="PTL_Code" HeaderText="PTL Code" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="sku_type" HeaderText="SKU type" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                 <asp:BoundField DataField="ActiveSku" HeaderText="SKU" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:TemplateField HeaderText="Active Item" HeaderStyle-Width="150px" Visible="false" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White">
                                                    <ItemTemplate>
                                                        <asp:HiddenField runat="server" ID="hdnactivesku" Value='<%#Eval("ActiveSku")%>'></asp:HiddenField>
                                                        <asp:DropDownList runat="server" ID="ddlskulist" onchange="changeSku(this);">
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Description" HeaderText="Description" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Alternate_Sku_code" HeaderText="Alternate Sku" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                            </div>
                            <div class="form-actions">
                                <asp:LinkButton runat="server" ID="btnUpdate" ValidationGroup="Document" Visible="false" CausesValidation="true"
                                    CssClass="btn btn-primary" Text="Update" OnClick="btnUpdate_Click">
                                </asp:LinkButton>
                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn" Visible="false"></asp:Button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function changeSku(obj) {
            var _value = $(obj).val().split('|');
            $(obj).parent().parent().children().eq(4).text(_value[0]);
            $(obj).parent().parent().children().eq(5).text(_value[1]);
        }
        window.onload = function () {
            $('select[id^=ddlskulist]').change(function () {
                alert($(this).val());
            });
        }
    </script>
</asp:Content>

