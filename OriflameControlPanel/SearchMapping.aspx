﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="SearchMapping.aspx.cs" Inherits="SearchMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server" ID="updateControl" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="content" class="span10">
                <ul class="breadcrumb" style="margin-bottom: 4px;">
                    <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
                    <li><a href="CreateOperator.aspx">Search Mapping</a></li>
                </ul>
                <div class="row-fluid">
                    <div class="box span12">
                        <div class="box-header">
                            <h2>
                                <i class="halflings-icon edit"></i><span class="break"></span>Search Mapping</h2>
                        </div>
                        <div class="box-content">
                            <div class="form-horizontal">
                                <fieldset>
                                    <p>
                                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                                    </p>
                                    <div class="box-content">
                                        <asp:TextBox runat="server" placeholder="Bay Name" ValidationGroup="Document" ID="txtBayName"></asp:TextBox>
                                        <asp:TextBox runat="server" placeholder="Sku Code" ValidationGroup="Document" ID="txtsku"></asp:TextBox>
                                        <asp:TextBox runat="server" placeholder="Location" ValidationGroup="Document" ID="txtLocation"></asp:TextBox>
                                        <asp:LinkButton runat="server" ID="btnSearch" ValidationGroup="Document" CausesValidation="true"
                                            CssClass="btn btn-primary" Text="Search" OnClick="btnSearch_Click">
                                        </asp:LinkButton>
                                    </div>
                                    <div class="box-content">
                                        <asp:Panel ID="Panel2" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                            <table class="table table-striped table-condensed">
                                                <asp:GridView runat="server" ID="gridSearch" BackColor="White" AutoGenerateColumns="false"
                                                    AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                                    <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                                    <Columns>
                                                        <asp:BoundField DataField="BayName" HeaderText="Bay Name" HeaderStyle-Width="200px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="PTLLocation" HeaderText="PTLLocation" HeaderStyle-Width="200px" HeaderStyle-Font-Bold="false"
                                                            HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="sku_code" HeaderText="SKU" HeaderStyle-Width="100px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="Description" HeaderText="Description" HeaderStyle-Width="100px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                        <asp:BoundField DataField="Alternate_Sku_Code" HeaderText="Alternate Sku" HeaderStyle-Width="400px"
                                                            HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="Black" />
                                                    <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                                </asp:GridView>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

