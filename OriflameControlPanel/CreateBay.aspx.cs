﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Data;
using MySql.Data.MySqlClient;
using System.Configuration;

namespace ControlPanel
{
    public partial class CreateBay : System.Web.UI.Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                log4net.Config.XmlConfigurator.Configure();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataTable dtuser = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
                using (MySqlCommand mycmd = new MySqlCommand("sp_createBay", mycon))
                {
                    mycmd.CommandType = CommandType.StoredProcedure;
                    mycmd.Parameters.AddWithValue("_bayname", txtBayName.Text.Trim());
                    mycmd.Parameters.AddWithValue("_bayIp", txtBayIP.Text.Trim());
                    mycmd.Parameters.AddWithValue("_scannerIP", txtBayScanner.Text.Trim());
                    mycmd.Parameters.AddWithValue("_lcdIP", txtBayLCD.Text.Trim());
                    mycmd.Parameters.AddWithValue("_createdBy", Convert.ToString(Session["User_Id"]));
                    using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                    {
                        dtuser = new DataTable();
                        myadp.Fill(dtuser);
                    }
                }
                if (dtuser != null && dtuser.Rows.Count > 0)
                {
                    lblMessage.Text = "Bay " + txtBayName.Text.Trim() + " for " + txtBayIP.Text.Trim() + " already exist!";
                }
                else
                {
                    lblMessage.Text = "Bay " + txtBayName.Text.Trim() + " has created Successfully ";
                }
            }
            catch (MySqlException ex)
            {
                log.Error(ex);
            }
            finally
            {
                if (dtuser != null)
                {
                    dtuser.Dispose();
                }
            }

        }
    }
}