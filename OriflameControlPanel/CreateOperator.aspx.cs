﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Data;
namespace ControlPanel
{
    public partial class CreateOperator : System.Web.UI.Page
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                log4net.Config.XmlConfigurator.Configure();
                BindUserList();
            }
        }
        void BindUserList()
        {
            DataTable dtUsers = null;
            try
            {
               using (MySqlConnection mycon=new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
                {
                    using(MySqlCommand  mycmd=new MySqlCommand("SELECT DISTINCT `User_ID`,`user_category`,`UserCode`,`CreatedBy`,`CreatedDate` FROM `tbl_users`",mycon))
                    {
                        using(MySqlDataAdapter myadp=new MySqlDataAdapter(mycmd))
                        {
                            dtUsers = new DataTable();
                            myadp.Fill(dtUsers);
                        }
                    }
                }
               if(dtUsers!=null && dtUsers.Rows.Count > 0)
                {
                    gridUserList.DataSource = dtUsers;
                    gridUserList.DataBind();
                }
    
            }
            catch(Exception ex)
            {
                log.Error(ex);
                dtUsers = null;
            }

        }
        public static string GetMD5(string text)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            byte[] result = md5.Hash;
            StringBuilder str = new StringBuilder();

            for (int i = 1; i < result.Length; i++)
            {
                str.Append(result[i].ToString("x2"));
            }

            return str.ToString();
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataTable dtuser = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
                using (MySqlCommand mycmd = new MySqlCommand("sp_createOperator", mycon))
                {
                    mycmd.CommandType = CommandType.StoredProcedure;
                    mycmd.Parameters.AddWithValue("_name", txtOperatorName.Text.Trim());
                    mycmd.Parameters.AddWithValue("_code", txtEmpCode.Text.Trim());
                    mycmd.Parameters.AddWithValue("_createdBy", Convert.ToString(Session["User_Id"]));
                    mycmd.Parameters.AddWithValue("_userType", ddlUserType.SelectedItem.Text.Trim());
                    mycmd.Parameters.AddWithValue("_password", (ddlUserType.SelectedItem.Text.Trim() == "Operator" ? string.Empty : GetMD5(ConfigurationManager.AppSettings["pwd"].ToString())));
                    using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                    {
                        dtuser = new DataTable();
                        myadp.Fill(dtuser);
                    }
                }
                if (dtuser != null && dtuser.Rows.Count > 0)
                {
                    lblMessage.Text = "User " + txtOperatorName.Text.Trim() + " or " + txtEmpCode.Text.Trim() + " already exist!";
                }
                else
                {
                    lblMessage.Text = "User " + txtOperatorName.Text.Trim() + " has created Successfully ";
                }
            }
            catch (MySqlException ex)
            {
                log.Error(ex);
            }
            finally
            {
                if (dtuser != null)
                {
                    dtuser.Dispose();
                }
            }

        }
    }
}