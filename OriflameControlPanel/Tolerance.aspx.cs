﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.Data;

public partial class Tolerance : System.Web.UI.Page
{
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            log4net.Config.XmlConfigurator.Configure();
            getCurrentConfiguration(string.Empty, 0, false);
        }
    }
    void getCurrentConfiguration(string _ipaddress, decimal _tolerance, bool _flag)
    {
        try
        {
            DataTable dtTolerance = null;
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                using (MySqlCommand mycmd = new MySqlCommand("sp_WeightTolerance", mycon))
                {
                    if (mycon.State == ConnectionState.Closed)
                        mycon.Open();
                    mycmd.CommandType = CommandType.StoredProcedure;
                    mycmd.Parameters.AddWithValue("_ipaddress", _ipaddress);
                    mycmd.Parameters.AddWithValue("_tolerance", _tolerance);
                    mycmd.Parameters.AddWithValue("_flag", (_flag ? 1 : 0));
                    using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                    {
                        dtTolerance = new DataTable();
                        myadp.Fill(dtTolerance);
                    }
                    //MySqlDataReader rdr = mycmd.ExecuteReader();
                    //if (rdr.HasRows)
                    //{
                    //    while (rdr.Read())
                    //    {
                    //        txtIP.Text = Convert.ToString(rdr[0]);
                    //        txtIP.Enabled = false;
                    //        txttolerance.Text = Convert.ToString(rdr[1]);
                    //        hdntolerance.Value = Convert.ToString(rdr[1]);
                    //        rdr.NextResult();
                    //    }
                    //}
                    gridWeightTolerance.DataSource = dtTolerance;
                    gridWeightTolerance.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        //if (txttolerance.Text == hdntolerance.Value)
        //{
        //    lblMessage.Text = "Same Configuration isalready exist for IP :" + txtIP.Text;
        //    return;
        //}
        //getCurrentConfiguration(txtIP.Text, Convert.ToDecimal(txttolerance.Text), true);
    }
}