﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
using System.IO;

public partial class CartonHistory : System.Web.UI.Page
{
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            log4net.Config.XmlConfigurator.Configure();
            getCartonDetails("", "");
        }
    }
    private void ExportGridToExcel()
    {
        GridView gridView = new GridView();
        gridView.DataSource = (object)(this.Session["_InvoiceData"] as DataTable);
        gridView.DataBind();
        this.Response.Clear();
        this.Response.Buffer = true;
        this.Response.ClearContent();
        this.Response.ClearHeaders();
        this.Response.Charset = "";
        string str = "Crate_Invoice_data-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xls";
        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter((TextWriter)stringWriter);
        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        this.Response.ContentType = "application/vnd.ms-excel";
        this.Response.AddHeader("Content-Disposition", "attachment;filename=" + str);
        gridView.GridLines = GridLines.Both;
        gridView.HeaderStyle.Font.Bold = true;
        gridView.RenderControl(writer);
        this.Response.Write(stringWriter.ToString());
        this.Response.End();
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        if (txtCartonCode.Text.Trim() != string.Empty)
        {
            string _filer = string.Empty;
            _filer = rdoCrate.Checked ? "CRT" : string.Empty;
            _filer = rdoInvoice.Checked ? "INV" : string.Empty;
            _filer = rdoBatch.Checked ? "BAT" : string.Empty;
            gridCartonHistory.DataSource = getCartonDetails(txtCartonCode.Text.Trim(), _filer);
            gridCartonHistory.DataBind();
        }
    }
    DataTable getCartonDetails(string CartonCode, string _CodeType)
    {
        DataTable dtlist = null;
        try
        {

            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                using (MySqlCommand mycmd = new MySqlCommand("sp_getCartonHistory", mycon))
                {
                    if (mycon.State == ConnectionState.Closed)
                        mycon.Open();
                    mycmd.CommandType = CommandType.StoredProcedure;
                    mycmd.Parameters.AddWithValue("_carton_code", CartonCode);
                    mycmd.Parameters.AddWithValue("_CodeType", _CodeType);
                    using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                    {
                        dtlist = new DataTable();
                        myadp.Fill(dtlist);
                        this.Session["_InvoiceData"] = dtlist;
                    }
                }
            }
            if (dtlist != null && dtlist.Rows.Count > 0)
            {
                lblInvoiceWeight.Text = "Invoice Weight : " + Convert.ToString(dtlist.Rows[0]["Weight"]);
            }
            else
            {
                lblInvoiceWeight.Text = "No Invoice Found";
            }
            return dtlist;
        }
        catch (Exception ex)
        {
            log.Error(ex);
            return null;
        }
        finally
        {
            if (dtlist != null)
                dtlist.Dispose();
        }
    }

    protected void btnDownlaod_Click(object sender, EventArgs e)
    {
        ExportGridToExcel();
    }
}