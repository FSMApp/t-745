﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ScanBasedReport : System.Web.UI.Page
{
    string _fromdatetime = string.Empty;
    string _enddatetime = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            this.inputStartFromTime.Text = "00:00:00";
            this.inputEndFromTime.Text = "00:00:00";
            this.inputStartToTime.Text = "00:00:00";
            this.inputEndToTime.Text = "00:00:00";
            hdnReportType.Value = "Sku";
        }

    }
    void BindGrid(string _fromdatetime, string _todatetime, string _Code, string _ReportType)
    {
        DataTable data = null;
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            using (MySqlCommand mycmd = new MySqlCommand("sp_getScannedData_Report", mycon))
            {
                mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                mycmd.Parameters.AddWithValue("_fromdatetime", _fromdatetime);
                mycmd.Parameters.AddWithValue("_todatetime", _todatetime);
                mycmd.Parameters.AddWithValue("_Code", _Code);
                mycmd.Parameters.AddWithValue("_ReportType", _ReportType);

                using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                {
                    data = new DataTable();
                    myadp.Fill(data);
                }
            }
            // if (data != null && data.Rows.Count > 0)
            //{
            Session["ScanData"] = data;
            gridOrderDetails.DataSource = data;
            gridOrderDetails.DataBind();
            //}
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportGridToExcel();
    }

    private void ExportGridToExcel()
    {
        GridView gridView = new GridView();
        gridView.DataSource = (object)(this.Session["ScanData"] as DataTable);
        gridView.DataBind();
        this.Response.Clear();
        this.Response.Buffer = true;
        this.Response.ClearContent();
        this.Response.ClearHeaders();
        this.Response.Charset = "";
        string str = "ScanSkuReport-" + (object)DateTime.Now + ".xls";
        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter((TextWriter)stringWriter);
        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        this.Response.ContentType = "application/vnd.ms-excel";
        this.Response.AddHeader("Content-Disposition", "attachment;filename=" + str);
        gridView.GridLines = GridLines.Both;
        gridView.HeaderStyle.Font.Bold = true;
        gridView.RenderControl(writer);
        this.Response.Write(stringWriter.ToString());
        this.Response.End();
    }



    protected void inputEndToTime_TextChanged(object sender, EventArgs e)
    {

    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (this.inputFromDate.Text != string.Empty)
            this._fromdatetime = this.inputFromDate.Text;
        if (this.inputToDate.Text != string.Empty)
            this._enddatetime = this.inputToDate.Text;
        if (this.inputStartFromTime.Text != "00:00:00")
        {
            _fromdatetime = _fromdatetime + " " + this.inputStartFromTime.Text;
        }
        if (this.inputEndToTime.Text != "00:00:00")
            _enddatetime = _enddatetime + " " + this.inputEndToTime.Text;
        if (hdnReportType.Value == "Date")
        {
            this._fromdatetime = Convert.ToDateTime(this._fromdatetime).ToString("yyyy/MM/dd HH:mm:ss");
            this._enddatetime = Convert.ToDateTime(this._enddatetime).ToString("yyyy/MM/dd HH:mm:ss");
        }
        else
        {
            this._fromdatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
            this._enddatetime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");
        }
        BindGrid(this._fromdatetime, this._enddatetime, txtCode.Text.Trim(), hdnReportType.Value);
    }
}