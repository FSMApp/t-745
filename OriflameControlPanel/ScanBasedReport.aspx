﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="ScanBasedReport.aspx.cs" Inherits="ScanBasedReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="CustomDatePicker/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="CustomDatePicker/bootstrap-clockpicker.min.css" />
    <script src="scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        function datepicker() {
            var min = $("#hdnMinValue").val();
            var max = $("#hdnMaxValue").val();

            $(".datepicker").datepicker({
                inline: true,
                minDate: new Date(min),
                maxDate: new Date(max),
                dateFormat: 'dd/mm/yy'
            });
        }
        function ShowPopup(message) {
            $(function () {
                $("#dialog").html(message);
                $("#dialog").dialog({
                    title: "Shipment Inducted Report",
                    buttons: {
                        Close: function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        };
    </script>
    <style type="text/css">
        .control-container {
            width: 100% !important;
            float: left;
            text-align: left;
            height: 63px !important;
        }

        .label-title {
            float: left;
            width: 120px;
        }

        .navbar h3 {
            color: #f5f5f5;
            margin-top: 14px;
        }

        .hljs-pre {
            background: #f8f8f8;
            padding: 3px;
        }

        .input-group {
            width: 110px;
            margin-bottom: 10px;
        }

        .pull-center {
            margin-left: auto;
            margin-right: auto;
        }

        @media (min-width: 768px) {
            .container {
                max-width: 730px;
            }
        }

        @media (max-width: 767px) {
            .pull-center {
                float: right;
            }
        }

        .form-actions {
            padding-left: 10px !important;
            border-top: none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content" class="span10">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="ScanBasedReport.aspx">Scan Based Sku List</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>
                        <asp:Label runat="server" ID="ReportTitle" />Scan Based Sku List
                    </h2>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <div class="box-content">
                                <div style="width: 100%; float: left;">
                                    <asp:Panel runat="server" ID="reportFilter">
                                        <span style="font-size: 17px; color: green; text-align: left; width: 200px; margin-top: 8px; float: left;">Select Report Filter:</span>
                                        <asp:RadioButton runat="server" ID="rdoSkuWise" ClientIDMode="Static" Checked="true" Style="width: 150px; float: left;" Text="Sku Wise" GroupName="Reports" /><%--OnCheckedChanged="rdoOpenOrderWise_CheckedChanged"--%>
                                        <asp:RadioButton runat="server" ID="rdoDateWise" ClientIDMode="Static" Style="width: 150px; float: left;" Text="Date Wise" GroupName="Reports" /><%--OnCheckedChanged="rdoCloseCartonWise_CheckedChanged"--%>
                                        <asp:RadioButton runat="server" ID="rdoInvoice" ClientIDMode="Static" Style="width: 150px; float: left;" Text="Invoice Wise" GroupName="Reports" /><%--OnCheckedChanged="rdoCloseOrderWise_CheckedChanged"--%>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div style="width: 100%; float: left;" id="divReportFilter">
                                <asp:Panel runat="server" ID="panel2">
                                    <asp:Panel runat="server" ID="Panel3">
                                        <div class="control-container">
                                            <div class="label-title" id="reportType">QR Code</div>
                                            <asp:TextBox runat="server" ID="txtCode" TextMode="MultiLine" Height="57px" Width="299px"></asp:TextBox>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                            </div>
                            <div style="width: 100%; float: left; display: none;" id="divDateFilter">
                                <asp:Panel runat="server" ID="panelReportContainer">
                                    <asp:Panel runat="server" ID="pnlFromDate">
                                        <div class="control-container" style="height: 60px!important;">
                                            <span class="label-title">From Date</span>
                                            <asp:TextBox runat="server" ID="inputFromDate" Style="float: left; height: 28px; margin-right: 4px;"
                                                CssClass="datepicker"></asp:TextBox>
                                            <div class="input-group clockpicker pull-center" data-placement="left" data-align="top"
                                                data-autoclose="true">
                                                <asp:TextBox runat="server" ID="inputStartFromTime" Style="border: 1px solid #ccc; height: 28px; width: 70px;"
                                                    placeholder="Start Time" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            </div>
                                            <asp:Panel ID="pnlFromSecondryTime" runat="server" Visible="false">
                                                <div class="input-group clockpicker pull-center" data-placement="left" data-align="top"
                                                    data-autoclose="true">
                                                    <asp:TextBox runat="server" ID="inputEndFromTime" Style="border: 1px solid #ccc; height: 28px; width: 70px;"
                                                        placeholder="Start Time" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </asp:Panel>
                                    <asp:Panel runat="server" ID="pnlToDate">
                                        <div class="control-container" style="height: 60px!important;">
                                            <span class="label-title">To Date</span>
                                            <asp:TextBox runat="server" ID="inputToDate" Style="float: left; height: 28px; margin-right: 4px;"
                                                CssClass="datepicker"></asp:TextBox>
                                            <div class="input-group clockpicker pull-center" data-placement="left" data-align="top"
                                                data-autoclose="true">
                                                <asp:TextBox runat="server" ID="inputStartToTime" Style="border: 1px solid #ccc; height: 28px; width: 70px;"
                                                    placeholder="End Time" CssClass="form-control"></asp:TextBox>
                                                <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                            </div>
                                            <asp:Panel ID="pnlToSecondryTime" runat="server" Visible="false">
                                                <div class="input-group clockpicker pull-center" data-placement="left" data-align="top"
                                                    data-autoclose="true">
                                                    <asp:TextBox runat="server" ID="inputEndToTime" Style="border: 1px solid #ccc; height: 28px; width: 70px;"
                                                        placeholder="End Time" CssClass="form-control" OnTextChanged="inputEndToTime_TextChanged"></asp:TextBox>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                            </asp:Panel>
                                            &nbsp;
                       
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>
                            </div>
                            <br />
                            <br />
                            <asp:Panel runat="server">
                                <div class="control-container" style="height: 60px!important;">
                                    <asp:Button runat="server" ID="btnSubmit" Text="Submit" CssClass="btn btn-success" OnClick="btnSubmit_Click" />
                                    <asp:Button runat="server" ID="btnExport" Text="Export" CssClass="btn btn-success" OnClick="btnExport_Click" />
                                </div>
                            </asp:Panel>

                            <asp:Panel ID="Panel1" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                <table class="table table-striped table-condensed">
                                    <asp:GridView runat="server" ID="gridOrderDetails" EmptyDataText="Not Data Found for Given Filters" BackColor="White" AutoGenerateColumns="false"
                                        AllowPaging="false" BorderStyle="None" Width="100%">
                                        <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:BoundField DataField="Order_No" HeaderText="Invoice No" HeaderStyle-Width="100px"
                                                HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Zone_ID" HeaderText="Zone ID" HeaderStyle-Width="100px"
                                                HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Pick_Location" HeaderText="Picking Station" HeaderStyle-Width="100px"
                                                HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="sku_code" HeaderText="SKU Scanned with QR Code" HeaderStyle-Width="100px"
                                                HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="ScanDatetime" HeaderText="Scan Date Time" HeaderStyle-Width="100px"
                                                HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Invoice_Date" HeaderText="Invoice Date" HeaderStyle-Width="100px"
                                                HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="Login_Id" HeaderText="Picker ID" HeaderStyle-Width="100px"
                                                HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            <asp:BoundField DataField="WareHouse" HeaderText="Location Code" HeaderStyle-Width="100px"
                                                HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                        </Columns>
                                        <FooterStyle BackColor="White" ForeColor="Black" />
                                        <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                    </asp:GridView>
                                </table>
                            </asp:Panel>

                        </fieldset>
                        <asp:HiddenField runat="server" ID="hdnReportType" Value="sku" ClientIDMode="Static" />
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="CustomDatePicker/bootstrap-clockpicker.min.js"></script>
        <script type="text/javascript">
            $("input[type=radio]").change(function () {
                $("#ContentPlaceHolder1_txtCode").val('');
                var filter = $(this).parent().parent().parent().find('label:first').text();
                switch (filter) {
                    case "Sku Wise":
                        $("#reportType").html("QR Code");
                        $("#hdnReportType").val('Sku');
                        $("#divReportFilter").show();
                        $("#divDateFilter").hide();

                        break;
                    case "Invoice Wise":
                        $("#divReportFilter").show();
                        $("#divDateFilter").hide();
                        $("#reportType").html("Invoice Number");
                        $("#hdnReportType").val('Invoice');
                        break;
                    default:
                        $("#divReportFilter").hide();
                        $("#divDateFilter").show();
                        $("#hdnReportType").val('Date');
                        break;
                }
            });
            $('.clockpicker').clockpicker()
        .find('input').change(function () {
            console.log(this.value);
        });
            var input = $('#single-input').clockpicker({
                placement: 'bottom',
                align: 'left',
                autoclose: true,
                'default': 'now'
            });

            $('.clockpicker-with-callbacks').clockpicker({
                donetext: 'Done',
                init: function () {
                    console.log("colorpicker initiated");
                },
                beforeShow: function () {
                    console.log("before show");
                },
                afterShow: function () {
                    console.log("after show");
                },
                beforeHide: function () {
                    console.log("before hide");
                },
                afterHide: function () {
                    console.log("after hide");
                },
                beforeHourSelect: function () {
                    console.log("before hour selected");
                },
                afterHourSelect: function () {
                    console.log("after hour selected");
                },
                beforeDone: function () {
                    console.log("before done");
                },
                afterDone: function () {
                    console.log("after done");
                }
            })
        .find('input').change(function () {
            console.log(this.value);
        });

            // Manually toggle to the minutes view
            $('#check-minutes').click(function (e) {
                // Have to stop propagation here
                e.stopPropagation();
                input.clockpicker('show')
                .clockpicker('toggleView', 'minutes');
            });
            if (/mobile/i.test(navigator.userAgent)) {
                $('input').prop('readOnly', true);
            }
        </script>
    </div>
</asp:Content>

