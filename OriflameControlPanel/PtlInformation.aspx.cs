﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PtlInformation : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["bayname"] = Request.QueryString["BayId"].ToString();
        lblBayname.Text = Request.QueryString["BayId"].ToString();
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    private void BindGrid()
    {
        DataTable dtBay = null;
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            using (MySqlCommand mycmd = new MySqlCommand("sp_getBayCurrentStatusbyPTL", mycon))
            {
                mycmd.CommandType = System.Data.CommandType.StoredProcedure;
                mycmd.Parameters.AddWithValue("_bayname", Convert.ToString(ViewState["bayname"]));
                using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                {
                    dtBay = new DataTable();
                    myadp.Fill(dtBay);
                }
            }
            if (dtBay != null && dtBay.Rows.Count > 0)
            {
                gridBayPTLList.DataSource = dtBay;
                gridBayPTLList.DataBind();
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }
    protected void timer_Tick(object sender, EventArgs e)
    {
        BindGrid();
        updateTimer.Update();
    }
}