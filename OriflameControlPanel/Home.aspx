﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Home.aspx.cs" Inherits="Home" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .form-group label {
            display: inline-block !important;
            color: black !important;
            font-weight: bold !important;
        }

        .radio-inline {
            width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:ScriptManager runat="server">
    </asp:ScriptManager>

    <div id="content" class="span10">
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Dashboard.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="Dashboard.aspx">Dashboard</a></li>
        </ul>


        <div class="box-content">
            <div class="row-fluid">
                <asp:UpdatePanel runat="server" ID="updatePage" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="form-group" style="width: 90%; padding-left: 30px;">
                            <label>Frequency</label>
                            <label class="radio-inline">
                                <asp:RadioButton runat="server" GroupName="chart" ID="rdodaily" value="H" Checked="true" Text="Daily" AutoPostBack="true"></asp:RadioButton>
                            </label>
                            <label class="radio-inline">
                                <asp:RadioButton runat="server" GroupName="chart" ID="rdoweekly" value="W" Text="Weekly" AutoPostBack="true"></asp:RadioButton>
                            </label>
                            <label class="radio-inline">
                                <asp:RadioButton runat="server" GroupName="chart" ID="rdoDaywise" value="D" Text="Monthly" AutoPostBack="true"></asp:RadioButton>
                            </label>
                            <label class="radio-inline">
                                <asp:RadioButton runat="server" GroupName="chart" ID="rdomonthly" value="M" Text="Yearly" AutoPostBack="true"></asp:RadioButton>
                            </label>
                            <div style="float: right; padding: 0px 10px 10px 0px;">
                                <asp:Button runat="server" ID="btnDownload" Text="Export" CssClass="btn btn-primary" OnClick="btnDownload_Click" />
                            </div>
                        </div>
                        <div class="box blue span12" ontablet="span16" ondesktop="span14">
                            <div class="box-header">
                                <h2>
                                    <i class="halflings-icon align-justify"></i>
                                    <asp:Label runat="server" ID="lblEachesTitle" Text="Hour Wise Complete Eaches"></asp:Label>
                                </h2>

                            </div>
                            <asp:Chart ID="CompleteEaches" runat="server" Width="1200px" Height="400px" BorderlineColor="#000000">
                                <Series>
                                    <asp:Series Name="Series1">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="false">
                                        <AxisY>
                                            <MajorGrid LineColor="DarkGray" LineDashStyle="Dot" />
                                        </AxisY>
                                        <AxisX>
                                            <MajorGrid Enabled="False" />
                                        </AxisX>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </div>
                        <div class="box blue span12" ontablet="span16" ondesktop="span14">
                            <div class="box-header">
                                <h2>
                                    <i class="halflings-icon align-justify"></i>
                                    <asp:Label runat="server" ID="lblCartonTitle" Text="Hour Wise Complete Cartons"></asp:Label>
                                </h2>

                            </div>
                            <asp:Chart ID="CompleteCartons" runat="server" Width="1200px" Height="400px" BorderlineColor="#000000">
                                <Series>
                                    <asp:Series Name="Series1">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="false">
                                        <AxisY>
                                            <MajorGrid LineColor="DarkGray" LineDashStyle="Dot" />
                                        </AxisY>
                                        <AxisX>
                                            <MajorGrid Enabled="False" />
                                        </AxisX>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </div>
                        <div class="box blue span12" ontablet="span16" ondesktop="span14">
                            <div class="box-header">
                                <h2>
                                    <i class="halflings-icon align-justify"></i>
                                    <asp:Label runat="server" ID="lblTilteOrder" Text="Hour Wise Complete Orders"></asp:Label>
                                </h2>
                            </div>
                            <div class="box-content">
                                <asp:Chart ID="CompleteOrders" runat="server" Width="1200px" Height="400px">
                                    <Series>
                                        <asp:Series Name="Series1" XValueMember="CompletOrders" BorderWidth="5" ChartArea="ChartArea1" XAxisType="Primary">
                                        </asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="false">
                                        </asp:ChartArea>
                                    </ChartAreas>
                                </asp:Chart>

                            </div>
                        </div>
                        <div class="box blue span12" ontablet="span16" ondesktop="span14">
                            <div class="box-header">
                                <h2>
                                    <i class="halflings-icon align-justify"></i>
                                    <asp:Label runat="server" ID="lblBayTitle" Text="Hour Wise Complete Eaches in Bay"></asp:Label>
                                </h2>

                            </div>
                            <asp:Chart ID="CompleteEcahcesPerBay" runat="server" Width="1200px" Height="400px" BorderlineColor="#000000">
                                <Series>
                                    <asp:Series Name="Series1">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1" Area3DStyle-Enable3D="false">
                                        <AxisY>
                                            <MajorGrid LineColor="DarkGray" LineDashStyle="Dot" />
                                        </AxisY>
                                        <AxisX>
                                            <MajorGrid Enabled="False" />
                                        </AxisX>
                                    </asp:ChartArea>
                                </ChartAreas>
                            </asp:Chart>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnDownload" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>
    <asp:UpdatePanel runat="server" ID="updateTimer" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Timer runat="server" ID="timer" OnTick="timer_Tick" Interval="30000">
            </asp:Timer>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

