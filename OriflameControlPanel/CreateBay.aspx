﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeFile="CreateBay.aspx.cs" Inherits="ControlPanel.CreateBay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content" class="span10">
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right">
            </i></li>
            <li><a href="CreateOperator.aspx">Create Bay(Zone)</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>Bay Creation Form</h2>
                    <%--<div class="box-icon" runat="server" id="viewAll">
                        <asp:HyperLink ID="hyplnk" runat="server" Style="color: White;" NavigateUrl="ActivityMasterAdmin.aspx">View All</asp:HyperLink>
                    </div>--%>
                </div>
                <div class="box-content" >
                    <div class="form-horizontal" >
                        <fieldset>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label></p>
                            <div class="control-group">
                                <label class="control-label">
                                    Bay Name <span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" ID="txtBayName"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Document"
                                        runat="server" ControlToValidate="txtBayName" ErrorMessage="Operator Name is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Bay IP Address <span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" ID="txtBayIP"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Document"
                                        runat="server" ControlToValidate="txtBayIP" ErrorMessage="Employee Code is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Bay Scanner IP Address<span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" ID="txtBayScanner"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Document"
                                        runat="server" ControlToValidate="txtBayScanner" ErrorMessage="Employee Code is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Bay LCD IP Address<span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" ID="txtBayLCD"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Document"
                                        runat="server" ControlToValidate="txtBayLCD" ErrorMessage="Employee Code is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-actions">
                                <asp:LinkButton runat="server" ID="btnCreate" ValidationGroup="Document" CausesValidation="true"
                                    CssClass="btn btn-primary" Text="Create" OnClick=btnSave_Click>
                                </asp:LinkButton>
                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn"></asp:Button>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
</asp:Content>
