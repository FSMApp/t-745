﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class OrderSummary : System.Web.UI.Page
{
    private string _fromdatetime = string.Empty;
    private string _enddatetime = string.Empty;
    private DataTable _dtReportExportData;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.Page.IsPostBack)
        {
            this.inputStartFromTime.Text = "00:00:00";
            this.inputEndFromTime.Text = "00:00:00";
            this.inputStartToTime.Text = "00:00:00";
            this.inputEndToTime.Text = "00:00:00";
        }
    }

    protected void btngetData_Click(object sender, EventArgs e)
    {
        if (this.inputFromDate.Text != string.Empty)
            this._fromdatetime = this.inputFromDate.Text;
        if (this.inputToDate.Text != string.Empty)
            this._enddatetime = this.inputToDate.Text;
        if (this.inputStartFromTime.Text != string.Empty)
        {
            _fromdatetime = _fromdatetime + " " + this.inputStartFromTime.Text;
        }
        if (this.inputStartToTime.Text != string.Empty)
        {
            _enddatetime = _enddatetime + " " + this.inputStartToTime.Text;
        }
        this._fromdatetime = Convert.ToDateTime(this._fromdatetime).ToString("yyyy/MM/dd HH:mm:ss");
        this._enddatetime = Convert.ToDateTime(this._enddatetime).ToString("yyyy/MM/dd HH:mm:ss");
        this.gridOrderDetails.Visible = false;
        this.BindGrid(this._fromdatetime, this._enddatetime, this.gridOrderDetails);
    }
    private void BindGrid(string _fromDatetime, string _endDatetime, GridView grd)
    {
        DataTable dataTable = (DataTable)null;
        try
        {
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                using (MySqlCommand selectCommand = new MySqlCommand("SP_OrderSummaryReport", connection))
                {
                    selectCommand.CommandType = CommandType.StoredProcedure;
                    selectCommand.Parameters.AddWithValue("_fromdate", (object)_fromDatetime);
                    selectCommand.Parameters.AddWithValue("_todate", (object)_endDatetime);
                    using (MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(selectCommand))
                    {
                        dataTable = new DataTable();
                        mySqlDataAdapter.Fill(dataTable);
                    }
                }
                this._dtReportExportData = dataTable;
                this.Session["_dtReportExportData"] = (object)dataTable;
                grd.DataSource = (object)dataTable;
                grd.Visible = true;
                grd.DataBind();
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }
    private void ExportGridToExcel()
    {
        GridView gridView = new GridView();
        gridView.DataSource = (object)(this.Session["_dtReportExportData"] as DataTable);
        gridView.DataBind();
        this.Response.Clear();
        this.Response.Buffer = true;
        this.Response.ClearContent();
        this.Response.ClearHeaders();
        this.Response.Charset = "";
        string str = "OrderSummaryReport-" + (object)DateTime.Now + ".xls";
        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter((TextWriter)stringWriter);
        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        this.Response.ContentType = "application/vnd.ms-excel";
        this.Response.AddHeader("Content-Disposition", "attachment;filename=" + str);
        gridView.GridLines = GridLines.Both;
        gridView.HeaderStyle.Font.Bold = true;
        gridView.RenderControl(writer);
        this.Response.Write(stringWriter.ToString());
        this.Response.End();
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        ExportGridToExcel();
    }
}