﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="QRCodeStats.aspx.cs" Inherits="QRCodeStats" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link rel="stylesheet" type="text/css" href="CustomDatePicker/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="CustomDatePicker/bootstrap-clockpicker.min.css" />
    <script src="scripts/jquery-1.9.1.js" type="text/javascript"></script>
    <script type="text/javascript">
        function datepicker() {
            var min = $("#hdnMinValue").val();
            var max = $("#hdnMaxValue").val();

            $(".datepicker").datepicker({
                inline: true,
                minDate: new Date(min),
                maxDate: new Date(max),
                dateFormat: 'dd/mm/yy'
            });
        }
        function ShowPopup(message) {
            $(function () {
                $("#dialog").html(message);
                $("#dialog").dialog({
                    title: "Shipment Inducted Report",
                    buttons: {
                        Close: function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            });
        };
    </script>
    <style type="text/css">
        .control-container {
            width: 100% !important;
            float: left;
            text-align: left;
            height: 63px !important;
        }

        .label-title {
            float: left;
            width: 120px;
        }

        .navbar h3 {
            color: #f5f5f5;
            margin-top: 14px;
        }

        .hljs-pre {
            background: #f8f8f8;
            padding: 3px;
        }

        .input-group {
            width: 110px;
            margin-bottom: 10px;
        }

        .pull-center {
            margin-left: auto;
            margin-right: auto;
        }

        @media (min-width: 768px) {
            .container {
                max-width: 730px;
            }
        }

        @media (max-width: 767px) {
            .pull-center {
                float: right;
            }
        }

        .form-actions {
            padding-left: 10px !important;
            border-top: none !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="content" class="span10">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="OrderSummary.aspx">Report</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>
                        <asp:Label runat="server" ID="ReportTitle" />QRCode Stats Reports
                    </h2>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <div class="box-content">
                                <div style="height: 36px; font-weight: bold; float: left;">
                                    <asp:Label runat="server" Style="color: green;" ID="lblMessage"></asp:Label>
                                </div>
                                <div style="width: 100%; float: left;">
                                    <asp:Panel runat="server" ID="panelReportContainer">
                                        <asp:Panel runat="server" ID="pnlFromDate">
                                            <div class="control-container" style="height: 60px!important;">
                                                <span class="label-title">From Date</span>
                                                <asp:TextBox runat="server" ID="inputFromDate" AutoCompleteType="None" Style="float: left; height: 28px; margin-right: 4px;"
                                                    CssClass="datepicker"></asp:TextBox>
                                                <div class="input-group clockpicker pull-center" data-placement="left" data-align="top"
                                                    data-autoclose="true">
                                                    <asp:TextBox runat="server" ID="inputStartFromTime" AutoCompleteType="None" Style="border: 1px solid #ccc; height: 28px; width: 70px;"
                                                        placeholder="Start Time" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                                <asp:Panel ID="pnlFromSecondryTime" runat="server" Visible="false">
                                                    <div class="input-group clockpicker pull-center" data-placement="left" data-align="top"
                                                        data-autoclose="true">
                                                        <asp:TextBox runat="server" ID="inputEndFromTime" Style="border: 1px solid #ccc; height: 28px; width: 70px;"
                                                            placeholder="Start Time" CssClass="form-control"></asp:TextBox>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                    </div>
                                                </asp:Panel>
                                                &nbsp;&nbsp;<%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                    ControlToValidate="inputFromDate" ValidationGroup="report" ForeColor="Red" ErrorMessage="Select Date"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="pnlToDate">
                                            <div class="control-container" style="height: 60px!important;">
                                                <span class="label-title">To Date</span>
                                                <asp:TextBox runat="server" ID="inputToDate" AutoCompleteType="None" Style="float: left; height: 28px; margin-right: 4px;"
                                                    CssClass="datepicker"></asp:TextBox>
                                                <div class="input-group clockpicker pull-center" data-placement="left" data-align="top"
                                                    data-autoclose="true">
                                                    <asp:TextBox runat="server" ID="inputStartToTime" AutoCompleteType="None" Style="border: 1px solid #ccc; height: 28px; width: 70px;"
                                                        placeholder="End Time" CssClass="form-control"></asp:TextBox>
                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                </div>
                                                <asp:Panel ID="pnlToSecondryTime" runat="server" Visible="false">
                                                    <div class="input-group clockpicker pull-center" data-placement="left" data-align="top"
                                                        data-autoclose="true">
                                                        <asp:TextBox runat="server" AutoCompleteType="None" ID="inputEndToTime" Style="border: 1px solid #ccc; height: 28px; width: 70px;"
                                                            placeholder="End Time" CssClass="form-control"></asp:TextBox>
                                                        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
                                                    </div>
                                                </asp:Panel>
                                                &nbsp;
                       
                                            </div>
                                        </asp:Panel>
                                    </asp:Panel>

                                </div>
                                <div class="form-actions">
                                    <asp:LinkButton runat="server" ID="btngetData" ValidationGroup="Document" CausesValidation="true"
                                        CssClass="btn btn-primary" Text="GetData" OnClick="btngetData_Click">
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnExport" ValidationGroup="Document" CausesValidation="true"
                                        CssClass="btn btn-primary" Text="Export" OnClick="btnExport_Click">
                                    </asp:LinkButton>
                                </div>
                                <hr />
                                <asp:Panel ID="Panel1" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table style="width: 100%;">
                                        <asp:GridView runat="server" ID="gridOrderDetails" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderStyle="None">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="PickingDate" HeaderText="Scan Date" HeaderStyle-Width="100px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="TotalInvoice" HeaderText="Total Invoice" HeaderStyle-Width="100px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="ScannableQty" HeaderText="Total Scannable QRcode" HeaderStyle-Width="100px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="ScannnedQty" HeaderText="Total Scanned Qty" HeaderStyle-Width="100px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="CustomDatePicker/bootstrap-clockpicker.min.js"></script>
    <script type="text/javascript">
        $('.clockpicker').clockpicker().find('input').change(function () {
            console.log(this.value);
        });
        var input = $('#single-input').clockpicker({
            placement: 'bottom',
            align: 'left',
            autoclose: true,
            'default': 'now'
        });

        $('.clockpicker-with-callbacks').clockpicker({
            donetext: 'Done',
            init: function () {
                console.log("colorpicker initiated");
            },
            beforeShow: function () {
                console.log("before show");
            },
            afterShow: function () {
                console.log("after show");
            },
            beforeHide: function () {
                console.log("before hide");
            },
            afterHide: function () {
                console.log("after hide");
            },
            beforeHourSelect: function () {
                console.log("before hour selected");
            },
            afterHourSelect: function () {
                console.log("after hour selected");
            },
            beforeDone: function () {
                console.log("before done");
            },
            afterDone: function () {
                console.log("after done");
            }
        })
    .find('input').change(function () {
        console.log(this.value);
    });

        // Manually toggle to the minutes view
        $('#check-minutes').click(function (e) {
            // Have to stop propagation here
            e.stopPropagation();
            input.clockpicker('show')
            .clockpicker('toggleView', 'minutes');
        });
        if (/mobile/i.test(navigator.userAgent)) {
            $('input').prop('readOnly', true);
        }
    </script>
</asp:Content>

