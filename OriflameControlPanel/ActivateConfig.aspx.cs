﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Data;
public partial class ActivateConfig : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            BindGrid();
        }
    }
    void BindGrid()
    {
        DataTable dtconfigs = null;
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            using (MySqlCommand mycmd = new MySqlCommand("sp_getPendingConfigurations", mycon))
            {
                mycmd.CommandType = CommandType.StoredProcedure;
                using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                {
                    dtconfigs = new DataTable();
                    myadp.Fill(dtconfigs);
                }
            }
            if (dtconfigs != null && dtconfigs.Rows.Count > 0)
            {
                gridConfigInfo.DataSource = dtconfigs;
                gridConfigInfo.DataBind();
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }

    protected void gridConfigInfo_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        DataTable dtconfigs = null;

        try
        {
            Int64 _configid = Convert.ToInt64(gridConfigInfo.Rows[Convert.ToInt32(e.CommandArgument)].Cells[0].Text);
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            using (MySqlCommand mycmd = new MySqlCommand("sp_addSkuConfigtoQueue", mycon))
            {
                mycmd.CommandType = CommandType.StoredProcedure;
                mycmd.Parameters.AddWithValue("_config_id", _configid);
                mycmd.Parameters.AddWithValue("_uploadby", Session["User_Id"]);
                mycmd.Parameters.AddWithValue("_file_name", e.CommandName);
                mycmd.Parameters.AddWithValue("_filepath", string.Empty);
                mycmd.Parameters.AddWithValue("_type", string.Empty);
                using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                {
                    dtconfigs = new DataTable();
                    myadp.Fill(dtconfigs);
                }
            }
            if (dtconfigs != null && dtconfigs.Rows.Count > 0)
            {
                gridConfigInfo.DataSource = dtconfigs;
                gridConfigInfo.DataBind();
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }
}