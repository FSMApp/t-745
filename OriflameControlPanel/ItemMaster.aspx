﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="ItemMaster.aspx.cs" Inherits="ControlPanel.ItemMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="Scripts/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-1.9.1.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div id="content" class="span10">
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="CreateOperator.aspx">Upload Sku</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>Item Master</h2>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>



                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                            </p>
                            <div class="control-group">
                                <label class="control-label">
                                    Upload Sku Configration
                                </label>
                                <div class="controls">
                                    <asp:UpdatePanel runat="server" ID="updateControl" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:FileUpload runat="server" ValidationGroup="Config" ID="inputUpload" accept="application/vnd.ms-excel" />
                                            <a href="Templete/SKUmaster.xlsx" class="btn-xs dst">Download Sku Master Templete</a>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <br />
                                    <asp:LinkButton runat="server" ID="btnCreate" ValidationGroup="Document" CausesValidation="true" Style="display: inline-table"
                                        CssClass="btn btn-primary" Text="Upload" OnClick="btnCreate_Click">
                                    </asp:LinkButton>
                                    <asp:LinkButton runat="server" ID="btnSubmit" ValidationGroup="Document" CausesValidation="true" Style="display: inline-table"
                                        CssClass="btn btn-primary" Text="Submit" OnClick="btnSubmit_Click">
                                    </asp:LinkButton>
                                    <asp:Button runat="server" ID="btnClear" OnClick="btnClear_Click" Text="Clear" CssClass="btn" Style="display: inline-table"></asp:Button>
                                </div>
                            </div>
                            <div class="control-group">
                                <div class="btn btn-primary" style="width: auto; padding: 5px 10px 5px 10px; text-align: center; cursor: pointer; display: none;" id="divNewBay">New Bays</div>
                                <div class="btn btn-primary" style="width: auto; padding: 5px 10px 5px 10px; text-align: center; cursor: pointer; display: none;" id="divNewRowBay">New Rows</div>
                                <div class="btn btn-primary" style="width: auto; padding: 5px 10px 5px 10px; text-align: center; cursor: pointer; display: none;" id="divNewPTLMapping">New Bay PTL Mapping</div>
                                <div class="btn btn-primary" style="width: auto; padding: 5px 10px 5px 10px; text-align: center; cursor: pointer; display: none;" id="divNewSKUMaster">PTL SKU Mapping</div>
                            </div>
                            <div class="box-content">
                                <asp:Panel ID="PnlNewBay" runat="server" CssClass="sku" Style="display: none; width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridNewBay" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="BayName" HeaderText="Bay Name" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="DisplayName" HeaderText="Display Name"
                                                    HeaderStyle-Width="400px" HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF"
                                                    HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="LCDIPAddress" HeaderText="LCD IP Address" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="ControllerIPAddress" HeaderText="Controller IP Address" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="ScannerIPaddress" HeaderText="Scanner IP address" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Location" HeaderText="Location" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PnlNewRowBay" runat="server" CssClass="sku" Style="display: none; width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridNewRowBay" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="BayName" HeaderText="Bay Name" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="RowNumber" HeaderText="Row Number" HeaderStyle-Width="400px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="RowName" HeaderText="Row Name" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PnlNewPTLMapping" runat="server" CssClass="sku" Style="display: none; width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridBayPTLMapping" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="BayName" HeaderText="Bay Name" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="RowName" HeaderText="Row Name" HeaderStyle-Width="400px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="PTLNo" HeaderText="PTL No" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Status" HeaderText="Status" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="PnlNewSKUMaster" runat="server" CssClass="sku" Style="display: none; width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridSKUMaster" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="Module#" HeaderText="Module" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="SKUNO" HeaderText="Sku Code" HeaderStyle-Width="400px" HeaderStyle-Font-Bold="false"
                                                    HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Description" HeaderText="Description" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="WH" HeaderText="Warehouse Location" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Location type" HeaderText="Location Type" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="Sku Type" HeaderText="Sku Type" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                            </div>

                        </fieldset>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var isvisible = false;
            $('.sku').each(function () {
                if ($(this).css("display") == "none" && !isvisible) {
                    $(this).css("display", "block");
                    isvisible = true;
                }
                else {
                    $(this).css("display", "none");
                }
            });
            $("div[id*=divNewBay]").css("display", ($('#<%=gridNewBay.ClientID %>').find('tr').length > 0) ? "inline-table" : "none");
            $("div[id*=divNewRowBay]").css("display", ($('#<%=gridNewRowBay.ClientID %>').find('tr').length > 0) ? "inline-table" : "none");
            $("div[id*=divNewPTLMapping]").css("display", ($('#<%=gridBayPTLMapping.ClientID %>').find('tr').length > 0) ? "inline-table" : "none");
            $("div[id*=divNewSKUMaster]").css("display", ($('#<%=gridSKUMaster.ClientID %>').find('tr').length > 0) ? "inline-table" : "none");
            $("div[id^=divNew").click(function () {
                $('.sku').css("display", "none");
                $('div[id*=Pnl' + $(this).attr("id").replace('div', '') + ']').css("display", "block");
            });
            $('table').each(function () {
                var _header = false;
                $(this).find('tr').each(function () {
                    if ($(this).find('th:last').text() == "Status") {
                        _header = true;
                    }
                    else {
                        console.log(_header);
                        if (_header) {
                            if ($(this).find('td:last').text() != '' && _header) {
                                $(this).find('td:last').css('color', 'red').css('font-weight', 'bold');
                            }
                            else if ($(this).find('td:last').text() == '') {
                                $(this).find('td:last').text('Successfully uploaded');
                                $(this).find('td:last').css('color', 'green').css('font-weight', 'bold');
                            }
                        }
                    }
                })
                _header = false;
            });
        });
    </script>
</asp:Content>
