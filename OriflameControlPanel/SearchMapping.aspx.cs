﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using MySql.Data.MySqlClient;
using log4net;
using System.Data;

public partial class SearchMapping : System.Web.UI.Page
{
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string _condition = string.Empty;
        DataTable dtSearch = null;
        try
        {
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                using (MySqlCommand mycmd = new MySqlCommand("sp_SearchMapping", mycon))
                {
                    mycmd.CommandType = CommandType.StoredProcedure;
                    if (mycon.State == ConnectionState.Closed)
                        mycon.Open();
                    if (txtBayName.Text.Trim() != string.Empty)
                        _condition = "ZM.Display_Name = '" + txtBayName.Text.Trim() + "' AND ";
                    if (txtsku.Text.Trim() != string.Empty)
                        _condition = "sm.Sku='" + txtsku.Text.Trim() + "' AND ";
                    if (txtLocation.Text.Trim() != string.Empty)
                        _condition = "ZPM.PICK_Location='" + txtLocation.Text.Trim() + "' AND ";
                    _condition = "SELECT DISTINCT ZM.Display_Name AS BayName,ZPM.`Pick_Location` AS PTLLocation,ZPM.`PTL`, " +
                                  " PSK.`Sku` AS sku_code,'' AS Description,PSK.`Sku_Type`,'' AS Alternate_Sku_Code FROM `tbl_zone_master` ZM" +
                                  " INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.ZONE_ID=ZM.ZONE_ID" +
                                  " INNER JOIN `tbl_ptl_sku_mapping` PSK ON PSK.`Pick_Location`=ZPM.`Pick_Location` WHERE " + _condition.Substring(0, _condition.Length - 4).Trim();
                    mycmd.Parameters.AddWithValue("_condition", _condition);
                    using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                    {
                        dtSearch = new DataTable();
                        myadp.Fill(dtSearch);
                    }
                }
            }
            if (dtSearch != null && dtSearch.Rows.Count > 0)
            {
                gridSearch.DataSource = dtSearch;
                gridSearch.DataBind();
            }
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }
        finally
        {
            if (dtSearch != null)
                dtSearch.Dispose();
        }
    }
}