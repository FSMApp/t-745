﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using FileReader;
using System.Data;
using MySql.Data.MySqlClient;

namespace ControlPanel
{
    public partial class ItemMaster : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            gridNewBay.DataSource = null;
            gridNewBay.DataBind();
            gridNewRowBay.DataSource = null;
            gridNewRowBay.DataBind();
            gridBayPTLMapping.DataSource = null;
            gridBayPTLMapping.DataBind();
            gridSKUMaster.DataSource = null;
            gridSKUMaster.DataBind();
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            DataSet result = null;
            string _filepath = Convert.ToString(ViewState["filePath"]);
            try
            {
                using (clsFileReadHelper objfile = new clsFileReadHelper(_filepath))
                {
                    result = objfile.getFileData();
                }
                foreach (DataTable dt in result.Tables)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        col.ColumnName = dt.Rows[0][col.ColumnName].ToString().Replace(" ", "");
                    }
                    dt.Rows.RemoveAt(0);
                    dt.Columns.Add("status", typeof(System.String));
                    dt.AcceptChanges();
                    switch (dt.TableName)
                    {
                        //case "NewBay":
                        //    CreateNewBays(dt);
                        //    break;
                        //case "BayPTLMapping":
                        //    CreateBayPTLMapping(dt);
                        //    break;
                        case "SKUMaster":
                            uploadskuConfiguration(Path.GetFileName(_filepath), _filepath, Path.GetExtension(_filepath));
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                clsPanelsLog.writelog(ex);
            }
        }
        void uploadskuConfiguration(string filename, string filePath, string filetype)
        {
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
                {
                    mycon.Open();
                    using (MySqlCommand mycmd = new MySqlCommand("sp_addSkuConfigtoQueue", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("_config_id", 0);
                        mycmd.Parameters.AddWithValue("_file_name", filename);
                        mycmd.Parameters.AddWithValue("_filepath", filePath);
                        mycmd.Parameters.AddWithValue("_type", filetype);
                        mycmd.Parameters.AddWithValue("_uploadby", Convert.ToString(Session["User_Id"]));
                    }
                }
                lblMessage.Text = "Configuration has been uploaded to run successfully";
            }
            catch(Exception ex)
            {
                clsPanelsLog.writelog(ex);
                lblMessage.Text = "There is error in file! Please contact to Administrator";
            }
        }
        void CreateNewBays(DataTable dtBays)
        {
            string _bayname = string.Empty;
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                mycon.Open();
                foreach (DataRow dr in dtBays.Rows)
                {
                    try
                    {
                        using (MySqlCommand mycmd = new MySqlCommand("sp_createZone", mycon))
                        {
                            mycmd.CommandType = CommandType.StoredProcedure;
                            mycmd.Parameters.AddWithValue("_name", Convert.ToString(dr["BayName"]));
                            mycmd.Parameters.AddWithValue("display_name", Convert.ToString(dr["displayName"]));
                            mycmd.Parameters.AddWithValue("_ip_address", Convert.ToString(dr["ControllerIPAddress"]));
                            mycmd.Parameters.AddWithValue("_created_by", Session["User_Id"]);
                            mycmd.Parameters.AddWithValue("_lcd_ip_address", Convert.ToString(dr["lcdipaddress"]));
                            mycmd.Parameters.AddWithValue("_scanner_ip_address", Convert.ToString(dr["scanneripaddress"]));
                            _bayname = (string)mycmd.ExecuteScalar();
                            if (!string.IsNullOrEmpty(_bayname))
                            {
                                dr["status"] = _bayname + " already exist";
                                dtBays.AcceptChanges();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        clsPanelsLog.writelog(ex);
                    }
                }
                gridNewBay.DataSource = dtBays;
                gridNewBay.DataBind();
            }
        }
        void CreateBayPTLMapping(DataTable dtPtlList)
        {
            string _bayname = string.Empty;
            using (MySqlConnection mycon = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                mycon.Open();
                foreach (DataRow dr in dtPtlList.Rows)
                {
                    try
                    {
                        using (MySqlCommand mycmd = new MySqlCommand("sp_BayPTLMapping", mycon))
                        {
                            mycmd.CommandType = CommandType.StoredProcedure;
                            mycmd.Parameters.AddWithValue("_bayname", Convert.ToString(dr["BayName"]));
                            mycmd.Parameters.AddWithValue("_rowname", Convert.ToString(dr["Rowname"]));
                            mycmd.Parameters.AddWithValue("_ptlno", Convert.ToString(dr["PTLNo"]));
                            mycmd.Parameters.AddWithValue("_createdby", Session["User_Id"]);
                            _bayname = mycmd.ExecuteScalar() == DBNull.Value ? string.Empty : (string)mycmd.ExecuteScalar();
                            if (!string.IsNullOrEmpty(_bayname))
                            {
                                dr["status"] = _bayname;
                                dtPtlList.AcceptChanges();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        clsPanelsLog.writelog(ex);
                    }
                }
                gridBayPTLMapping.DataSource = dtPtlList;
                gridBayPTLMapping.DataBind();
            }
        }
        protected void btnCreate_Click(object sender, EventArgs e)
        {
            if (inputUpload.HasFile)
            {

                string _filename = inputUpload.FileName;
                inputUpload.SaveAs(Path.Combine(Server.MapPath("~"), ConfigurationManager.AppSettings["ItemMasterLocation"].ToString(), _filename));
                DataSet result = null;
                ViewState["filePath"] = Path.Combine(Server.MapPath("~"), ConfigurationManager.AppSettings["ItemMasterLocation"].ToString(), _filename);
                try
                {
                    using (clsFileReadHelper objfile = new clsFileReadHelper(Path.Combine(Server.MapPath("~"), ConfigurationManager.AppSettings["ItemMasterLocation"].ToString(), _filename)))
                    {
                        result = objfile.getFileData();
                    }

                    foreach (DataTable dt in result.Tables)
                    {
                        foreach (DataColumn col in dt.Columns)
                        {
                            col.ColumnName = dt.Rows[0][col.ColumnName].ToString().Replace(" ", "");
                        }
                        dt.Rows.RemoveAt(0);
                        dt.Columns.Add("status", typeof(System.String));
                        dt.AcceptChanges();
                        switch (dt.TableName)
                        {
                            //case "NewBay":
                            //    gridNewBay.DataSource = dt;
                            //    gridNewBay.DataBind();
                            //    break;
                            //case "NewBayRow":
                            //    gridNewRowBay.DataSource = dt;
                            //    gridNewRowBay.DataBind();
                            //    break;
                            //case "BayPTLMapping":
                            //    gridBayPTLMapping.DataSource = dt;
                            //    gridBayPTLMapping.DataBind();
                            //    break;
                            case "SKUMaster":
                                gridSKUMaster.DataSource = dt;
                                gridSKUMaster.DataBind();
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    clsPanelsLog.writelog(ex);
                }
            }
        }
    }
}