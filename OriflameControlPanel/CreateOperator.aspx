﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeFile="CreateOperator.aspx.cs" Inherits="ControlPanel.CreateOperator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content" class="span10">
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right"></i></li>
            <li><a href="CreateOperator.aspx">Create User</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>User Creation Form</h2>
                    <%--<div class="box-icon" runat="server" id="viewAll">
                        <asp:HyperLink ID="hyplnk" runat="server" Style="color: White;" NavigateUrl="ActivityMasterAdmin.aspx">View All</asp:HyperLink>
                    </div>--%>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label>
                            </p>
                            <div class="control-group">
                                <label class="control-label">
                                    User Name <span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" ID="txtOperatorName"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Document"
                                        runat="server" ControlToValidate="txtOperatorName" ErrorMessage="Operator Name is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Employee Code <span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:TextBox runat="server" ValidationGroup="Document" ID="txtEmpCode"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Document"
                                        runat="server" ControlToValidate="txtEmpCode" ErrorMessage="Employee Code is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    User Type <span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList runat="server" ValidationGroup="Document" ID="ddlUserType" AppendDataBoundItems="true">
                                        <asp:ListItem Text="Select" Value=""></asp:ListItem>
                                        <asp:ListItem Text="ADMIN" Value="Admin"></asp:ListItem>
                                        <asp:ListItem Text="MANAGER" Value="MANAGER"></asp:ListItem>
                                        <asp:ListItem Text="OPERATOR" Value="OPERATOR"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ValidationGroup="Document" ID="RequiredFieldValidator"
                                        runat="server" ControlToValidate="ddlUserType" ErrorMessage="User Type is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-actions">
                                <asp:LinkButton runat="server" ID="btnCreate" ValidationGroup="Document" CausesValidation="true"
                                    CssClass="btn btn-primary" Text="Create" OnClick="btnSave_Click">
                                </asp:LinkButton>
                                <asp:Button runat="server" ID="btnClear" Text="Clear" CssClass="btn btn-primary"></asp:Button>
                                <asp:Button runat="server" ID="btnSearch" Text="Search User" CssClass="btn btn-primary"></asp:Button>
                            </div>
                            <br />
                            <div class="box-content">
                                <asp:Panel ID="Panel2" runat="server" Style="width: 100%; overflow: visible;" ScrollBars="Horizontal">
                                    <table class="table table-striped table-condensed">
                                        <asp:GridView runat="server" ID="gridUserList" BackColor="White" AutoGenerateColumns="false"
                                            AllowPaging="false" BorderColor="#3366CC" BorderStyle="None" BorderWidth="1px">
                                            <RowStyle ForeColor="#00000" HorizontalAlign="Center" />
                                            <Columns>
                                                <asp:BoundField DataField="User_ID" HeaderText="User Name" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="UserCode" HeaderText="Employee Code" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="user_category" HeaderText="Employee Category" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="CreatedBy" HeaderText="Created By" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                                <asp:BoundField DataField="CreatedDate" HeaderText="Created on" HeaderStyle-Width="400px"
                                                    HeaderStyle-Font-Bold="false" HeaderStyle-BackColor="#2D89EF" HeaderStyle-ForeColor="White" />
                                            </Columns>
                                            <FooterStyle BackColor="White" ForeColor="Black" />
                                            <HeaderStyle BackColor="White" Font-Bold="False" ForeColor="#16829B" />
                                        </asp:GridView>
                                    </table>
                                </asp:Panel>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
            <!--/span-->
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#<%= btnClear.ClientID %>").click(function () {
                alert('sdfsdfs');
            });
        });
    </script>
</asp:Content>
