﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using MySql.Data.MySqlClient;
using System.IO;

public partial class DetailReport : System.Web.UI.Page
{
    private string _reportType = string.Empty;
    private string _reportdata = string.Empty;
    private string _fromdatetime = string.Empty;
    private string _enddatetime = string.Empty;
    private DataTable _dtReportExportData;

    protected void Page_Load(object sender, EventArgs e)
    {
        GridView gd = null;
        if (!this.Page.IsPostBack)
        {
            this.inputStartFromTime.Text = "00:00:00";
            this.inputEndFromTime.Text = "00:00:00";
            this.inputStartToTime.Text = "00:00:00";
            this.inputEndToTime.Text = "00:00:00";
        }
        if (this.Request.QueryString.Count == 0)
            return;
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["Type"])))
        {
            ViewState["Type"] = Convert.ToString(Request.QueryString["Type"]);
            switch (Convert.ToString(Request.QueryString["Type"]))
            {
                case "C":
                    ReportTitle.Text = "Carton Wise Hourly Report";
                    gridOrderDetails.Visible = false;
                    gridCartonDetails.Visible = true;
                    gd = gridCartonDetails;
                    break;
                case "O":
                    ReportTitle.Text = "Order Wise Hourly Report";
                    gridOrderDetails.Visible = true;
                    gridCartonDetails.Visible = false;
                    gd = gridOrderDetails;
                    break;
                case "B":
                    this._reportType = "B";
                    this._reportdata = "OB";
                    this.ReportTitle.Text = "Bay Wise Report";
                    this.gridOrderDetails.Visible = true;
                    this.gridCartonDetails.Visible = false;
                    break;
            }

        }
        if (string.IsNullOrEmpty(Convert.ToString(this.Request.QueryString["CT"])))
            return;
        if (Convert.ToString(this.Request.QueryString["CT"]) == "H")
        {
            this.ViewState["Hour"] = (object)Convert.ToString(this.Request.QueryString["D"]);
            this.ReportTitle.Text = this.ReportTitle.Text + " between " + this.Request.QueryString["H"].Split('-')[0].ToString() + " and " + Convert.ToString(this.Request.QueryString["H"]).Split('-')[1];
            this.pnlToDate.Visible = false;
            this.pnlFromDate.Visible = true;
            this.pnlFromSecondryTime.Visible = true;
            this.pnlToSecondryTime.Visible = false;
            this.reportFilter.Visible = false;
            this.getReportDatabyFilter();
        }
        if (!(Convert.ToString(this.Request.QueryString["CT"]) == "W"))
            return;
        this.ViewState["Hour"] = (object)Convert.ToDateTime(this.Request.QueryString["D"]);
        this.inputFromDate.Text = Convert.ToDateTime(this.Request.QueryString["D"]).ToString("dd/MM/yyyy");
        this.inputToDate.Text = Convert.ToDateTime(this.Request.QueryString["D"]).ToString("dd/MM/yyyy");
        this.inputStartFromTime.Text = "00:00:00";
        this.inputStartToTime.Text = "23:59:59";
        this.reportFilter.Visible = false;
        this.getReportDatabyFilter();
    }
    private void BindGrid(string _fromDatetime, string _endDatetime, string type, string _data, GridView grd)
    {
        DataTable dataTable = (DataTable)null;
        string cmdText = string.Empty;
        try
        {
            switch (_data)
            {
                case "O":
                    cmdText = "sp_getDateTimeWiseORderData";
                    break;
                case "C":
                    cmdText = "sp_getDateTimeWiseCartonsData";
                    break;
                case "B":
                    cmdText = "sp_getDateTimeWiseBayData";
                    break;
            }
            using (MySqlConnection connection = new MySqlConnection(ConfigurationManager.ConnectionStrings["dbconn"].ConnectionString))
            {
                using (MySqlCommand selectCommand = new MySqlCommand(cmdText, connection))
                {
                    selectCommand.CommandType = CommandType.StoredProcedure;
                    selectCommand.Parameters.AddWithValue("_fromdatetime", (object)_fromDatetime);
                    selectCommand.Parameters.AddWithValue("_todatetime", (object)_endDatetime);
                    selectCommand.Parameters.AddWithValue("_reportType", (object)this._reportType);
                    using (MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(selectCommand))
                    {
                        dataTable = new DataTable();
                        mySqlDataAdapter.Fill(dataTable);
                    }
                }
                this._dtReportExportData = dataTable;
                this.Session["_dtReportExportData"] = (object)dataTable;
                grd.DataSource = (object)dataTable;
                grd.DataBind();
            }
        }
        catch (Exception ex)
        {
            clsPanelsLog.writelog(ex);
        }
    }

    private void ExportGridToExcel()
    {
        GridView gridView = new GridView();
        gridView.DataSource = (object)(this.Session["_dtReportExportData"] as DataTable);
        gridView.DataBind();
        this.Response.Clear();
        this.Response.Buffer = true;
        this.Response.ClearContent();
        this.Response.ClearHeaders();
        this.Response.Charset = "";
        string str = "Report-" + (object)DateTime.Now + ".xls";
        StringWriter stringWriter = new StringWriter();
        HtmlTextWriter writer = new HtmlTextWriter((TextWriter)stringWriter);
        this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        this.Response.ContentType = "application/vnd.ms-excel";
        this.Response.AddHeader("Content-Disposition", "attachment;filename=" + str);
        gridView.GridLines = GridLines.Both;
        gridView.HeaderStyle.Font.Bold = true;
        gridView.RenderControl(writer);
        this.Response.Write(stringWriter.ToString());
        this.Response.End();
    }

    private void getReportDatabyFilter()
    {
        if (this.pnlFromSecondryTime.Visible || this.pnlToSecondryTime.Visible)
            return;
        if (this.inputFromDate.Text != string.Empty)
            this._fromdatetime = this.inputFromDate.Text;
        if (this.inputToDate.Text != string.Empty)
            this._enddatetime = this.inputToDate.Text;
        if (this.inputStartFromTime.Text != string.Empty)
        {
            DetailReport detailReport = this;
            detailReport._fromdatetime = detailReport._fromdatetime + " " + this.inputStartFromTime.Text;
        }
        if (this.inputStartToTime.Text != string.Empty)
        {
            DetailReport detailReport = this;
            detailReport._enddatetime = detailReport._enddatetime + " " + this.inputStartToTime.Text;
        }
        this._fromdatetime = Convert.ToDateTime(this._fromdatetime).ToString("yyyy/MM/dd HH:mm:ss");
        this._enddatetime = Convert.ToDateTime(this._enddatetime).ToString("yyyy/MM/dd HH:mm:ss");
        if (this._reportdata == "O")
        {
            this.gridOrderDetails.Visible = true;
            this.gridCartonDetails.Visible = false;
            this.BindGrid(this._fromdatetime, this._enddatetime, this._reportType, this._reportdata, this.gridOrderDetails);
        }
        else if (this._reportdata == "C")
        {
            this.gridOrderDetails.Visible = false;
            this.gridCartonDetails.Visible = true;
            this.BindGrid(this._fromdatetime, this._enddatetime, this._reportType, this._reportdata, this.gridCartonDetails);
        }
        else
        {
            if (!(this._reportdata == "B"))
                return;
            this.gridOrderDetails.Visible = false;
            this.gridCartonDetails.Visible = true;
            this.BindGrid(this._fromdatetime, this._enddatetime, this._reportType, this._reportdata, this.gridCartonDetails);
        }
    }

    protected void btngetData_Click(object sender, EventArgs e)
    {
        if (this.rdoOpenCartonWise.Checked)
        {
            this._reportType = "O";
            this._reportdata = "C";
        }
        if (this.rdoCloseCartonWise.Checked)
        {
            this._reportType = "C";
            this._reportdata = "C";
        }
        if (this.rdoOpenOrderWise.Checked)
        {
            this._reportType = "O";
            this._reportdata = "O";
        }
        if (this.rdoCloseOrderWise.Checked)
        {
            this._reportType = "C";
            this._reportdata = "O";
        }
        if (this.rdoBayWiseData.Checked)
        {
            this._reportType = "B";
            this._reportdata = "B";
        }
        this.getReportDatabyFilter();
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        if (this.rdoOpenCartonWise.Checked)
        {
            this._reportType = "O";
            this._reportdata = "C";
        }
        if (this.rdoCloseCartonWise.Checked)
        {
            this._reportType = "C";
            this._reportdata = "C";
        }
        if (this.rdoOpenOrderWise.Checked)
        {
            this._reportType = "O";
            this._reportdata = "O";
        }
        if (this.rdoCloseOrderWise.Checked)
        {
            this._reportType = "C";
            this._reportdata = "O";
        }
        if (this.rdoBayWiseData.Checked)
        {
            this._reportType = "B";
            this._reportdata = "B";
        }
        this.ExportGridToExcel();
    }
}