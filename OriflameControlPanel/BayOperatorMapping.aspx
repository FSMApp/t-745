﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeFile="BayOperatorMapping.aspx.cs" Inherits="ControlPanel.BayOperatorMapping" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="content" class="span10">
        <ul class="breadcrumb" style="margin-bottom: 4px;">
            <li><i class="icon-home"></i><a href="Default.aspx">Home</a> <i class="icon-angle-right">
            </i></li>
            <li><a href="CreateOperator.aspx">Bay Operator Mapping</a></li>
        </ul>
        <div class="row-fluid">
            <div class="box span12">
                <div class="box-header">
                    <h2>
                        <i class="halflings-icon edit"></i><span class="break"></span>Bay Operator Mapping
                        Form</h2>
                </div>
                <div class="box-content">
                    <div class="form-horizontal">
                        <fieldset>
                            <p>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ForeColor="Green" runat="server" ID="lblMessage"></asp:Label></p>
                            <div class="control-group">
                                <label class="control-label">
                                    Bay Name <span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList runat="server" ValidationGroup="Document" ID="ddlBayName">
                                        <asp:ListItem Text="P10" Value="P10"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Document"
                                        runat="server" ControlToValidate="ddlBayName" ErrorMessage="Bay Name is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                    Operator Name <span style="color: Red;">*</span>
                                </label>
                                <div class="controls">
                                    <asp:DropDownList runat="server" ValidationGroup="Document" ID="ddlOperator">
                                        <asp:ListItem Text="Test" Value="Test"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Document"
                                        runat="server" ControlToValidate="ddlOperator" ErrorMessage="Operatror Name is required."
                                        ForeColor="Red"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-actions">
                                <asp:LinkButton runat="server" ID="btnCreate" ValidationGroup="Document" CausesValidation="true"
                                    CssClass="btn btn-primary" Text="Submit">
                                </asp:LinkButton>
                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn"></asp:Button>
                            </div>
                    </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <!--/span-->
    </div>
    </div>
</asp:Content>
