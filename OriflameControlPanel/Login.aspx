﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />
    <title>Login </title>
    <style type="text/css">
        @import url(http://fonts.googleapis.com/css?family=Exo:100,200,400);
        @import url(http://fonts.googleapis.com/css?family=Source+Sans+Pro:700,400,300);

        body {
            overflow: hidden;
        }

        .body {
            position: absolute;
            top: -20px;
            left: -20px;
            right: -40px;
            bottom: -40px;
            width: auto;
            height: auto;
            background-image: url('Images/1p2iEYsA.png');
            background-size: cover;
           /* -webkit-filter: blur(1px);*/
            z-index: 0;
        }

        .grad {
            right: 4px;
            bottom: 234px;
            width: auto;
            height: auto; /* Chrome,Safari4+ */
            z-index: 1;
            opacity: 0.7;
        }


        .login1 {
            margin-top: 10px;
            float: left;
        }


        .login {
            position: absolute;
            top: calc(46% - 75px);
            left: calc(42% - 50px);
            height: 150px;
            width: 260px;
            padding: 10px;
            z-index: 2;
        }

            .login input[type=text] {
                width: 250px;
                height: 30px;
                border: 2px solid #dadada;
                border-radius: 2px;
                color: black !important;
                font-family: 'Exo', sans-serif;
                font-size: 16px;
                font-weight: 400;
                padding: 4px;
            }

            .login input[type=password] {
                width: 250px;
                height: 30px;
                border: 2px solid #dadada;
                border-radius: 2px;
                color: black !important;
                font-family: 'Exo', sans-serif;
                font-size: 16px;
                font-weight: 400;
                padding: 4px;
                margin-top: 10px;
            }

            .login input[type=button] {
                width: 260px;
                height: 35px;
                background-color: #4C6D8F;
                border: 1px solid #fff;
                cursor: pointer;
                border-radius: 2px;
                color: #a18d6c;
                font-family: 'Exo', sans-serif;
                font-size: 16px;
                font-weight: 400;
                padding: 6px;
                margin-top: 10px;
            }

        .page-heading {
            font-size: 40px;
            color: red;
            width: 100%;
            margin: auto;
            font-family: Segoe Ui;
            margin-top: 0px;
            height: 100px;
            text-align: center;
        }
    </style>
    <script type="text/javascript" src="Scripts/prefixfree.min.js"></script>
</head>
<body>
    <form id="form1" class="form-horizontal" runat="server">
        <div class="body">
        </div>
        <div class="grad" style="margin-top: 100px; width: 57%; margin-left: auto; margin-right: auto;">
            <asp:Image ID="Image1" ImageUrl="Images/ori_logo.png" runat="server" Style="float: left; margin-top: 34px; margin-left: -16px; width: 23%;" />
            <asp:Image ID="Image2" ImageUrl="Images/falconlogo.jpg" runat="server" Style="float: right; width: 25%;" />
            <div class="page-heading">
                RDC North
            </div>
        </div>
        <div class="login">
            <div style="font-size: 15px; height: 30px; float: left;">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </div>
            <div class="control">
                <asp:TextBox ID="user" placeholder="username" CssClass="login1" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="user"
                    Style="color: red; visibility: visible; float: left; margin-top: 5px; margin-bottom: -5px;"
                    ErrorMessage="Enter User Name" ForeColor="White"></asp:RequiredFieldValidator>
            </div>
            <div class="control">
                <asp:TextBox ID="password" CssClass="login1" placeholder="password" TextMode="Password"
                    runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="password"
                    Style="color: red; visibility: visible; float: left; margin-top: 5px; margin-bottom: -5px;"
                    ErrorMessage="Enter Password" ForeColor="White"></asp:RequiredFieldValidator>
            </div>
            <div class="control">
                <asp:Button ID="btnLogin" runat="server" Text="Login" Width="260px"
                    OnClick="btnLogin_Click" BorderWidth="1px"  BackColor="#4C6D8F"
                    BorderColor="#FFFFFF" Height="35px" ForeColor="white" Font-Size="16" Font-Names="'Exo',sans-serif" />
            </div>

        </div>
    </form>
</body>
</html>

