﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
/// <summary>
/// Summary description for clsPanelsLog
/// </summary>
public static class clsPanelsLog
{
    private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    static clsPanelsLog()
    {
        log4net.Config.XmlConfigurator.Configure();
    }
    public static void writelog(Exception ex)
    {
        log.Error(ex);
    }
}