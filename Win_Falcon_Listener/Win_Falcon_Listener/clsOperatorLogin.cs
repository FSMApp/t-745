﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Win_Falcon_Listener
{
    public class clsOperatorLogin
    {
        public string Scan_Mode { get; set; }
        public string Scanner_IPAddress { get; set; }
        public string Emp_Code { get; set; }
    }
    public static class clsComman
    {
        public static string getLightColor(string _Mode)
        {
            string _color_code = string.Empty;
            switch (Convert.ToString(_Mode))
            {
                case "RED":
                case "EXT":
                    _color_code = "100";
                    break;
                case "GREEN":
                case "NORMAL":
                case "PAS":
                    _color_code = "001";
                    break;
                default:
                    _color_code = "010";
                    break;
            }
            return _color_code;
        }
    }
}
