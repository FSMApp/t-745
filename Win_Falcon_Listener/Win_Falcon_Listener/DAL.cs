﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
using System.Configuration;
namespace Win_Falcon_Listener
{
    public class DAL
    {
        string _constr = ConfigurationManager.ConnectionStrings["Constr"].ConnectionString;

        public void  AddQRCode(string _scancode,string IP)
        {
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    string _query = "SP_addQRCodeData";
                    using (MySqlCommand mycmd = new MySqlCommand(_query, mycon))
                    {
                        mycon.Open();
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("_Scancode", _scancode);
                        mycmd.Parameters.AddWithValue("_ip", IP);
                        mycmd.ExecuteNonQuery();
                    }
                }
            }
            catch(Exception ex)
            {
                clsErrorLogs.WriteLogs("error", clsErrorLogs.generateErrorMessage(ex));
            }
        }
        public DataTable ResetZone(string ScannerIP)
        {
            DataTable dtIP = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    string _query = "Select ID, Zone_IPAddress as PTL_IPAddress,PTL_SendPort from tbl_Zone_Master  where Scanner_IPAddress=?ScannerIP order by ID";
                    using (MySqlCommand mycmd = new MySqlCommand(_query, mycon))
                    {
                        mycmd.CommandType = CommandType.Text;
                        mycmd.Parameters.AddWithValue("?ScannerIP", ScannerIP);
                        using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                        {
                            dtIP = new DataTable();
                            myadp.Fill(dtIP);
                        }
                    }
                }
                return dtIP;
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", clsErrorLogs.generateErrorMessage(ex));
                return null;
            }
        }
        public DataTable getPTLReceived_PortListwithIPAddress()
        {
            DataTable dtPortptl = null;
            //string _query = string.Empty;
            try
            {
                //_query = "  SELECT DISTINCT Zone_IPAddress,PTL_ReceivePort,PTL_SendPort FROM `tbl_zone_master` ZM ";
                //_query += " INNER JOIN  `tbl_zone_ptl_mapping` ZPM ON ZPM.Zone_id = ZM.Zone_Id ";
                //_query += " WHERE ZM.Status = 'ACTIVE'; ";
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    using (MySqlCommand mycmd = new MySqlCommand("SP_getIPList", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                        {
                            dtPortptl = new DataTable();
                            myadp.Fill(dtPortptl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                dtPortptl = null;
                clsErrorLogs.WriteLogs("error", clsErrorLogs.generateErrorMessage(ex));
            }
            finally
            {
                if (dtPortptl != null)
                {
                    dtPortptl.Dispose();
                }
            }
            return dtPortptl;
        }
        public string UpdateWeightData(string cartonId, string weight, string ipAddress, int portNo, string counter_no)
        {
            MySqlTransaction trans = null;
            string orderStatus = "";
            try
            {
                using (MySqlConnection connec = new MySqlConnection(_constr))
                {
                    connec.Open();
                    trans = connec.BeginTransaction();
                    if (!string.IsNullOrEmpty(cartonId))
                    {
                        clsErrorLogs.WriteLogs("dbcalls", "call SP_AddweightStatustoCompletePick('" + cartonId + "','" + ipAddress + "','" + weight + "')" + ";" + DateTime.Now);
                        try
                        {
                            using (MySqlCommand cmd = new MySqlCommand("SP_AddweightStatustoCompletePick", connec, trans))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@_Carate_code", cartonId.Trim());
                                cmd.Parameters.AddWithValue("@_iPaddress", ipAddress);
                                cmd.Parameters.AddWithValue("@_weight", weight);
                                object status = cmd.ExecuteScalar();
                                orderStatus = Convert.ToString(status);
                                trans.Commit();
                            }
                        }
                        catch (Exception ex)
                        {
                            trans.Rollback();
                            clsErrorLogs.WriteLogs("error", ex.ToString());//log.Error(ex);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString());//log.Error(ex);
            }
            return orderStatus;
        }
        public string getMessagetoDisplayonLCD(string _LCD_IP, string _Crate_Code,string _type)
        {
            object data = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    mycon.Open();
                    using (MySqlCommand mycmd = new MySqlCommand("SP_getDataforDisplayonLED", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("_LCD_IP", _LCD_IP);
                       // mycmd.Parameters.AddWithValue("_Crate_Code", _Crate_Code);
                        //mycmd.Parameters.AddWithValue("_type", _type);
                        data = mycmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                data = null;
                clsErrorLogs.WriteLogs("error", clsErrorLogs.generateErrorMessage(ex));
            }
            if (Convert.ToString(data) != string.Empty)
            {
                clsErrorLogs.WriteLogs("dbcalls", "call SP_getDataforDisplayonLED('" + _LCD_IP + "','" + _Crate_Code + "')" + ";" + DateTime.Now);
            }
            return Convert.ToString(data);

        }
        //public string getMessagetoDisplayonLCD(string _LCD_IP)
        //{
        //    object data = null;
        //    try
        //    {
        //        using (MySqlConnection mycon = new MySqlConnection(_constr))
        //        {
        //            mycon.Open();
        //            using (MySqlCommand mycmd = new MySqlCommand("SP_getDataforDisplayonLED", mycon))
        //            {
        //                mycmd.CommandType = CommandType.StoredProcedure;
        //                mycmd.Parameters.AddWithValue("_LCD_IP", _LCD_IP);
        //                data = mycmd.ExecuteScalar();
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        data = null;
        //        clsErrorLogs.WriteLogs("error", clsErrorLogs.generateErrorMessage(ex));
        //    }
        //    if (Convert.ToString(data) != string.Empty)
        //    {
        //        clsErrorLogs.WriteLogs("dbcalls", "call SP_getDataforDisplayonLED('" + _LCD_IP + "')" + ";" + DateTime.Now);
        //    }
        //    return Convert.ToString(data);

        //}
        public string AddweightStatustoCompletePick(string _carate_code, string _ipaddress, double _weight)
        {
            object data = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    mycon.Open();
                    clsErrorLogs.WriteLogs("dbcalls", "call SP_AddweightStatustoCompletePick('" + _carate_code + "','" + _ipaddress + "','" + _weight + "')" + ";" + DateTime.Now);
                    using (MySqlCommand mycmd = new MySqlCommand("SP_AddweightStatustoCompletePick", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("_carate_code", _carate_code);
                        mycmd.Parameters.AddWithValue("_iPaddress", _ipaddress);
                        mycmd.Parameters.AddWithValue("_weight", _weight);
                        data = mycmd.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                data = null;
                clsErrorLogs.WriteLogs("error", clsErrorLogs.generateErrorMessage(ex));
            }
            return Convert.ToString(data);
        }
        public int AddScanner_PickData(string Carton_code, string order_number, string ipAddress)
        {
            int count = 0;
            try
            {
                clsErrorLogs.WriteLogs("dbcalls", "call SP_addScanner_PickData('" + Carton_code + "','" + order_number + "','" + ipAddress + "')" + ";" + DateTime.Now);
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    mycon.Open();
                    //using (MySqlTransaction tran = mycon.BeginTransaction())
                    //{
                    try
                    {
                        using (MySqlCommand mycmd = new MySqlCommand("SP_addScanner_PickData", mycon))
                        {
                            mycmd.CommandType = CommandType.StoredProcedure;
                            mycmd.Parameters.AddWithValue("_Carton_code", Carton_code);
                            mycmd.Parameters.AddWithValue("_sku_code", order_number);
                            mycmd.Parameters.AddWithValue("_zone_Name", ipAddress);
                            count = mycmd.ExecuteNonQuery();
                        }
                        ///tran.Commit();
                    }
                    catch (Exception ex)
                    {
                        clsErrorLogs.WriteLogs("error", ex.ToString());
                        // tran.Rollback();
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString());
            }
            return count;
        }
        public string validate_EmployeeCode(string msg)
        {
            object _code = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    mycon.Open();
                    using (MySqlCommand mycmd = new MySqlCommand("SELECT `User_ID` FROM `tbl_users` WHERE `UserCode`=?MSG Limit 1;", mycon))
                    {
                        clsErrorLogs.WriteLogs("dbcalls", mycmd.CommandText + ": " + msg);
                        mycmd.CommandType = CommandType.Text;
                        mycmd.Parameters.AddWithValue("?MSG", msg);
                        _code = mycmd.ExecuteScalar();
                    }
                }
                if (_code != null)
                    return Convert.ToString(_code);
                else
                    return "0";
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString());
                return "0";
            }

        }
        public DataTable getNONPickLEDMEssage(string _Cratecode, string Scanner_IP)
        {
            DataTable objMSg = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    mycon.Open();
                    using (MySqlCommand mycmd = new MySqlCommand("SPGetNONPickLED", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("_Crate_Code", _Cratecode);
                        mycmd.Parameters.AddWithValue("_IP", Scanner_IP);
                        using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                        {
                            objMSg = new DataTable();
                            myadp.Fill(objMSg);
                        }
                    }
                }
                return objMSg;
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString());
                return null;
            }
        }
        public DataTable ValidateInvoice(string message, string IPaddress)
        {
            DataTable dt = null;
            try
            {
                clsErrorLogs.WriteLogs("dbcalls", "call SP_ValidateCarton_code('" + message + "','" + IPaddress + "')" + ";" + DateTime.Now);
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    mycon.Open();
                    using (MySqlCommand mycmd = new MySqlCommand("SP_ValidateCarton_code", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("_Carton_code", message);
                        mycmd.Parameters.AddWithValue("_IPAddress", IPaddress);
                        MySqlDataReader rdr = mycmd.ExecuteReader();
                        dt = new DataTable();
                        dt.Load(rdr);
                        rdr.Close();
                        rdr.Dispose();
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString());
                return null;
            }
            finally
            {
                if (dt != null)
                {
                    dt.Dispose();
                }
            }
        }
        public DataTable UpdatePickConfirmation(string _Confirmation, string _msg, string _IPAddress)
        {
            DataTable dtpick = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    clsErrorLogs.WriteLogs("dbcalls", "Call SP_PickConfirmation('" + _msg + "','" + _IPAddress + "','" + _Confirmation + "');");
                    using (MySqlCommand mycmd = new MySqlCommand("SP_PickConfirmation", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("_scan_code", _msg);
                        mycmd.Parameters.AddWithValue("_ip", _IPAddress);
                        mycmd.Parameters.AddWithValue("_mode", _Confirmation);
                        using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                        {
                            dtpick = new DataTable();
                            myadp.Fill(dtpick);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                dtpick = null;
                clsErrorLogs.WriteLogs("error", clsErrorLogs.generateErrorMessage(ex));
            }
            finally
            {
                if (dtpick != null)
                {
                    dtpick.Dispose();
                }
            }
            return dtpick;

        }
        public DataTable Request_for_PTL(string _empcode, string _scanner_ip, int _mode)
        {
            DataTable dtptl = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    clsErrorLogs.WriteLogs("dbcalls", "Call SP_Request_for_PTL('" + _empcode + "','" + _scanner_ip + "','" + _mode + "');");
                    using (MySqlCommand mycmd = new MySqlCommand("SP_Request_for_PTL", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("_ScanCode", _empcode);
                        mycmd.Parameters.AddWithValue("_scanner_ip", _scanner_ip);
                        mycmd.Parameters.AddWithValue("_mode", _mode);
                        using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                        {
                            dtptl = new DataTable();
                            myadp.Fill(dtptl);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                dtptl = null;
                clsErrorLogs.WriteLogs("error", clsErrorLogs.generateErrorMessage(ex));
            }
            finally
            {
                if (dtptl != null)
                {
                    dtptl.Dispose();
                }
            }
            return dtptl;
        }
    }
}
