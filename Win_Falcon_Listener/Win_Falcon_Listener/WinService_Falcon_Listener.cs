﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Win_Falcon_Listener
{
    public partial class WinService_Falcon_Listener : ServiceBase
    {
        Thread threadTCPListener = null;
        int _PortnoR = 50001;
        ManualResetEvent allDone = null;
        DataTable dtPTLs = null;
        ConcurrentBag<clsOperatorLogin> lstOperatorLogin = null;
        UdpClient UDPListener = null;
        UdpClient PLCListener;
        UdpClient[] lstUDPListener = null;
        string _ptlmsg = string.Empty;
        string _weigingMachine_msg = string.Empty;
        DAL clsDM = null;
        public ConcurrentDictionary<string, Boolean> ptlAC = new ConcurrentDictionary<string, Boolean>();
        List<clsCartonWisePicking> ScanBasedPicking = new List<clsCartonWisePicking>();
        Dictionary<string, int> SendPortList = new Dictionary<string, int>();
        int _PortnoPLC = 0;
        System.Timers.Timer objPLCDataTimer = null;
        System.Timers.Timer objLCDDataTimer = null;
        public WinService_Falcon_Listener()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            clsDM = new DAL();
            lstOperatorLogin = new ConcurrentBag<clsOperatorLogin>();
            try
            {
                dtPTLs = clsDM.getPTLReceived_PortListwithIPAddress();
                if (dtPTLs != null)
                {
                    clsErrorLogs.WriteLogs("error", "IP Found : " + dtPTLs.Rows.Count.ToString());
                    foreach (DataRow dr in dtPTLs.Rows)
                    {
                        if (!SendPortList.Keys.Contains(Convert.ToString(dr["IPAddress"])))
                            SendPortList.Add(Convert.ToString(dr["IPAddress"]), Convert.ToInt32(dr["SendPort"]));
                    }
                }
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
            }
            /*  TCP Listener */
            //Thread.Sleep(60000);
            allDone = new ManualResetEvent(false);
            threadTCPListener = new Thread(new ThreadStart(StartListening));
            threadTCPListener.Start();

            /* PTL Listener */
            lstUDPListener = new UdpClient[dtPTLs.Rows.Count];
            listentoUDP(0);
            listentoPLC(0);
            objPLCDataTimer = new System.Timers.Timer();
            objPLCDataTimer.Elapsed += ObjPLCDataTimer_Elapsed;
            objPLCDataTimer.Interval = 200;
            objPLCDataTimer.Start();

            //objLCDDataTimer = new System.Timers.Timer();
            //objLCDDataTimer.Elapsed += ObjLCDDataTimer_Elapsed;
            //objLCDDataTimer.Enabled = true;
            //objLCDDataTimer.Interval = 1000;
            //objLCDDataTimer.Start();

            try
            {
                for (int imsg = 0; imsg < dtPTLs.Rows.Count; imsg++)
                {
                    if (Convert.ToString(dtPTLs.Rows[imsg]["_Type"]) == "PTL")
                        resetPTL(Convert.ToString(dtPTLs.Rows[imsg]["IPAddress"]), "000", Convert.ToInt32(dtPTLs.Rows[imsg]["SendPort"]));
                }
                for (int imsg = 0; imsg < dtPTLs.Rows.Count; imsg++)
                {
                    if (Convert.ToString(dtPTLs.Rows[imsg]["_Type"]) == "LED")
                        sendMsgtoLCD(Convert.ToInt32(dtPTLs.Rows[imsg]["IPAddress"]), new String(' ', 80), Convert.ToString(dtPTLs.Rows[imsg]["IPAddress"]));
                }
            }
            catch
            {

            }
        }

        //private void ObjLCDDataTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        //{
        //    if (objLCDDataTimer.Enabled)
        //    {
        //        objLCDDataTimer.Enabled = false;
        //        LCDDisplayData();
        //        objLCDDataTimer.Enabled = true;
        //    }
        //}
        void sendMsgtoLCD(int portNo, string message, string ipAddress)
        {
            int retryNo = 0;
            try
            {
                //if (ipAddress == "192.168.10.119")
                //{
                //    message = "ASDFG";
                //}
                string str = "@@" + message + "$$";
                clsErrorLogs.WriteLogs("senddisplay_log", str + ";" + str.Length + ";" + ipAddress + ";" + DateTime.Now.ToString());
                byte[] databyte = Encoding.ASCII.GetBytes(str);
                //if (SendPortList.Keys.Contains(ipAddress.Trim()))
                //{
                while (retryNo < 2)
                {
                    UDPListener.Send(databyte, databyte.Length, ipAddress.Trim(), portNo);// SendPortList[ipAddress.Trim()]
                    retryNo++;
                    Thread.Sleep(20);
                }
                //}
                //else
                //{
                //    clsErrorLogs.WriteLogs("error", "Key not  found :" + ipAddress + ";" + DateTime.Now.ToString());
                //}
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
            }
        }
        //void LCDDisplayData()
        //{
        //    foreach (DataRow dr in dtPTLs.Select("_Type='LED'"))
        //    {
        //        string _msg = clsDM.getMessagetoDisplayonLCD(Convert.ToString(dr["IPAddress"]));
        //        if (_msg != string.Empty)
        //            sendMsgtoLCD(Convert.ToInt32(dr["SendPort"]), _msg, Convert.ToString(dr["IPAddress"]));
        //    }
        //}
        private void ObjPLCDataTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (objPLCDataTimer.Enabled)
            {
                objPLCDataTimer.Enabled = false;
                readPLCData();
                objPLCDataTimer.Enabled = true;
            }
        }

        protected override void OnStop()
        {
            try
            {
                foreach (UdpClient u in lstUDPListener)
                {
                    u.Close();
                }
                PLCListener.Close();
                UDPListener.Close();
                objPLCDataTimer.Enabled = false;
                objPLCDataTimer.Stop();

                //objLCDDataTimer.Enabled = false;
                //objLCDDataTimer.Stop();
            }
            catch
            {
            }
        }

        #region PLC for Weighing 
        void readPLCData()
        {
            try
            {

                DataRow[] dr = dtPTLs.Select("_Type='Weight'");
                if (dr != null)
                {
                    //clsErrorLogs.WriteLogs("error", "IP :"+  Convert.ToString(dr[0]["IPAddress"])+", Port :"+ Convert.ToInt32(dr[0]["SendPort"]).ToString());
                    byte[] sendBytes = new byte[] { 0x80, 0x00, 0x03, 0x00, 0x01, 0x00, 0x00, 0x64, 0x00, 0x00, 0x01, 0x01, 0x82, 0x11, 0x30, 0x00, 0x00, 0x3D };
                    PLCListener.Send(sendBytes, sendBytes.Length, Convert.ToString(dr[0]["IPAddress"]), Convert.ToInt32(dr[0]["SendPort"]));
                }
                //HandleClientRequest.writeSendLogs(sendBytes.ToString(), plc_IP);
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString());//log.Error(ex);
            }
        }
        void listentoPLC(int retryNo)
        {
            if (retryNo < 3)
            {
                try
                {
                    PLCListener = new UdpClient(9600);
                    //handle for forcing listening to start.
                    uint IOC_IN = 0x80000000;
                    uint IOC_VENDOR = 0x18000000;
                    uint SIO_UDP_CONNRESET = IOC_IN | IOC_VENDOR | 12;
                    PLCListener.Client.IOControl((int)SIO_UDP_CONNRESET, new byte[] { Convert.ToByte(false) }, null);
                    //listen normally
                    PLCListener.BeginReceive(PLCDataReceived, PLCListener);
                }
                catch (Exception ex)
                {
                    retryNo++;
                    clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
                    listentoPLC(retryNo);
                }
            }
        }
        private static Byte[] clearPLCData(string ipAddress)
        {
            string msg = string.Empty;
            while (msg.Length < 122)
                msg = msg + "#";

            char[] charValues = msg.ToCharArray();
            string hexOutput = string.Empty;
            foreach (char _eachChar in charValues)
            {
                int value = Convert.ToInt32(_eachChar);
                hexOutput += String.Format("{0:X}", value);
            }

            hexOutput = "80000300010000640000010282113000003D" + hexOutput;
            return StringToByteArray(hexOutput);
        }
        private static Byte[] sendMsgToPLC(string msg)
        {
            char[] charValues = msg.ToCharArray();
            string hexOutput = string.Empty;
            foreach (char _eachChar in charValues)
            {
                int value = Convert.ToInt32(_eachChar);
                hexOutput += String.Format("{0:X}", value);
            }

            hexOutput = "8000030001000064000001028211F8000008" + hexOutput;
            return StringToByteArray(hexOutput);
        }
        public static byte[] StringToByteArray(string hex)
        {
            return Enumerable.Range(0, hex.Length)
                             .Where(x => x % 2 == 0)
                             .Select(x => Convert.ToByte(hex.Substring(x, 2), 16))
                             .ToArray();
        }
        void PLCDataReceived(IAsyncResult ar)
        {
            UdpClient c = (UdpClient)ar.AsyncState;
            // Boolean listen = true;
            try
            {
                IPEndPoint receivedIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                Byte[] receivedBytes = c.EndReceive(ar, ref receivedIpEndPoint);
                Byte[] modifiedBytes = new Byte[receivedBytes.Length - 14];
                Array.Copy(receivedBytes, 14, modifiedBytes, 0, modifiedBytes.Length);
                string msg = ASCIIEncoding.ASCII.GetString(modifiedBytes);
                string ipAddress = receivedIpEndPoint.Address.ToString();
                string ptl_id = string.Empty;

                if (msg.StartsWith("##") == false && msg.Replace("\0", "").Trim().Length > 0)
                {
                    //LogR.Info(msg + ";" + msg.Length + ";" + ipAddress + ";" + DateTime.Now);
                    clsErrorLogs.WriteLogs("received", msg + ";" + msg.Length + ";" + ipAddress + ";" + DateTime.Now);
                    try
                    {
                        // DataRow[] dr = dtPTLs.Select("_Type='Weight'");
                        Byte[] sendBytes = clearPLCData(ipAddress);
                        PLCListener.Send(sendBytes, sendBytes.Length, ipAddress, 9600);
                    }
                    catch (Exception ex)
                    {
                        clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
                    }
                }

                try
                {
                    c.BeginReceive(PLCDataReceived, ar.AsyncState);
                    //listen = false;
                }
                catch (Exception ex)
                {
                    clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
                }

                string operation = string.Empty;
                string bay_id = string.Empty;
                string mode = string.Empty;
                string order_number = string.Empty;

                if (msg.Replace("\0", "").Trim().StartsWith("@@BARCODES"))
                {
                    if (_weigingMachine_msg != msg.Replace("\0", "").Trim())
                    {
                        _weigingMachine_msg = msg.Replace("\0", "").Trim();
                        string orderStatus = string.Empty;
                        string[] spliMsg = Regex.Split(msg.Replace("\0", "").Trim(), "% ");
                        string weight = spliMsg[spliMsg.Length - 2];
                        if (weight.Length > 3)
                            weight = weight.Substring(0, 2) + "." + weight.Substring(2, 2);
                        string _counter = spliMsg[spliMsg.Length - 1];
                        //for (int l = 1; l < spliMsg.Length - 2; l++)
                        //{
                        spliMsg[1] = spliMsg[1].Replace("%", string.Empty);
                        if (spliMsg[1].Length > 0 && spliMsg[1].IndexOf("#") == -1)
                        {
                            orderStatus = clsDM.UpdateWeightData(spliMsg[1], weight, ipAddress, 9600, _counter);
                            orderStatus = ("@@REJECT, " + _counter + ", " + (orderStatus.ToUpper() == "COMPLETED" ? "00" : "01"));
                            byte[] data = sendMsgToPLC(orderStatus);
                            int j = 0;
                            while (j < 3)
                            {
                                PLCListener.Send(data, data.Length, ipAddress, 9600);
                                j++;
                            }
                            clsErrorLogs.WriteLogs("sendplc_log", orderStatus + ";" + orderStatus.Length + ";" + ipAddress + ";" + DateTime.Now);
                        }
                        else
                        {
                            orderStatus = ("@@REJECT, " + _counter + ", 01");
                            byte[] data = sendMsgToPLC(orderStatus);
                            int j = 0;
                            while (j < 3)
                            {
                                PLCListener.Send(data, data.Length, ipAddress, 9600);
                                j++;
                            }
                            clsErrorLogs.WriteLogs("sendplc_log", orderStatus + " NOREAD ;" + orderStatus.Length + "; " + ipAddress + ";" + DateTime.Now);
                        }
                        // }
                    }

                }
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
            }
            finally
            {
                if (true)//listen
                {
                    try
                    {
                        c.BeginReceive(PLCDataReceived, ar.AsyncState);
                    }
                    catch (Exception ex)
                    {
                        clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
                    }
                }
            }
            // Restart listening for udp data packages
        }
        #endregion
        #region PTL Module
        void listentoUDP(int retryNo)
        {
            if (retryNo < 3)
            {
                try
                {
                    uint IOC_IN = 0x80000000;
                    uint IOC_VENDOR = 0x18000000;
                    uint SIO_UDP_CONNRESET = IOC_IN | IOC_VENDOR | 12;
                    UDPListener = new UdpClient(this._PortnoR);
                    UDPListener.Client.IOControl((int)SIO_UDP_CONNRESET, new byte[] { Convert.ToByte(false) }, null);
                    UDPListener.BeginReceive(UDPDataReceived, UDPListener);
                    string _receive_portNo = string.Empty;
                    // clsErrorLogs.WriteLogs("error", "PTLCards :" + dtPTLs.Select("_Type='PTL'").Length.ToString());
                    for (int ilistner = 0; ilistner < dtPTLs.Rows.Count; ilistner++)
                    {
                        try
                        {
                            if (dtPTLs.Rows[ilistner]["_Type"].ToString() == "PTL")
                            {
                                if (_receive_portNo.IndexOf(dtPTLs.Rows[ilistner]["ReceivedPort"].ToString().Trim()) == -1)
                                {
                                    _receive_portNo += dtPTLs.Rows[ilistner]["ReceivedPort"].ToString() + ",";
                                    lstUDPListener[ilistner] = new UdpClient(Convert.ToInt32(dtPTLs.Rows[ilistner]["ReceivedPort"].ToString().Trim()));
                                    lstUDPListener[ilistner].Client.IOControl((int)SIO_UDP_CONNRESET, new byte[] { Convert.ToByte(false) }, null);
                                    lstUDPListener[ilistner].BeginReceive(UDPDataReceived, lstUDPListener[ilistner]);
                                    clsErrorLogs.WriteLogs("error", "UDPListner " + (ilistner + 1).ToString() + " Started on Port No " + dtPTLs.Rows[ilistner]["ReceivedPort"].ToString() + " at " + DateTime.Now);

                                }
                            }
                        }
                        catch (Exception ex1)
                        {
                            clsErrorLogs.WriteLogs("error : ", ex1.ToString() + "at Port No " + dtPTLs.Rows[ilistner]["ReceivedPort"].ToString()); // log.Error(ex);
                        }

                    }
                }
                catch (Exception ex)
                {
                    retryNo++;
                    clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
                    listentoUDP(retryNo);
                }
            }
        }

        void UDPDataReceived(IAsyncResult ar)
        {
            UdpClient c = (UdpClient)ar.AsyncState;
            //Boolean listen = true;
            try
            {
                IPEndPoint receivedIpEndPoint = new IPEndPoint(IPAddress.Any, 0);
                Byte[] receivedBytes = c.EndReceive(ar, ref receivedIpEndPoint);
                string msg = ASCIIEncoding.ASCII.GetString(receivedBytes);
                string ipAddress = receivedIpEndPoint.Address.ToString();
                try
                {
                    c.BeginReceive(UDPDataReceived, ar.AsyncState);
                    //listen = false;
                }
                catch (Exception ex)
                {
                    clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
                }
                string ptl_id = string.Empty;
                string operation = string.Empty;
                string bay_id = string.Empty;
                string mode = string.Empty;
                string order_number = string.Empty;

                if (_ptlmsg != msg)
                {
                    _ptlmsg = msg;
                    DataRow[] dr = dtPTLs.Select("IPAddress='" + ipAddress + "'");
                    if (dr != null && dr.Length > 0)
                    {
                        switch (Convert.ToString(dr[0]["_Type"]))
                        {
                            case "PTL":
                                msg = msg.Replace("\r\n\0", "").Replace("\r\n", "").Trim();
                                if (msg.StartsWith("@@") && ((msg.Length == 10)))
                                {
                                    ptl_id = msg.Substring(2, 4);
                                    operation = msg.Substring(6, 2);
                                    bay_id = "";
                                }
                                else
                                {
                                    return;
                                }
                                try
                                {
                                    switch (operation)
                                    {
                                        case "B1":
                                            clsErrorLogs.WriteLogs("receivedptl", msg + ";" + msg.Length + ";" + ipAddress + ";" + DateTime.Now);
                                            DataRow[] drPtl = dtPTLs.Select("_Type='PTL' and IPAddress='" + ipAddress + "'");
                                            if (drPtl != null && drPtl.Length > 0)
                                            {
                                                sendMsgtoPTL(ptl_id, Convert.ToInt32(drPtl[0]["SendPort"]), "000", "000", false, ipAddress, ptl_id, string.Empty, string.Empty);
                                                DataTable dtPTLList = clsDM.UpdatePickConfirmation(operation, ptl_id, ipAddress);
                                                if (dtPTLList != null && dtPTLList.Rows.Count > 0)
                                                {
                                                    string _msg = string.Empty;
                                                    string LCD_IPAddress = string.Empty;
                                                    Int32 LCD_SendPort = 0;
                                                    for (int imsg = 0; imsg < dtPTLList.Rows.Count; imsg++)
                                                    {
                                                        string _ptlcode = Convert.ToString(dtPTLList.Rows[imsg]["Pick_Location"]);
                                                        Int32 _portno = Convert.ToInt32(dtPTLList.Rows[imsg]["PTL_SendPort"]);
                                                        string _IPaddress = Convert.ToString(dtPTLList.Rows[imsg]["PTL_IPAddress"]);
                                                        string _color_code = string.Empty;
                                                        _color_code = clsComman.getLightColor(Convert.ToString(dtPTLList.Rows[imsg]["Sku_Type"]).ToUpper());
                                                        LCD_IPAddress = Convert.ToString(dtPTLList.Rows[imsg]["LCD_IPAddress"]);
                                                        LCD_SendPort = Convert.ToInt32(dtPTLList.Rows[imsg]["LCD_SendPort"]);
                                                        _msg = Convert.ToString(dtPTLList.Rows[imsg]["PickQty"]);
                                                        sendMsgtoPTL(_ptlcode, _portno, Convert.ToString(dtPTLList.Rows[imsg]["PickQty"]), _color_code, false, _IPaddress, _ptlcode, string.Empty, Convert.ToString(dtPTLList.Rows[imsg]["Carate_Code"]));
                                                    }
                                                    if (_msg == "PAS" || _msg == "EXT")
                                                    {
                                                        sendMsgtoLCD(LCD_SendPort, new String(' ', 80), LCD_IPAddress);
                                                    }
                                                    //System.Threading.Thread.Sleep(3000);
                                                    //for (int imsg = 0; imsg < dtPTLList.Rows.Count; imsg++)
                                                    //{
                                                    //    resetPTL(Convert.ToString(dtPTLList.Rows[imsg]["PTL_IPAddress"]), "000", Convert.ToInt32(dtPTLList.Rows[imsg]["PTL_SendPort"]));
                                                    //}
                                                }
                                                //else
                                                //{
                                                //    switch (Convert.ToString(drPtl[0]["Zone_Type"]))
                                                //    {
                                                //        case "SCANNABLE":
                                                //            sendMsgtoPTL(ptl_id, Convert.ToInt32(drPtl[0]["SendPort"]), "000", "010", false, ipAddress, string.Empty, string.Empty, string.Empty);
                                                //            break;
                                                //        case "NORMAL":
                                                //            sendMsgtoPTL(ptl_id, Convert.ToInt32(drPtl[0]["SendPort"]), "000", "001", false, ipAddress, string.Empty, string.Empty, string.Empty);
                                                //            break;
                                                //    }

                                                //}
                                            }
                                            break;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);

                                }
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
            }
        }
        void resetPTL(string ipAddress, string light, int portNo)
        {
            string str = "@ALLLRST" + light + "$$";
            byte[] databyte = Encoding.ASCII.GetBytes(str);
            int retryNo = 0;
            try
            {
                while (retryNo < 3)
                {
                    Thread.Sleep(100);
                    UDPListener.Send(databyte, databyte.Length, ipAddress, portNo);
                    retryNo++;
                }
                clsErrorLogs.WriteLogs("sendptl_log", str + "," + str.Length + ";" + ipAddress + ";" + DateTime.Now.ToString());
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
            }
        }

        void sendMsgtoPTL(string ptl_id, int portNo, string message, string light, Boolean reset, string ipAddress, string ptl_location, string Row_Name, string Carton_Code)
        {
            if (ptl_id.Length == 4)
            {
                try
                {
                    string str = "@" + ptl_id + message + light + "$$";
                    clsErrorLogs.WriteLogs("sendptl_log", Carton_Code + ";" + str + ";" + str.Length + ";" + ipAddress + ";" + DateTime.Now);
                    byte[] databyte = Encoding.ASCII.GetBytes(str);
                    int retryNo = 0;
                    if (!ptlAC.ContainsKey(ptl_id))
                        ptlAC.TryAdd(ptl_id, false);
                    else
                        ptlAC[ptl_id] = false;
                    while (!ptlAC[ptl_id] && retryNo < 3)
                    {
                        Thread.Sleep(30);
                        UDPListener.Send(databyte, databyte.Length, ipAddress, SendPortList[ipAddress]);
                        retryNo++;
                    }
                    //if (ptlAC.ContainsKey(ptl_id))
                    //    ptlAC[ptl_id] = false;
                }
                catch (Exception ex)
                {
                    clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
                }
            }
        }
        #endregion
        #region Scanner Module       

        void GetScanbasedPicking(string order_number, string ipAddress, DataTable InvoiceData)
        {
            using (DataTable dtcarton = InvoiceData)//clsDM.ValidateInvoice(order_number, ipAddress))
            {
                if (dtcarton != null && dtcarton.Rows.Count > 0 && Convert.ToString(dtcarton.Rows[0][0]).Trim().Length > 0)
                {
                    if (ScanBasedPicking != null && (ScanBasedPicking.Select(c => c.Carton_code == order_number).Count() == 0 || ScanBasedPicking.Select(c => c.Scanner_IPAddress == ipAddress).Count() == 0))
                    {
                        ScanBasedPicking.Add(new clsCartonWisePicking()
                        {
                            Carton_code = order_number,
                            Zone_IPAddress = Convert.ToString(dtcarton.Rows[0]["Zone_IP_Address"]),
                            Scanner_IPAddress = ipAddress,
                            Item_Count = 0
                        });
                    }
                    else
                    {
                        try
                        {
                            ScanBasedPicking.Remove(ScanBasedPicking.Where(c => c.Scanner_IPAddress == ipAddress).ToList<clsCartonWisePicking>()[0]);
                        }
                        catch
                        {
                        }
                        ScanBasedPicking.Add(new clsCartonWisePicking()
                        {
                            Carton_code = order_number,
                            Zone_IPAddress = Convert.ToString(dtcarton.Rows[0]["Zone_IP_Address"]),
                            Scanner_IPAddress = ipAddress,
                            Item_Count = 0
                        });
                    }
                }
                else
                {
                    List<clsCartonWisePicking> objPicking = ScanBasedPicking.Where(c => c.Scanner_IPAddress == ipAddress).ToList<clsCartonWisePicking>();
                    if (objPicking != null)
                    {
                        clsDM.AddScanner_PickData(objPicking[0].Carton_code, order_number, objPicking[0].Zone_IPAddress);
                    }
                }
            }
        }
        void StartListening()
        {
            try
            {
                // clsErrorLogs.WriteLogs("error", "Scanner Listining Start at Port : " + this._PortnoR.ToString());//log.Error(ex);
                byte[] bytes = new Byte[1024];
                IPEndPoint localEndPoint = new IPEndPoint(IPAddress.Any, this._PortnoR);
                Socket listener = new Socket(AddressFamily.InterNetwork,
                    SocketType.Stream, ProtocolType.Tcp);
                // Bind the socket to the local endpoint and listen for incoming connections.

                try
                {
                    listener.Bind(localEndPoint);
                    listener.Listen(100);
                    clsErrorLogs.WriteLogs("error", "Scanner Listining Start at Port : " + this._PortnoR.ToString());//log.Error(ex);
                    while (true)
                    {
                        // Set the event to nonsignaled state.
                        allDone.Reset();
                        // Start an asynchronous socket to listen for connections.
                        // Console.WriteLine("Waiting for a connection...");
                        try
                        {
                            clsErrorLogs.WriteLogs("error", "Scanner Listining Start at Port : " + this._PortnoR.ToString());//log.Error(ex);
                            listener.BeginAccept(new AsyncCallback(AcceptCallback), listener);
                        }
                        catch
                        {
                        }
                        // Wait until a connection is made before continuing.
                        allDone.WaitOne();
                    }
                }
                catch (ThreadAbortException tex)
                {
                    clsErrorLogs.WriteLogs("error_other", tex.ToString());
                }
                catch (SocketException sex)
                {
                    clsErrorLogs.WriteLogs("error_other", sex.ToString());
                }
                catch (Exception ex)
                {
                    clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
                }
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString()); // log.Error(ex);
            }
            //Console.WriteLine("\nPress ENTER to continue...");
            // Console.Read();
        }
        void AcceptCallback(IAsyncResult ar)
        {
            // Signal the main thread to continue.
            allDone.Set();

            // Get the socket that handles the client request.
            Socket listener = (Socket)ar.AsyncState;
            Socket handler = listener.EndAccept(ar);

            // Create the state object.
            StateObject state = new StateObject();
            state.workSocket = handler;
            handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                new AsyncCallback(ReadCallback), state);
        }
        public void ReadCallback(IAsyncResult ar)
        {
            String content = String.Empty;
            // Retrieve the state object and the handler socket
            // from the asynchronous state object.
            // Read data from the client socket. 
            StateObject state = (StateObject)ar.AsyncState;
            Socket handler = state.workSocket;
            string msg = string.Empty;
            string ipAddress = string.Empty;
            Boolean listen = true;

            ipAddress = ((IPEndPoint)handler.RemoteEndPoint).Address.ToString();
            try
            {
                SocketError errorCode;
                int bytesRead = handler.EndReceive(ar, out errorCode);
                if (errorCode != SocketError.Success)
                {
                    bytesRead = 0;
                }
                if (bytesRead > 0)
                {
                    // There  might be more data, so store the data received so far.
                    state.sb.Length = 0;
                    state.sb.Append(Encoding.ASCII.GetString(
                        state.buffer, 0, bytesRead));
                    content = state.sb.ToString();
                    if (content.EndsWith("\r\n"))
                    {
                        content = content.Substring(0, content.Length - 2);
                    }

                    try
                    {
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                                                new AsyncCallback(ReadCallback), state);
                        listen = false;
                    }
                    catch (Exception ex)
                    {
                        clsErrorLogs.WriteLogs("error", ex.ToString() + ";" + ex.ToString().Length + ";" + DateTime.Now.ToString());
                    }

                    if (content.Length > 3)
                    {
                        if (content.IndexOf(",") != -1)
                        {
                            string[] received = content.Split(',');
                            ipAddress = received[1];
                            msg = received[0];
                        }
                        else
                        {
                            msg = content;
                        }
                        int _mode = 2;
                        DataTable dtPTLList = null;
                        string EnableScanner_Picking = ConfigurationManager.AppSettings["EnableScanner_Picking"].ToString();
                        clsErrorLogs.WriteLogs("received", msg + ";" + ipAddress + ";" + msg.Length + ";" + DateTime.Now.ToString());
                        /* Iniialzie Scan with Login/Logout/EmployeCode */
                        switch (msg.ToUpper())
                        {
                            case "LOGIN":
                                _mode = 3;
                                dtPTLList = clsDM.Request_for_PTL(msg, ipAddress, _mode);
                                ProceesLogin(ipAddress);
                                break;
                            case "LOGOUT":
                                _mode = 1;
                                dtPTLList = clsDM.Request_for_PTL(msg, ipAddress, _mode);
                                ProcessLogout(ipAddress, dtPTLList);
                                break;
                            default:
                                if (msg.ToLower().IndexOf("www.oriflame.com") != -1)
                                {
                                    clsDM.AddQRCode(msg, ipAddress);
                                }
                                //if (lstOperatorLogin != null)
                                //{
                                //var _login = lstOperatorLogin.Where(O => O.Scanner_IPAddress == ipAddress && O.Emp_Code == null).ToList<clsOperatorLogin>();
                                //if (_login != null && _login.Count() > 0 && _userid != "0")
                                //{
                                //    _mode = 0;
                                //    dtPTLList = clsDM.Request_for_PTL(msg, ipAddress, _mode);
                                //    if (dtPTLList != null && dtPTLList.Rows.Count > 0)
                                //    {
                                //        _login[0].Emp_Code = msg;
                                //    }
                                //}
                                //else if (_login != null && _login.Count() > 0 && _userid == "0")
                                //{
                                //    DataRow[] drPTL = dtPTLs.Select("IPAddress='" + ipAddress + "'");
                                //    if (drPTL != null && drPTL.Length > 0)
                                //    {
                                //        int _portno = Convert.ToInt32(dtPTLs.Select("IPAddress='" + ipAddress + "'")[0]["SendPort"]);
                                //        resetPTL(ipAddress, "100", _portno);
                                //    }
                                //}
                                string _userid = clsDM.validate_EmployeeCode(msg);
                                if (_userid != "0")
                                {
                                    _mode = 0;
                                    dtPTLList = clsDM.Request_for_PTL(msg, ipAddress, _mode);
                                }
                                //else if (_userid == "0")
                                //{
                                //    DataRow[] drPTL = dtPTLs.Select("IPAddress='" + ipAddress + "'");
                                //    if (drPTL != null && drPTL.Length > 0)
                                //    {
                                //        int _portno = Convert.ToInt32(dtPTLs.Select("IPAddress='" + ipAddress + "'")[0]["SendPort"]);
                                //        resetPTL(ipAddress, "100", _portno);
                                //    }
                                //}
                                else
                                {
                                    _mode = 2;
                                    DataTable isCrate = clsDM.ValidateInvoice(msg, ipAddress);
                                    if (isCrate != null && isCrate.Rows.Count > 0)
                                    {
                                        //if (Convert.ToString(isCrate.Rows[0]["Invoice_Number"]) == string.Empty)// && Convert.ToString(isCrate.Rows[0]["Zone_Type"]) != "NORMAL"
                                        //{
                                        //    if (EnableScanner_Picking == "1")
                                        //    {
                                        //        GetScanbasedPicking(msg, ipAddress, isCrate);
                                        //    }
                                        //}
                                        //else
                                        //{
                                        if (Convert.ToString(isCrate.Rows[0]["Invoice_Number"]) != string.Empty)
                                        {
                                            dtPTLList = clsDM.Request_for_PTL(msg, ipAddress, _mode);
                                            if (dtPTLList.Select("Sku_Type in ('SCANNABLE')") != null)
                                            {
                                                DataRow[] drScan = dtPTLList.Select("Sku_Type in ('SCANNABLE')");
                                                if (drScan != null && drScan.Length > 0)
                                                {
                                                    if (ScanBasedPicking != null && (ScanBasedPicking.Select(c => c.Carton_code == msg).Count() == 0 || ScanBasedPicking.Select(c => c.Scanner_IPAddress == ipAddress).Count() == 0))
                                                    {
                                                        ScanBasedPicking.Add(new clsCartonWisePicking()
                                                        {
                                                            Carton_code = msg,
                                                            Zone_IPAddress = Convert.ToString(drScan[0]["PTL_IPAddress"]),
                                                            Scanner_IPAddress = ipAddress,
                                                            Item_Count = 0
                                                        });
                                                    }
                                                }
                                                //}
                                            }

                                        }
                                    }
                                }
                                //}
                                //        else
                                //        {
                                //    lstOperatorLogin = new ConcurrentBag<clsOperatorLogin>();
                                //    DataRow[] drPTL = dtPTLs.Select("IPAddress='" + ipAddress + "'");
                                //    if (drPTL != null && drPTL.Length > 0)
                                //    {
                                //        int _portno = Convert.ToInt32(dtPTLs.Select("IPAddress='" + ipAddress + "'")[0]["SendPort"]);
                                //        resetPTL(ipAddress, "100", _portno);
                                //    }
                                //}
                                break;
                        }
                        ProcessPicktoLight(_mode, dtPTLList, msg, ipAddress);
                    }
                }
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString() + ";" + ex.ToString().Length + ";" + DateTime.Now.ToString());
            }
            finally
            {
                if (listen)
                {
                    try
                    {
                        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0,
                                                new AsyncCallback(ReadCallback), state);

                    }
                    catch (Exception ex)
                    {
                        clsErrorLogs.WriteLogs("error", ex.ToString() + ";" + ex.ToString().Length + ";" + DateTime.Now.ToString());
                    }
                }

            }
        }
        void ProceesLogin(string ipAddress)
        {
            try
            {
                if (lstOperatorLogin == null || lstOperatorLogin.Where(P => P.Scanner_IPAddress == ipAddress).Count() == 0)
                {
                    lstOperatorLogin.Add(new clsOperatorLogin()
                    {
                        Scan_Mode = "LOGIN",
                        Scanner_IPAddress = ipAddress,
                        Emp_Code = null
                    });
                }
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString() + ";" + ipAddress + ";" + ex.ToString().Length + ";" + DateTime.Now.ToString());
            }
        }
        void ProcessLogout(string ipAddress, DataTable dtPTLList)
        {
            try
            {
                if (lstOperatorLogin != null)
                {
                    var _login = lstOperatorLogin.Where(O => O.Scanner_IPAddress == ipAddress && O.Emp_Code != null).ToList<clsOperatorLogin>();
                    if (_login != null && _login.Count() > 0)
                    {
                        clsOperatorLogin objaction = _login[0];
                        if (dtPTLList != null && dtPTLList.Rows.Count > 0)
                        {
                            lstOperatorLogin.TryTake(out objaction);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clsErrorLogs.WriteLogs("error", ex.ToString() + ";" + ipAddress + ";" + ex.ToString().Length + ";" + DateTime.Now.ToString());
            }
        }
        void ProcessPicktoLight(int _mode, DataTable dtPTLList, string msg, string ipAddress)
        {
            clsErrorLogs.WriteLogs("received", msg + ";" + ipAddress + ";" + msg.Length + ";" + DateTime.Now.ToString());
            string EnableScanner_Picking = ConfigurationManager.AppSettings["EnableScanner_Picking"].ToString();
            switch (_mode)
            {
                case 0: //Login
                    if (dtPTLList != null && dtPTLList.Rows.Count > 0)
                    {
                        for (int imsg = 0; imsg < dtPTLList.Rows.Count; imsg++)
                        {
                            Int32 _portno = Convert.ToInt32(dtPTLList.Rows[imsg]["PTL_SendPort"]);
                            string _IPaddress = Convert.ToString(dtPTLList.Rows[imsg]["PTL_IPAddress"]);
                            resetPTL(_IPaddress, "002", _portno);
                        }
                    }
                    break;
                case 1: //Logout
                    if (dtPTLList != null && dtPTLList.Rows.Count > 0)
                    {
                        for (int imsg = 0; imsg < dtPTLList.Rows.Count; imsg++)
                        {
                            Int32 _portno = Convert.ToInt32(dtPTLList.Rows[imsg]["PTL_SendPort"]);
                            string _IPaddress = Convert.ToString(dtPTLList.Rows[imsg]["PTL_IPAddress"]);
                            resetPTL(_IPaddress, "200", _portno);
                        }
                    }
                    break;
                default: //Employee Code|| Crate || QrCode
                    if (dtPTLList != null && dtPTLList.Rows.Count > 0)
                    {
                        /* Carate Scan to Start Pick */
                        DataTable dtResetZone = clsDM.ResetZone(ipAddress);
                        if (dtResetZone != null)
                        {
                            for (int imsg = 0; imsg < dtResetZone.Rows.Count; imsg++)
                            {
                                resetPTL(Convert.ToString(dtResetZone.Rows[imsg]["PTL_IPAddress"]), "000", Convert.ToInt32(dtResetZone.Rows[imsg]["PTL_SendPort"]));
                            }
                            dtResetZone = null;
                        }
                        string _MSG = string.Empty;
                        Int32 LCD_SendPort = 0;
                        string LCD_IPAddress = string.Empty;
                        for (int imsg = 0; imsg < dtPTLList.Rows.Count; imsg++)
                        {
                            string _ptlcode = Convert.ToString(dtPTLList.Rows[imsg]["Pick_Location"]);
                            Int32 _portno = Convert.ToInt32(dtPTLList.Rows[imsg]["PTL_SendPort"]);
                            string _IPaddress = Convert.ToString(dtPTLList.Rows[imsg]["PTL_IPAddress"]);
                            string _color_code = string.Empty;
                            LCD_IPAddress = Convert.ToString(dtPTLList.Rows[imsg]["LCD_IPAddress"]);
                            LCD_SendPort = Convert.ToInt32(dtPTLList.Rows[imsg]["LCD_SendPort"]);
                            _MSG = Convert.ToString(dtPTLList.Rows[imsg]["PickQty"]);
                            _color_code = clsComman.getLightColor(Convert.ToString(dtPTLList.Rows[imsg]["Sku_Type"]).ToUpper());
                            sendMsgtoPTL(_ptlcode, _portno, _MSG, _color_code, false, _IPaddress, _ptlcode, string.Empty, Convert.ToString(dtPTLList.Rows[imsg]["Carate_Code"]));
                        }
                        //foreach (DataRow dr in dtPTLs.Select("_Type='LED' and IPAddress='" + LCD_IPAddress + "'"))
                        //{
                        string _msg = clsDM.getMessagetoDisplayonLCD(LCD_IPAddress, msg, "LED");
                        if (_msg != string.Empty)
                            sendMsgtoLCD(LCD_SendPort, _msg, LCD_IPAddress);
                        //}
                        if (_MSG == "PAS" || _MSG == "EXT")
                        {
                            sendMsgtoLCD(LCD_SendPort, new String(' ', 80), LCD_IPAddress);
                            //System.Threading.Thread.Sleep(1000);
                            //DataTable dtResetZone1 = clsDM.ResetZone(ipAddress);
                            //if (dtResetZone1 != null)
                            //{
                            //    for (int imsg = 0; imsg < dtResetZone1.Rows.Count; imsg++)
                            //    {
                            //        resetPTL(Convert.ToString(dtResetZone1.Rows[imsg]["PTL_IPAddress"]), "000", Convert.ToInt32(dtResetZone1.Rows[imsg]["PTL_SendPort"]));
                            //    }
                            //    dtResetZone1 = null;
                            //}
                        }

                    }
                    else
                    {
                        //foreach (DataRow dr in dtPTLs.Select("_Type='LED' and IPAddress='" + _Displayip + "'"))
                        //{
                        //    string _msg = clsDM.getMessagetoDisplayonLCD(Convert.ToString(dr["IPAddress"]), msg);
                        //    if (_msg != string.Empty)
                        //        sendMsgtoLCD(Convert.ToInt32(dr["SendPort"]), _msg, Convert.ToString(dr["IPAddress"]));
                        //}
                        //if (msg.IndexOf("P&PBIN") != -1)
                        //{
                        //    DataTable dtLED = clsDM.getNONPickLEDMEssage(msg, ipAddress);
                        //    if (dtLED != null && dtLED.Rows.Count > 0)
                        //    {
                        //        foreach (DataRow dr in dtLED.Rows)
                        //        {
                        //            sendMsgtoLCD(Convert.ToInt32(dr["LCD_SendPort"]), Convert.ToString(dr["Message"]), Convert.ToString(dr["LCD_IPAddress"]));
                        //        }
                        //    }
                        //}
                        //DataTable dtLED = clsDM.getNONPickLEDMEssage(msg, ipAddress);
                        //if (dtLED != null && dtLED.Rows.Count > 0)
                        //{
                        //    sendMsgtoLCD(Convert.ToInt32(dtLED.Rows[0]["LCD_SendPort"]), Convert.ToString(dtLED.Rows[0]["Message"]), Convert.ToString(dtLED.Rows[0]["LCD_IPAddress"]));
                        //}
                        if (EnableScanner_Picking == "0")
                        {
                            dtPTLList = clsDM.UpdatePickConfirmation("S1", msg, ipAddress);
                            if (dtPTLList != null && dtPTLList.Rows.Count > 0)
                            {
                                /* Scan Based Sku Pick Confirmation */
                                string _MSG = string.Empty;
                                Int32 LCD_SendPort = 0;
                                string LCD_IPAddress = string.Empty;
                                for (int imsg = 0; imsg < dtPTLList.Rows.Count; imsg++)
                                {
                                    string _ptlcode = Convert.ToString(dtPTLList.Rows[imsg]["PTL"]);
                                    Int32 _portno = Convert.ToInt32(dtPTLList.Rows[imsg]["PTL_SendPort"]);
                                    string _IPaddress = Convert.ToString(dtPTLList.Rows[imsg]["Zone_IPAddress"]);
                                    string _color_code = string.Empty;
                                    LCD_IPAddress = Convert.ToString(dtPTLList.Rows[imsg]["LCD_IPAddress"]);
                                    LCD_SendPort = Convert.ToInt32(dtPTLList.Rows[imsg]["LCD_SendPort"]);
                                    _MSG = Convert.ToString(dtPTLList.Rows[imsg]["PickQty"]);
                                    _color_code = clsComman.getLightColor(Convert.ToString(dtPTLList.Rows[imsg]["Sku_Type"]).ToUpper());
                                    sendMsgtoPTL(_ptlcode, _portno, _MSG, _color_code, false, _IPaddress, _ptlcode, string.Empty, Convert.ToString(dtPTLList.Rows[imsg]["Carate_Code"]));
                                }
                                if (_MSG == "PAS" || _MSG == "EXT")
                                {
                                    sendMsgtoLCD(LCD_SendPort, new String(' ', 80), LCD_IPAddress);
                                }
                                //if (_MSG == "PAS" || _MSG == "EXT")
                                //{
                                //    System.Threading.Thread.Sleep(1000);
                                //    DataTable dtResetZone = clsDM.ResetZone(ipAddress);
                                //    if (dtResetZone != null)
                                //    {
                                //        for (int imsg = 0; imsg < dtResetZone.Rows.Count; imsg++)
                                //        {
                                //            resetPTL(Convert.ToString(dtResetZone.Rows[imsg]["PTL_IPAddress"]), "000", Convert.ToInt32(dtResetZone.Rows[imsg]["PTL_SendPort"]));
                                //        }
                                //        dtResetZone = null;
                                //    }
                                //}
                            }
                        }
                    }
                    break;
            }
        }
        #endregion
    }
}
