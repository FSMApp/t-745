﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Win_Falcon_Listener
{
    public class clsCartonWisePicking
    {
        public string Carton_code
        {
            get;
            set;
        }
        public string Zone_IPAddress
        {
            get;
            set;
        }
        public string Scanner_IPAddress
        {
            get;
            set;
        }
        public int Item_Count
        {
            get;
            set;
        }
    }
}
