﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinSer_Advance
{
    public static class clsErrorLogs
    {
        public static string generateErrorMessage(Exception ex)
        {
            StackTrace st = new StackTrace(ex, true);
            //Get the first stack frame
            StackFrame frame = st.GetFrame(0);

            //Get the file name
            string fileName = frame.GetFileName();

            //Get the method name
            string methodName = frame.GetMethod().Name;

            //Get the line number from the stack frame
            int line = frame.GetFileLineNumber();

            //Get the column number
            int col = frame.GetFileColumnNumber();
            return ex.Message + " ,at Line Number : " + line.ToString() + " ,in method : " + methodName + ", in source File : " + fileName;
        }
        public static void WriteLogs(string _type, string Message)
        {
            string filePath = string.Empty;
            switch (_type.ToString())
            {
                case "error":
                    filePath = ConfigurationManager.AppSettings["ErrorLogs"].ToString();
                    Message = Message + ";" + Message.Length + ";" + DateTime.Now;
                    break;
                case "sendptl_log":
                case "sendplc_log":
                    filePath = ConfigurationManager.AppSettings["SendPTLData"].ToString();
                    break;
                case "received":
                    filePath = ConfigurationManager.AppSettings["ReceivedData"].ToString();
                    break;
                case "dbcalls":
                    filePath = ConfigurationManager.AppSettings["DBCalls"].ToString();
                    break;
                case "error_other":
                    filePath = ConfigurationManager.AppSettings["other_errors"].ToString();
                    Message = Message + ";" + Message.Length + ";" + DateTime.Now;
                    break;
                case "senddisplay_log":
                    filePath = ConfigurationManager.AppSettings["senddisplay_log"].ToString();
                    Message = Message + ";" + Message.Length + ";" + DateTime.Now;
                    break;
                case "alarms":
                    filePath = ConfigurationManager.AppSettings["alarmsDBCalls"].ToString();
                    Message = Message + ";" + Message.Length + ";" + DateTime.Now;
                    break;
                case "Email":
                    filePath = ConfigurationManager.AppSettings["Email"].ToString();
                    Message = Message + ";" + Message.Length + ";" + DateTime.Now;
                    break;
                case "Archive":
                    filePath = ConfigurationManager.AppSettings["Archive"].ToString();
                    Message = Message + ";" + Message.Length + ";" + DateTime.Now;
                    break;
            }
            try
            {
                if (File.Exists(Path.Combine(filePath + DateTime.Now.ToString("dd-MM-yyyy") + ".log")))
                {
                    using (StreamWriter sw = File.AppendText(Path.Combine(filePath + DateTime.Now.ToString("dd-MM-yyyy") + ".log")))
                    {
                        sw.WriteLine(Message);
                    }
                }
                else
                {
                    using (StreamWriter sw = File.CreateText(System.IO.Path.Combine(filePath + DateTime.Now.ToString("dd-MM-yyyy") + ".log")))
                    {
                        sw.WriteLine(Message);
                    }
                }
            }
            catch //(Exception ex)
            {
                //WriteLogs("error", ex.ToString());
            }
        }

    }
}
