﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Timers;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Net.Configuration;
using System.Net;
using FalconUtilities.Email;

namespace WinSer_Advance
{
    public partial class WinService_Advance : ServiceBase
    {
        Timer _timerArchive = null;
        DateTime ProcessingDateTime;
        Timer objLogTimer = null;
        Timer objDailyMailer = null;
        Timer _objQrcode = null;
        int FailCount = 0;
        bool FileReceived = false;
        string InvalidFile = string.Empty;
        string _DBConnection = ConfigurationManager.ConnectionStrings["Constr"].ConnectionString;
        string header = "<thead><tr><th>FileName</th><th>QR Code Count</th></tr></thead><tbody>";
        string InvalidFileheader = "<thead><tr><th>File Name</th><th>Message</th></tr></thead><tbody>";
        string Invalidheader = "<thead><tr><th>Line No</th><th>Invoice</th><th>QR Code</th><th>Capture Date</th><th>File Name</th><th>Message</th></tr></thead><tbody>";

        public WinService_Advance()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _timerArchive = new Timer();
            _timerArchive.Interval = 60 * 1000 * 60;
            _timerArchive.Elapsed += _timerArchive_Elapsed;
            _timerArchive.Enabled = true;
            _timerArchive.Start();

            objLogTimer = new Timer();
            objLogTimer.Interval = 60 * 1000 * 60;
            objLogTimer.Enabled = true;
            objLogTimer.Elapsed += objLogTimer_Elapsed;
            objLogTimer.Start();

            _objQrcode = new Timer();
            _objQrcode.Interval = 60 * 1000 * 60;
            _objQrcode.Elapsed += _objQrcode_Elapsed;
            _objQrcode.Enabled = true;
            _objQrcode.Start();

        }

        private void _objQrcode_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_objQrcode.Enabled)
            {
                FileReceived = false;
                _objQrcode.Enabled = false;
                if (DateTime.Now.Hour >= 23 && (ProcessingDateTime.ToString("yyyy/MM/dd") == "0001-01-01" || ProcessingDateTime.Day < DateTime.Now.Day))
                {
                    string InvalidContent = string.Empty;
                    string emailContent = CVSFileProcess(out InvalidContent);


                    InvalidFile = InvalidFile.Length > 0 ? InvalidFileheader + InvalidFile + "</tbody>" : InvalidFile;

                    string EmailBody = File.ReadAllText(Path.Combine(ConfigurationManager.AppSettings["TemplateFilePath"].ToString(), "TableEmail_Template.html"));
                    if (emailContent.Length > 0 && InvalidContent.IndexOf("<td>") == -1)
                    {
                        EmailBody =   EmailBody.Replace("@Body", header+emailContent);
                        SendEmail(EmailBody, "[Success] file upload successfully");
                        FileReceived = true;
                    }
                    InvalidContent = InvalidContent.Trim().Length > 0 ? Invalidheader + InvalidContent.Trim() + "</tbody>" : InvalidContent.Trim();
                    if (InvalidContent.Trim().Length > 10 && InvalidContent.IndexOf("<td>") != -1)
                    {
                        if (emailContent.Length == 0)
                        {
                            EmailBody = File.ReadAllText(Path.Combine(ConfigurationManager.AppSettings["TemplateFilePath"].ToString(), "TableEmail_Template.html"));
                            EmailBody = EmailBody.Replace("@Body", InvalidContent);
                            SendEmail(EmailBody, "[Error] Invalid Data");
                            InvalidContent = string.Empty;
                        }
                        else
                        {
                            string NewBody = string.Empty;

                            EmailBody = File.ReadAllText(Path.Combine(ConfigurationManager.AppSettings["TemplateFilePath"].ToString(), "TableEmail_Template.html"));
                            NewBody =  EmailBody.Replace("@Body", header+emailContent);

                            EmailBody = File.ReadAllText(Path.Combine(ConfigurationManager.AppSettings["TemplateFilePath"].ToString(), "TableEmail_Template.html"));
                            NewBody += EmailBody.Replace("@Body", InvalidContent);

                            SendEmail(NewBody, "[Error] Invalid Data");
                        }
                    }
                    if (InvalidFile.Trim().Length > 10)
                    {
                        EmailBody = File.ReadAllText(Path.Combine(ConfigurationManager.AppSettings["TemplateFilePath"].ToString(), "TableEmail_Template.html"));
                        EmailBody = EmailBody.Replace("@Body", InvalidFile);
                        SendEmail(EmailBody, "[Error] Invalid File");
                        InvalidFile = string.Empty;
                    }
                    if (!FileReceived)//!FileNotReceived() &&
                    {
                        ProcessingDateTime = DateTime.Now;
                        string subject = "[Error] File Missing";
                        string body = "No file received on date :-" + DateTime.Now.ToString("yyyy/MM/dd");
                        SendEmail(body, subject);
                    }
                }
                _objQrcode.Enabled = true;
            }
        }
        string CVSFileProcess(out string InvalidContent)
        {
            StringBuilder sbrEmail = new StringBuilder();
            string EmailContent = string.Empty;
            string _InvalidContent = string.Empty;
            StringBuilder sbrInvalid = new StringBuilder();
            string _SourcePath = ConfigurationManager.AppSettings["csvFilePath"].ToString();
            string _bkpPath = ConfigurationManager.AppSettings["csvFilePathBkp"].ToString();
            //sbrEmail.AppendLine("<thead><tr><th>FileName</th><th>QR Code Count</th></tr></thead><tbody>");
            foreach (string d in Directory.GetDirectories(_SourcePath))
            {
                var Files = Directory.GetFiles(d, "*.csv", SearchOption.TopDirectoryOnly);
                foreach (var _f in Files)
                {
                    FileReceived = true;
                    ProcessingDateTime = DateTime.Now;
                    string[] _data;
                    try
                    {
                        int _lineNo = 1;
                        int SuccessCount = 0;
                        _data = File.ReadAllLines(_f);
                        foreach (var _line in _data)
                        {
                            if (_lineNo != 1)
                            {
                                if (AddQrCodeData(_lineNo, _line.Split(','), Path.GetFileName(_f), out _InvalidContent))
                                    SuccessCount++;
                                else
                                    FailCount++;
                                sbrInvalid.AppendLine(_InvalidContent);
                            }
                            _lineNo++;
                        }
                        if (SuccessCount > 0)
                        {
                            sbrEmail.AppendLine("<tr><td>" + Path.GetFileName(_f) + "</td><td>" + SuccessCount.ToString() + "</td></tr>");
                            SuccessCount = 0;
                        }
                        File.Copy(_f, Path.Combine(_bkpPath, Path.GetFileName(_f)), true);
                        File.Delete(_f);
                    }
                    catch (Exception ex)
                    {
                        clsErrorLogs.WriteLogs("error", ex.ToString());
                    }
                }
                Files = Directory.GetFiles(d, "*.*", SearchOption.TopDirectoryOnly);
                foreach (var _f in Files)
                {
                    if (Path.GetExtension(_f).ToLower() != ".csv")
                    {
                        InvalidFile += "<tr><td>" + Path.GetFileName(_f) + "</td><td>Invalid File Extension(Required .csv only)</td></tr>";
                    }
                    File.Copy(_f, Path.Combine(_bkpPath, Path.GetFileName(_f)), true);
                    File.Delete(_f);
                }
            }
            if (sbrEmail.Length > 10)
            {
                sbrEmail.AppendLine("</tbody>");
                EmailContent = sbrEmail.ToString();
            }
            InvalidContent = sbrInvalid.ToString();
            sbrInvalid.Length = 0;
            return EmailContent;
        }
        bool AddQrCodeData(int lineno, string[] data, string _FileName, out string InvalidContent)
        {
            bool filestatus = false;
            string _InvalidContent = string.Empty;
            try
            {
                DateTime dt;
                long invoice;
                if (DateTime.TryParse(data[2], out dt))
                {
                    data[2] = Convert.ToDateTime(data[2]).ToString("yyyy-MM-dd hh:mm:ss");
                }
                else
                {
                    _InvalidContent += "<tr><td>" + lineno + "</td><td>" + data[3] + "</td><td>" + data[4] + "</td><td>" + data[2] + "</td><td>" + _FileName + "</td><td>Invalid Datetime Format</td></tr>";
                }
                if (data[4].IndexOf("www.oriflame.com/?q=") == -1 || data[4].Length < 27)
                {
                    _InvalidContent += "<tr><td>" + lineno + "</td><td>" + data[3] + "</td><td>" + data[4] + "</td><td>" + data[2] + "</td><td>" + _FileName + "</td><td>Invalid QR Code</td></tr>";
                }
                if (data[3].Substring(0, 2) != data[0] || data[3].Length < 12 || !Int64.TryParse(data[3], out invoice))
                {
                    _InvalidContent += "<tr><td>" + lineno + "</td><td>" + data[3] + "</td><td>" + data[4] + "</td><td>" + data[2] + "</td><td>" + _FileName + "</td><td>Invalid Invoice Number</td></tr>";
                }
                if (_InvalidContent.Trim().Length == 0)
                {
                    string _strdata = "'" + string.Join("','", data, 0, data.Length - 1) + "','" + _FileName + "','RDC East'";
                    string _query = string.Empty;
                    _query = "INSERT INTO tbl_scan_pick_details(WareHouse,Login_User,ScanDatetime,Invoice_No,ScanedCode,File_Name,WareHouseName)";
                    _query += " VALUES(" + _strdata + ")";
                    using (MySqlConnection mycon = new MySqlConnection(_DBConnection))
                    {
                        mycon.Open();
                        using (MySqlCommand mycmd = new MySqlCommand(_query, mycon))
                        {
                            mycmd.CommandType = CommandType.Text;
                            mycmd.ExecuteNonQuery();
                        }
                    }
                    filestatus = true;
                }
                else
                {
                    filestatus = false;
                }
            }
            catch (Exception ex)
            {
                filestatus = false;
                if (ex.Message.ToLower().IndexOf("duplicate") != -1)
                {
                    _InvalidContent += "<tr><td>" + lineno + "</td><td>" + data[3] + "</td><td>" + data[4] + "</td><td>" + data[2] + "</td><td>" + _FileName + "</td><td>Duplicate QR Code</td></tr>";
                }
                clsErrorLogs.WriteLogs("error", ex.ToString());
            }
            finally
            {

            }
            InvalidContent = _InvalidContent;
            return filestatus;
        }
        private void ObjDailyMailer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (objDailyMailer.Enabled)
            {
                string _dailyEmailer = string.Empty;
                string _weekEmailer = string.Empty;
                string _monthlyEmailer = string.Empty;
                string _yearEmailer = string.Empty;
                objDailyMailer.Enabled = false;
                DataTable dtEmailer = getEmailerList();
                if (dtEmailer != null && dtEmailer.Rows.Count > 0)
                {
                    foreach (DataRow dremail in dtEmailer.Rows)
                    {
                        switch (Convert.ToString(dremail["Frequency"]))
                        {
                            case "Daily":
                                _dailyEmailer += Convert.ToString(dremail["Email_id"]) + ";";
                                break;
                            case "Weekly":
                                _weekEmailer += Convert.ToString(dremail["Email_id"]) + ";";
                                break;
                            case "Monthly":
                                _monthlyEmailer += Convert.ToString(dremail["Email_id"]) + ";";
                                break;
                            case "Yearly":
                                _yearEmailer += Convert.ToString(dremail["Email_id"]) + ";";
                                break;
                        }
                    }
                }
                _dailyEmailer = _dailyEmailer.Trim(';');
                _weekEmailer = _weekEmailer.Trim(';');
                _monthlyEmailer = _monthlyEmailer.Trim(';');
                _yearEmailer = _yearEmailer.Trim(';');
                if (_dailyEmailer.Length > 5)
                {
                    EmailReport("D", _dailyEmailer);
                }
                if (_weekEmailer.Length > 5)
                {
                    EmailReport("D", _weekEmailer);
                }
                if (_monthlyEmailer.Length > 5)
                {
                    EmailReport("D", _monthlyEmailer);
                }
                if (_yearEmailer.Length > 5)
                {
                    EmailReport("D", _yearEmailer);
                }
                objDailyMailer.Enabled = true;
            }
        }
        public static void SendEmail(string body, string subject)
        {
            using (Emailing e = new Emailing())
            {
                e.Body = body;
                string[] _r = ConfigurationManager.AppSettings["Recipients"].ToString().Split(';');
                e.Recipient = _r;// "vineet@falconautoonline.com;maheshm@falconautoonline.com"
                e.Subject = subject;// ;
                e.Send();
            }
        }
        public static void SendMail(string to, string subject, string body)
        {
            Configuration oConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var mailSettings = oConfig.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;

            if (mailSettings != null)
            {
                int port = mailSettings.Smtp.Network.Port;
                string from = mailSettings.Smtp.From;
                string host = mailSettings.Smtp.Network.Host;
                string pwd = mailSettings.Smtp.Network.Password;
                string uid = mailSettings.Smtp.Network.UserName;

                var message = new MailMessage
                {
                    From = new MailAddress(@from)
                };
                message.To.Add(new MailAddress(to));
                message.CC.Add(new MailAddress(from));
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;

                var client = new SmtpClient
                {
                    Host = host,
                    Port = port,
                    Credentials = new NetworkCredential(uid, pwd),
                    EnableSsl = true
                };

                try
                {
                    client.Send(message);
                }
                catch (Exception ex)
                {
                }
            }
        }
        void EmailReport(string _frequency, string emails)
        {
            DataTable dtReportData = getDailyReport(_frequency);
            if (dtReportData != null && dtReportData.Rows.Count > 0)
            {
                StringBuilder sbrEmailer = new StringBuilder();
                sbrEmailer.AppendLine("<thead><th>Invoice date</th><th>Orders</th><th>Crates</th><th>Sku</th><th>Eaches</th><th>Start Pick</th><th>End Pick</th><th>Total Pick Time</th><th>Picked Crates</th><th>Picked Lines</th><th>Picked Eaches</th><th>Moderated Rate</th><th>%Crates</th></thead>");
                sbrEmailer.AppendLine("<tbody>");
                foreach (DataRow drreport in dtReportData.Rows)
                {
                    sbrEmailer.AppendLine("<tr>");
                    foreach (DataColumn dc in dtReportData.Columns)
                    {
                        sbrEmailer.AppendLine("<td>" + Convert.ToString(drreport[dc.ColumnName]) + "</td>");
                    }
                    sbrEmailer.AppendLine("</tr>");
                }
                sbrEmailer.AppendLine("</tbody>");
                if (sbrEmailer.ToString().Length > 0)
                {
                    string _t = File.ReadAllText(System.IO.Path.Combine(ConfigurationManager.AppSettings["TemplateFilePath"].ToString(), "TableEmail_Template.html"));
                    SendMail(emails, _frequency + " Productivity Report", _t.Replace("@Body", sbrEmailer.ToString()));
                }
            }
        }
        DataTable getEmailerList()
        {
            DataTable dtEmailer = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection())
                {
                    using (MySqlCommand mycmd = new MySqlCommand("select * from tbl_email_master where isActive=1", mycon))
                    {
                        mycmd.CommandType = CommandType.Text;
                        using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                        {
                            dtEmailer = new DataTable();
                            myadp.Fill(dtEmailer);
                        }
                    }
                }
                return dtEmailer;
            }
            catch (Exception ex)
            {
                dtEmailer = null;
                return dtEmailer;
            }
            finally
            {
                if (dtEmailer != null)
                {
                    dtEmailer.Dispose();
                }
            }
        }
        DataTable getDailyReport(string _frequency)
        {
            DataTable dtData = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_DBConnection))
                {
                    mycon.Open();
                    using (MySqlCommand mycmd = new MySqlCommand("SP_DetailReport", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.Parameters.AddWithValue("", _frequency);
                        using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                        {
                            dtData = new DataTable();
                            myadp.Fill(dtData);
                        }
                    }
                }
                return dtData;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                if (dtData != null)
                {
                    dtData.Dispose();
                }
            }
        }
        bool FileNotReceived()
        {
            string Query = "SELECT `ScanedCode` FROM `tbl_scan_pick_details` WHERE DATE(`ScanDatetime`)= DATE(NOW()) AND `WareHouse`!= '40' limit 1";
            MySqlDataReader rdr;
            using (MySqlConnection mycon = new MySqlConnection(_DBConnection))
            {
                mycon.Open();
                using (MySqlCommand mycmd = new MySqlCommand(Query, mycon))
                {
                    mycmd.CommandType = CommandType.Text;
                    rdr = mycmd.ExecuteReader();
                }
            }
            if (rdr.HasRows)
            {
                rdr.Close();
                return true;
            }
            else
            {
                rdr.Close();
                return false;
            }
        }
        void objLogTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (objLogTimer.Enabled)
            {
                objLogTimer.Enabled = false;
                deleteLogFiles();
                objLogTimer.Enabled = true;
            }
        }
        public static void deleteLogFiles()
        {
            string logs_path = string.Empty;
            logs_path = ConfigurationManager.AppSettings["LogPath"];

            if (!File.Exists(logs_path))
            {
                using (StreamWriter sw = File.CreateText(Path.Combine(logs_path, String.Format("{0}_{1:dd-MM-yyyy}.{2}", "QRCode", DateTime.Now, ".csv"))))
                {
                    sw.WriteLine("Message;Length;Timestamp;FileName");
                }
            }
            if (logs_path != null && Directory.Exists(logs_path))
            {
                string[] files = Directory.GetFiles(logs_path, "*.log", SearchOption.TopDirectoryOnly);
                foreach (string file in files)
                {
                    FileInfo fi = new FileInfo(file);
                    if (fi.CreationTime < DateTime.Now.AddDays((-1) * 30))
                        try
                        {
                            string[] data = File.ReadAllLines(file);
                            foreach (var line in data)
                            {
                                using (FileStream aFile = new FileStream(Path.Combine(logs_path, String.Format("{0}_{1:dd-MM-yyyy}.{2}", "QRCode", DateTime.Now, ".csv")), FileMode.Append, FileAccess.Write))
                                {
                                    using (StreamWriter sw = new StreamWriter(aFile))
                                    {
                                        sw.WriteLine(line + ";" + Path.GetFileName(file));
                                    }
                                }
                            }
                            File.Delete(file);
                        }
                        catch (Exception ex)
                        {
                            clsErrorLogs.WriteLogs("error", ex.ToString());
                        }
                }
            }
        }
        private void _timerArchive_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (_timerArchive.Enabled)
            {
                _timerArchive.Enabled = false;
                ArchiveData();
                _timerArchive.Enabled = true;
            }
        }

        protected override void OnStop()
        {
        }
        void ArchiveData()
        {
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_DBConnection))
                {
                    mycon.Open();
                    using (MySqlCommand mycmd = new MySqlCommand("SP_ArchiveData", mycon))
                    {
                        mycmd.CommandType = CommandType.StoredProcedure;
                        mycmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}