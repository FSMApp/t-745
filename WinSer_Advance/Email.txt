Hi Shyam,

Below are the notifications that should be sent by falcon in success , unsuccessful and semi-successful scenarios.

Notifications :

Success notification : this is the case when falcon received file successfully
Subject : [Success] file upload successfully

Mail body : file <xxxxxxxxxxxx> on dated <xxxxxxxx> has been uploaded successfully. <xxxx> Number of invoices uploaded today.

Recipients : DL<xxxx>

Frequency : daily

Error notification:  File missing : this is the case when falcon doesn�t received any file on particular date
subject : [Error] File Missing

Mail body : No file received on date<xxxxxxx>.

Recipients : DL<xxxx>

Note : this should be sent to us with high importance.

Frequency : when required

Error notification:  Invalid file  : this is the case when file is invalid on particular date
subject : [Error] Invalid File 

Mail body : Invalid file received on date<xxxxxxxx>

Recipients : DL<xxxx>

Note : this should be sent to us with high importance.

Frequency : when required

Error notification:  Data Error: this is the case when file is correct but some data is invalid
subject : [Error] Invalid Data 

Mail body : falcon received invalid data on <this and that line number>. Invoice processed from this files <file name> are <number of invoices>.

Recipients : DL<xxxx>

Note : this should be sent to us with high importance.

              Frequency : when required



Email User Name � initdelhi@oriflame.com

Password � Ori54321 (O in caps)

Sender Email � it.india@oriflame.com

Email Server � relay.prg.ori.local

Server Port � 25