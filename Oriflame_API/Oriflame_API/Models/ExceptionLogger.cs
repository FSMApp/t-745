﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace Oriflame_API.Models
{
    public static class ExceptionLogger
    {
        public static void LogJSON(string Json, string asn)
        {
            try
            {

                string _sourcePath = ConfigurationManager.AppSettings["inputdatalog"].ToString();
                if (File.Exists(Path.Combine(_sourcePath, asn + ".txt")))
                    File.Delete(Path.Combine(_sourcePath, asn + ".txt"));
                File.WriteAllText(Path.Combine(_sourcePath, asn + ".txt"), Json);
                //string query = @"INSERT INTO  tbl_TOJSON(Content,ASN_Number) values(?json,?asn)";

                //MySqlParameter[] param = new MySqlParameter[] {
                //    new MySqlParameter("json",Json),
                //    new MySqlParameter("asn",Json)
                //};
                //General.DMLWithParameter(query, param);
            }
            catch
            {

            }
        }
        //Log Exception
        public static void LogAllError(Exception x)
        {


            try
            {
                string errorCode = x.GetType().Name;

                var st = new StackTrace(x, true);
                var frames = st.GetFrames();
                var traceString = "";
                //foreach (var frame in frames)
                //{
                //    if (frame.GetFileLineNumber() < 1)
                //        continue;

                //    string[] filePathArray = frame.GetFileName().Split('\\');

                //    string fileName = filePathArray.Last();
                //    string methode = frame.GetMethod().Name;
                //    int lineNumber = frame.GetFileLineNumber();

                //    traceString = "File Name =>" + fileName + System.Environment.NewLine + ", Methode => " + methode + System.Environment.NewLine + ", Line =>" + lineNumber;


                string query = @"insert into error_log(source,message) values(?error_code,?message)";

                MySqlParameter[] param = new MySqlParameter[] {
                new MySqlParameter("error_code","API"),
                new MySqlParameter("message",x.ToString())
            };

                General.DMLWithParameter(query, param);
                //}
            }
            catch (Exception e)
            {

                //string query = @"insert into error_log(error_code,error_desc,error_timestamp) values('ExceptionLogger','ExceptionLogger','" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "')";
                //General.DML(query, true);
            }

        }
    }
}