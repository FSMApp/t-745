﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oriflame_API.Models
{
    public class LineItem
    {
        public string line_number { get; set; }
        public string product_code { get; set; }
        public string order_quantity { get; set; }
        public string location { get; set; }
        public string station { get; set; }
    }
}