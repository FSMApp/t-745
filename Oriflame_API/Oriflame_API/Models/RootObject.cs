﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Oriflame_API.Models
{
    public class RootObject
    {
        public string invoice_number { get; set; }
        public string order_number { get; set; }
        public string warehouse_id { get; set; }
        public List<LineItem> line_items { get; set; }
        public string order_date { get; set; }
        public string invoice_date { get; set; }
        public string sales_date { get; set; }
        public string due_date { get; set; }
        public string delivery_date { get; set; }
        public string order_status { get; set; }
        public double order_weight { get; set; }
    }
}