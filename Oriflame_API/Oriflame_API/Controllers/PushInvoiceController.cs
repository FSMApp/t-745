﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Oriflame_API.Models;
using Newtonsoft.Json;
using System.Text;
using System.Web.Script.Serialization;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Oriflame_API.Controllers
{
    public class PushInvoiceController : ApiController
    {
        delegate void MAnupulatedata_del(dynamic invoice, Int64 _orderid, Int64 fileid);

        private static string CreateRandomFileName(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789-_";
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }
        void ManupulateData(dynamic invoice, Int64 _orderid, Int64 fileid)
        {
            string _sale_date = Convert.ToDateTime(invoice.sales_date).ToString("yyyy-MM-dd HH:mm:ss");
            // _sale_date =_sale_date.Replace("T", " ");
            string strquery = string.Format("Update tbl_order_header  set File_ID={6}, Order_Number='{0}',Update_DateTime=NOW(), Order_Weight={1}, Warehouse_id={3}, Line_Items={4}, Sales_date='{5}' where Invoice_Number='{2}'",
                invoice.order_number, invoice.order_weight, invoice.invoice_number, invoice.warehouse_id, invoice.line_items.Count, _sale_date, fileid);
            General.DML(strquery);
            strquery = string.Empty;
            foreach (var lines in invoice.line_items)
            {
                try
                {
                    try
                    {
                        strquery = string.Format("Insert into tbl_order_line_header(Order_id,Line_Item_Seq_no,Sku,Pick_Qty,Pick_Location,Location) Values({0},{1},'{2}',{3},'{4}','{5}')",
                        _orderid, lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location);
                        General.DML(strquery);
                    }
                    catch
                    {
                        strquery = string.Format("Update tbl_order_line_header set Line_Item_Seq_no={0},Pick_Qty={2},Pick_Location='{3}',Location='{4}' where Order_id={5} and Sku={1};",
                            lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location, _orderid);
                        General.DML(strquery);
                    }
                }
                catch (Exception inner)
                {
                    ExceptionLogger.LogAllError(inner);
                }
            }
        }
        [System.Web.Http.HttpPost]
        // GET: API
        public HttpResponseMessage Post([FromBody] dynamic data)
        {
            List<ResponseData> Responsedata = null;
            //List<RootObject> pushorderdata = null;
            // var _data = JsonConvert.DeserializeObject(data);
            // dynamic json = JsonConvert.DeserializeObject(data.ToString());
            Int64 fileid = DateTime.Now.Ticks;
            string _file_name = fileid.ToString() + CreateRandomFileName(5);
            //var info = json["moreinfo:info913802:example"].Value;
            // RootObject[] a = JsonConvert.DeserializeObject<RootObject[]>(data);
            try
            {
                //JavaScriptSerializer objdata = new JavaScriptSerializer();
                //pushorderdata = JsonConvert.DeserializeObject(data);
                //pushorderdata= objdata.Deserialize<List<RootObject>>(data);
                DateTime API_Process = DateTime.Now;
                _file_name = data.batch_number;
                //Int64 Orderid = 0;
                string _fileQuery = "insert into  tbl_file_Master(File_Name)values('" + _file_name + "');";
                General.DML(_fileQuery, true);
                ExceptionLogger.LogJSON(data.ToString(), _file_name);
                if (data != null)
                {
                    Responsedata = new List<ResponseData>();
                    //foreach (var d in data)
                    Parallel.ForEach((IEnumerable<dynamic>)data.data, new ParallelOptions { MaxDegreeOfParallelism = 5 }, invoice =>
                    {
                        // foreach (var invoice in data.data)
                        //{
                        try
                        {
                            string _status = General.SelectScaler("Call SP_GetInvoiceStatus('" + invoice.invoice_number + "');");
                            if (_status == string.Empty)
                            {
                                //string _sale_date = Convert.ToDateTime(invoice.sales_date).ToString("yyyy-MM-dd HH:mm:ss");
                                //string strquery = string.Format("Insert into tbl_order_header (Order_Number, Order_Weight, Invoice_Number, Warehouse_id, Line_Items, Sales_date,File_ID) Values('{0}','{1}',{2},'{3}','{4}','{5}','{6}')",
                                //    invoice.order_number, invoice.order_weight, invoice.invoice_number, invoice.warehouse_id, invoice.line_items.Count, _sale_date, fileid);
                                //Orderid = General.DML(strquery, true);
                                //strquery = string.Empty;
                                //if (Orderid > 0)
                                //{
                                //    foreach (var lines in invoice.line_items)
                                //    {
                                //        strquery = string.Format("Insert ignore into tbl_order_line_header(Order_id,Line_Item_Seq_no,Sku,Pick_Qty,Pick_Location,Location) Values({0},{1},'{2}',{3},'{4}','{5}')",
                                //            Orderid, lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location);
                                //        General.DML(strquery);
                                //    }
                                //}
                                Responsedata.Add(new ResponseData()
                                {
                                    invoice_number = invoice.invoice_number,
                                    status = true
                                });
                            }
                            else if (_status.Split('|')[0].ToUpper() == "PENDING")
                            {
                                //Int64 _orderid = Convert.ToInt64(_status.Split('|')[1]);
                                //new MAnupulatedata_del(ManupulateData).Invoke(invoice, _orderid, fileid);
                                Responsedata.Add(new ResponseData()
                                {
                                    invoice_number = invoice.invoice_number,
                                    status = true
                                });
                            }
                            else
                            {
                                Responsedata.Add(new ResponseData()
                                {
                                    invoice_number = invoice.invoice_number,
                                    status = false
                                });
                            }

                        }
                        catch (Exception innerex)
                        {
                            ExceptionLogger.LogAllError(innerex);
                            Responsedata.Add(new ResponseData()
                            {
                                invoice_number = invoice.invoice_number,
                                status = false
                            });
                        }
                    });
                }

                /*
                foreach (var invoice in data)
                {
                    Int64 Orderid = 0;
                    try
                    {
                        string _status = General.SelectScaler("Call SP_GetInvoiceStatus('" + invoice.invoice_number + "');");
                        if (_status == string.Empty)
                        {
                            string _sale_date = Convert.ToDateTime(invoice.sales_date).ToString("yyyy-MM-dd HH:mm:ss");
                            string strquery = string.Format("Insert into tbl_order_header (Order_Number, Order_Weight, Invoice_Number, Warehouse_id, Line_Items, Sales_date,File_ID) Values('{0}','{1}',{2},'{3}','{4}','{5}','{6}')",
                                invoice.order_number, invoice.order_weight, invoice.invoice_number, invoice.warehouse_id, invoice.line_items.Count, _sale_date, fileid);
                            Orderid = General.DML(strquery, true);
                            strquery = string.Empty;
                            if (Orderid > 0)
                            {
                                foreach (var lines in invoice.line_items)
                                {
                                    strquery = string.Format("Insert ignore into tbl_order_line_header(Order_id,Line_Item_Seq_no,Sku,Pick_Qty,Pick_Location,Location) Values({0},{1},'{2}',{3},'{4}','{5}')",
                                        Orderid, lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location);
                                    General.DML(strquery);
                                }
                            }
                        }
                        else if (_status.Split('|')[0].ToUpper() == "PENDING")
                        {
                            Int64 _orderid = Convert.ToInt64(_status.Split('|')[1]);
                            string _sale_date = Convert.ToDateTime(invoice.sales_date).ToString("yyyy-MM-dd HH:mm:ss");
                           // _sale_date =_sale_date.Replace("T", " ");
                            string strquery = string.Format("Update tbl_order_header  set File_ID={6}, Order_Number='{0}',Update_DateTime=NOW(), Order_Weight={1}, Warehouse_id={3}, Line_Items={4}, Sales_date='{5}' where Invoice_Number='{2}'",
                                invoice.order_number, invoice.order_weight, invoice.invoice_number, invoice.warehouse_id, invoice.line_items.Count, _sale_date, fileid);
                            General.DML(strquery);
                            strquery = string.Empty;
                            foreach (var lines in invoice.line_items)
                            {
                                try
                                {
                                    try
                                    {
                                        strquery = string.Format("Insert into tbl_order_line_header(Order_id,Line_Item_Seq_no,Sku,Pick_Qty,Pick_Location,Location) Values({0},{1},'{2}',{3},'{4}','{5}')",
                                        Orderid, lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location);
                                        General.DML(strquery);
                                    }
                                    catch
                                    {
                                        strquery = string.Format("Update tbl_order_line_header set Line_Item_Seq_no={0},Pick_Qty={2},Pick_Location='{3}',Location='{4}' where Order_id={5} and Sku={1};",
                                            lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location, _orderid);
                                        General.DML(strquery);
                                    }
                                }
                                catch (Exception inner)
                                {
                                    ExceptionLogger.LogAllError(inner);
                                }
                            }
                        }
                        Responsedata.Add(new ResponseData()
                        {
                            invoice_number = invoice.invoice_number,
                            status =(Orderid ==0?false : true)
                        });
                    }
                    catch (Exception innerex)
                    {
                        ExceptionLogger.LogAllError(innerex);
                        Responsedata.Add(new ResponseData()
                        {
                            invoice_number = invoice.invoice_number,
                            status = false
                        });
                    }
                }*/

                string _upfileQuery = "Update tbl_file_Master set API_StartProcess='" + API_Process.ToString("yyyy-MM-dd HH:mm:ss") + "', API_ResponseTime=Now() where File_Name='" + _file_name + "';";
                General.DML(_upfileQuery, true);
                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(Responsedata), Encoding.UTF8, "application/json");
                return response;
            }
            catch (Exception e)
            {
                Responsedata.Add(new ResponseData()
                {
                    invoice_number = string.Empty,
                    status = false
                });
                ExceptionLogger.LogAllError(e);
                var response = Request.CreateResponse(HttpStatusCode.BadRequest);
                response.Content = new StringContent(JsonConvert.SerializeObject(Responsedata), Encoding.UTF8, "application/json");
                return response;
            }
            //return View();
        }

    }
}
