﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Oriflame_API.Models;
namespace Oriflame_API.Controllers
{
    public class PushInvoiceData : ApiController
    {
        [System.Web.Http.HttpPost]
        // GET: API
        public HttpResponseMessage Post(dynamic data)
        {
            List<ResponseData> Responsedata = null;
            List<RootObject> pushorderdata = null;
            try
            {
                pushorderdata = JsonConvert.DeserializeObject(data);
                if (pushorderdata != null)
                {
                    Responsedata = new List<ResponseData>();
                    foreach (RootObject invoice in pushorderdata)
                    {
                        try
                        {
                            string _status = General.SelectScaler("Call SP_GetInvoiceStatus('" + invoice.invoice_number + "');");
                            if (_status == string.Empty)
                            {
                                string strquery = string.Format("Insert into tbl_order_header (Order_Number, Order_Datetime, Order_Weight, Order_Status, Invoice_Number, Invoice_Date, Warehouse_id, Line_Items, Sales_date, due_Date, Delivery_date) Values('{0}','{1}',{2},'{3}','{4}','{5}',{6},{7},'{8}','{9}','{10}')",
                                    invoice.order_number, invoice.order_date, invoice.order_weight, invoice.order_status, invoice.invoice_number, invoice.invoice_date, invoice.warehouse_id, invoice.line_items.Count(), invoice.sales_date, invoice.due_date, invoice.delivery_date);
                                Int64 Orderid = General.DML(strquery, true);
                                strquery = string.Empty;
                                foreach (LineItem lines in invoice.line_items)
                                {
                                    strquery = string.Format("Insert ignore into tbl_order_line_header(Order_id,Line_Item_Seq_no,Sku,Pick_Qty,Pick_Location,Location) Values({0},{1},'{2}',{3},'{4}','{5}')",
                                        Orderid, lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location);
                                    General.DML(strquery);
                                }
                            }
                            else if (_status.Split('|')[0].ToUpper() == "PENDING")
                            {
                                Int64 _orderid = Convert.ToInt64(_status.Split('|')[1]);
                                string strquery = string.Format("Update tbl_order_header set Order_Number='{0}', Order_Datetime='{1}', Order_Weight={2}, Order_Status='{3}', Invoice_Date='{5}', Warehouse_id={6}, Line_Items={7}, Sales_date='{8}', due_Date='{9}', Delivery_date='{10}' where Invoice_Number='{4}'",
                                    invoice.order_number, invoice.order_date, invoice.order_weight, invoice.order_status, invoice.invoice_number, invoice.invoice_date, invoice.warehouse_id, invoice.line_items.Count(), invoice.sales_date, invoice.due_date, invoice.delivery_date);
                                General.DML(strquery);
                                strquery = string.Empty;
                                foreach (LineItem lines in invoice.line_items)
                                {
                                    strquery = string.Format("Update tbl_order_line_header set Line_Item_Seq_no={0},Sku='{1}',Pick_Qty={2},Pick_Location='{3}',Location='{4}' where Order_id={5}",
                                        lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location, _orderid);
                                    General.DML(strquery);
                                }
                            }
                            Responsedata.Add(new ResponseData()
                            {
                                invoice_number = invoice.invoice_number,
                                status = true
                            });
                        }
                        catch(Exception ex)
                        {
                            Responsedata.Add(new ResponseData()
                            {
                                invoice_number = invoice.invoice_number,
                                status = false
                            });
                        }
                    }
                }
                var response = Request.CreateResponse(HttpStatusCode.OK);
                response.Content = new StringContent(JsonConvert.SerializeObject(Responsedata), Encoding.UTF8, "application/json");
                return response;
            }
            catch (Exception e)
            {
                Responsedata.Add(new ResponseData()
                {
                    invoice_number = string.Empty,
                    status = false
                });
                ExceptionLogger.LogAllError(e);
                var response = Request.CreateResponse(HttpStatusCode.BadRequest);
                response.Content = new StringContent(JsonConvert.SerializeObject(Responsedata), Encoding.UTF8, "application/json");
                return response;
            }
            //return View();
        }
    }
}