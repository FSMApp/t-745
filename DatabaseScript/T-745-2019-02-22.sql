/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 5.6.20-log : Database - dwh_falcon
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dwh_falcon` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dwh_falcon`;

/*Table structure for table `_tmp_queue` */

DROP TABLE IF EXISTS `_tmp_queue`;

CREATE TABLE `_tmp_queue` (
  `QueueID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Pick_Location` varchar(30) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Sku_Type` varchar(20) DEFAULT NULL,
  `Scanner_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_IPAddress` varchar(30) DEFAULT NULL,
  `LCD_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_SendPort` int(11) DEFAULT NULL,
  `LCD_SendPort` int(11) DEFAULT NULL,
  `PickQty` int(3) DEFAULT NULL,
  `Picked_Qty` int(3) DEFAULT NULL,
  `Status` enum('INPROCESS','COMPLETED','PENDING','NOTPICK') DEFAULT 'PENDING',
  `PickStart` datetime DEFAULT NULL,
  `PickConfirmation` datetime DEFAULT NULL,
  `PickDuration` int(11) DEFAULT NULL,
  `DisplayMSG` tinyint(1) DEFAULT '0',
  `Operator_id` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`QueueID`),
  UNIQUE KEY `Unique_Pick` (`Invoice_Number`,`Sku`),
  KEY `PickDatetime` (`PickStart`),
  KEY `pickend` (`PickConfirmation`),
  KEY `Scanner` (`Scanner_IPAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=44671 DEFAULT CHARSET=latin1;

/*Table structure for table `error_log` */

DROP TABLE IF EXISTS `error_log`;

CREATE TABLE `error_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `source` varchar(20) DEFAULT NULL,
  `message` text,
  `Error_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_carate_invoice_mapping` */

DROP TABLE IF EXISTS `tbl_carate_invoice_mapping`;

CREATE TABLE `tbl_carate_invoice_mapping` (
  `Pick_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Barcode` varchar(30) DEFAULT NULL,
  `Source_IPAddress` varchar(30) DEFAULT NULL,
  `Scanner_IP` varchar(30) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Mapping_Date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Mapped_By` varchar(30) DEFAULT NULL,
  `Status` enum('PENDING','INPROCESS','COMPLETED','ERROR','INDUCTED') DEFAULT 'PENDING',
  `Inducted` datetime DEFAULT NULL,
  `FirstPick` datetime DEFAULT NULL,
  `Weiging` datetime DEFAULT NULL,
  `Weighing_IP` varchar(30) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `Actual_Weight` decimal(10,3) DEFAULT NULL,
  `Capture_Weight` decimal(10,3) DEFAULT NULL,
  PRIMARY KEY (`Pick_id`),
  UNIQUE KEY `Mapping` (`Carate_Barcode`,`Invoice_Number`),
  KEY `Carate_Code` (`Carate_Barcode`),
  KEY `Invoice_Number` (`Invoice_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=94298 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_carate_invoice_mapping_archive` */

DROP TABLE IF EXISTS `tbl_carate_invoice_mapping_archive`;

CREATE TABLE `tbl_carate_invoice_mapping_archive` (
  `Pick_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Barcode` varchar(30) DEFAULT NULL,
  `Source_IPAddress` varchar(30) DEFAULT NULL,
  `Scanner_IP` varchar(30) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Mapping_Date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Mapped_By` varchar(30) DEFAULT NULL,
  `Status` enum('INDUCTED','ERROR','COMPLETED','INPROCESS','PENDING') DEFAULT NULL,
  `FirstPick` datetime DEFAULT NULL,
  `Weiging` datetime DEFAULT NULL,
  `Weighing_IP` varchar(30) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `Actual_Weight` decimal(10,3) DEFAULT NULL,
  `Capture_Weight` decimal(10,3) DEFAULT NULL,
  `Inducted` datetime DEFAULT NULL,
  PRIMARY KEY (`Pick_id`),
  KEY `Carate_Code` (`Carate_Barcode`),
  KEY `Invoice_Number` (`Invoice_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=72244 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_carate_master` */

DROP TABLE IF EXISTS `tbl_carate_master`;

CREATE TABLE `tbl_carate_master` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Crate_Code` varchar(20) DEFAULT NULL,
  `Crate_Weight` decimal(10,4) DEFAULT NULL,
  `AddedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isActive` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Crate_Code` (`Crate_Code`)
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_carate_status` */

DROP TABLE IF EXISTS `tbl_carate_status`;

CREATE TABLE `tbl_carate_status` (
  `Pick_Status` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `Actual_Weight` decimal(10,3) DEFAULT NULL,
  `Capture_Weight` decimal(10,3) DEFAULT NULL,
  `Status` enum('INDUCTED','INPROCESS','COMPLETED','ERROR') DEFAULT 'INDUCTED',
  `Inducted` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `FirstPick` datetime DEFAULT NULL,
  `Weiging` datetime DEFAULT NULL,
  `Weighing_IP` varchar(30) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`Pick_Status`),
  UNIQUE KEY `Invoice` (`Invoice_No`),
  UNIQUE KEY `Crate_Code` (`Carate_Code`)
) ENGINE=InnoDB AUTO_INCREMENT=383 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_carate_status_archive` */

DROP TABLE IF EXISTS `tbl_carate_status_archive`;

CREATE TABLE `tbl_carate_status_archive` (
  `Pick_Status` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `Actual_Weight` decimal(10,3) DEFAULT NULL,
  `Capture_Weight` decimal(10,3) DEFAULT NULL,
  `Status` enum('INDUCTED','INPROCESS','COMPLETED','ERROR') DEFAULT 'INDUCTED',
  `Inducted` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `FirstPick` datetime DEFAULT NULL,
  `Weiging` datetime DEFAULT NULL,
  `Weighing_IP` varchar(30) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`Pick_Status`),
  UNIQUE KEY `Unique_Pick` (`Carate_Code`,`Invoice_No`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_config_master` */

DROP TABLE IF EXISTS `tbl_config_master`;

CREATE TABLE `tbl_config_master` (
  `Configid` int(10) NOT NULL AUTO_INCREMENT,
  `File_Name` varchar(100) DEFAULT NULL,
  `UploadDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `UploadBy` varchar(30) DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`Configid`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_email_master` */

DROP TABLE IF EXISTS `tbl_email_master`;

CREATE TABLE `tbl_email_master` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Email_id` varchar(100) DEFAULT NULL,
  `Frequency` enum('Daily','Weekly','Monthly','Yearly') DEFAULT 'Daily',
  `LastEmailSent` datetime DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_file_master` */

DROP TABLE IF EXISTS `tbl_file_master`;

CREATE TABLE `tbl_file_master` (
  `File_ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `File_Name` varchar(100) DEFAULT NULL,
  `File_Status` tinyint(4) DEFAULT '0',
  `API_StartProcess` datetime DEFAULT NULL,
  `API_ResponseTime` datetime DEFAULT NULL,
  `File_Received` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `File_ProcessStarted` datetime DEFAULT NULL,
  `File_Processed` datetime DEFAULT NULL,
  PRIMARY KEY (`File_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19545 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_hour_weekday_monthlist` */

DROP TABLE IF EXISTS `tbl_hour_weekday_monthlist`;

CREATE TABLE `tbl_hour_weekday_monthlist` (
  `ID` int(5) DEFAULT NULL,
  `Value` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `Type` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `Quarter` varchar(2) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_operator_login` */

DROP TABLE IF EXISTS `tbl_operator_login`;

CREATE TABLE `tbl_operator_login` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Login_Id` varchar(30) DEFAULT NULL,
  `Login_DateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Logout_Datetime` datetime DEFAULT NULL,
  `Duration` bigint(20) DEFAULT NULL,
  `Login_By` varchar(30) DEFAULT NULL COMMENT 'Scanner IP Address',
  PRIMARY KEY (`ID`),
  KEY `Login_By` (`Login_By`)
) ENGINE=InnoDB AUTO_INCREMENT=1338 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_operator_login_archive` */

DROP TABLE IF EXISTS `tbl_operator_login_archive`;

CREATE TABLE `tbl_operator_login_archive` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Login_Id` varchar(30) DEFAULT NULL,
  `Login_DateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Logout_Datetime` datetime DEFAULT NULL,
  `Duration` bigint(20) DEFAULT NULL,
  `Login_By` varchar(30) DEFAULT NULL COMMENT 'Scanner IP Address',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Login_Info` (`Login_Id`,`Login_DateTime`)
) ENGINE=InnoDB AUTO_INCREMENT=1070 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_order_error_details` */

DROP TABLE IF EXISTS `tbl_order_error_details`;

CREATE TABLE `tbl_order_error_details` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Error_Message` varchar(300) DEFAULT NULL,
  `Error_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=402 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_order_header` */

DROP TABLE IF EXISTS `tbl_order_header`;

CREATE TABLE `tbl_order_header` (
  `Order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Order_Number` varchar(30) DEFAULT NULL,
  `Order_Datetime` datetime DEFAULT CURRENT_TIMESTAMP,
  `Order_Weight` decimal(10,3) DEFAULT NULL,
  `Order_Status` varchar(20) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Invoice_Date` datetime DEFAULT CURRENT_TIMESTAMP,
  `Warehouse_id` varchar(5) DEFAULT NULL,
  `Line_Items` int(11) DEFAULT NULL,
  `Sales_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `due_Date` datetime DEFAULT CURRENT_TIMESTAMP,
  `Delivery_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `Added_Datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `File_ID` varchar(100) DEFAULT NULL,
  `Update_DateTime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Order_id`),
  UNIQUE KEY `Invoice` (`Invoice_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=103425 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_order_header_archive` */

DROP TABLE IF EXISTS `tbl_order_header_archive`;

CREATE TABLE `tbl_order_header_archive` (
  `Order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Order_Number` varchar(30) DEFAULT NULL,
  `Order_Datetime` datetime DEFAULT NULL,
  `Order_Weight` decimal(10,3) DEFAULT NULL,
  `Order_Status` varchar(20) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Invoice_Date` datetime DEFAULT NULL,
  `Warehouse_id` varchar(5) DEFAULT NULL,
  `Line_Items` int(11) DEFAULT NULL,
  `Sales_date` datetime DEFAULT NULL,
  `due_Date` datetime DEFAULT NULL,
  `Delivery_date` datetime DEFAULT NULL,
  `Added_Datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Order_id`),
  KEY `Invoice` (`Invoice_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=72244 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_order_line_header` */

DROP TABLE IF EXISTS `tbl_order_line_header`;

CREATE TABLE `tbl_order_line_header` (
  `Order_line_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Order_id` bigint(20) DEFAULT NULL,
  `Line_Item_Seq_no` int(11) DEFAULT NULL COMMENT 'line_number',
  `Sku` varchar(30) DEFAULT NULL COMMENT 'product_code',
  `Pick_Qty` int(11) DEFAULT NULL COMMENT 'order_quantity',
  `Pick_Location` varchar(20) DEFAULT NULL COMMENT 'Station',
  `Location` varchar(20) DEFAULT NULL COMMENT 'Location',
  `Added_DateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Order_line_Id`),
  UNIQUE KEY `Unique_Pick` (`Order_id`,`Sku`)
) ENGINE=InnoDB AUTO_INCREMENT=1147872 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_order_line_header_archive` */

DROP TABLE IF EXISTS `tbl_order_line_header_archive`;

CREATE TABLE `tbl_order_line_header_archive` (
  `Order_line_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Order_id` bigint(20) DEFAULT NULL,
  `Line_Item_Seq_no` int(11) DEFAULT NULL COMMENT 'line_number',
  `Sku` varchar(30) DEFAULT NULL COMMENT 'product_code',
  `Pick_Qty` int(11) DEFAULT NULL COMMENT 'order_quantity',
  `Pick_Location` varchar(20) DEFAULT NULL COMMENT 'Station',
  `Location` varchar(20) DEFAULT NULL COMMENT 'Location',
  `Added_DateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Order_line_Id`),
  UNIQUE KEY `Unique_Pick` (`Order_id`,`Sku`)
) ENGINE=InnoDB AUTO_INCREMENT=817383 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_ptl_sku_mapping` */

DROP TABLE IF EXISTS `tbl_ptl_sku_mapping`;

CREATE TABLE `tbl_ptl_sku_mapping` (
  `Mapping_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Pick_Location` varchar(30) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Sku_Type` enum('NORMAL','SCANBASED') DEFAULT 'NORMAL',
  `Config_id` int(10) DEFAULT '1',
  PRIMARY KEY (`Mapping_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_request_queue_for_ptl` */

DROP TABLE IF EXISTS `tbl_request_queue_for_ptl`;

CREATE TABLE `tbl_request_queue_for_ptl` (
  `QueueID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Pick_Location` varchar(30) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Sku_Type` varchar(20) DEFAULT NULL,
  `Scanner_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_IPAddress` varchar(30) DEFAULT NULL,
  `LCD_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_SendPort` int(11) DEFAULT NULL,
  `LCD_SendPort` int(11) DEFAULT NULL,
  `PickQty` int(3) DEFAULT NULL,
  `Picked_Qty` int(3) DEFAULT NULL,
  `Status` enum('INPROCESS','COMPLETED','PENDING','NOTPICK') DEFAULT 'PENDING',
  `PickStart` datetime DEFAULT NULL,
  `PickConfirmation` datetime DEFAULT NULL,
  `PickDuration` int(11) DEFAULT NULL,
  `DisplayMSG` tinyint(1) DEFAULT '0',
  `Operator_id` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`QueueID`),
  UNIQUE KEY `Unique_Pick` (`Invoice_Number`,`Sku`),
  KEY `Scanner` (`Scanner_IPAddress`),
  KEY `Sku_Type` (`Sku_Type`),
  KEY `PickDatetime` (`PickStart`),
  KEY `PickEnd` (`PickConfirmation`),
  KEY `pick_location` (`Pick_Location`),
  KEY `Crate_Code` (`Carate_Code`),
  KEY `Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=933680 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_request_queue_for_ptl_archive` */

DROP TABLE IF EXISTS `tbl_request_queue_for_ptl_archive`;

CREATE TABLE `tbl_request_queue_for_ptl_archive` (
  `QueueID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Pick_Location` varchar(30) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Sku_Type` varchar(20) DEFAULT NULL,
  `Scanner_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_IPAddress` varchar(30) DEFAULT NULL,
  `LCD_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_SendPort` int(11) DEFAULT NULL,
  `LCD_SendPort` int(11) DEFAULT NULL,
  `PickQty` int(3) DEFAULT NULL,
  `Picked_Qty` int(3) DEFAULT NULL,
  `Status` enum('INPROCESS','COMPLETED','PENDING','NOTPICK') DEFAULT 'PENDING',
  `PickStart` datetime DEFAULT NULL,
  `PickConfirmation` datetime DEFAULT NULL,
  `PickDuration` int(11) DEFAULT NULL,
  `DisplayMSG` tinyint(1) DEFAULT '0',
  `Operator_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`QueueID`),
  UNIQUE KEY `Unique_Pick` (`Invoice_Number`,`Sku`),
  KEY `Crate_Code` (`Carate_Code`),
  KEY `Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=684064 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_scan_pick_details` */

DROP TABLE IF EXISTS `tbl_scan_pick_details`;

CREATE TABLE `tbl_scan_pick_details` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ScanedCode` varchar(100) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Ser_no` varchar(30) DEFAULT NULL,
  `ScanDatetime` datetime NOT NULL,
  `Scan_by` varchar(30) DEFAULT 'RDC',
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `QueueID` bigint(20) DEFAULT '0',
  `Login_User` varchar(20) DEFAULT NULL,
  `WareHouse` varchar(10) DEFAULT '40',
  `File_Name` varchar(100) DEFAULT 'PTL',
  `WareHouseName` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `QRCode` (`ScanedCode`),
  KEY `Invoice_no` (`Invoice_No`),
  KEY `ScanDate` (`ScanDatetime`),
  KEY `Scanner` (`Scan_by`)
) ENGINE=InnoDB AUTO_INCREMENT=281760 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_scan_pick_details_2` */

DROP TABLE IF EXISTS `tbl_scan_pick_details_2`;

CREATE TABLE `tbl_scan_pick_details_2` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ScanedCode` varchar(100) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Ser_no` varchar(30) DEFAULT NULL,
  `ScanDatetime` datetime NOT NULL,
  `Scan_by` varchar(30) DEFAULT 'RDC',
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `QueueID` bigint(20) DEFAULT '0',
  `Login_User` varchar(20) DEFAULT NULL,
  `WareHouse` varchar(10) DEFAULT '40',
  `File_Name` varchar(100) DEFAULT 'PTL',
  PRIMARY KEY (`ID`),
  KEY `Invoice_no` (`Invoice_No`),
  KEY `QRCode` (`ScanedCode`),
  KEY `ScanDate` (`ScanDatetime`)
) ENGINE=InnoDB AUTO_INCREMENT=3333276 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_scan_pick_details_archive` */

DROP TABLE IF EXISTS `tbl_scan_pick_details_archive`;

CREATE TABLE `tbl_scan_pick_details_archive` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ScanedCode` varchar(100) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Ser_no` varchar(30) DEFAULT NULL,
  `ScanDatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Scan_by` varchar(30) DEFAULT NULL,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `QueueID` bigint(20) DEFAULT NULL,
  `Login_User` varchar(20) DEFAULT NULL,
  `WareHouse` varchar(10) DEFAULT '40',
  `File_Name` varchar(100) DEFAULT 'PTL',
  `WareHouseName` varchar(30) DEFAULT 'RDC North',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Scan_Pick` (`Sku`,`Ser_no`),
  KEY `QrCode` (`ScanedCode`),
  KEY `ScanDate` (`ScanDatetime`),
  KEY `Scanner` (`Scan_by`)
) ENGINE=InnoDB AUTO_INCREMENT=48628 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `User_ID` varchar(30) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `user_category` enum('OPERATOR','ADMIN','MANAGER','DEVELOPER') DEFAULT 'OPERATOR',
  `UserCode` varchar(20) DEFAULT NULL,
  `CreatedBy` varchar(20) DEFAULT NULL,
  `CreatedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UserCode` (`UserCode`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_weight_tolerance_config` */

DROP TABLE IF EXISTS `tbl_weight_tolerance_config`;

CREATE TABLE `tbl_weight_tolerance_config` (
  `Config_id` int(10) NOT NULL AUTO_INCREMENT,
  `From_Weight` decimal(10,3) DEFAULT NULL,
  `To_Weight` decimal(10,3) DEFAULT NULL,
  `Tolerance_per` decimal(10,1) DEFAULT NULL,
  `Static_Tolerance` decimal(10,3) DEFAULT NULL,
  `Activation_Date` datetime DEFAULT CURRENT_TIMESTAMP,
  `DeActivation_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Table structure for table `tbl_zone_master` */

DROP TABLE IF EXISTS `tbl_zone_master`;

CREATE TABLE `tbl_zone_master` (
  `Id` int(5) NOT NULL AUTO_INCREMENT,
  `ZONE_ID` int(11) DEFAULT NULL,
  `Display_Name` varchar(10) DEFAULT NULL,
  `Zone_Name` varchar(10) DEFAULT NULL,
  `Zone_IPAddress` varchar(30) DEFAULT NULL,
  `LCD_IPAddress` varchar(30) DEFAULT NULL,
  `Scanner_IPAddress` varchar(30) DEFAULT NULL,
  `No_of_PTL` int(3) DEFAULT NULL,
  `Added_Datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `PTL_SendPort` bigint(5) DEFAULT NULL,
  `PTL_ReceivePort` bigint(5) DEFAULT NULL,
  `LCD_SendPort` bigint(5) DEFAULT NULL,
  `Scanner_ReceivePort` bigint(5) DEFAULT NULL,
  `Status` enum('ACTIVE','INACTIVE','LOGIN') DEFAULT 'INACTIVE',
  `Zone_Type` enum('NORMAL','SCANNABLE') DEFAULT NULL,
  `PTLFrom` varchar(4) DEFAULT NULL,
  `PTLTo` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Zone` (`Zone_IPAddress`),
  KEY `LCD` (`LCD_IPAddress`),
  KEY `Scanner` (`Scanner_IPAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Table structure for table `tbl_zone_ptl_mapping` */

DROP TABLE IF EXISTS `tbl_zone_ptl_mapping`;

CREATE TABLE `tbl_zone_ptl_mapping` (
  `Mapping_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Zone_ID` int(11) DEFAULT NULL,
  `PTL` varchar(4) DEFAULT NULL,
  `Pick_Location` varchar(30) DEFAULT NULL,
  `PTL_Type` varchar(10) DEFAULT 'NORMAL',
  `isActive` tinyint(4) DEFAULT '1',
  `MSG` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`Mapping_ID`),
  UNIQUE KEY `Zone_PTL` (`Zone_ID`,`PTL`)
) ENGINE=InnoDB AUTO_INCREMENT=811 DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_qrcode` */

DROP TABLE IF EXISTS `tmp_qrcode`;

CREATE TABLE `tmp_qrcode` (
  `QRCode` varchar(200) DEFAULT NULL,
  `Scanner_IP` varchar(30) DEFAULT NULL,
  `Date_Time` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tmp_qrcodelatest` */

DROP TABLE IF EXISTS `tmp_qrcodelatest`;

CREATE TABLE `tmp_qrcodelatest` (
  `QRCode` varchar(200) DEFAULT NULL,
  `Scanner_IP` varchar(30) DEFAULT NULL,
  `Length` int(11) DEFAULT NULL,
  `Date_Time` datetime DEFAULT NULL,
  KEY `date_time` (`Date_Time`),
  KEY `Scanner` (`Scanner_IP`),
  KEY `QrCode` (`QRCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `tmpqrcodescandata` */

DROP TABLE IF EXISTS `tmpqrcodescandata`;

CREATE TABLE `tmpqrcodescandata` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ScanedCode` varchar(100) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Ser_no` varchar(30) DEFAULT NULL,
  `ScanDatetime` datetime NOT NULL,
  `Scan_by` varchar(30) DEFAULT 'RDC',
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `QueueID` bigint(20) DEFAULT '0',
  `Login_User` varchar(20) DEFAULT NULL,
  `WareHouse` varchar(10) DEFAULT '40',
  `File_Name` varchar(100) DEFAULT 'PTL',
  PRIMARY KEY (`ID`),
  KEY `Invoice_no` (`Invoice_No`),
  KEY `QRCode` (`ScanedCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/* Procedure structure for procedure `SPGetNONPickLED` */

/*!50003 DROP PROCEDURE IF EXISTS  `SPGetNONPickLED` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SPGetNONPickLED`(_Crate_Code varchar(30),_IP varchar(30))
BEGIN
		select  CONCAT('INV  :',REPEAT(' ',(14- LENGTH(Invoice_Number))),RIGHT(Invoice_Number,14),
			       'CRT  :',REPEAT(' ',(14- LENGTH(Carate_Code))),Carate_Code,
			       'ZONE :',REPEAT(' ',(14- LENGTH(Zones))),Zones,			        
			       'PIK  :',REPEAT(' ',(14-LENGTH(Picks))),Picks
			      ) AS Message,LCD_SendPort,LCD_IPAddress from (
		Select distinct Q.`Carate_Code`,Q.`Invoice_Number`,Count(Distinct `QueueID`) Picks,
		GROUP_CONCAT(DISTINCT REPLACE(ZM.Display_Name,'ONE-','') SEPARATOR ',') as Zones,`LCD_SendPort`,`LCD_IPAddress` From 
		`tbl_request_queue_for_ptl` Q 
		inner Join `tbl_carate_invoice_mapping` CM  on CM.`Carate_Barcode`=Q.Carate_Code 
		INNER JOIN `tbl_zone_master` ZM  ON ZM.`Scanner_IPAddress`=Q.`Scanner_IPAddress`
		WHERE Q.`Carate_Code`=_Crate_Code and CM.Status Not in ('ERROR','COMPLETED')
		group by Q.Invoice_Number,Q.Carate_Code,`LCD_SendPort`,`LCD_IPAddress`) T limit 1;		
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_AddNewSku` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_AddNewSku` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_AddNewSku`(_Sku_code varchar(30),_sku_type varchar(20),_Description varchar(100),_Pick_Location varchar(20))
BEGIN
              IF Exists(select `Sku` from `tbl_ptl_sku_mapping` where Sku=_Sku_code) then
		Update `tbl_ptl_sku_mapping` set `Sku_Type`= CONVERT(_sku_type , char) ,
		PICK_Location=_Pick_Location where Sku=_Sku_code;
	      else 
		insert into `tbl_ptl_sku_mapping`(`Pick_Location`,`Sku`,`Sku_Type`)
		values(_Pick_Location,_Sku_code,_sku_type);
	     End if;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_addQRCodeData` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_addQRCodeData` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_addQRCodeData`(_Scancode varchar(100),_ip varchar(30))
BEGIN
		declare _invoice varchar(30);
		declare _Crate varchar(20);
		Declare _User varchar(10);
		select `Invoice_Number`,`Carate_Code`,`Operator_id` from `tbl_request_queue_for_ptl` where `Scanner_IPAddress`=_ip and `Status`='INPROCESS' order by `QueueID` limit 1
		into _invoice,_Crate,_User;
		IF Not Exists(select `Crate_Code` from `tbl_carate_master` where `Crate_Code`=_Scancode) then
			if not  exists(select `UserCode` from `tbl_users` where `UserCode`=_Scancode) then
				insert into `tbl_scan_pick_details`(`ScanedCode`,`ScanDatetime`,`Scan_by`,`Carate_Code`,`Invoice_No`,`Login_User`,`WareHouse`)
				values (_Scancode,now(),_ip,_Crate,_invoice,_User,40);
			end if;
		end if;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_addScanner_PickData` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_addScanner_PickData` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_addScanner_PickData`(_invoice varchar(30),_scan_code varchar(200),_IPAddress varchar(30))
BEGIN
		INSERT IGNORE INTO tbl_scan_Pick_details(`Carate_Code`,`Invoice_No`, `Sku`,`Ser_no`,`Scan_by`,QueueID,ScanedCode)
		SELECT `Carate_Code`,_invoice,'',0,_IPAddress,`QueueID`,_scan_code FROM `tbl_request_queue_for_ptl` 
		WHERE  STATUS='INPROCESS' AND `Scanner_IPAddress`=_IPAddress limit 1 ;
			
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_AddweightStatustoCompletePick` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_AddweightStatustoCompletePick` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_AddweightStatustoCompletePick`(_Carate_code varchar(30),_iPaddress varchar(30),_weight decimal(10,3))
BEGIN		
		DECLARE _Carate_weight DECIMAL(10,3);
		DECLARE _weightstatus VARCHAR(10);
		DECLARE t_Per DECIMAL(10,2);
		DECLARE _Captured_Weight DECIMAL(10,3);
		DECLARE _s_Tolerance DECIMAL(10,3);
		Declare _Inv varchar(30);
		SET _Captured_Weight=_weight;
		
		SELECT CM.`Actual_Weight` FROM `tbl_carate_invoice_mapping` CM 
		WHERE CM.`Carate_Barcode`=_Carate_code order by `Pick_id` desc limit 1 INTO _Carate_weight;	
				
		SELECT WC.Tolerance_Per,WC.Static_Tolerance FROM tbl_weight_tolerance_config WC
		WHERE _Carate_weight BETWEEN WC.From_weight AND WC.To_weight LIMIT 1 INTO t_per ,_s_Tolerance;

		SELECT  CASE WHEN (((100+t_per) *CAST(_Carate_weight AS DECIMAL(10,3)))/100)+_s_Tolerance >=CAST(_Captured_Weight AS DECIMAL(10,3))
		 AND  (((100-t_per) *CAST(_Carate_weight AS DECIMAL(10,3)))/100)-_s_Tolerance <=CAST(_Captured_Weight AS DECIMAL(10,3)) THEN 'COMPLETED' ELSE 'ERROR' END  AS STATUS
		INTO _weightstatus;
		
		SELECT IFNULL(_weightstatus,'ERROR') AS StatusforWeightMachine;			
		
		Update `tbl_request_queue_for_ptl` set Status='NOTPICK',pickstart=IFNULL(pickstart,Now()),
		PickConfirmation=Now() 
		where Status in ('INPROCESS','PENDING') and `Carate_Code`=_Carate_code;
				
	        select Invoice_Number from `tbl_carate_invoice_mapping` where `Carate_Barcode`=_Carate_Code 
	        order by `Pick_id` desc limit 1 into  _Inv;
	        
		UPDATE `tbl_carate_invoice_mapping` SET Weiging=NOW(),`Capture_Weight`=_weight,Weighing_IP= _iPaddress,
		Duration=TIMESTAMPDIFF(MINUTE,`Inducted`, NOW()),`Status`=_weightstatus
		WHERE `Carate_Barcode`=_Carate_code and `Invoice_Number`=_Inv;
				
		Update `tbl_carate_master` set isActive=1 where `Crate_Code`=_Carate_code and isActive=0;

	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_ArchiveData` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_ArchiveData` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ArchiveData`()
BEGIN
	DECLARE _Interval INT;
	SET _Interval=31;
	IF HOUR(NOW())>=22 THEN

		INSERT IGNORE INTO `tbl_carate_invoice_mapping_archive`(`Carate_Barcode`,`Source_IPAddress`,`Scanner_IP`,`Invoice_Number`,`Mapping_Date`,`Mapped_By`,`Status`,`FirstPick`,`Weiging`,`Weighing_IP`,`Duration`,`Actual_Weight`,`Capture_Weight`,`Inducted`)
		SELECT CM.`Carate_Barcode`,CM.`Source_IPAddress`,CM.`Scanner_IP`,CM.`Invoice_Number`,CM.`Mapping_Date`,CM.`Mapped_By` ,`Status`,`FirstPick`,`Weiging`,`Weighing_IP`,`Duration`,`Actual_Weight`,`Capture_Weight`,`Inducted`
		FROM `tbl_carate_invoice_mapping` CM  
		WHERE CM.`Status` IN ('COMPLETED','ERROR') AND DATE(CM.`Weiging`)< DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));		
		
		INSERT IGNORE INTO `tbl_operator_login_Archive`(`Login_Id`,`Login_DateTime`,`Logout_Datetime`,`Duration`,`Login_By`)
		SELECT `Login_Id`,`Login_DateTime`,`Logout_Datetime`,`Duration`,`Login_By` FROM `tbl_operator_login` 
		WHERE DATE(`Logout_Datetime`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));		
		
		INSERT IGNORE INTO `tbl_order_line_header_Archive` (`Order_id`,`Line_Item_Seq_no`,`Sku`,`Pick_Qty`,`Pick_Location`,`Location`,`Added_DateTime`)
		SELECT OL.`Order_id`,OL.`Line_Item_Seq_no`,OL.`Sku`,OL.`Pick_Qty`,OL.`Pick_Location`,OL.`Location`,OL.`Added_DateTime`
		FROM `tbl_order_line_header` OL  
		INNER JOIN `tbl_order_header` OH  ON OL.Order_id=OH.Order_id
		INNER JOIN `tbl_carate_invoice_mapping` CS  ON CS.`Invoice_Number`=OH.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR') AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));
		
		
		INSERT IGNORE INTO `tbl_order_header_Archive`(`Order_Number`,`Order_Datetime`,`Order_Weight`,`Order_Status`,`Invoice_Number`,`Invoice_Date`,`Warehouse_id`,`Line_Items`,`Sales_date`,`due_Date`,`Delivery_date`,`Added_Datetime`)
		SELECT OH.`Order_Number`,OH.`Order_Datetime`,OH.`Order_Weight`,OH.`Order_Status`,OH.`Invoice_Number`,OH.`Invoice_Date`,OH.`Warehouse_id`,OH.`Line_Items`,OH.`Sales_date`,OH.`due_Date`,OH.`Delivery_date`,OH.`Added_Datetime`
		FROM `tbl_order_header` OH  
		INNER JOIN `tbl_carate_invoice_mapping` CS  ON CS.`Invoice_Number`=OH.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR')AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));
				
		
		INSERT IGNORE INTO `tbl_request_queue_for_ptl_archive` (`Carate_Code`,`Invoice_Number`,`Pick_Location`,`Sku`,`Sku_Type`,`Scanner_IPAddress`,`PTL_IPAddress`,`LCD_IPAddress`,`PTL_SendPort`,`LCD_SendPort`,`PickQty`,`Picked_Qty`,`Status`,`PickStart`,`PickConfirmation`,`PickDuration`,`DisplayMSG`,`Operator_id`)
		SELECT Q.`Carate_Code`,Q.`Invoice_Number`,Q.`Pick_Location`,Q.`Sku`,Q.`Sku_Type`,Q.`Scanner_IPAddress`,Q.`PTL_IPAddress`,Q.`LCD_IPAddress`,Q.`PTL_SendPort`,Q.`LCD_SendPort`,Q.`PickQty`,Q.`Picked_Qty`,Q.`Status`,Q.`PickStart`,Q.`PickConfirmation`,Q.`PickDuration`,Q.`DisplayMSG`,Q.Operator_id
		FROM `tbl_request_queue_for_ptl` Q 
		INNER JOIN `tbl_carate_invoice_mapping` CS  ON CS.`Invoice_Number`=Q.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR') AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));
		
		
		INSERT INTO `tbl_scan_pick_details_archive`(`Sku`,`Ser_no`,`ScanDatetime`,`Scan_by`,`Carate_Code`,`Invoice_No`,`ScanedCode`,`QueueID`,`WareHouse`,`Login_User`,WareHouseName)
		SELECT S.`Sku`,S.`Ser_no`,S.`ScanDatetime`,S.`Scan_by`,S.`Carate_Code`,S.`Invoice_No` ,S.`ScanedCode`,S.`QueueID`,S.`WareHouse`,S.`Login_User`,S.WareHouseName
		FROM `tbl_scan_pick_details` S
		INNER JOIN `tbl_carate_invoice_mapping` CS  ON CS.`Invoice_Number`=S.`Invoice_No`
		WHERE CS.`Status` IN ('COMPLETED','ERROR') AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));
		
		
		DELETE S  FROM `tbl_scan_pick_details` S		
		INNER JOIN `tbl_carate_invoice_mapping` CS  ON CS.`Invoice_Number`=S.`Invoice_No`
		WHERE CS.`Status` IN ('COMPLETED','ERROR')AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));	
		
		DELETE FROM `tbl_operator_login` WHERE DATE(`Logout_Datetime`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));
		
		DELETE OL FROM 	`tbl_order_line_header` OL  
		INNER JOIN `tbl_order_header` OH  ON OL.Order_id=OH.Order_id
		INNER JOIN `tbl_carate_invoice_mapping` CS  ON CS.`Invoice_Number`=OH.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR') AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));	
		
		DELETE OH  FROM `tbl_order_header` OH  
		INNER JOIN `tbl_carate_invoice_mapping` CS  ON CS.`Invoice_Number`=OH.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR')AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));	
			
		DELETE Q FROM tbl_request_queue_for_ptl Q 
		INNER JOIN `tbl_carate_invoice_mapping` CS  ON CS.`Invoice_Number`=Q.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR')AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));			
		
		DELETE CM FROM `tbl_carate_invoice_mapping` CM  
		WHERE CM.`Status` IN ('COMPLETED','ERROR') AND DATE(CM.`Weiging`)< DATE(DATE_ADD(NOW(),INTERVAL -_Interval DAY));
		
	END IF;	
		
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_ChangePwd` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_ChangePwd` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ChangePwd`(_oldPwd varchar(30),_newPwd varchar(30),_user varchar(30))
BEGIN
		Update `tbl_users` set Password=_newPwd where `UserCode`=_user and `Password`=_oldPwd;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_createOperator` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_createOperator` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_createOperator`(_name varchar(20),_code varchar(30),_createdBy varchar(20),_userType varchar(10),_password varchar(32))
BEGIN
	 Declare _username varchar(20);
	 select count(1) from tbl_users where (USER_ID=_name or UserCode=_code) into _username;
	 IF _username>0 then
		Begin
			 select _name,_code;
		End;
	Else 
		Begin
			insert into tbl_users (USER_ID,PASSWORD, USER_CATEGORY,UserCode,CreatedBy,CreatedDate)
			values(_name,_password,Upper(_userType),_code,_createdBy,now());
		End;
	End IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_DetailReport` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_DetailReport` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_DetailReport`(_frequency VARCHAR(1))
BEGIN
		IF _frequency='D' THEN
			SELECT DATE(CM.`Inducted`) AS Order_Date,COUNT(distinct CM.`Invoice_Number`) OrderCount,COUNT(distinct CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Orders as PickedOrder,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', 
			(Q.Crates/COUNT(distinct CM.`Carate_Barcode`))*100 '%Crate'
			FROM `tbl_order_header` OH 
			INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header` OLH ON OLH.`Order_id`=OH.order_id
			Left OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,Count(distinct CM.Invoice_Number) as Orders,
					COUNT(distinct `Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers 
					FROM`tbl_request_queue_for_ptl` Q
					INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=Q.Invoice_Number AND Q.Carate_Code=CM.Carate_Barcode	AND DATE(Q.PickStart)=DATE(`Inducted`)					
					WHERE DATE(Q.`PickStart`)=DATE(NOW()) AND Q.Status  IN ('COMPLETED','ERROR')
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(CM.`Inducted`)
			WHERE DATE(CM.`Inducted`)=DATE(NOW())  and CM.Status  IN ('COMPLETED','ERROR')
			GROUP BY DATE(CM.`Inducted`),Q.StartPick,Q.EndPick,Q.PickDuration,Q.Orders,Q.Crates,Q.LineItems,Q.Eaches;
		END IF;
		IF _frequency='W' THEN
			SELECT DATE(OH.`Order_Datetime`) AS Order_Date,COUNT(distinct OH.`Order_Number`) OrderCount,COUNT(distinct CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Orders AS PickedOrder,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', 
			(Q.Crates/COUNT( DISTINCT CM.`Carate_Barcode`))*100 '%Crate'
			FROM `tbl_order_header` OH 
			INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header` OLH ON OLH.`Order_id`=OH.order_id
			right OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,COUNT(DISTINCT CM.Invoice_Number) AS Orders,
					COUNT(distinct`Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers 
					FROM`tbl_request_queue_for_ptl` Q
					INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=Q.Invoice_Number and Q.Carate_Code=CM.Carate_Barcode	AND DATE(Q.PickStart)=DATE(`Inducted`)		
					WHERE DATE(Q.`PickStart`)BETWEEN DATE_ADD(NOW(),INTERVAL -7 DAY) AND DATE(NOW()) and  Q.Status  IN ('COMPLETED','ERROR')
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(OH.`Order_Datetime`)
			WHERE DATE(OH.`Order_Datetime`) between DATE_Add(NOW(),interval -7 day) and Date(Now())  AND CM.Status  IN ('COMPLETED','ERROR')
			GROUP BY DATE(OH.`Order_Datetime`) order by DATE(OH.`Order_Datetime`) desc;
		END IF; 
		if _frequency='M' then
			SELECT DATE(OH.`Order_Datetime`) AS Order_Date,COUNT(distinct OH.`Order_Number`) OrderCount,COUNT(distinct CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Orders AS PickedOrder,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', 
			(Q.Crates/COUNT(DISTINCT CM.`Carate_Barcode`))*100 '%Crate'
			FROM `tbl_order_header` OH 
			INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header` OLH ON OLH.`Order_id`=OH.order_id
			right OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,COUNT(DISTINCT CM.Invoice_Number) AS Orders,
					COUNT(distinct`Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers 
					FROM`tbl_request_queue_for_ptl` Q
					INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=Q.Invoice_Number AND Q.Carate_Code=CM.Carate_Barcode	AND DATE(Q.PickStart)=DATE(`Inducted`)				
					WHERE DATE(Q.`PickStart`)BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY) AND DATE(NOW()) and Q.Status  IN ('COMPLETED','ERROR')
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(OH.`Order_Datetime`)
			WHERE DATE(OH.`Order_Datetime`) BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY) AND DATE(NOW())AND CM.Status  IN ('COMPLETED','ERROR')
			GROUP BY DATE(OH.`Order_Datetime`) ORDER BY DATE(OH.`Order_Datetime`) DESC;
		End if;
		if _frequency='Y' then
			SELECT DATE(OH.`Order_Datetime`) AS Order_Date,COUNT(distinct OH.`Order_Number`) OrderCount,COUNT(distinct CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Orders AS PickedOrder,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', 
			(Q.Crates/COUNT( DISTINCT CM.`Carate_Barcode`))*100 '%Crate'
			FROM `tbl_order_header` OH 
			INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header` OLH ON OLH.`Order_id`=OH.order_id
			right OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,COUNT(DISTINCT CM.Invoice_Number) AS Orders,
					COUNT( distinct`Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers
					 FROM`tbl_request_queue_for_ptl` Q
					 INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=Q.Invoice_Number AND Q.Carate_Code=CM.Carate_Barcode AND DATE(Q.PickStart)=DATE(`Inducted`)					
					 WHERE DATE(Q.`PickStart`)BETWEEN DATE_ADD(NOW(),INTERVAL -12 MONTH) AND DATE(NOW()) and Q.Status='COMPLETED'
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(OH.`Order_Datetime`)
			WHERE DATE(OH.`Order_Datetime`) BETWEEN DATE_ADD(NOW(),INTERVAL -12 Month) AND DATE(NOW())AND CM.Status  IN ('COMPLETED','ERROR')
			GROUP BY DATE(OH.`Order_Datetime`)
			union
			SELECT DATE(OH.`Order_Datetime`) AS Order_Date,COUNT(distinct OH.`Order_Number`) OrderCount,COUNT(distinct CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Orders AS PickedOrder,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', 
			(Q.Crates/COUNT(DISTINCT CM.`Carate_Barcode`))*100 '%Crate'
			FROM `tbl_order_header_Archive` OH 
			INNER JOIN `tbl_carate_invoice_mapping_Archive` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header_Archive` OLH ON OLH.`Order_id`=OH.order_id
			right OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,COUNT(DISTINCT CM.Invoice_Number) AS Orders,
					COUNT(distinct `Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers 
					FROM`tbl_request_queue_for_ptl_Archive` Q
					INNER JOIN `tbl_carate_invoice_mapping_Archive` CM  ON CM.`Invoice_Number`=Q.Invoice_Number AND Q.Carate_Code=CM.Carate_Barcode	 AND DATE(Q.PickStart)=DATE(`Inducted`)					
					WHERE DATE(Q.`PickStart`)BETWEEN DATE_ADD(NOW(),INTERVAL -12 MONTH) AND DATE(NOW()) and Q.Status='COMPLETED'
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(OH.`Order_Datetime`)
			WHERE DATE(OH.`Order_Datetime`) BETWEEN DATE_ADD(NOW(),INTERVAL -12 MONTH) AND DATE(NOW())AND CM.Status  IN ('COMPLETED','ERROR')
			GROUP BY DATE(OH.`Order_Datetime`) ORDER BY DATE(Order_Date) DESC;
		End if;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getBayConfigurationDetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getBayConfigurationDetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getBayConfigurationDetails`(_bayname varchar(5))
BEGIN
             IF _bayname!='' then
		Begin
		  Select Zone_Name,Zone_IPAddress as Zone_IP_Address,Display_Name,Scanner_IpAddress as Scanner_IP_Address ,
		  LCD_IpAddress as Display_IP_Address,'Admin' as CreatedBy
		  from tbl_zone_master where Zone_Name=_bayname limit 1;
		End;
	     Else
		Begin
		  SELECT Zone_Name,Zone_IPAddress as Zone_IP_Address,Display_Name,Scanner_IpAddress as Scanner_IP_Address,
		  LCD_IpAddress as Display_IP_Address,'Admin' AS CreatedBy
		  FROM tbl_zone_master ;
		End;
	    End IF;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getBayCurrentStatus` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getBayCurrentStatus` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getBayCurrentStatus`()
BEGIN
		 SELECT DISTINCT CONCAT(zm.Display_Name,'[ <b>',ZM.`Status`,'</b> ]')  AS Zone_Name ,'InProgress' AS InProgressSku,'Completed' AS ProcessedSku,IFNULL(A.itemcount,0) AS inProgressitems,
				IFNULL(A.TotalQty ,0) AS inProgressQty,IFNULL(B.itemcount,0) AS Processeditems,IFNULL(B.TotalQty,0) AS Processedqty,
				C.Carton_Code AS CurrentCarton, I.Carton_Code AS PrevCarton,L.Carton_Code AS NextCarton ,zm.Zone_Type AS Location 
				FROM tbl_zone_master zm 
				LEFT OUTER JOIN (
						  SELECT queue.`Scanner_IPAddress`,'INPROCESS'  AS STATUS, COUNT(queue.`Sku`)  AS itemcount,SUM(queue.`PickQty`)  AS TotalQty 
						  FROM   tbl_request_queue_for_ptl queue 
						  WHERE queue.`Status`='INPROCESS' AND DATE(queue.`PickStart`)=DATE(NOW())
						  GROUP BY queue.Scanner_IPAddress,queue.`Status`
						 ) A ON A.`Scanner_IPAddress` =zm.Scanner_IPAddress
				LEFT OUTER JOIN (
						  SELECT queue.Scanner_IPAddress,'COMPLETED' AS STATUS,COUNT(queue.`Sku`)  AS itemcount,SUM(queue.`PickQty`)  AS TotalQty 
						  FROM   tbl_request_queue_for_ptl queue 
						  WHERE queue.`Status`='COMPLETED' AND DATE(queue.PickStart)=DATE(NOW())
						  GROUP BY queue.Scanner_IPAddress,queue.Status 
						 ) B ON B.Scanner_IPAddress =zm.Scanner_IPAddress
				LEFT OUTER JOIN (
						    SELECT MAx(C.QueueID) as QueueID,C.Scanner_IPAddress,Max(C.`Carate_Code`) AS Carton_Code , 'CRATE' AS Ordertype,C.Status AS isCompeleted 
						    FROM tbl_request_queue_for_ptl C 
						    WHERE C.`Status`='INPROCESS' AND DATE(C.PickStart)=DATE(NOW())
						    group by C.Scanner_IPAddress,C.Status
						 ) C ON C.Scanner_IPAddress=zm.Scanner_IPAddress
				LEFT OUTER JOIN  (
						    SELECT Max(C.QueueID) as QueueID,C.Scanner_IPAddress,MAx(C.`Invoice_Number`) AS Carton_Code , 'INVOICE' AS Ordertype,C.Status AS isCompeleted 
						    FROM tbl_request_queue_for_ptl C 
						    WHERE C.`Status`='INPROCESS' AND DATE(C.PickStart)=DATE(NOW())
						    GROUP BY C.Scanner_IPAddress,C.Status
						  ) I ON I.Scanner_IPAddress=zm.Scanner_IPAddress
				LEFT OUTER JOIN  (
						    SELECT distinct 0 as QueueID,C.Scanner_IPAddress,C.`Operator_id` AS Carton_Code , 'OPERATOR' AS Ordertype,C.Status AS isCompeleted 
						    FROM tbl_request_queue_for_ptl C 
						    where C.Status='INPROCESS' AND DATE(C.PickStart)=DATE(NOW())
						    /*FROM `tbl_zone_master` ZM 
						    INNER JOIN `tbl_operator_login` OP ON OP.`Login_By`=ZM.Scanner_IPAddress
						    INNER JOIN `tbl_users` TU  ON TU.`UserCode`=OP.Login_Id
						    WHERE DATE(OP.`Login_DateTime`)=DATE(NOW()) and OP.`Logout_Datetime` is null*/
						 ) L ON L.Scanner_IPAddress=zm.Scanner_IPAddress
				ORDER BY  REPLACE(zm.Display_Name,'ZONE-','');
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getBaywisePTLConfigurations` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getBaywisePTLConfigurations` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getBaywisePTLConfigurations`(_bayname varchar(5))
BEGIN
             Select zpm.Zone_ID as Zone_name, PM.PICK_LOcation as PTLLocation,'' as Location,psm.Sku_Type,
             sm.SKU  as ActiveSku, '' as  Description,sm.SKU  as Alternate_Sku_code 
             from `tbl_zone_master` zpm 
			 inner Join `tbl_zone_ptl_mapping` PM on PM.ZONE_ID=zpm.ZONE_ID
			 inner Join `tbl_ptl_sku_mapping` psm on psm.PICK_LOcation=PM.PICK_LOcation
             inner Join sku_master sm on sm.Sku_ID=psm.Sku_ID
             where zpm.Zone_Name=_bayname order by PM.PICK_Location;
             
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getCartonDataforBalanceWeight` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getCartonDataforBalanceWeight` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getCartonDataforBalanceWeight`(_Carton_Code varchar(30))
BEGIN
	Declare _weightinSystem decimal(10,3);
	Select case  when CM.`Actual_Weight`>CM.`Capture_Weight` then ' Under Weight' 
	when CM.`Actual_Weight`<CM.`Capture_Weight` then 'Over Weight' else 'Same Weigth' end as STATUS
	,CM.`Carate_Barcode` as Carton_Code,CM.`Weighing_IP` as Weighing_Machine_IP,concat(CM.`Actual_Weight`,'/',CM.`Capture_Weight`) AS Weight
	 from `tbl_order_header` OH
	inner Join `tbl_carate_invoice_mapping` CM  on CM.`Invoice_Number`=OH.`Invoice_Number` 
	-- inner Join `tbl_carate_status` CS  on  CS.`Invoice_No`=CM.`Invoice_Number`
	where  CM.`Invoice_Number`=_Carton_Code and CM.`Weiging` is not null limit 1 
	union
	SELECT CASE  WHEN CM.`Actual_Weight`>CM.`Capture_Weight` THEN ' Under Weight' 
	WHEN CM.`Actual_Weight`<CM.`Capture_Weight` THEN 'Over Weight' ELSE 'Same Weigth' END AS STATUS
	,CM.`Carate_Barcode` AS Carton_Code,CM.`Weighing_IP` AS Weighing_Machine_IP,CONCAT(CM.`Actual_Weight`,'/',CM.`Capture_Weight`) AS Weight
	FROM `tbl_order_header` OH
	INNER JOIN `tbl_carate_invoice_mapping_Archive` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number` 
	-- INNER JOIN `tbl_carate_status_Archive` CS  ON CS.`Invoice_No`=CM.`Invoice_Number`
	WHERE  CM.`Invoice_Number`=_Carton_Code AND CM.`Weiging` IS NOT NULL LIMIT 1;		
	
	Select CM.Carate_Barcode as Carton_Code, OH.Invoice_Number, OL.Sku,OL.Pick_Qty,OL.Pick_Location,
	IFNULL(`Sku_Type`,'NORMAL') as Sku_Type
	from `tbl_order_header` OH
	inner Join `tbl_order_line_header` OL  on OH.Order_id=OL.Order_id
	Left outer join `tbl_ptl_sku_mapping` SM on SM.SKU=OL.Sku 
	LEFT OUTER JOIN `tbl_config_master` C  on C.`Configid`=SM.`Config_id` and C.`isActive`=1
	inner Join `tbl_carate_invoice_mapping` CM  on CM.`Invoice_Number`=OH.`Invoice_Number`
	where CM.`Invoice_Number` =_Carton_Code -- CartonStatus='Error' AND 
	union
	SELECT CM.Carate_Barcode AS Carton_Code, OH.Invoice_Number, OL.Sku,OL.Pick_Qty,OL.Pick_Location,
	IFNULL(`Sku_Type`,'NORMAL') AS Sku_Type
	FROM `tbl_order_header` OH
	INNER JOIN `tbl_order_line_header` OL  ON OH.Order_id=OL.Order_id
	LEFT OUTER JOIN `tbl_ptl_sku_mapping` SM ON SM.SKU=OL.Sku 
	LEFT OUTER JOIN `tbl_config_master` C  ON C.`Configid`=SM.`Config_id` AND C.`isActive`=1
	INNER JOIN `tbl_carate_invoice_mapping_Archive` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
	WHERE CM.`Invoice_Number` =_Carton_Code; -- CartonStatus='Error' AND 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getCartonHistory` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getCartonHistory` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getCartonHistory`(_carton_code varchar(30),_CodeType varchar(10))
BEGIN
		IF _CodeType='BAT' THEN
		IF NOT EXISTS (SELECT Invoice_Number FROM tbl_order_header WHERE File_ID=_carton_code LIMIT 1) THEN
			BEGIN
				SELECT IFNULL(CM.Carate_Barcode,'NA') as  Carate_Barcode,OH.Order_Number AS Order_No,OH.Invoice_Number,'' AS Record_Type,OH.`Sales_date` AS Order_Date,
				OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION ,IFNULL(Q.Status,'PENDING') AS STATUS,IFNULL(Q.`Operator_id`,'NA') Operator_id
				FROM tbl_order_line_header_archive OL 
				INNER JOIN tbl_order_header_archive OH ON  OL.Order_Id=OH.Order_Id 
				LEFT OUTER JOIN `tbl_carate_invoice_mapping_Archive` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
				LEFT OUTER JOIN `tbl_request_queue_for_ptl` Q  ON Q.`Invoice_Number`=CM.Invoice_Number AND Q.`Carate_Code`=CM.Carate_Barcode AND Q.Sku=OL.Sku				
				WHERE OH.File_ID=_carton_code;
			END;
		ELSE
			BEGIN
				SELECT IFNULL(CM.Carate_Barcode,'NA') AS  Carate_Barcode, OH.Order_Number AS Order_No,OH.Invoice_Number,'' AS Record_Type,OH.`Sales_date` AS Order_Date,
				OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION ,IFNULL(Q.Status,'PENDING') AS STATUS,IFNULL(Q.`Operator_id`,'NA') Operator_id
				FROM tbl_order_line_header OL 
				INNER JOIN tbl_order_header OH ON  OL.Order_Id=OH.Order_Id 
				LEFT OUTER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
				LEFT OUTER JOIN `tbl_request_queue_for_ptl` Q  ON Q.`Invoice_Number`=CM.Invoice_Number AND Q.`Carate_Code`=CM.Carate_Barcode AND Q.Sku=OL.Sku				
				WHERE OH.File_ID=_carton_code;
			END;
		END IF;
	END IF;
	IF _CodeType='CRT' then
		IF Exists (Select Carate_Barcode from tbl_carate_invoice_mapping_Archive where Carate_Barcode=_carton_code limit 1) then
			Begin
				Select CM.Carate_Barcode,OH.Order_Number as Order_No,OH.Invoice_Number ,'' as Record_Type,OH.`Sales_Date` as Order_Date,
				OL.Sku,OL.Pick_Qty as Qty,OH.Order_weight as Weight,OL.PICK_LOCATION ,Q.Status,Q.`Operator_id`
				from tbl_order_line_header_archive OL 
				inner join tbl_order_header_archive OH on  OL.Order_Id=OH.Order_Id 
				inner Join `tbl_carate_invoice_mapping_Archive` CM  on CM.`Invoice_Number`=OH.`Invoice_Number`
				inner Join `tbl_request_queue_for_ptl` Q  on Q.`Invoice_Number`=CM.Invoice_Number and Q.`Carate_Code`=CM.Carate_Barcode and Q.Sku=OL.Sku
				where CM.`Carate_Barcode`=_carton_code;
			End;
		Else
			Begin
				SELECT CM.Carate_Barcode,OH.Order_Number AS Order_No,OH.Invoice_Number ,'' AS Record_Type,OH.`Sales_Date` AS Order_Date,
				OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION ,Q.Status,Q.`Operator_id`
				FROM tbl_order_line_header OL 
				INNER JOIN tbl_order_header OH ON  OL.Order_Id=OH.Order_Id 
				INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
				INNER JOIN `tbl_request_queue_for_ptl` Q  ON Q.`Invoice_Number`=CM.Invoice_Number AND Q.`Carate_Code`=CM.Carate_Barcode AND Q.Sku=OL.Sku				
				WHERE CM.`Carate_Barcode`=_carton_code;		
			End;
		End IF;
	End if;
	if _CodeType='INV' then
		IF NOT EXISTS (SELECT Invoice_Number FROM tbl_order_header WHERE Invoice_Number=_carton_code LIMIT 1) THEN
			BEGIN
				SELECT IFNULL(CM.Carate_Barcode,'NA') AS  Carate_Barcode,OH.Order_Number AS Order_No,OH.Invoice_Number,'' AS Record_Type,OH.`Sales_date` AS Order_Date,
				OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION ,IFNULL(Q.Status,'PENDING') as Status,IFNULL(Q.`Operator_id`,'NA') Operator_id
				FROM tbl_order_line_header_archive OL 
				INNER JOIN tbl_order_header_archive OH ON  OL.Order_Id=OH.Order_Id 
				Left outer JOIN `tbl_carate_invoice_mapping_Archive` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
				Left outer JOIN `tbl_request_queue_for_ptl` Q  ON Q.`Invoice_Number`=CM.Invoice_Number AND Q.`Carate_Code`=CM.Carate_Barcode AND Q.Sku=OL.Sku				
				WHERE OH.Invoice_Number=_carton_code;
			END;
		ELSE
			BEGIN
				SELECT IFNULL(CM.Carate_Barcode,'NA') AS  Carate_Barcode, OH.Order_Number AS Order_No,OH.Invoice_Number,'' AS Record_Type,OH.`Sales_date` AS Order_Date,
				OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION ,IFNULL(Q.Status,'PENDING') AS STATUS,IFNULL(Q.`Operator_id`,'NA') Operator_id
				FROM tbl_order_line_header OL 
				INNER JOIN tbl_order_header OH ON  OL.Order_Id=OH.Order_Id 
				LEFT OUTER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
				LEFT OUTER JOIN `tbl_request_queue_for_ptl` Q  ON Q.`Invoice_Number`=CM.Invoice_Number AND Q.`Carate_Code`=CM.Carate_Barcode AND Q.Sku=OL.Sku				
				WHERE OH.Invoice_Number=_carton_code;
			END;
		END IF;
	End if;
	if _CodeType='' then
		SELECT CM.Carate_Barcode,OH.Order_Number AS Order_No,OH.Invoice_Number,'' AS Record_Type,OH.`Sales_date` AS Order_Date,
		OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION ,IFNULL(Q.Status,'PENDING') AS STATUS,IFNULL(Q.`Operator_id`,'NA') Operator_id
		FROM tbl_order_line_header OL 
		INNER JOIN tbl_order_header OH ON  OL.Order_Id=OH.Order_Id 
		LEFT OUTER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
		LEFT OUTER JOIN `tbl_request_queue_for_ptl` Q  ON Q.`Invoice_Number`=CM.Invoice_Number AND Q.`Carate_Code`=CM.Carate_Barcode AND Q.Sku=OL.Sku					
		WHERE Date(OH.`Added_Datetime`)=Date(Now());
	End IF;	    
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getDashboardGraphData` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getDashboardGraphData` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getDashboardGraphData`(_type varchar(2))
BEGIN
               IF _type='H'  Then
			Begin
				SELECT CONCAT(H.Value,'-',case when H.Value+1>23 then 00 else H.Value+1 end) AS HoursName,IFNULL(T.CompletCartons,0) CompletCartons , 
				DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))  AS CartonCompletionDate ,H.Value,H.ID
				FROM tbl_hour_WeekDay_monthlist H  LEFT OUTER JOIN (
				SELECT HOUR(q.`FirstPick`)  AS Hours,COUNT(DISTINCT `Carate_Barcode`) CompletCartons,DATE(q.`FirstPick`) AS ScanTimeStamp FROM 
				`tbl_carate_invoice_mapping` q 
				WHERE q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -24 HOUR)  AND NOW()  and Q.`Status` in ('COMPLETED','ERROR') -- not in ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`),HOUR(q.`FirstPick`) ) T  ON T.Hours=H.Value and Date(T.ScanTimeStamp) = DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))
				WHERE H.Type='H' 
				order by CartonCompletionDate,H.ID ;
				
				SELECT CONCAT(H.Value,'-',CASE WHEN H.Value+1>23 THEN 00 ELSE H.Value+1 END) AS HoursName,IFNULL(T.CompletOrders,0) CompletOrders ,
				 DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))  AS OrderCompletionDate ,H.Value,H.ID
				FROM tbl_hour_WeekDay_monthlist H  LEFT OUTER JOIN (
				SELECT HOUR(q.`FirstPick`)  AS Hours,COUNT(DISTINCT OL.Order_ID) CompletOrders,Date(q.`FirstPick`) as ScanTimeStamp FROM 
				`tbl_carate_invoice_mapping` q 
				LEFT OUTER JOIN `tbl_order_header` OL ON OL.`Invoice_Number`=q.Invoice_Number
				WHERE (q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -24 HOUR)  AND NOW()) AND Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`),HOUR(q.`FirstPick`) ) T  ON T.Hours=H.Value AND DATE(T.ScanTimeStamp) = DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))
				WHERE H.Type='H'  ORDER BY OrderCompletionDate,H.ID;
				
				SELECT CONCAT(H.Value,'-',CASE WHEN H.Value+1>23 THEN 00 ELSE H.Value+1 END) AS HoursName,IFNULL(T.CompletEaches,0) CompletEaches , 
				DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))  AS EachesCompletionDate,H.Value ,H.ID 
				FROM tbl_hour_WeekDay_monthlist H  LEFT OUTER JOIN (
				SELECT HOUR(q.FirstPick)  AS Hours,sum(oh.`Pick_Qty`) CompletEaches,DATE(q.FirstPick) AS ScanTimeStamp FROM 
				tbl_carate_invoice_mapping q 
				inner Join  `tbl_order_header` ol on OL.`Invoice_Number`=q.Invoice_Number
				inner Join `tbl_order_line_header` OH  on OH.Order_id=OL.Order_id
				WHERE q.FirstPick BETWEEN DATE_ADD(NOW(),INTERVAL -24 HOUR)  AND NOW()  AND Q.`Status`  IN ('COMPLETED','ERROR') --  NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.FirstPick),HOUR(q.FirstPick) ) T  ON T.Hours=H.Value AND DATE(T.ScanTimeStamp) = DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))
				WHERE H.Type='H'
				ORDER BY EachesCompletionDate,H.ID;
				
				SELECT Bay ,SUM(CompletEachesinBay) AS CompletEachesinBay,ScanTimeStamp 
				FROM (
					SELECT DISTINCT ZM.Display_Name  AS Bay, IFNULL(CompletEachesinBay,0) AS CompletEachesinBay ,DATE(IFNULL(ScanTimeStamp,NOW())) ScanTimeStamp FROM 
					tbl_Zone_Master ZM  LEFT OUTER JOIN (
					SELECT Scanner_IPAddress,DATE(q.PickStart) ScanTimeStamp,SUM(`PickQty`) CompletEachesinBay
					FROM `tbl_request_queue_for_ptl` q                                 
					WHERE DATE(q.`PickStart`) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -0 HOUR)) AND DATE(NOW()) AND q.status  IN ('COMPLETED','ERROR')
					GROUP BY Scanner_IPAddress,DATE(q.PickStart)) Q
					ON ZM.`Scanner_IPAddress`=q.`Scanner_IPAddress` 
				) T  
				GROUP BY Bay,ScanTimeStamp;
			END;
		End IF;
	      IF _type='W' then
			Begin
				
				select WeekName,sum(CompletCartons) as CompletCartons,CartonCompletionDate,daydate,MAx(ID) as ID from (
				SELECT CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']') AS WeekName,
				COUNT(DISTINCT `Carate_Barcode`) CompletCartons ,
				CONCAT('Week of Month (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS CartonCompletionDate,DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) daydate,W.ID FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN  tbl_carate_invoice_mapping q  ON DATE(q.FirstPick)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) AND Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				AND  q.FirstPick BETWEEN DATE_ADD(NOW(),INTERVAL -7 DAY)  AND DATE_ADD(NOW(), INTERVAL 0 DAY)
				WHERE W.Type='W'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),DATE(q.FirstPick),DAYNAME(q.FirstPick) 
				union
				SELECT CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']') AS WeekName,
				COUNT(DISTINCT `Carate_Barcode`) CompletCartons ,
				CONCAT('Week of Month (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS CartonCompletionDate,DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) daydate,W.ID FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN  tbl_carate_invoice_mapping_archive q  ON Date(q.FirstPick)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) AND Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				AND  q.FirstPick BETWEEN DATE_ADD(Now(),INTERVAL -7 DAY)  AND DATE_ADD(NOW(), INTERVAL 0 DAY)
				WHERE W.Type='W'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),DATE(q.FirstPick),DAYNAME(q.FirstPick) 
				) T GROUP BY  WeekName ,CartonCompletionDate ,daydate ORDER BY DATE_ADD(NOW(),INTERVAL -ID DAY);
				
				select WeekName,sum(CompletOrders) as CompletOrders,OrderCompletionDate,daydate,Max(ID) as ID from (
				SELECT CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']') AS WeekName,
				IFNULL(T.OrderCount,0) AS CompletOrders,CONCAT('Week of Month (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS OrderCompletionDate ,
				DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) daydate,W.ID
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS OrderCompletionDate,COUNT(DISTINCT OL.Order_ID) AS OrderCount 
				FROM tbl_carate_invoice_mapping Q 
				INNER JOIN tbl_order_header OL  ON OL.`Invoice_Number`=Q.`Invoice_Number`
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -7 DAY)  AND DATE_ADD(NOW(), INTERVAL 0 DAY) AND Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.OrderCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				WHERE W.Type='W'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				union
				SELECT CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']') AS WeekName,
				IFNULL(T.OrderCount,0) AS CompletOrders,CONCAT('Week of Month (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS OrderCompletionDate ,
				DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) daydate,W.ID
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS OrderCompletionDate,COUNT(DISTINCT OL.Order_ID) AS OrderCount 
				FROM tbl_carate_invoice_mapping_archive Q 
				INNER JOIN tbl_order_header OL  ON OL.`Invoice_Number`=Q.`Invoice_Number`
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -7 DAY)  AND DATE_ADD(NOW(), INTERVAL 0 DAY) and Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.OrderCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY))
				WHERE W.Type='W'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				) T group by  WeekName ,OrderCompletionDate ,daydate ORDER BY DATE_ADD(NOW(),INTERVAL -ID DAY);
				
				select WeekName,sum(CompletEaches) as CompletEaches,EachesCompletionDate,daydate,Max(ID)As ID from (
				SELECT CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']') AS WeekName,
				IFNULL(T.EachesCount,0) AS CompletEaches,CONCAT('Week of Month (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS EachesCompletionDate ,
				DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) daydate,W.ID
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS EachesCompletionDate,SUM(oH.`Pick_Qty`) AS EachesCount 
				FROM tbl_carate_invoice_mapping Q 
				INNER JOIN tbl_order_header OL  ON OL.`Invoice_Number`=Q.`Invoice_Number`
				INNER JOIN `tbl_order_line_header` OH  ON OH.Order_id=OL.Order_id
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -7 DAY)  AND DATE_ADD(NOW(), INTERVAL 0 DAY) AND Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.EachesCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				WHERE W.Type='W'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				union 
				SELECT CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']') AS WeekName,
				IFNULL(T.EachesCount,0) AS CompletEaches,CONCAT('Week of Month (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS EachesCompletionDate ,
				DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) daydate,W.ID
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS EachesCompletionDate,sum(oH.`Pick_Qty`) AS EachesCount 
				FROM tbl_carate_invoice_mapping_archive Q 
				INNER JOIN tbl_order_header OL  ON OL.`Invoice_Number`=Q.`Invoice_Number`
				INNER JOIN `tbl_order_line_header` OH  ON OH.Order_id=OL.Order_id
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -7 DAY)  AND DATE_ADD(NOW(), INTERVAL 0 DAY) AND Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.EachesCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				WHERE W.Type='W'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				) T  Group by WeekName ,EachesCompletionDate,daydate ORDER BY DATE_ADD(NOW(),INTERVAL -ID DAY);
				
			        SELECT Bay ,SUM(CompletEachesinBay) AS CompletEachesinBay,ScanTimeStamp 
				FROM (
					SELECT DISTINCT ZM.Display_Name  AS Bay, IFNULL(CompletEachesinBay,0) AS CompletEachesinBay ,DATE(IFNULL(ScanTimeStamp,NOW())) ScanTimeStamp FROM 
					tbl_Zone_Master ZM  LEFT OUTER JOIN (
					SELECT Scanner_IPAddress,DATE(q.PickStart) ScanTimeStamp,SUM(`PickQty`) CompletEachesinBay
					FROM `tbl_request_queue_for_ptl` q                                 
					WHERE DATE(q.`PickStart`) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -0 HOUR)) AND DATE(NOW()) AND q.status  IN ('COMPLETED','ERROR')
					GROUP BY Scanner_IPAddress,DATE(q.PickStart)) Q
					ON ZM.`Scanner_IPAddress`=q.`Scanner_IPAddress` 
				) T  
				GROUP BY Bay,ScanTimeStamp;
				
		        End;
	       End IF;
	       IF _type='D' then
			Begin
				SELECT Day_Name,COUNT(DISTINCT Q.`Carate_Barcode`) CompletCartons,CartonCompletionDate,daydate  FROM (
				SELECT DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) AS Day_Name,CONCAT('Week (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID DAY))) / 7) + 1,')') AS CartonCompletionDate,
				DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) daydate  
				FROM tbl_hour_WeekDay_monthlist W WHERE W.Type='D'  
				ORDER BY DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) ) T
				LEFT OUTER JOIN  `tbl_carate_invoice_mapping` q  ON DATE(q.FirstPick)=T.Day_Name 
				AND  q.FirstPick BETWEEN DATE_ADD(NoW(),INTERVAL -31 DAY)  AND DATE_ADD(NOW(),INTERVAL 0 DAY)
				GROUP BY Day_Name,CartonCompletionDate,daydate
				order by Day_Name;				
			         
			        select  daydate,sum(CompletOrders) as CompletOrders,OrderCompletionDate,Day_Name,MAx(ID)as ID from (
				SELECT DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) AS daydate,
				IFNULL(T.OrderCount,0) AS CompletOrders,CONCAT('Week (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS OrderCompletionDate ,
				DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) Day_Name,W.ID
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS OrderCompletionDate,COUNT(DISTINCT OL.Order_ID) AS OrderCount 
				FROM `tbl_carate_invoice_mapping` Q 
				INNER JOIN tbl_order_header OL  ON OL.`Invoice_Number`=Q.`Invoice_Number`
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY)  AND NOW() and Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.OrderCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				WHERE W.Type='D'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				union
				SELECT DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) AS daydate,
				IFNULL(T.OrderCount,0) AS CompletOrders,CONCAT('Week (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS OrderCompletionDate ,
				DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) Day_Name,W.ID
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS OrderCompletionDate,COUNT(DISTINCT OL.Order_ID) AS OrderCount 
				FROM `tbl_carate_invoice_mapping_archive` Q 
				INNER JOIN tbl_order_header_archive OL  ON OL.`Invoice_Number`=Q.`Invoice_Number`
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY)  AND NOW() and Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.OrderCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				WHERE W.Type='D'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				) T  group by daydate,OrderCompletionDate,Day_Name ORDER BY DATE_ADD(NOW(),INTERVAL -ID DAY);
				
				select daydate ,sum(CompletEaches) as CompletEaches,EachesCompletionDate,Day_Name,MAx(Id) as ID from (
				SELECT DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) AS daydate,
				IFNULL(T.EachesCount,0) AS CompletEaches,CONCAT('Week (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS EachesCompletionDate ,
				DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) Day_Name,W.ID
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS EachesCompletionDate,SUM(oH.`Pick_Qty`) AS EachesCount 
				FROM `tbl_carate_invoice_mapping` Q 
				INNER JOIN tbl_order_header OL  ON OL.`Invoice_Number`=Q.`Invoice_Number`
				INNER JOIN `tbl_order_line_header` OH  ON OH.Order_id=OL.Order_id
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY)  AND NOW() AND Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.EachesCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				WHERE W.Type='D'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				union
				SELECT DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) AS daydate,
				IFNULL(T.EachesCount,0) AS CompletEaches,CONCAT('Week (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS EachesCompletionDate ,
				DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) Day_Name,W.ID
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS EachesCompletionDate,SUM(oH.`Pick_Qty`) AS EachesCount 
				FROM `tbl_carate_invoice_mapping_archive` Q 
				INNER JOIN tbl_order_header_archive OL  ON OL.`Invoice_Number`=Q.`Invoice_Number`
				INNER JOIN `tbl_order_line_header_archive` OH  ON OH.Order_id=OL.Order_id
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY)  AND NOW() AND Q.`Status`  IN ('COMPLETED','ERROR') -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.EachesCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				WHERE W.Type='D'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				) T  group by daydate ,EachesCompletionDate,Day_Name ORDER BY DATE_ADD(NOW(),INTERVAL -ID+1 DAY);
								
				SELECT Bay ,SUM(CompletEachesinBay) AS CompletEachesinBay,ScanTimeStamp 
				FROM (
					SELECT DISTINCT ZM.Display_Name  AS Bay, IFNULL(CompletEachesinBay,0) AS CompletEachesinBay ,DATE(IFNULL(ScanTimeStamp,NOW())) ScanTimeStamp FROM 
					tbl_Zone_Master ZM  LEFT OUTER JOIN (
					SELECT Scanner_IPAddress,DATE(q.PickStart) ScanTimeStamp,SUM(`PickQty`) CompletEachesinBay
					FROM `tbl_request_queue_for_ptl` q                                 
					WHERE DATE(q.`PickStart`) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -0 HOUR)) AND DATE(NOW()) AND q.status  IN ('COMPLETED','ERROR')
					GROUP BY Scanner_IPAddress,DATE(q.PickStart)) Q
					ON ZM.`Scanner_IPAddress`=q.`Scanner_IPAddress` 
				) T  
				GROUP BY Bay,ScanTimeStamp;
			End;
	      End IF;
	      IF _type='M' THEN
			BEGIN
				SELECT W.Value AS sMonthName,COUNT(DISTINCT `Carate_Barcode`) CompletCartons,YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) CartonCompletionDate
				,W.Quarter _Quarter FROM tbl_hour_WeekDay_monthlist W 
				LEFT OUTER JOIN  `tbl_carate_invoice_mapping` q  ON MONTH(q.`FirstPick`)=W.ID 
				AND  q.FirstPick BETWEEN DATE_ADD(Now(),INTERVAL -12 MONTH)  AND Now() and Q.`Status`='COMPLETED' -- NOT IN ('INDUCTED','INPROCESS')
				WHERE TYPE='M' 
				GROUP BY W.Value,W.Quarter,YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH))
				ORDER BY YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) , W.ID;
			
				SELECT W.Value AS sMonthName,IFNULL(T.CompletOrders,0) AS CompletOrders,YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) OrderCompletionDate
				,W.Quarter _Quarter FROM tbl_hour_WeekDay_monthlist W 
				LEFT OUTER JOIN (
				SELECT MONTHNAME(Q.FirstPick) AS sMonthName,YEAR(Q.FirstPick) AS sYear ,COUNT( DISTINCT ol.Order_id) AS CompletOrders  FROM
				tbl_order_header OL  
				INNER JOIN   tbl_carate_invoice_mapping q ON OL.`Invoice_Number`=Q.`Invoice_Number`
				WHERE DATE(q.FirstPick) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -12 MONTH))  AND DATE(NOW()) 
				AND Q.`Status`='COMPLETED' -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY MONTHNAME(Q.FirstPick)) T  ON CONVERT(T.sMonthName, CHAR)=CONVERT(W.Value,CHAR)
				WHERE TYPE='M' 
				ORDER BY YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) , W.ID;
				
				SELECT W.Value AS sMonthName,IFNULL(T.EachesCount,0) AS CompletEaches,YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) EachesCompletionDate
				,W.Quarter _Quarter FROM tbl_hour_WeekDay_monthlist W 
				LEFT OUTER JOIN (
				SELECT MONTHNAME(Q.FirstPick) AS sMonthName,YEAR(Q.FirstPick) AS sYear ,sum(oH.`Pick_Qty`) AS EachesCount  FROM
				tbl_order_header OL  
				INNER JOIN   tbl_carate_invoice_mapping q  ON OL.`Invoice_Number`=Q.`Invoice_Number`
				INNER JOIN `tbl_order_line_header` OH  ON OH.Order_id=OL.Order_id
				WHERE DATE(q.FirstPick) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -12 MONTH))  AND DATE(NOW())
				AND Q.`Status`='COMPLETED' -- NOT IN ('INDUCTED','INPROCESS')
				GROUP BY MONTHNAME(Q.FirstPick)) T  ON CONVERT(T.sMonthName, CHAR)=CONVERT(W.Value,CHAR)
				WHERE TYPE='M' 
				ORDER BY YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) , W.ID;
				
				SELECT Bay ,SUM(CompletEachesinBay) AS CompletEachesinBay,ScanTimeStamp 
				FROM (
					SELECT distinct ZM.Display_Name  AS Bay, IFNULL(CompletEachesinBay,0) AS CompletEachesinBay ,DATE(IFNULL(ScanTimeStamp,NOW())) ScanTimeStamp FROM 
					tbl_Zone_Master ZM  LEFT OUTER JOIN (
					SELECT Scanner_IPAddress,DATE(q.PickStart) ScanTimeStamp,SUM(`PickQty`) CompletEachesinBay
					FROM `tbl_request_queue_for_ptl` q                                 
					WHERE DATE(q.`PickStart`) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -0 HOUR)) AND DATE(NOW()) AND q.status='COMPLETED'
					GROUP BY Scanner_IPAddress,DATE(q.PickStart)) Q
					ON ZM.`Scanner_IPAddress`=q.`Scanner_IPAddress` 
				) T  
				GROUP BY Bay,ScanTimeStamp;
				
		        END;
	       END IF;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_getDataforDisplayonLED` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_getDataforDisplayonLED` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_getDataforDisplayonLED`(_LCD_IP varchar(30))
BEGIN
		Declare _Scanner_IP VARCHAR(30)	;
		Declare _Zone varchar(100);
		Declare _Inv varchar(30);
		
		select `Invoice_Number` from `tbl_request_queue_for_ptl` Q
		where Q.`LCD_IPAddress`=_LCD_IP AND Q.`Status`='INPROCESS' ORDER BY QueueID DESC  limit 1 into _Inv;
		
		select GROUP_CONCAT(DISTINCT REPLACE(ZM.Display_Name,'ONE-','') order by ID SEPARATOR ',') 
		from tbl_zone_master ZM  
		inner Join tbl_request_queue_for_ptl Q on Q.Scanner_IPAddress=ZM.Scanner_IPAddress
		where Q.Invoice_Number =_Inv into _Zone;
		 
		select `Scanner_IPAddress` from `tbl_request_queue_for_ptl` 
		where `LCD_IPAddress`=_LCD_IP and `Status`='INPROCESS' limit 1  into _Scanner_IP;
			
		select  Concat('INV  :',repeat(' ',(14- Length(Q.`Invoice_Number`))),Right(Q.`Invoice_Number`,14),
			       'CRT  :',REPEAT(' ',(14- Length(Q.`Carate_Code`))),Q.`Carate_Code`,
			       'ZONE :',REPEAT(' ',(14- LENGTH(_Zone))),_Zone			        ,
			       'PICK :',REPEAT(' ',(14-Length(COUNT(distinct Q.`QueueID`)))),count(distinct Q.`QueueID`)
			     ) as Message
		from `tbl_request_queue_for_ptl` Q
		inner Join `tbl_zone_master` ZM  on ZM.`Scanner_IPAddress`=Q.`Scanner_IPAddress`
		where Q.`Scanner_IPAddress`=_Scanner_IP and Q.`Status`='INPROCESS' and Q.DisplayMSG=0 and Q.Invoice_Number=_Inv;
		
		Update  `tbl_request_queue_for_ptl` set DisplayMSG=1 
		where DisplayMSG=0 and `Scanner_IPAddress`=_Scanner_IP  and `Status`='INPROCESS';-- and `Status`='INPROCESS';
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_getDataforDisplayonLEDforCrate` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_getDataforDisplayonLEDforCrate` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_getDataforDisplayonLEDforCrate`(_LCD_IP varchar(30),_Crate_Code varchar(30))
BEGIN
		
		select  Concat('INV  :',repeat(' ',(14- Length(Q.`Invoice_Number`))),Right(Q.`Invoice_Number`,14),
			       'CRT  :',REPEAT(' ',(14- Length(Q.`Carate_Code`))),Q.`Carate_Code`,
			       'ZONE :',REPEAT(' ',(14- LENGTH(GROUP_CONCAT(DISTINCT Replace(ZM.Display_Name,'ONE-','') SEPARATOR ',')))),
			        Group_Concat(distinct REPLACE(ZM.Display_Name,'ONE-','') SEPARATOR ','),
			       'PIK  :',REPEAT(' ',(14-Length(COUNT(distinct Q.`QueueID`)))),count(distinct Q.`QueueID`)
			     ) as Message
		from `tbl_request_queue_for_ptl` Q
		inner Join `tbl_zone_master` ZM  on ZM.`Scanner_IPAddress`=Q.`Scanner_IPAddress`
		where Q.`Carate_Code`=_Crate_Code;
	
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getErrorReport` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getErrorReport` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getErrorReport`(_fromdatetime datetime,_todatetime datetime)
BEGIN
		Select E.`Invoice_Number`,E.`Sku`,E.`Error_Message`,E.`Error_Date` ,OL.`Pick_Location`,OH.`Sales_date`
		from `tbl_order_error_details` E
		inner Join `tbl_order_header` OH  on OH.`Invoice_Number`=E.Invoice_Number
		inner Join `tbl_order_line_header` OL on OL.`Sku`=E.Sku
		where `Error_Date` between _fromdatetime and _todatetime and Length(Trim(E.Sku))>0
		union
		SELECT E.`Invoice_Number`,E.`Sku`,E.`Error_Message`,E.`Error_Date`,'' as Pick_Location,OH.Sales_date 
		FROM `tbl_order_error_details` E
		INNER JOIN `tbl_order_header` OH  ON OH.`Invoice_Number`=E.Invoice_Number
		WHERE `Error_Date` BETWEEN _fromdatetime AND _todatetime AND LENGTH(TRIM(E.Sku))=0;

	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_GetInvoiceStatus` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_GetInvoiceStatus` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GetInvoiceStatus`(_Invoice_No varchar(30))
BEGIN
             SELECT concat(IFNULL(CS.`Status`,'PENDING') ,'|',`Order_id`) as _Status
             FROM `tbl_order_header` OH 
             LEFT OUTER JOIN  `tbl_carate_status` CS ON OH.`Invoice_Number`=CS.Invoice_No 
             WHERE OH.`Invoice_Number`=_Invoice_No LIMIT 1;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_getIPList` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_getIPList` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_getIPList`()
BEGIN
		SELECT DISTINCT `Zone_IPAddress` AS IPAddress,`PTL_SendPort` AS SendPort,`PTL_ReceivePort`AS ReceivedPort,'PTL' AS _Type ,Zone_Type
		FROM `tbl_zone_master` 
		UNION ALL
		SELECT DISTINCT `LCD_IPAddress` AS IPAddress,`LCD_SendPort` AS SendPort,`LCD_SendPort` AS ReceivedPort,'LED' AS _Type ,Zone_Type
		FROM `tbl_zone_master`
		UNION ALL
		SELECT DISTINCT `Scanner_IPAddress` AS IPAddress,`Scanner_ReceivePort` AS SendPort,`Scanner_ReceivePort` AS ReceivedPort,'Scanner' AS _Type ,Zone_Type
		FROM `tbl_zone_master`
		UNION ALL
		SELECT '192.168.10.20' AS IPAddress,'50001' AS SendPort,'50001' AS ReceivedPort,'Carate' AS _Type,'Normal'
		UNION ALL
	        SELECT '192.168.250.1' AS IPAddress,'9600' AS SendPort,'9600' AS ReceivedPort,'Weight' AS _Type, 'Normal';
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_GetNOTPickedDetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_GetNOTPickedDetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GetNOTPickedDetails`()
BEGIN
		SELECT * FROM `tbl_request_queue_for_ptl` WHERE STATUS='NOTPICK' and `PickStart`=Now();
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getPickerProductivity_Report` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getPickerProductivity_Report` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getPickerProductivity_Report`(_fromdatetime DATETIME,_todatetime DATETIME)
BEGIN

	select distinct Q.`Operator_id` as `Login_Id`,Q.`Invoice_Number` as Order_No,OH.`Invoice_Date`,Q.`Sku`,Q.`PickQty` as Pick_Qty,
	TimeStampdiff(Second,`PickStart`,`PickConfirmation`) as PickDuration,
	ZM.`Display_Name` as Zone_ID,Q.`Pick_Location`,OH.`Order_Weight`,CS.`Capture_Weight`,time(`Weiging`) as Weiging
	from `tbl_request_queue_for_ptl` Q  
	inner Join `tbl_order_header` OH  on OH.`Invoice_Number`=Q.Invoice_Number
	inner Join `tbl_order_line_header` OLH on OLH.Order_id=OH.Order_id
	inner Join `tbl_zone_master` ZM  on ZM.`Scanner_IPAddress`=Q.`Scanner_IPAddress` and Q.`PTL_IPAddress`=ZM.`Zone_IPAddress`
	inner Join `tbl_zone_ptl_mapping` ZPM  on ZPM.`Zone_ID`=ZM.ID and ZPM.PTL=Q.Pick_Location
	inner Join  `tbl_carate_invoice_mapping` CS  on CS.`Invoice_Number`=OH.Invoice_Number and CS.`Carate_Barcode`=Q.Carate_Code
	-- inner Join `tbl_zone_ptl_mapping` ZPM  on ZPM.Zone_ID=ZM.ID and ZPM.PTL=Q.Pick_Location
	where Q.Status='COMPLETED' and Q.`PickStart`  BETWEEN _fromdatetime AND _todatetime
	order by Q.`Operator_id`,TIMESTAMPDIFF(SECOND,`PickStart`,`PickConfirmation`);
	-- where OL.`Login_DateTime` between _fromdatetime and _todatetime;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getScannedData_Report` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getScannedData_Report` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getScannedData_Report`(_fromdatetime DATETIME,_todatetime DATETIME,_Code VARCHAR(3000),_ReportType VARCHAR(20))
BEGIN
		SET _Code=REPLACE(REPLACE(_Code,'\r\n',','),'www.oriflame.com/?q=','');
	        
		IF _ReportType='Date' THEN
			SELECT distinct S.`Invoice_No` AS Order_No, IFNULL(S.`Carate_Code`,'NA') AS Carton_Code,replace(ScanedCode,'www.oriflame.com/?q=','') AS Sku_Code ,
			'NA' as `Pick_Location` ,ScanDatetime ,IFNULL(OH.`Invoice_Date`,ScanDatetime) as Invoice_Date,
			1 as  Qty,S.`Login_User` Login_Id,ZM.`Display_Name` AS Zone_ID,S.WareHouseName as `WareHouse`
			FROM `tbl_scan_pick_details` S
			-- INNER JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By`
			Left outer JOIN `tbl_zone_master` ZM  ON ZM.`Scanner_IPAddress`=S.`Scan_by`
			-- INNER JOIN `tbl_request_queue_for_ptl` Q ON Q.`QueueID`=S.`QueueID`
			Left outer JOIN `tbl_order_header` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
			WHERE DATE(S.`ScanDatetime`) BETWEEN DATE(_fromdatetime) AND DATE(_todatetime)
			union
			SELECT DISTINCT S.`Invoice_No` AS Order_No, IFNULL(S.`Carate_Code`,'NA') AS Carton_Code,REPLACE(ScanedCode,'www.oriflame.com/?q=','') AS Sku_Code ,
			'NA' AS `Pick_Location` ,ScanDatetime ,IFNULL(OH.`Invoice_Date`,ScanDatetime) AS Invoice_Date,
			1 AS  Qty,S.`Login_User` Login_Id,ZM.`Display_Name` AS Zone_ID,S.WareHouseName AS `WareHouse`
			FROM `tbl_scan_pick_details_archive` S
			-- INNER JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By`
			LEFT OUTER JOIN `tbl_zone_master` ZM  ON ZM.`Scanner_IPAddress`=S.`Scan_by`
			-- INNER JOIN `tbl_request_queue_for_ptl` Q ON Q.`QueueID`=S.`QueueID`
			LEFT OUTER JOIN `tbl_order_header_archive` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
			WHERE DATE(S.`ScanDatetime`) BETWEEN DATE(_fromdatetime) AND DATE(_todatetime);
			-- GROUP BY S.Invoice_No,S.Carate_Code,S.Sku;
		END IF;
		IF _ReportType='Invoice' THEN
			SELECT distinct S.`Invoice_No` AS Order_No, IFNULL(S.`Carate_Code`,'NA') AS Carton_Code,replace(ScanedCode,'www.oriflame.com/?q=','') AS Sku_Code ,
			'NA' as Pick_Location ,(ScanDatetime) AS ScanDatetime,IFNULL(OH.`Invoice_Date`,ScanDatetime) as Invoice_Date,
			COUNT(S.Sku) Qty,OL.`Login_Id`,ZM.`Display_Name` AS Zone_ID,S.WareHouseName AS `WareHouse`
			FROM `tbl_scan_pick_details` S
			Left outer JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By`
			Left outer JOIN `tbl_zone_master` ZM  ON ZM.`Scanner_IPAddress`=OL.`Login_By`
			-- INNER JOIN `tbl_request_queue_for_ptl` Q ON Q.`QueueID`=S.`QueueID`
			Left outer JOIN `tbl_order_header` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
			WHERE LOCATE(OH.Invoice_Number,_Code)>0
			GROUP BY S.Invoice_No,S.Carate_Code,S.Sku
			union 
			SELECT DISTINCT S.`Invoice_No` AS Order_No, IFNULL(S.`Carate_Code`,'NA') AS Carton_Code,REPLACE(ScanedCode,'www.oriflame.com/?q=','') AS Sku_Code ,
			'NA' AS Pick_Location ,(ScanDatetime) AS ScanDatetime,IFNULL(OH.`Invoice_Date`,ScanDatetime) AS Invoice_Date,
			COUNT(S.Sku) Qty,OL.`Login_Id`,ZM.`Display_Name` AS Zone_ID,S.WareHouseName AS `WareHouse`
			FROM `tbl_scan_pick_details_archive` S
			LEFT OUTER JOIN `tbl_operator_login_archive` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By`
			LEFT OUTER JOIN `tbl_zone_master` ZM  ON ZM.`Scanner_IPAddress`=OL.`Login_By`
			-- INNER JOIN `tbl_request_queue_for_ptl` Q ON Q.`QueueID`=S.`QueueID`
			LEFT OUTER JOIN `tbl_order_header_archive` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
			WHERE LOCATE(OH.Invoice_Number,_Code)>0
			GROUP BY S.Invoice_No,S.Carate_Code,S.Sku;
		END IF;
		IF _ReportType ='Sku' THEN
			IF LOCATE('.',_Code)=0 THEN
				SELECT S.`Invoice_No` AS Order_No, IFNULL(S.`Carate_Code`,'NA') AS Carton_Code,Replace(ScanedCode,'www.oriflame.com/?q=','') AS Sku_Code ,
				'NONE' AS Pick_Location ,S.ScanDatetime AS ScanDatetime,IFNULL(OH.`Invoice_Date`,ScanDatetime) AS Invoice_Date,
				COUNT(S.Sku) Qty,S.`Login_User` AS Login_Id,ZM.`Display_Name` AS Zone_ID,S.WareHouseName AS `WareHouse`
				FROM `tbl_scan_pick_details` S
				-- INNER JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By` 
				Left outer JOIN `tbl_zone_master` ZM  ON ZM.`Scanner_IPAddress`=S.`Scan_by`
				Left outer JOIN `tbl_order_header` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
				WHERE LOCATE(REPLACE(ScanedCode,'www.oriflame.com/?q=',''),_Code)>0
				GROUP BY S.Invoice_No,S.Carate_Code,S.ScanedCode
				union
				SELECT S.`Invoice_No` AS Order_No, IFNULL(S.`Carate_Code`,'NA') AS Carton_Code,REPLACE(ScanedCode,'www.oriflame.com/?q=','') AS Sku_Code ,
				'NONE' AS Pick_Location ,S.ScanDatetime AS ScanDatetime,IFNULL(OH.`Invoice_Date`,ScanDatetime) AS Invoice_Date,
				COUNT(S.Sku) Qty,S.`Login_User` AS Login_Id,ZM.`Display_Name` AS Zone_ID,S.WareHouseName AS `WareHouse`
				FROM `tbl_scan_pick_details_archive` S
				-- INNER JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By` 
				LEFT OUTER JOIN `tbl_zone_master` ZM  ON ZM.`Scanner_IPAddress`=S.`Scan_by`
				LEFT OUTER JOIN `tbl_order_header_archive` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
				WHERE LOCATE(REPLACE(ScanedCode,'www.oriflame.com/?q=',''),_Code)>0
				GROUP BY S.Invoice_No,S.Carate_Code,S.ScanedCode;
			ELSE 
				SELECT S.`Invoice_No` AS Order_No, IFNULL(S.`Carate_Code`,'NA') AS Carton_Code,CONCAT(S.Sku,'.',S.`Ser_no`) AS Sku_Code ,
				Q.`Pick_Location` ,ScanDatetime AS ScanDatetime,IFNULL(OH.`Invoice_Date`,ScanDatetime) AS Invoice_Date,
				COUNT(S.Sku) Qty,S.`Login_User` AS Login_Id,ZM.`Display_Name` AS Zone_ID,S.WareHouseName AS `WareHouse`
				FROM `tbl_scan_pick_details` S
				-- INNER JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By`
				Left outer JOIN `tbl_zone_master` ZM  ON ZM.`Scanner_IPAddress`=S.`Scan_by`
				Left outer JOIN `tbl_request_queue_for_ptl` Q ON Q.`QueueID`=S.`QueueID`
				Left outer JOIN `tbl_order_header` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
				WHERE CONCAT(S.Sku,'.',S.`Ser_no`)=_Code
				GROUP BY S.Invoice_No,S.Carate_Code,S.Sku
				union 
				SELECT S.`Invoice_No` AS Order_No, IFNULL(S.`Carate_Code`,'NA') AS Carton_Code,CONCAT(S.Sku,'.',S.`Ser_no`) AS Sku_Code ,
				Q.`Pick_Location` ,ScanDatetime AS ScanDatetime,IFNULL(OH.`Invoice_Date`,ScanDatetime) AS Invoice_Date,
				COUNT(S.Sku) Qty,S.`Login_User` AS Login_Id,ZM.`Display_Name` AS Zone_ID,S.WareHouseName AS `WareHouse`
				FROM `tbl_scan_pick_details_archive` S
				-- INNER JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By`
				Left outer JOIN `tbl_zone_master` ZM  ON ZM.`Scanner_IPAddress`=S.`Scan_by`
				Left outer JOIN `tbl_request_queue_for_ptl_archive` Q ON Q.`QueueID`=S.`QueueID`
				LEFT OUTER JOIN `tbl_order_header_archive` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
				WHERE CONCAT(S.Sku,'.',S.`Ser_no`)=_Code
				GROUP BY S.Invoice_No,S.Carate_Code,S.Sku;
			END IF;
		END IF;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getTableWiseDabataseSize` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getTableWiseDabataseSize` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getTableWiseDabataseSize`()
BEGIN
			SELECT
			  TABLE_NAME AS `Table`,
			  ROUND((DATA_LENGTH + INDEX_LENGTH) / 1024 / 1024) AS `Size (MB)`
			FROM
			  information_schema.TABLES
			WHERE
			  TABLE_SCHEMA = 'amway_ptl_540'
			ORDER BY
			  (DATA_LENGTH + INDEX_LENGTH)
			DESC;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_GetValidateInvoice` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_GetValidateInvoice` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GetValidateInvoice`(_Invoice_No varchar(30))
BEGIN
                Select OH.`Invoice_Number` from `tbl_order_header` OH 
                Left outer Join `tbl_carate_invoice_mapping` CM  on CM.`Invoice_Number`=OH.Invoice_Number
                Left outer Join `tbl_carate_status` CS  on CS.`Invoice_No`=OH.OH.`Invoice_Number`
                where OH.Invoice_Number= _Invoice_No  and CS.`Status` is null;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_Map_CaratetoInvoiceandPrepareList` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_Map_CaratetoInvoiceandPrepareList` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Map_CaratetoInvoiceandPrepareList`(_Carate_code varchar(30),_system_IP varchar(30),_invoice_no varchar(30),_user varchar(30))
BEGIN
	Declare _scanner_IP VARCHAR(30);
	Declare _MEssageinfo varchar(1000);
	Declare _Error varchar(1000);
	Declare _Crate_Weight decimal(10,2);
	Declare _PrevCrate varchar(100);
	Declare _MyCrate varchar(30);
	Declare _Token_Drop varchar(100);
	SET _scanner_IP='192.168.10.220';
	set _MEssageinfo='';
	set _Error='';
	IF Not Exists (Select `Invoice_Number` from `tbl_order_header` where `Invoice_Number`=	_invoice_no) then
		set _MEssageinfo='INVALID INVOICE NUMBER SCANNED!';
		select _MEssageinfo as ErrorStatus;
	Else

 	SELECT `Crate_Code` FROM `tbl_carate_master` 
 	WHERE  Crate_Code= SUBSTRING(_Carate_code,LOCATE('P&P',_Carate_code))  order by id  limit 1 into _MyCrate; 
 	
 	-- set _MyCrate=CONCAT('P&PBIN',TRIM(LEADING '0' FROM REPLACE(_MyCrate,'P&PBIN','')))       
        set _Carate_code=IFNULL(_MyCrate,_Carate_code);
        
	insert into `tbl_carate_status_archive` (`Carate_Code`,`Invoice_No`,`Actual_Weight`,`Capture_Weight`,`Status`,`Inducted`,`FirstPick`,`Weiging`,`Weighing_IP`,`Duration`)
	select `Carate_Code`,`Invoice_No`,`Actual_Weight`,`Capture_Weight`,`Status`,`Inducted`,`FirstPick`,`Weiging`,`Weighing_IP`,`Duration` from `tbl_carate_status`
	where Carate_Code=_Carate_code and Status in ('COMPLETED','ERROR');
	Delete from tbl_carate_status where Carate_Code=_Carate_code and Status in ('COMPLETED','ERROR');
	
	
	 IF not Exists(select `Carate_Barcode` from `tbl_carate_invoice_mapping` where `Carate_Barcode`= _Carate_code and `Status`='INPROCESS') then
		select `Crate_Weight` from `tbl_carate_master` where `Crate_Code`=_Carate_code into _Crate_Weight;
		IF NOT Exists(select `Invoice_Number` from `tbl_carate_invoice_mapping` where `Invoice_Number`=_invoice_no) then
			
			insert IGNORE into tbl_carate_invoice_mapping(`Carate_Barcode`,`Source_IPAddress`,
			`Scanner_IP`,`Invoice_Number`,`Mapping_Date`,`Mapped_By`,Inducted,Actual_Weight,STATUS)
			SELECT _Carate_code,_system_IP,_scanner_IP,_invoice_no,NOW(),_user,NOW(),(Order_Weight+_Crate_Weight),'INDUCTED' FROM `tbl_order_header` OH 
			WHERE `Invoice_Number`=_invoice_no;
			
			/*INSERT IGNORE INTO `tbl_carate_status` (`Carate_Code`,Invoice_No,`Actual_Weight`,`Status`,Inducted)
			SELECT _Carate_code,_invoice_no,(`Order_Weight`+_Crate_Weight) AS Actual_Weight,'INDUCTED',NOW() FROM `tbl_order_header` OH 
			WHERE `Invoice_Number`=_invoice_no;*/
			
			insert IGNORE into `tbl_request_queue_for_ptl`(`Carate_Code`,`Invoice_Number`,`Pick_Location`,`Sku`,`Sku_Type`,`Scanner_IPAddress`,`PickQty`,
			`Picked_Qty`,`Status`,`PTL_IPAddress`,`LCD_IPAddress`,`PTL_SendPort`,`LCD_SendPort`)
			SELECT _Carate_code, _invoice_no,ZPM.`PTL`,Ol.`Sku`,ZPM.PTL_Type,ZM.`Scanner_IPAddress`,OL.`Pick_Qty`,0,'PENDING',
			ZM.`Zone_IPAddress`,ZM.`LCD_IPAddress`,ZM.`PTL_SendPort`,ZM.`LCD_SendPort` 
			FROM tbl_zone_master ZM  
			INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.Zone_Id=ZM.ID
			INNER JOIN `tbl_order_line_header` OL ON OL.`Pick_Location`=ZPM.`Pick_Location`
			INNER JOIN `tbl_order_header` OH  ON OH.`Order_id`=OL.`Order_id`
			INNER JOIN `tbl_carate_invoice_mapping` CIM ON CIM.`Invoice_Number`=OH.`Invoice_Number`
			-- INNER JOIN `tbl_ptl_sku_mapping` PSM  ON PSM.`Pick_Location`=ZPM.Pick_Location
			WHERE CIM.`Carate_Barcode`=_Carate_code and OH.`Invoice_Number`=_invoice_no;			
			
			UPDATE`tbl_carate_master` SET isActive=0 WHERE `Crate_Code`= _Carate_code AND `isActive`=1;
			SET _MEssageinfo='';
		
		 Else
		    IF Not Exists(select  `Carate_Barcode` from `tbl_carate_invoice_mapping` where `Carate_Barcode`= _Carate_code and `Status`='INPROCESS') then	
		      IF  Exists (select `Carate_Barcode` FROM tbl_carate_invoice_mapping 
				WHERE Invoice_Number=_invoice_no and Status ='INDUCTED') then
				select `Carate_Barcode` from tbl_carate_invoice_mapping 
				where Invoice_Number=_invoice_no limit 1 into _PrevCrate;
				
				Update tbl_carate_invoice_mapping set Carate_Barcode=_Carate_code where Invoice_Number=_invoice_no;
				Update tbl_request_queue_for_ptl set Carate_Code=_Carate_code where Invoice_Number=_invoice_no;
				UPDATE`tbl_carate_master` SET isActive=0 WHERE `Crate_Code`= _Carate_code AND `isActive`=1;
				UPDATE`tbl_carate_master` SET isActive=1 WHERE `Crate_Code`= _PrevCrate AND `isActive`=0;
				-- Update `tbl_carate_status` set `Carate_Code`=_Carate_code where Carate_Code=_PrevCrate;
				
				INSERT IGNORE INTO `tbl_request_queue_for_ptl`(`Carate_Code`,`Invoice_Number`,`Pick_Location`,`Sku`,`Sku_Type`,`Scanner_IPAddress`,`PickQty`,
				`Picked_Qty`,`Status`,`PTL_IPAddress`,`LCD_IPAddress`,`PTL_SendPort`,`LCD_SendPort`)
				SELECT _Carate_code, _invoice_no,ZPM.`PTL`,Ol.`Sku`,ZPM.PTL_Type,ZM.`Scanner_IPAddress`,OL.`Pick_Qty`,0,'PENDING',
				ZM.`Zone_IPAddress`,ZM.`LCD_IPAddress`,ZM.`PTL_SendPort`,ZM.`LCD_SendPort` 
				FROM tbl_zone_master ZM  
				INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.Zone_Id=ZM.ID
				INNER JOIN `tbl_order_line_header` OL ON OL.`Pick_Location`=ZPM.`Pick_Location`
				INNER JOIN `tbl_order_header` OH  ON OH.`Order_id`=OL.`Order_id`
				INNER JOIN `tbl_carate_invoice_mapping` CIM ON CIM.`Invoice_Number`=OH.`Invoice_Number`
				WHERE CIM.`Carate_Barcode`=_Carate_code AND OH.`Invoice_Number`=_invoice_no
				order by ZPM.`PTL`;			
			
				set _MEssageinfo='';
			else
				SET _Error= concat('Invoice :',_invoice_no,' PICKING  IS ALREADY IN PROGRESS/COMPLETED');
			End if;
		   End if;				
		End if;
		select OL.`Order_line_Id` from `tbl_order_line_header` OL 
		inner Join `tbl_order_header` OH  on OH.Order_id=OL.Order_id
		where OH.`Invoice_Number`=_invoice_no and OL.`Pick_Location`='9999' limit 1 into  _Token_Drop;
		set _Token_Drop=case when _Token_Drop is null then '' else '<br/><span style=''color:green;''>OTHER MATERIAL</span>' end;
		
		IF LENGTH(_MEssageinfo)=0 then	
			Select Concat('<span style=''color:black;''>CRATE & INVOICE MAPPED SUCCESSFULLY.</span>',_Token_Drop ) 
			as EmptyLocation from
			(select sum(OLH.Pick_Qty) EmptyCount from  `tbl_order_line_header` OLH  
			inner Join `tbl_order_header` OH  on OH.`Order_id`=OLH.`Order_id`
			inner Join `tbl_carate_invoice_mapping` CR on CR.`Invoice_Number`=OH.`Invoice_Number`
			where (OLH.Pick_Location='-1') and OLH.`Pick_Qty`>0 
			and CR.`Invoice_Number`=_invoice_no) T where T.EmptyCount>0  limit 1 into _MEssageinfo;
		End if;
		if Length(_Error)>0 then
			select _Error as ErrorStatus;
		else
			select _MEssageinfo as  EmptyLocation;
		end if;	
		
	 Else 
		/*SELECT CASE WHEN CS.Status IS NULL THEN 'INVALID INVOICE/CRATE NUMBER' ELSE IFNULL(OH.`Invoice_Number`,'INVALID INVOICE/CRATE NUMBER') END  AS ErrorStatus FROM `tbl_order_header` OH 
		LEFT OUTER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
		LEFT OUTER JOIN `tbl_carate_status` CS  ON CS.`Invoice_No`=OH.`Invoice_Number`
		where OH.`Invoice_Number`=_invoice_no  LIMIT 1 into  _MEssageinfo; */
		IF Length(_MEssageinfo)=0 then
			select Concat('CRATE :',`Carate_Barcode`,' ALREADY INPROCESS FOR INVOICE :',`Invoice_Number` ,' ,MAPPED BY ',`Mapped_By`,'on ',`Mapping_Date`) as ErrorStatus 
			from `tbl_carate_invoice_mapping` CM 
			inner Join `tbl_carate_status` CS  on CS.`Carate_Code`=CM.Carate_Barcode  where CM.`Carate_Barcode`=_Carate_code 
			and CS.`Status` not in ('COMPLETED','ERROR') limit 1 into _MEssageinfo;
		End IF;
		SELECT _MEssageinfo AS  ErrorStatus;
	 End if;
	END IF;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_OrderDetailReport` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_OrderDetailReport` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_OrderDetailReport`(_fromdate datetime,_todate datetime)
BEGIN
		SELECT Mapping_Date,Invoice_Number,`Carate_Barcode` AS Crate, STATUS AS Remarks,`Actual_Weight`,`Capture_Weight`,(Capture_Weight-Actual_Weight) Difference 
		FROM `tbl_carate_invoice_mapping` M  
		WHERE Mapping_date BETWEEN _fromdate AND _todate;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_OrderSummaryReport` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_OrderSummaryReport` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_OrderSummaryReport`(_fromdate datetime,_todate datetime)
BEGIN

		SELECT A.Mapping_Date,A.MinMapping_Date AS FromTime,A.MaxMapping_Date  AS  ToTime,A.TotalInvoices,IFNULL(B.CompletedInvoice,0) as CompletedInvoice,
		IFNULL(C.ErrorInvoice,0) as ErrorInvoice,IFNULL(D.InprocessInvoice,0) AS InprocessInvoice,
		(IFNULL(B.CompletedInvoice,0)/(A.TotalInvoices-IFNULL(D.InprocessInvoice,0)))*100 'Success_Rate'   FROM (
		SELECT DATE(Mapping_Date) AS Mapping_Date,TIME(MAX(Mapping_Date)) AS MaxMapping_Date,TIME(MIN(Mapping_Date)) AS MinMapping_Date ,COUNT(Invoice_Number) TotalInvoices FROM 
		(SELECT Mapping_Date,Invoice_Number,STATUS FROM `tbl_carate_invoice_mapping` M  
		WHERE Mapping_date BETWEEN _fromdate AND _todate) A
		GROUP BY  DATE(Mapping_Date))  A
		LEFT OUTER JOIN 
		(SELECT DATE(Mapping_Date) AS Mapping_Date,COUNT(Invoice_Number) CompletedInvoice FROM 
		(SELECT Mapping_Date,Invoice_Number,STATUS FROM `tbl_carate_invoice_mapping` M  
		WHERE Mapping_date BETWEEN _fromdate AND _todate AND STATUS='COMPLETED') A
		GROUP BY  DATE(Mapping_Date)) B ON A.Mapping_Date=B.Mapping_Date
		LEFT OUTER JOIN 
		(SELECT DATE(Mapping_Date) AS Mapping_Date,COUNT(Invoice_Number) ErrorInvoice FROM 
		(SELECT Mapping_Date,Invoice_Number,STATUS FROM `tbl_carate_invoice_mapping` M  
		WHERE Mapping_date BETWEEN _fromdate AND _todate AND STATUS='ERROR') A
		GROUP BY  DATE(Mapping_Date)) C ON A.Mapping_Date=C.Mapping_Date
		LEFT OUTER JOIN 
		(SELECT DATE(Mapping_Date) AS Mapping_Date,COUNT(Invoice_Number) InprocessInvoice FROM 
		(SELECT Mapping_Date,Invoice_Number,STATUS FROM `tbl_carate_invoice_mapping` M  
		WHERE Mapping_date BETWEEN _fromdate AND _todate AND STATUS NOT IN ('COMPLETED','ERROR')) A
		GROUP BY  DATE(Mapping_Date)) D ON A.Mapping_Date=D.Mapping_Date;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_PickConfirmation` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_PickConfirmation` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PickConfirmation`(_mode varchar(10),_scan_code varchar(50),_ip varchar(30))
BEGIN
		Declare _sku_code varchar(30);
		declare _ser_no varchar(30);
		Declare _Carate_Code varchar(30);
		Declare _BayScanner_IP varchar(30);
		Declare _Login_User varchar(30);
		Declare _MSG  varchar(3);
		IF _mode='B1' then					
			SELECT IFNULL(Q.`Carate_Code`,'0') FROM  `tbl_request_queue_for_ptl` Q
			INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
			WHERE Q.`Pick_Location`=_scan_code AND ZM.`Zone_IPAddress`=_ip and  
			Q.Status='INPROCESS' AND ZM.Status='ACTIVE' LIMIT 1 INTO _Carate_Code;
			
			SELECT IFNULL(Q.`Scanner_IPAddress`,'0') FROM  `tbl_request_queue_for_ptl` Q
			INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
			WHERE Q.`Pick_Location`=_scan_code 
			and Q.Status='INPROCESS' AND ZM.`Zone_IPAddress`=_ip AND ZM.Status='ACTIVE' LIMIT 1 INTO _BayScanner_IP;
			
			Update `tbl_request_queue_for_ptl` Q 
			inner Join `tbl_zone_master` ZM  on ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
			set Q.`Status`='COMPLETED',Q.`PickConfirmation`=now(),Q.`Picked_Qty`=Q.`PickQty`,
			PickDuration=TIMESTAMPDIFF(MINUTE,NOW(),`PickStart`)
			where Q.`Pick_Location`=_scan_code and Q.Status='INPROCESS' and ZM.`Zone_IPAddress`=_ip and ZM.Status='ACTIVE';			
						 			
			IF not Exists (select Q.`QueueID`  from `tbl_request_queue_for_ptl` Q
			where Q.`Scanner_IPAddress`=_BayScanner_IP and Carate_Code=_Carate_Code and `Status`='INPROCESS') Then
				IF Exists (Select `QueueID` from `tbl_request_queue_for_ptl` 
					where `Carate_Code`=_Carate_Code and `Status`='PENDING') then
						SELECT  distinct ZM.Zone_IPAddress as PTL_IPAddress,ZPM.PTL as Pick_Location,ZM.`PTL_SendPort`,'PAS' AS PickQty,
						'GREEN1' as `Sku_Type`,ZM.`LCD_IPAddress`,ZM.`LCD_SendPort`,Q.`Carate_Code`
					        from `tbl_zone_master` ZM  
					        inner Join `tbl_zone_ptl_mapping` ZPM  on ZPM.Zone_ID=ZM.ID
					        inner Join tbl_request_queue_for_ptl Q on Q.Scanner_IPAddress=ZM.Scanner_IPAddress
						WHERE Q.`Status`='COMPLETED' AND Q.`Scanner_IPAddress`=_BayScanner_IP 
						and ZM.Status='ACTIVE' and Q.`Carate_Code`=_Carate_Code and ZPM.MSG=1 order by ZPM.PTL;
				else
						SELECT distinct ZM.Zone_IPAddress as PTL_IPAddress,ZPM.PTL AS Pick_Location,ZM.`PTL_SendPort`,'EXT' AS PickQty,
						'RED' AS `Sku_Type`,ZM.`LCD_IPAddress`,ZM.`LCD_SendPort`,Q.`Carate_Code`
					        FROM `tbl_zone_master` ZM  
					        INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.Zone_ID=ZM.ID					        
					        INNER JOIN tbl_request_queue_for_ptl Q ON Q.Scanner_IPAddress=ZM.Scanner_IPAddress
						WHERE Q.`Status`='COMPLETED' AND Q.`Scanner_IPAddress`=_BayScanner_IP 
						AND ZM.Status='ACTIVE' AND Q.`Carate_Code`=_Carate_Code AND ZPM.MSG=1 ORDER BY ZPM.PTL;
				end if;
			end if;
			
		else 
			if Exists(select `QueueID` from `tbl_request_queue_for_ptl` where `Sku_Type`!='NORMAL' and `Scanner_IPAddress`=_ip and Status='INPROCESS') then
			IF _scan_code!='LOGIN' and _scan_code!='LOGOUT' then
			if Not exists(select `Crate_Code` from `tbl_carate_master` where `Crate_Code`=_scan_code) then
				select `Login_Id` from `tbl_operator_login` where `Login_By`=_ip and `Logout_Datetime` is null  
				order by `ID` desc limit 1 into  _Login_User;
				if LOCATE('www.',_scan_code) <0 and Locate('.',_scan_code)>0 then
					set _sku_code=LEFT(_scan_code,LOCATE('.',_scan_code)-1); 
					set _ser_no=REVERSE(LEFT(REVERSE(_scan_code),LOCATE('.',REVERSE(_scan_code))-1));
					
					INSERT IGNORE INTO tbl_scan_Pick_details(`Carate_Code`,`Invoice_No`, ScanDatetime,`Sku`,`Ser_no`,`Scan_by`,QueueID,ScanedCode,`Login_User`)
					SELECT `Carate_Code`,`Invoice_Number`,Now(),_sku_code,_ser_no,_ip,`QueueID`,_scan_code,_Login_User FROM `tbl_request_queue_for_ptl` 
					WHERE `Sku` =_sku_code AND STATUS='INPROCESS' AND `Scanner_IPAddress`=_ip ;
					
					UPDATE `tbl_request_queue_for_ptl` SET `Picked_Qty`= (CASE WHEN `PickQty`>`Picked_Qty` THEN `Picked_Qty`+1 ELSE Picked_Qty END)
					WHERE `Sku` =_sku_code AND STATUS='INPROCESS' AND `Scanner_IPAddress`=_ip;
				
				/*else 
					set _ser_no=0;
					IF Not Exists(select `UserCode` from `tbl_users` where `UserCode`=_scan_code) then				
						INSERT IGNORE INTO tbl_scan_Pick_details(`Carate_Code`,`Invoice_No`,`ScanDatetime`, `Sku`,`Ser_no`,`Scan_by`,QueueID,ScanedCode,`Login_User`)
						SELECT `Carate_Code`,`Invoice_Number`,now(),_sku_code,_ser_no,_ip,0,_scan_code,_Login_User FROM `tbl_request_queue_for_ptl` 
						WHERE  STATUS='INPROCESS' AND `Scanner_IPAddress`=_ip limit 1 ;
					end if;*/
				end if;
				
				IF Exists (Select `Carate_Code` from `tbl_request_queue_for_ptl` 
						where `Sku` =_scan_code  and (`PickQty`-`Picked_Qty`)>0 AND STATUS='INPROCESS') then
					SELECT  Q.`PTL_IPAddress`,Q.`Pick_Location`,Q.`PTL_SendPort`,
					CONCAT(REPEAT('0', 3- LENGTH((`PickQty`-`Picked_Qty`))),(`PickQty`-`Picked_Qty`))  AS PickQty,`Sku_Type`,`LCD_IPAddress`,`LCD_SendPort`
					FROM `tbl_request_queue_for_ptl` Q
					WHERE Q.`Sku` =_scan_code AND Q.STATUS='INPROCESS' and Q.`Scanner_IPAddress`=_ip;
				else
					SELECT `Carate_Code` FROM  `tbl_request_queue_for_ptl` 
					WHERE Scanner_IPAddress=_ip AND `Status`='INPROCESS' ORDER BY QueueID DESC LIMIT 1 INTO _Carate_Code;
					
					UPDATE `tbl_request_queue_for_ptl` SET `Status`='COMPLETED',`PickConfirmation`=NOW(),
					PickDuration=TIMESTAMPDIFF(MINUTE,NOW(),`PickStart`)
					WHERE `Sku` =_scan_code  AND STATUS='INPROCESS';		
					
					IF NOT EXISTS (SELECT `QueueID`  FROM `tbl_request_queue_for_ptl` 
					WHERE `Scanner_IPAddress`=_ip AND Carate_Code= _Carate_Code and  `Status`='INPROCESS') THEN
						IF EXISTS (SELECT `QueueID` FROM `tbl_request_queue_for_ptl` 
							WHERE `Carate_Code`=_Carate_Code AND `Status`='PENDING') THEN
							SELECT  DISTINCT ZM.Zone_IPAddress,ZPM.PTL AS Pick_Location,ZM.`PTL_SendPort`,
							'PAS' AS PickQty,'GREEN1' AS `Sku_Type`,ZM.`LCD_IPAddress`,ZM.`LCD_SendPort`,Q.`Carate_Code`
							FROM `tbl_zone_master` ZM  
							INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.Zone_ID=ZM.ID
							INNER JOIN tbl_request_queue_for_ptl Q ON Q.Scanner_IPAddress=ZM.Scanner_IPAddress
							WHERE Q.`Status`='COMPLETED' AND Q.`Scanner_IPAddress`=_ip 
							AND ZM.Status='ACTIVE' AND Q.`Carate_Code`=_Carate_Code AND ZPM.MSG=1 ORDER BY ZPM.PTL;
						ELSE
							SELECT  DISTINCT ZM.Zone_IPAddress,ZPM.PTL AS Pick_Location,ZM.`PTL_SendPort`,
							'EXT' AS PickQty,'RED' AS `Sku_Type`,ZM.`LCD_IPAddress`,ZM.`LCD_SendPort`,Q.`Carate_Code`
							FROM `tbl_zone_master` ZM  
							INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.Zone_ID=ZM.ID
							INNER JOIN tbl_request_queue_for_ptl Q ON Q.Scanner_IPAddress=ZM.Scanner_IPAddress
							WHERE Q.`Status`='COMPLETED' AND Q.`Scanner_IPAddress`=_ip 
							AND ZM.Status='ACTIVE' AND Q.`Carate_Code`=_Carate_Code AND ZPM.MSG=1 ORDER BY ZPM.PTL;
						END IF;
					END IF;
				End if;
			End if;
			End if;
			End if;			  
		End IF;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_ProductTracking` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_ProductTracking` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ProductTracking`(_fromdate date,_todate date)
BEGIN
		select PickingDate,sum(TotalInvoice) as TotalInvoice,sum(ScannableQty) as ScannableQty,sum(ScannnedQty) as ScannnedQty from (
		SELECT PickDate  AS PickingDate,TotalInvoice,ScannableQty,B.ScannnedQty FROM (
		SELECT  DATE(`PickStart`) AS PickDate,COUNT(DISTINCT `Invoice_Number`) TotalInvoice,SUM(`PickQty`) AS ScannableQty FROM `tbl_request_queue_for_ptl` Q
		WHERE DATE(`PickStart`) BETWEEN _fromdate AND _todate AND `Sku_Type`!='Normal'
		GROUP BY DATE(`PickStart`)) A LEFT OUTER JOIN (
		SELECT DATE(`ScanDatetime`) ScanDatetime,COUNT(DISTINCT `ScanedCode`) AS ScannnedQty FROM `tbl_scan_pick_details`
		WHERE DATE(`ScanDatetime`) BETWEEN _fromdate AND _todate AND `WareHouse`=40 and `Invoice_No` is not null
		GROUP BY DATE(ScanDatetime)) B  ON A.PickDate=B.ScanDatetime
		union
		SELECT PickDate  AS PickingDate,TotalInvoice,ScannableQty,B.ScannnedQty FROM (
		SELECT  DATE(`PickStart`) AS PickDate,COUNT(DISTINCT `Invoice_Number`) TotalInvoice,SUM(`PickQty`) AS ScannableQty 
		FROM `tbl_request_queue_for_ptl_archive` Q
		WHERE DATE(`PickStart`) BETWEEN _fromdate AND _todate AND `Sku_Type`!='Normal'
		GROUP BY DATE(`PickStart`)) A LEFT OUTER JOIN (
		SELECT DATE(`ScanDatetime`) ScanDatetime,COUNT(DISTINCT `ScanedCode`) AS ScannnedQty FROM `tbl_scan_pick_details_archive`
		WHERE DATE(`ScanDatetime`) BETWEEN _fromdate AND _todate AND `WareHouse`=40 and `Invoice_No` is not null
		GROUP BY DATE(ScanDatetime)) B  ON A.PickDate=B.ScanDatetime) P
		group by PickingDate order by PickingDate;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_Request_for_PTL` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_Request_for_PTL` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Request_for_PTL`(_ScanCode varchar(30),_scanner_ip varchar(30),_mode int)
BEGIN
/* TRIM(TRAILING '\r\n' FROM Scanner_IPAddress)*/
 /* _mode :=> 0--> Login,
	      1--> Logout,
	      2--> Pick */
	      Declare _Crate_code varchar(100);
	      Declare _Login_user varchar(30);
	      DEclare _inv varchar(30);
	      select `Login_Id` from `tbl_operator_login` where `Login_By`=_scanner_ip 
	      and `Logout_Datetime` is null order by `ID` desc limit 1 into  _Login_user; 
		IF _mode=0 then
			if exists (select `User_ID` from `tbl_users` where UserCode=_ScanCode and `user_category`='OPERATOR') then
				if Not Exists (select Status from tbl_zone_master where Scanner_IPAddress=_scanner_ip and Status='ACTIVE') then
					insert into `tbl_operator_login` (`Login_Id`,`Login_DateTime`,`Login_By`)
					values(_ScanCode,now(),_scanner_ip);
					
					Update `tbl_zone_master` set `Status`='ACTIVE'  where `Scanner_IPAddress`=_scanner_ip;
					
					Select distinct `Zone_IPAddress` as PTL_IPAddress,`LCD_IPAddress`,`PTL_SendPort`,`LCD_SendPort` from `tbl_zone_master` ZM 
					inner Join `tbl_zone_ptl_mapping` ZPM  on ZPM.`Zone_ID`=ZM.`Zone_Id`
					where Scanner_IPAddress=_scanner_ip ;
				End if;
			end if;
		End if;
		if _mode=1 then
			update tbl_operator_login set `Logout_Datetime`=now(),`Duration`=Timestampdiff(minute,Login_DateTime,NOw())
			where `Login_By`=_scanner_ip and Logout_Datetime is null;
			
			UPDATE `tbl_zone_master` SET `Status`='INACTIVE'  WHERE `Scanner_IPAddress`=_scanner_ip;
			
			SELECT distinct `Zone_IPAddress` as PTL_IPAddress,`LCD_IPAddress`,`PTL_SendPort`,`LCD_SendPort` FROM `tbl_zone_master` ZM 
			INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.`Zone_ID`=ZM.`Zone_Id`
			WHERE Scanner_IPAddress=_scanner_ip;
		end if;
		if _mode=2 then	
			IF Exists(select `QueueID` from `tbl_request_queue_for_ptl` 
				where `Carate_Code`=_ScanCode and Scanner_IPAddress=_scanner_ip and Status in ('PENDING','INPROCESS')) then
				
			   SELECT Invoice_Number FROM `tbl_carate_invoice_mapping` WHERE `Carate_Barcode`=_ScanCode 
			   ORDER BY `Pick_id` DESC LIMIT 1 INTO  _Inv;
	       
			   Select Q.`Carate_Code`, Q.`PTL_IPAddress`,Q.`Pick_Location`,Q.`PTL_SendPort`,
			   CONCAT(REPEAT('0', 3- LENGTH((`PickQty`-`Picked_Qty`))),(`PickQty`-`Picked_Qty`))  as PickQty,Q.`Sku_Type`,Q.`LCD_IPAddress`,Q.`LCD_SendPort`
			   from `tbl_request_queue_for_ptl` Q 
			   inner Join `tbl_zone_master` ZM  on ZM.`Zone_IPAddress`=Q.PTL_IPAddress
			   where Q.`Scanner_IPAddress`=_scanner_ip  and Q.`Carate_Code`=_ScanCode and Q.invoice_Number=_Inv
			   and (Q.Status='PENDING' or `PickConfirmation` is null)
			   and ZM.Status='ACTIVE' and (Q.`PickQty`-Q.`Picked_Qty`)>0 order by Q.`PTL_IPAddress`,Q.`Pick_Location`;
			   
			   Update tbl_request_queue_for_ptl Q
			   INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.PTL_IPAddress
			   set Q.STATUS='INPROCESS' ,Q.`PickStart`=now() ,`Operator_id`=_Login_user
			   where Q.`Scanner_IPAddress`=_scanner_ip AND Q.`Carate_Code`=_ScanCode  
			   AND Q.STATUS='PENDING' AND ZM.Status='ACTIVE' AND Q.invoice_Number=_Inv;			   
			  			   
			   UPDATE `tbl_carate_invoice_mapping`  SET `Status`='INPROCESS',`FirstPick`=NOW() WHERE `Carate_Barcode`= _ScanCode
			   AND STATUS='INDUCTED' and  invoice_Number=_Inv;
			   
			   UPDATE  `tbl_request_queue_for_ptl` SET DisplayMSG=0
			   WHERE DisplayMSG=1 AND `Scanner_IPAddress`=_scanner_ip AND `Carate_Code`= _ScanCode and invoice_Number=_Inv;
		      Else
		       SELECT Invoice_Number FROM `tbl_carate_invoice_mapping` WHERE `Carate_Barcode`=_ScanCode 
			ORDER BY `Pick_id` DESC LIMIT 1 INTO  _Inv;
	       
		        IF Exists (Select `Crate_Code`  from `tbl_carate_master` where `Crate_Code`=_ScanCode) then
				IF not Exists (SELECT `QueueID` FROM `tbl_request_queue_for_ptl` 
					WHERE `Carate_Code`=_ScanCode AND Scanner_IPAddress=_scanner_ip AND STATUS in ('PENDING','INPROCESS') AND `Invoice_Number`=_Inv) then
						IF EXISTS (SELECT `QueueID` FROM `tbl_request_queue_for_ptl` 
						WHERE `Carate_Code`=_ScanCode AND `Status`='PENDING' AND `Invoice_Number`=_Inv) THEN
								SELECT  DISTINCT ZM.Zone_IPAddress AS PTL_IPAddress,ZPM.PTL AS Pick_Location,ZM.`PTL_SendPort`,'PAS' AS PickQty,
								'GREEN1' AS `Sku_Type`,ZM.`LCD_IPAddress`,ZM.`LCD_SendPort`, _ScanCode as `Carate_Code`
								FROM `tbl_zone_master` ZM  
								INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.Zone_ID=ZM.ID
								WHERE ZM.`Scanner_IPAddress`=_scanner_ip AND ZM.Status='ACTIVE' AND ZPM.MSG=1 order by ZM.Zone_IPAddress,ZPM.PTL;
						ELSE
								SELECT DISTINCT ZM.Zone_IPAddress AS PTL_IPAddress,ZPM.PTL AS Pick_Location,ZM.`PTL_SendPort`,'EXT' AS PickQty,
								'RED' AS `Sku_Type`,ZM.`LCD_IPAddress`,ZM.`LCD_SendPort`,_ScanCode AS `Carate_Code`
								FROM `tbl_zone_master` ZM  
								INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.Zone_ID=ZM.ID					        
								WHERE ZM.`Scanner_IPAddress`=_scanner_ip AND ZM.Status='ACTIVE' AND ZPM.MSG=1 ORDER BY ZM.Zone_IPAddress,ZPM.PTL;
						END IF;
				End IF;
			Else
				SELECT DISTINCT ZM.Zone_IPAddress AS PTL_IPAddress,ZPM.PTL AS Pick_Location,ZM.`PTL_SendPort`,'ERR' AS PickQty,
				'RED' AS `Sku_Type`,ZM.`LCD_IPAddress`,ZM.`LCD_SendPort`,Q.`Carate_Code`
				FROM `tbl_zone_master` ZM  
				INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.Zone_ID=ZM.ID					        
				WHERE ZM.Scanner_IPAddress=_scanner_ip AND ZM.Status='ACTIVE' AND ZPM.MSG=1;
			End if;
		      ENd IF;
		End if;
		if _mode=3 then
			update `tbl_zone_master` set `Status`='LOGIN' 
			where `Scanner_IPAddress`=_scanner_ip and Status !='ACTIVE';
		end if;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_ResetData` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_ResetData` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ResetData`()
BEGIN
        Truncate table `tbl_carate_invoice_mapping`;
        Truncate table `tbl_carate_status`;
        Truncate  table `tbl_carate_status_archive`;
        truncate table `tbl_request_queue_for_ptl`;
        truncate table `tbl_scan_pick_details`;
        truncate table `tbl_operator_login`;
        Truncate table `tbl_order_header`;
        truncate  table `tbl_order_line_header`;
        truncate table `tbl_file_master`;
        Update `tbl_carate_master` set isActive=1;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_SearchMapping` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_SearchMapping` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_SearchMapping`(_condition varchar(1000))
BEGIN
		/*SET @t1 =CONCAT("SELECT zpm.Zone_ID AS BayName,pm.PTLLocation,sm.sku_code,sm.Description,sm.Alternate_Sku_Code FROM zone_ptl_mapping zpm 
				INNER JOIN ptl_sku_mapping psm ON psm.PTL_code=zpm.PTL_Code
				INNER JOIN sku_master sm ON sm.SKU_ID=psm.SKU_ID 
				INNER JOIN ptl_master pm ON pm.PTL_code=psm.PTL_code WHERE " , _condition);*/
		 SET @t1=_condition;
		 PREPARE stmt3 FROM @t1;
		 EXECUTE stmt3;
		 DEALLOCATE PREPARE stmt3;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_UploadQrCode` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_UploadQrCode` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_UploadQrCode`( _get INT, _file_name VARCHAR(100),_user VARCHAR(30),_QrCode VARCHAR(30),_Pick_Location VARCHAR(30))
BEGIN
		DECLARE _id INT;
		IF _get=0 THEN
			IF _Pick_Location='' THEN
				UPDATE `tbl_config_master` SET `isActive`=0 WHERE `isActive`=1;
				INSERT INTO `tbl_config_master`(`File_Name`,`UploadBy`)
				VALUES(_file_name,_user);
			ELSE 	
				SELECT `Configid` FROM `tbl_config_master` WHERE `File_Name`=_file_name ORDER BY `Configid` DESC LIMIT 1 INTO _id;
				INSERT INTO `tbl_ptl_sku_mapping`(`Pick_Location`,`Sku`,`Config_id`,Sku_Type)
				VALUES(_Pick_Location,_QrCode,_id,'SCANBASED');
				
				UPDATE `tbl_zone_ptl_mapping` SET `Sku_Type`='NORMAL' WHERE Sku_Type!='NORMAL';
				UPDATE tbl_zone_ptl_mapping ZPM INNER JOIN tbl_ptl_sku_mapping S  ON S.Pick_Location=ZPM.`Pick_Location`
				SET ZPM.`PTL_Type`='SCANBASED';
			END IF;
		ELSE
			SELECT PS.`Pick_Location`,PS.`Sku` AS QrCode,C.`UploadBy` as UploadedBy,C.`UploadDate` as UploadedDate,ZPM.PTL FROM `tbl_ptl_sku_mapping` PS
			INNER JOIN `tbl_config_master` C  ON C.`Configid`=PS.`Config_id`
			INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.`Pick_Location`=PS.Pick_Location
			WHERE C.`isActive`=1;
		END IF;
		
		

		
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_ValidateCarton_code` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_ValidateCarton_code` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ValidateCarton_code`(_Carton_code varchar(100),_IPAddress varchar(30))
BEGIN
	     DEclare _ccode varchar(30);
	     Declare _ip varchar(30);
	     Declare _Zone_Type varchar(100);
             Select Carate_Barcode from `tbl_carate_invoice_mapping` OL  where `Carate_Barcode`=_Carton_code 
             and Status not in ('ERROR','COMPLETED') order by `Pick_id` desc limit 1 into _ccode;
             Select `Zone_IPAddress` from tbl_Zone_Master  where Scanner_IPAddress=_IPAddress limit 1 into _ip;
             select `PTL_Type`  from `tbl_zone_ptl_mapping` 
             where PTL_Type='SCANNABLE' and `PTL` in (
				select pick_location from `tbl_request_queue_for_ptl` where `Carate_Code`=_Carton_code and `Scanner_IPAddress`=_IPAddress
			) limit  1 into _Zone_Type;
             select IFNULL(_ccode,'') as Invoice_Number,_ip as Zone_IP_Address,IFNULL(_Zone_Type,'NORMAL') as Zone_Type;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_WeightTolerance` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_WeightTolerance` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_WeightTolerance`(_ipaddress varchar(30),_tolerance decimal(10,3),_flag bit)
BEGIN
           IF _flag=0 then
		Begin
			Select `From_Weight`,`To_Weight`,`Tolerance_per`,`Static_Tolerance` from `tbl_weight_tolerance_config`;
		End;
	   Else
		Begin
		       Update weight_config set isActive=0 ,DeActivation_Date=now() where isActive=1;
		       
		       insert into weight_config(IPAddress,Min_Percent,Max_Percent)
		       values(_ipaddress,_tolerance,_tolerance);
		       
		       SELECT IPAddress,Min_Percent FROM weight_config WHERE isActive=1;
		 End;
	 End IF;
	END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
