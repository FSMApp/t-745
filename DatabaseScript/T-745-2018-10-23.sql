/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 10.1.32-MariaDB : Database - dwh_falcon
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`dwh_falcon` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `dwh_falcon`;

/*Table structure for table `error_log` */

DROP TABLE IF EXISTS `error_log`;

CREATE TABLE `error_log` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `source` varchar(20) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `Error_Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `error_log` */

insert  into `error_log`(`ID`,`source`,`message`,`Error_Date`) values 
(1,'kjlk;','ljljl;','2018-10-13 18:45:15'),
(2,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 38','2018-10-15 17:41:54'),
(3,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 85','2018-10-15 17:41:54'),
(4,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 38','2018-10-15 17:44:00'),
(5,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 85','2018-10-15 17:44:00'),
(6,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 38','2018-10-15 17:55:13'),
(7,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 85','2018-10-15 17:55:13'),
(8,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 38','2018-10-15 18:06:49'),
(9,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 85','2018-10-15 18:06:49'),
(10,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 38','2018-10-15 18:15:31'),
(11,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 85','2018-10-15 18:15:31'),
(12,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 43','2018-10-15 18:47:16'),
(13,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 90','2018-10-15 18:47:16'),
(14,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 43','2018-10-15 18:49:24'),
(15,'API','Microsoft.CSharp.RuntimeBinder.RuntimeBinderException: \'Newtonsoft.Json.Linq.JProperty\' does not contain a definition for \'invoice_number\'\r\n   at CallSite.Target(Closure , CallSite , Object )\r\n   at System.Dynamic.UpdateDelegates.UpdateAndExecute1[T0,TRet](CallSite site, T0 arg0)\r\n   at Oriflame_API.Controllers.PushInvoiceController.Post(Object data) in D:\\Projects\\t-745\\Oriflame_API\\Oriflame_API\\Controllers\\PushInvoiceController.cs:line 90','2018-10-15 18:49:24');

/*Table structure for table `tbl_carate_invoice_mapping` */

DROP TABLE IF EXISTS `tbl_carate_invoice_mapping`;

CREATE TABLE `tbl_carate_invoice_mapping` (
  `Pick_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Barcode` varchar(30) DEFAULT NULL,
  `Source_IPAddress` varchar(30) DEFAULT NULL,
  `Scanner_IP` varchar(30) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Mapping_Date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Mapped_By` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Pick_id`),
  KEY `Carate_Code` (`Carate_Barcode`),
  KEY `Invoice_Number` (`Invoice_Number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_carate_invoice_mapping` */

/*Table structure for table `tbl_carate_invoice_mapping_archive` */

DROP TABLE IF EXISTS `tbl_carate_invoice_mapping_archive`;

CREATE TABLE `tbl_carate_invoice_mapping_archive` (
  `Pick_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Barcode` varchar(30) DEFAULT NULL,
  `Source_IPAddress` varchar(30) DEFAULT NULL,
  `Scanner_IP` varchar(30) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Mapping_Date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Mapped_By` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`Pick_id`),
  KEY `Carate_Code` (`Carate_Barcode`),
  KEY `Invoice_Number` (`Invoice_Number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_carate_invoice_mapping_archive` */

/*Table structure for table `tbl_carate_master` */

DROP TABLE IF EXISTS `tbl_carate_master`;

CREATE TABLE `tbl_carate_master` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Crate_Code` varchar(20) DEFAULT NULL,
  `Crate_Weight` decimal(10,2) DEFAULT NULL,
  `AddedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isActive` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Crate_Code` (`Crate_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_carate_master` */

/*Table structure for table `tbl_carate_status` */

DROP TABLE IF EXISTS `tbl_carate_status`;

CREATE TABLE `tbl_carate_status` (
  `Pick_Status` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `Actual_Weight` decimal(10,3) DEFAULT NULL,
  `Capture_Weight` decimal(10,3) DEFAULT NULL,
  `Status` enum('INDUCTED','INPROCESS','COMPLETED','ERROR') DEFAULT 'INDUCTED',
  `Inducted` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `FirstPick` datetime DEFAULT NULL,
  `Weiging` datetime DEFAULT NULL,
  `Weighing_IP` varchar(30) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`Pick_Status`),
  UNIQUE KEY `Carate_Code` (`Carate_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_carate_status` */

/*Table structure for table `tbl_carate_status_archive` */

DROP TABLE IF EXISTS `tbl_carate_status_archive`;

CREATE TABLE `tbl_carate_status_archive` (
  `Pick_Status` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `Actual_Weight` decimal(10,3) DEFAULT NULL,
  `Capture_Weight` decimal(10,3) DEFAULT NULL,
  `Status` enum('INDUCTED','INPROCESS','COMPLETED','ERROR') DEFAULT 'INDUCTED',
  `Inducted` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `FirstPick` datetime DEFAULT NULL,
  `Weiging` datetime DEFAULT NULL,
  `Weighing_IP` varchar(30) DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`Pick_Status`),
  UNIQUE KEY `Carate_Code` (`Carate_Code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_carate_status_archive` */

/*Table structure for table `tbl_email_master` */

DROP TABLE IF EXISTS `tbl_email_master`;

CREATE TABLE `tbl_email_master` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Email_id` varchar(100) DEFAULT NULL,
  `Frequency` enum('Daily','Weekly','Monthly','Yearly') DEFAULT 'Daily',
  `LastEmailSent` datetime DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_email_master` */

/*Table structure for table `tbl_hour_weekday_monthlist` */

DROP TABLE IF EXISTS `tbl_hour_weekday_monthlist`;

CREATE TABLE `tbl_hour_weekday_monthlist` (
  `ID` int(5) DEFAULT NULL,
  `Value` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `Type` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `Quarter` varchar(2) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `tbl_hour_weekday_monthlist` */

insert  into `tbl_hour_weekday_monthlist`(`ID`,`Value`,`Type`,`Quarter`) values 
(1,'1','H',NULL),
(2,'2','H',NULL),
(3,'3','H',NULL),
(4,'4','H',NULL),
(5,'5','H',NULL),
(6,'6','H',NULL),
(7,'7','H',NULL),
(8,'8','H',NULL),
(9,'9','H',NULL),
(10,'10','H',NULL),
(11,'11','H',NULL),
(12,'12','H',NULL),
(13,'13','H',NULL),
(14,'14','H',NULL),
(15,'15','H',NULL),
(16,'16','H',NULL),
(17,'17','H',NULL),
(18,'18','H',NULL),
(19,'19','H',NULL),
(20,'20','H',NULL),
(21,'21','H',NULL),
(22,'22','H',NULL),
(23,'23','H',NULL),
(1,'Monday','W',NULL),
(2,'Tuesday','W',NULL),
(3,'Wednesday','W',NULL),
(4,'Thursday','W',NULL),
(5,'Friday','W',NULL),
(6,'Saturday','W',NULL),
(7,'Sunday','W',NULL),
(10,'October','M','Q4'),
(11,'November','M','Q4'),
(12,'December','M','Q4'),
(1,'January','M','Q1'),
(2,'February','M','Q1'),
(3,'March','M','Q1'),
(4,'April','M','Q2'),
(5,'May','M','Q2'),
(6,'June','M','Q2'),
(7,'July','M','Q3'),
(8,'August','M','Q3'),
(9,'September','M','Q3'),
(1,'1','D',NULL),
(20,'20','D',NULL),
(3,'3','D',NULL),
(14,'14','D',NULL),
(5,'5','D',NULL),
(6,'6','D',NULL),
(7,'7','D',NULL),
(8,'8','D',NULL),
(9,'9','D',NULL),
(10,'10','D',NULL),
(11,'11','D',NULL),
(12,'12','D',NULL),
(13,'13','D',NULL),
(14,'14','D',NULL),
(15,'15','D',NULL),
(16,'16','D',NULL),
(17,'17','D',NULL),
(18,'18','D',NULL),
(19,'19','D',NULL),
(20,'20','D',NULL),
(29,'29','D',NULL),
(22,'22','D',NULL),
(23,'23','D',NULL),
(24,'24','D',NULL),
(25,'25','D',NULL),
(26,'26','D',NULL),
(27,'27','D',NULL),
(28,'28','D',NULL),
(29,'29','D',NULL),
(30,'30','D',NULL),
(31,'31','D',NULL),
(2,'2','D',NULL);

/*Table structure for table `tbl_operator_login` */

DROP TABLE IF EXISTS `tbl_operator_login`;

CREATE TABLE `tbl_operator_login` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Login_Id` varchar(30) DEFAULT NULL,
  `Login_DateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Logout_Datetime` datetime DEFAULT NULL,
  `Duration` bigint(20) DEFAULT NULL,
  `Login_By` varchar(30) DEFAULT NULL COMMENT 'Scanner IP Address',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Login_Info` (`Login_Id`,`Login_DateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_operator_login` */

/*Table structure for table `tbl_operator_login_archive` */

DROP TABLE IF EXISTS `tbl_operator_login_archive`;

CREATE TABLE `tbl_operator_login_archive` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Login_Id` varchar(30) DEFAULT NULL,
  `Login_DateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `Logout_Datetime` datetime DEFAULT NULL,
  `Duration` bigint(20) DEFAULT NULL,
  `Login_By` varchar(30) DEFAULT NULL COMMENT 'Scanner IP Address',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Login_Info` (`Login_Id`,`Login_DateTime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_operator_login_archive` */

/*Table structure for table `tbl_order_header` */

DROP TABLE IF EXISTS `tbl_order_header`;

CREATE TABLE `tbl_order_header` (
  `Order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Order_Number` varchar(30) DEFAULT NULL,
  `Order_Datetime` datetime DEFAULT NULL,
  `Order_Weight` decimal(10,3) DEFAULT NULL,
  `Order_Status` varchar(20) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Invoice_Date` datetime DEFAULT NULL,
  `Warehouse_id` varchar(5) DEFAULT NULL,
  `Line_Items` int(11) DEFAULT NULL,
  `Sales_date` datetime DEFAULT NULL,
  `due_Date` datetime DEFAULT NULL,
  `Delivery_date` datetime DEFAULT NULL,
  `Added_Datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `File_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`Order_id`),
  UNIQUE KEY `Invoice` (`Invoice_Number`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_order_header` */

insert  into `tbl_order_header`(`Order_id`,`Order_Number`,`Order_Datetime`,`Order_Weight`,`Order_Status`,`Invoice_Number`,`Invoice_Date`,`Warehouse_id`,`Line_Items`,`Sales_date`,`due_Date`,`Delivery_date`,`Added_Datetime`,`File_ID`) values 
(1,'29980926','0000-00-00 00:00:00',2.585,'','501819050245','0000-00-00 00:00:00','50',9,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-04 17:28:26',NULL),
(2,'29981101','0000-00-00 00:00:00',1.781,'','501819050246','0000-00-00 00:00:00','50',6,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-04 17:28:51',NULL),
(3,'29981335','0000-00-00 00:00:00',1.393,'','501819050247','0000-00-00 00:00:00','50',10,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-04 17:28:56',NULL),
(4,'29981800','0000-00-00 00:00:00',2.386,'','501819050248','0000-00-00 00:00:00','50',10,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-04 17:28:57',NULL),
(5,'29986394','0000-00-00 00:00:00',0.578,'','501819050250','0000-00-00 00:00:00','50',4,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-04 17:28:59',NULL),
(6,'32710992','0000-00-00 00:00:00',1.852,'','401819147796','0000-00-00 00:00:00','40',10,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 17:44:30',NULL),
(7,'32710993','0000-00-00 00:00:00',2.612,'','401819147797','0000-00-00 00:00:00','40',16,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:34',NULL),
(8,'32710994','0000-00-00 00:00:00',1.379,'','401819147798','0000-00-00 00:00:00','40',13,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:34',NULL),
(9,'32710995','0000-00-00 00:00:00',3.169,'','401819147799','0000-00-00 00:00:00','40',16,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:35',NULL),
(10,'32710997','0000-00-00 00:00:00',1.961,'','401819147801','0000-00-00 00:00:00','40',8,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:36',NULL),
(11,'32710998','0000-00-00 00:00:00',5.160,'','401819147802','0000-00-00 00:00:00','40',15,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:36',NULL),
(12,'32710999','0000-00-00 00:00:00',1.807,'','401819147803','0000-00-00 00:00:00','40',16,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:37',NULL),
(13,'32711000','0000-00-00 00:00:00',1.961,'','401819147804','0000-00-00 00:00:00','40',8,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:38',NULL),
(14,'32711003','0000-00-00 00:00:00',4.287,'','401819147807','0000-00-00 00:00:00','40',14,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:38',NULL),
(15,'32711004','0000-00-00 00:00:00',6.339,'','401819147808','0000-00-00 00:00:00','40',14,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:39',NULL),
(16,'32711006','0000-00-00 00:00:00',2.394,'','401819147810','0000-00-00 00:00:00','40',14,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:39',NULL),
(17,'32711008','0000-00-00 00:00:00',2.353,'','401819147812','0000-00-00 00:00:00','40',14,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:41',NULL),
(18,'32711010','0000-00-00 00:00:00',2.718,'','401819147814','0000-00-00 00:00:00','40',17,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:41',NULL),
(19,'32711011','0000-00-00 00:00:00',2.111,'','401819147815','0000-00-00 00:00:00','40',14,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 18:14:42',NULL),
(20,'32710623','0000-00-00 00:00:00',1.709,'','401819147465','0000-00-00 00:00:00','40',16,'2018-10-16 16:25:41','0000-00-00 00:00:00','0000-00-00 00:00:00','2018-10-15 19:25:15',636752282901039215);

/*Table structure for table `tbl_order_header_archive` */

DROP TABLE IF EXISTS `tbl_order_header_archive`;

CREATE TABLE `tbl_order_header_archive` (
  `Order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Order_Number` varchar(30) DEFAULT NULL,
  `Order_Datetime` datetime DEFAULT NULL,
  `Order_Weight` decimal(10,3) DEFAULT NULL,
  `Order_Status` varchar(20) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Invoice_Date` datetime DEFAULT NULL,
  `Warehouse_id` varchar(5) DEFAULT NULL,
  `Line_Items` int(11) DEFAULT NULL,
  `Sales_date` datetime DEFAULT NULL,
  `due_Date` datetime DEFAULT NULL,
  `Delivery_date` datetime DEFAULT NULL,
  `Added_Datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_order_header_archive` */

/*Table structure for table `tbl_order_line_header` */

DROP TABLE IF EXISTS `tbl_order_line_header`;

CREATE TABLE `tbl_order_line_header` (
  `Order_line_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Order_id` bigint(20) DEFAULT NULL,
  `Line_Item_Seq_no` int(11) DEFAULT NULL COMMENT 'line_number',
  `Sku` varchar(30) DEFAULT NULL COMMENT 'product_code',
  `Pick_Qty` int(11) DEFAULT NULL COMMENT 'order_quantity',
  `Pick_Location` varchar(20) DEFAULT NULL COMMENT 'Station',
  `Location` varchar(20) DEFAULT NULL COMMENT 'Location',
  `Added_DateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Order_line_Id`),
  UNIQUE KEY `Unique_Pick` (`Order_id`,`Sku`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_order_line_header` */

insert  into `tbl_order_line_header`(`Order_line_Id`,`Order_id`,`Line_Item_Seq_no`,`Sku`,`Pick_Qty`,`Pick_Location`,`Location`,`Added_DateTime`) values 
(1,2,1,'32176',2,'1064','PL','2018-10-03 12:33:32'),
(2,2,2,'31704',1,'962','PL','2018-10-03 12:33:35'),
(3,2,3,'124274',1,'','','2018-10-03 12:33:36'),
(4,3,1,'34414',2,'933','PL','2018-10-03 12:33:45'),
(5,3,2,'124274',1,'','','2018-10-03 12:33:46'),
(6,3,3,'34413',2,'1073','PL','2018-10-03 12:33:47'),
(7,3,4,'26659',1,'462','PL','2018-10-03 12:33:49'),
(8,1,1,'124274',1,'10','PL','2018-10-04 17:28:30'),
(9,1,2,'124518',1,'10','PL','2018-10-04 17:28:31'),
(11,1,4,'124998',1,'10','PL','2018-10-04 17:28:34'),
(12,1,5,'125581',1,'10','PL','2018-10-04 17:28:35'),
(13,1,6,'32463',1,'961','PL','2018-10-04 17:28:37'),
(14,1,7,'32576',1,'1365','PL','2018-10-04 17:28:38'),
(15,1,8,'28968',1,'1506','PL','2018-10-04 17:28:39'),
(16,1,9,'123386',1,'9999','PL','2018-10-04 17:28:40'),
(18,2,2,'30796',1,'213','PL','2018-10-04 17:28:54'),
(19,2,3,'32255',1,'341','PL','2018-10-04 17:28:56'),
(20,2,4,'32576',1,'1365','PL','2018-10-04 17:28:56'),
(21,2,5,'28968',1,'1506','PL','2018-10-04 17:28:56'),
(22,2,6,'123386',1,'9999','PL','2018-10-04 17:28:56'),
(24,3,2,'124518',1,'10','PL','2018-10-04 17:28:56'),
(26,3,4,'124998',1,'10','PL','2018-10-04 17:28:56'),
(27,3,5,'125581',1,'10','PL','2018-10-04 17:28:57'),
(28,3,6,'126211',1,'10','PL','2018-10-04 17:28:57'),
(29,3,7,'126619',1,'10','PL','2018-10-04 17:28:57'),
(30,3,8,'32463',1,'961','PL','2018-10-04 17:28:57'),
(31,3,9,'32576',1,'1365','PL','2018-10-04 17:28:57'),
(32,3,10,'566429',1,'','PL','2018-10-04 17:28:57'),
(33,4,1,'124274',1,'10','PL','2018-10-04 17:28:57'),
(34,4,2,'124518',5,'10','PL','2018-10-04 17:28:58'),
(36,4,4,'124998',1,'10','PL','2018-10-04 17:28:58'),
(37,4,5,'125581',1,'10','PL','2018-10-04 17:28:58'),
(38,4,6,'126211',1,'10','PL','2018-10-04 17:28:58'),
(39,4,7,'126619',1,'10','PL','2018-10-04 17:28:58'),
(40,4,8,'29446',1,'433','PL','2018-10-04 17:28:58'),
(41,4,9,'26850',1,'8888','PL','2018-10-04 17:28:58'),
(42,4,10,'125583',1,'','PL','2018-10-04 17:28:58'),
(43,5,1,'124274',1,'10','PL','2018-10-04 17:28:59'),
(44,5,2,'31850',1,'415','PL','2018-10-04 17:28:59'),
(45,5,3,'22715',1,'1062','PL','2018-10-04 17:28:59'),
(46,5,4,'32645',1,'1175','PL','2018-10-04 17:28:59'),
(47,6,1,'126812',1,'-1','PL','2018-10-15 17:44:34'),
(48,6,2,'124533',1,'-1','PL','2018-10-15 17:44:36'),
(49,6,3,'126965',1,'-1','PL','2018-10-15 17:44:37'),
(50,6,4,'126963',1,'-1','PL','2018-10-15 17:44:39'),
(51,6,5,'124274',1,'-1','PL','2018-10-15 17:44:44'),
(52,6,6,'34103',1,'1001','PL','2018-10-15 17:44:44'),
(53,6,7,'32255',1,'201','PL','2018-10-15 17:44:44'),
(54,6,8,'34100',1,'601','PL','2018-10-15 17:44:44'),
(55,6,9,'33488',5,'626','PL','2018-10-15 17:44:44'),
(56,6,10,'34033',1,'902','PL','2018-10-15 17:44:44'),
(57,7,1,'126963',1,'-1','PL','2018-10-15 18:14:34'),
(58,7,2,'124274',1,'-1','PL','2018-10-15 18:14:34'),
(59,7,3,'126812',1,'-1','PL','2018-10-15 18:14:34'),
(60,7,4,'124533',1,'-1','PL','2018-10-15 18:14:34'),
(61,7,5,'126965',1,'-1','PL','2018-10-15 18:14:34'),
(62,7,6,'31784',3,'1217','PL','2018-10-15 18:14:34'),
(63,7,7,'32255',1,'201','PL','2018-10-15 18:14:34'),
(64,7,8,'8048',1,'209','PL','2018-10-15 18:14:34'),
(65,7,9,'30660',2,'335','PL','2018-10-15 18:14:34'),
(66,7,10,'33727',1,'428','PL','2018-10-15 18:14:34'),
(67,7,11,'32646',1,'519','PL','2018-10-15 18:14:34'),
(68,7,12,'30670',1,'537','PL','2018-10-15 18:14:34'),
(69,7,13,'33452',1,'622','PL','2018-10-15 18:14:34'),
(70,7,14,'33488',6,'626','PL','2018-10-15 18:14:34'),
(71,7,15,'30612',1,'635','PL','2018-10-15 18:14:34'),
(72,7,16,'30063',1,'905','PL','2018-10-15 18:14:34'),
(73,8,1,'126963',1,'-1','PL','2018-10-15 18:14:34'),
(74,8,2,'124274',1,'-1','PL','2018-10-15 18:14:34'),
(75,8,3,'126812',1,'-1','PL','2018-10-15 18:14:35'),
(76,8,4,'124533',1,'-1','PL','2018-10-15 18:14:35'),
(77,8,5,'126965',1,'-1','PL','2018-10-15 18:14:35'),
(78,8,6,'32255',1,'201','PL','2018-10-15 18:14:35'),
(79,8,7,'8048',1,'209','PL','2018-10-15 18:14:35'),
(80,8,8,'18437',2,'527','PL','2018-10-15 18:14:35'),
(81,8,9,'33523',1,'529','PL','2018-10-15 18:14:35'),
(82,8,10,'33488',4,'626','PL','2018-10-15 18:14:35'),
(83,8,11,'30612',1,'635','PL','2018-10-15 18:14:35'),
(84,8,12,'30063',1,'905','PL','2018-10-15 18:14:35'),
(85,8,13,'31947',1,'933','PL','2018-10-15 18:14:35'),
(86,9,1,'126963',1,'-1','PL','2018-10-15 18:14:35'),
(87,9,2,'124274',1,'-1','PL','2018-10-15 18:14:35'),
(88,9,3,'126812',1,'-1','PL','2018-10-15 18:14:35'),
(89,9,4,'124533',1,'-1','PL','2018-10-15 18:14:35'),
(90,9,5,'126965',1,'-1','PL','2018-10-15 18:14:35'),
(91,9,6,'32474',1,'1137','PL','2018-10-15 18:14:35'),
(92,9,7,'32465',2,'131','PL','2018-10-15 18:14:35'),
(93,9,8,'32255',1,'201','PL','2018-10-15 18:14:36'),
(94,9,9,'8048',1,'209','PL','2018-10-15 18:14:36'),
(95,9,10,'125041',1,'4500','PL','2018-10-15 18:14:36'),
(96,9,11,'33523',4,'529','PL','2018-10-15 18:14:36'),
(97,9,12,'34100',3,'601','PL','2018-10-15 18:14:36'),
(98,9,13,'33488',5,'626','PL','2018-10-15 18:14:36'),
(99,9,14,'30612',1,'635','PL','2018-10-15 18:14:36'),
(100,9,15,'30063',1,'905','PL','2018-10-15 18:14:36'),
(101,9,16,'31951',1,'928','PL','2018-10-15 18:14:36'),
(102,10,1,'126812',1,'-1','PL','2018-10-15 18:14:36'),
(103,10,2,'124533',1,'-1','PL','2018-10-15 18:14:36'),
(104,10,3,'126965',1,'-1','PL','2018-10-15 18:14:36'),
(105,10,4,'126963',1,'-1','PL','2018-10-15 18:14:36'),
(106,10,5,'124274',1,'-1','PL','2018-10-15 18:14:36'),
(107,10,6,'32474',2,'1137','PL','2018-10-15 18:14:36'),
(108,10,7,'32255',1,'201','PL','2018-10-15 18:14:36'),
(109,10,8,'34100',2,'601','PL','2018-10-15 18:14:36'),
(110,11,1,'32346',0,'','PL','2018-10-15 18:14:36'),
(111,11,2,'126963',1,'-1','PL','2018-10-15 18:14:36'),
(112,11,3,'124274',1,'-1','PL','2018-10-15 18:14:36'),
(113,11,4,'126812',1,'-1','PL','2018-10-15 18:14:36'),
(114,11,5,'124533',1,'-1','PL','2018-10-15 18:14:36'),
(115,11,6,'126965',1,'-1','PL','2018-10-15 18:14:36'),
(116,11,7,'31784',4,'1217','PL','2018-10-15 18:14:37'),
(117,11,8,'32255',1,'201','PL','2018-10-15 18:14:37'),
(118,11,9,'8048',1,'209','PL','2018-10-15 18:14:37'),
(119,11,10,'26672',2,'212','PL','2018-10-15 18:14:37'),
(120,11,11,'30946',3,'4000','PL','2018-10-15 18:14:37'),
(121,11,12,'33488',6,'626','PL','2018-10-15 18:14:37'),
(122,11,13,'30612',1,'635','PL','2018-10-15 18:14:37'),
(123,11,14,'34104',5,'805','PL','2018-10-15 18:14:37'),
(124,11,15,'30063',1,'905','PL','2018-10-15 18:14:37'),
(125,12,1,'126963',1,'-1','PL','2018-10-15 18:14:37'),
(126,12,2,'124274',1,'-1','PL','2018-10-15 18:14:37'),
(127,12,3,'126812',1,'-1','PL','2018-10-15 18:14:37'),
(128,12,4,'124533',1,'-1','PL','2018-10-15 18:14:37'),
(129,12,5,'126965',1,'-1','PL','2018-10-15 18:14:37'),
(130,12,6,'32620',1,'1117','PL','2018-10-15 18:14:37'),
(131,12,7,'32474',1,'1137','PL','2018-10-15 18:14:37'),
(132,12,8,'31784',1,'1217','PL','2018-10-15 18:14:37'),
(133,12,9,'32465',3,'131','PL','2018-10-15 18:14:37'),
(134,12,10,'31949',2,'1413','PL','2018-10-15 18:14:37'),
(135,12,11,'32255',1,'201','PL','2018-10-15 18:14:37'),
(136,12,12,'8048',1,'209','PL','2018-10-15 18:14:38'),
(137,12,13,'30946',2,'4000','PL','2018-10-15 18:14:38'),
(138,12,14,'30612',1,'635','PL','2018-10-15 18:14:38'),
(139,12,15,'30063',1,'905','PL','2018-10-15 18:14:38'),
(140,12,16,'31951',2,'928','PL','2018-10-15 18:14:38'),
(141,13,1,'126812',1,'-1','PL','2018-10-15 18:14:38'),
(142,13,2,'124533',1,'-1','PL','2018-10-15 18:14:38'),
(143,13,3,'126965',1,'-1','PL','2018-10-15 18:14:38'),
(144,13,4,'126963',1,'-1','PL','2018-10-15 18:14:38'),
(145,13,5,'124274',1,'-1','PL','2018-10-15 18:14:38'),
(146,13,6,'32474',2,'1137','PL','2018-10-15 18:14:38'),
(147,13,7,'32255',1,'201','PL','2018-10-15 18:14:38'),
(148,13,8,'34104',2,'805','PL','2018-10-15 18:14:38'),
(149,14,1,'126963',1,'-1','PL','2018-10-15 18:14:38'),
(150,14,2,'124274',1,'-1','PL','2018-10-15 18:14:38'),
(151,14,3,'126812',1,'-1','PL','2018-10-15 18:14:38'),
(152,14,4,'124533',1,'-1','PL','2018-10-15 18:14:38'),
(153,14,5,'126965',1,'-1','PL','2018-10-15 18:14:38'),
(154,14,6,'32474',2,'1137','PL','2018-10-15 18:14:38'),
(155,14,7,'32465',1,'131','PL','2018-10-15 18:14:38'),
(156,14,8,'32255',1,'201','PL','2018-10-15 18:14:38'),
(157,14,9,'8048',1,'209','PL','2018-10-15 18:14:38'),
(158,14,10,'30660',3,'335','PL','2018-10-15 18:14:39'),
(159,14,11,'30612',1,'635','PL','2018-10-15 18:14:39'),
(160,14,12,'34104',4,'805','PL','2018-10-15 18:14:39'),
(161,14,13,'30063',1,'905','PL','2018-10-15 18:14:39'),
(162,14,14,'32258',2,'914','PL','2018-10-15 18:14:39'),
(163,15,1,'126963',1,'-1','PL','2018-10-15 18:14:39'),
(164,15,2,'124274',1,'-1','PL','2018-10-15 18:14:39'),
(165,15,3,'126812',1,'-1','PL','2018-10-15 18:14:39'),
(166,15,4,'124533',1,'-1','PL','2018-10-15 18:14:39'),
(167,15,5,'126965',1,'-1','PL','2018-10-15 18:14:39'),
(168,15,6,'34103',10,'1001','PL','2018-10-15 18:14:39'),
(169,15,7,'32255',1,'201','PL','2018-10-15 18:14:39'),
(170,15,8,'8048',1,'209','PL','2018-10-15 18:14:39'),
(171,15,9,'31946',2,'416','PL','2018-10-15 18:14:39'),
(172,15,10,'30612',1,'635','PL','2018-10-15 18:14:39'),
(173,15,11,'31806',2,'636','PL','2018-10-15 18:14:39'),
(174,15,12,'34104',1,'805','PL','2018-10-15 18:14:39'),
(175,15,13,'30063',1,'905','PL','2018-10-15 18:14:39'),
(176,15,14,'31951',2,'928','PL','2018-10-15 18:14:39'),
(177,16,1,'126963',1,'-1','PL','2018-10-15 18:14:39'),
(178,16,2,'124274',1,'-1','PL','2018-10-15 18:14:39'),
(179,16,3,'126812',1,'-1','PL','2018-10-15 18:14:39'),
(180,16,4,'124533',1,'-1','PL','2018-10-15 18:14:40'),
(181,16,5,'126965',1,'-1','PL','2018-10-15 18:14:40'),
(182,16,6,'31784',3,'1217','PL','2018-10-15 18:14:40'),
(183,16,7,'31949',2,'1413','PL','2018-10-15 18:14:40'),
(184,16,8,'32255',1,'201','PL','2018-10-15 18:14:40'),
(185,16,9,'8048',1,'209','PL','2018-10-15 18:14:40'),
(186,16,10,'32646',2,'519','PL','2018-10-15 18:14:40'),
(187,16,11,'33488',5,'626','PL','2018-10-15 18:14:40'),
(188,16,12,'30612',1,'635','PL','2018-10-15 18:14:41'),
(189,16,13,'30063',1,'905','PL','2018-10-15 18:14:41'),
(190,16,14,'31951',2,'928','PL','2018-10-15 18:14:41'),
(191,17,1,'126963',1,'-1','PL','2018-10-15 18:14:41'),
(192,17,2,'124274',1,'-1','PL','2018-10-15 18:14:41'),
(193,17,3,'126812',1,'-1','PL','2018-10-15 18:14:41'),
(194,17,4,'124533',1,'-1','PL','2018-10-15 18:14:41'),
(195,17,5,'126965',1,'-1','PL','2018-10-15 18:14:41'),
(196,17,6,'30523',4,'1020','PL','2018-10-15 18:14:41'),
(197,17,7,'32465',1,'131','PL','2018-10-15 18:14:41'),
(198,17,8,'32255',1,'201','PL','2018-10-15 18:14:41'),
(199,17,9,'8048',1,'209','PL','2018-10-15 18:14:41'),
(200,17,10,'32646',5,'519','PL','2018-10-15 18:14:41'),
(201,17,11,'33488',1,'626','PL','2018-10-15 18:14:41'),
(202,17,12,'30612',1,'635','PL','2018-10-15 18:14:41'),
(203,17,13,'31806',2,'636','PL','2018-10-15 18:14:41'),
(204,17,14,'30063',1,'905','PL','2018-10-15 18:14:41'),
(205,18,1,'126963',1,'-1','PL','2018-10-15 18:14:41'),
(206,18,2,'124274',1,'-1','PL','2018-10-15 18:14:41'),
(207,18,3,'126812',1,'-1','PL','2018-10-15 18:14:41'),
(208,18,4,'124533',1,'-1','PL','2018-10-15 18:14:41'),
(209,18,5,'126965',1,'-1','PL','2018-10-15 18:14:41'),
(210,18,6,'34103',1,'1001','PL','2018-10-15 18:14:41'),
(211,18,7,'32474',2,'1137','PL','2018-10-15 18:14:41'),
(212,18,8,'31784',3,'1217','PL','2018-10-15 18:14:41'),
(213,18,9,'32255',1,'201','PL','2018-10-15 18:14:42'),
(214,18,10,'8048',1,'209','PL','2018-10-15 18:14:42'),
(215,18,11,'33004',1,'311','PL','2018-10-15 18:14:42'),
(216,18,12,'30660',2,'335','PL','2018-10-15 18:14:42'),
(217,18,13,'33846',1,'505','PL','2018-10-15 18:14:42'),
(218,18,14,'30670',3,'537','PL','2018-10-15 18:14:42'),
(219,18,15,'33488',1,'626','PL','2018-10-15 18:14:42'),
(220,18,16,'30612',1,'635','PL','2018-10-15 18:14:42'),
(221,18,17,'30063',1,'905','PL','2018-10-15 18:14:42'),
(222,19,1,'126963',1,'-1','PL','2018-10-15 18:14:42'),
(223,19,2,'124274',1,'-1','PL','2018-10-15 18:14:42'),
(224,19,3,'126812',1,'-1','PL','2018-10-15 18:14:42'),
(225,19,4,'124533',1,'-1','PL','2018-10-15 18:14:42'),
(226,19,5,'126965',1,'-1','PL','2018-10-15 18:14:42'),
(227,19,6,'30523',5,'1020','PL','2018-10-15 18:14:42'),
(228,19,7,'32474',2,'1137','PL','2018-10-15 18:14:42'),
(229,19,8,'31950',1,'1321','PL','2018-10-15 18:14:42'),
(230,19,9,'32255',1,'201','PL','2018-10-15 18:14:42'),
(231,19,10,'8048',1,'209','PL','2018-10-15 18:14:42'),
(232,19,11,'32646',2,'519','PL','2018-10-15 18:14:42'),
(233,19,12,'30612',1,'635','PL','2018-10-15 18:14:42'),
(234,19,13,'31806',2,'636','PL','2018-10-15 18:14:42'),
(235,19,14,'30063',1,'905','PL','2018-10-15 18:14:43');

/*Table structure for table `tbl_order_line_header_archive` */

DROP TABLE IF EXISTS `tbl_order_line_header_archive`;

CREATE TABLE `tbl_order_line_header_archive` (
  `Order_line_Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Order_id` bigint(20) DEFAULT NULL,
  `Line_Item_Seq_no` int(11) DEFAULT NULL COMMENT 'line_number',
  `Sku` varchar(30) DEFAULT NULL COMMENT 'product_code',
  `Pick_Qty` int(11) DEFAULT NULL COMMENT 'order_quantity',
  `Pick_Location` varchar(20) DEFAULT NULL COMMENT 'Station',
  `Location` varchar(20) DEFAULT NULL COMMENT 'Location',
  `Added_DateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`Order_line_Id`),
  UNIQUE KEY `Unique_Pick` (`Order_id`,`Sku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_order_line_header_archive` */

/*Table structure for table `tbl_ptl_sku_mapping` */

DROP TABLE IF EXISTS `tbl_ptl_sku_mapping`;

CREATE TABLE `tbl_ptl_sku_mapping` (
  `Mapping_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Pick_Location` varchar(30) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Sku_Type` enum('NORMAL','Scan Based') DEFAULT 'NORMAL',
  PRIMARY KEY (`Mapping_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_ptl_sku_mapping` */

insert  into `tbl_ptl_sku_mapping`(`Mapping_ID`,`Pick_Location`,`Sku`,`Sku_Type`) values 
(1,'102','dsdsadasfa','Scan Based');

/*Table structure for table `tbl_request_queue_for_ptl` */

DROP TABLE IF EXISTS `tbl_request_queue_for_ptl`;

CREATE TABLE `tbl_request_queue_for_ptl` (
  `QueueID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Pick_Location` varchar(30) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Sku_Type` varchar(20) DEFAULT NULL,
  `Scanner_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_IPAddress` varchar(30) DEFAULT NULL,
  `LCD_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_SendPort` int(11) DEFAULT NULL,
  `LCD_SendPort` int(11) DEFAULT NULL,
  `PickQty` int(3) DEFAULT NULL,
  `Picked_Qty` int(3) DEFAULT NULL,
  `Status` enum('INPROCESS','COMPLETED','PENDING') DEFAULT 'PENDING',
  `PickStart` datetime DEFAULT NULL,
  `PickConfirmation` datetime DEFAULT NULL,
  `PickDuration` int(11) DEFAULT NULL,
  `DisplayMSG` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`QueueID`),
  UNIQUE KEY `Unique_Pick` (`Invoice_Number`,`Sku`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_request_queue_for_ptl` */

/*Table structure for table `tbl_request_queue_for_ptl_archive` */

DROP TABLE IF EXISTS `tbl_request_queue_for_ptl_archive`;

CREATE TABLE `tbl_request_queue_for_ptl_archive` (
  `QueueID` bigint(20) NOT NULL AUTO_INCREMENT,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_Number` varchar(30) DEFAULT NULL,
  `Pick_Location` varchar(30) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Sku_Type` varchar(20) DEFAULT NULL,
  `Scanner_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_IPAddress` varchar(30) DEFAULT NULL,
  `LCD_IPAddress` varchar(30) DEFAULT NULL,
  `PTL_SendPort` int(11) DEFAULT NULL,
  `LCD_SendPort` int(11) DEFAULT NULL,
  `PickQty` int(3) DEFAULT NULL,
  `Picked_Qty` int(3) DEFAULT NULL,
  `Status` enum('INPROCESS','COMPLETED','PENDING') DEFAULT 'PENDING',
  `PickStart` datetime DEFAULT NULL,
  `PickConfirmation` datetime DEFAULT NULL,
  `PickDuration` int(11) DEFAULT NULL,
  `DisplayMSG` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`QueueID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_request_queue_for_ptl_archive` */

/*Table structure for table `tbl_scan_pick_details` */

DROP TABLE IF EXISTS `tbl_scan_pick_details`;

CREATE TABLE `tbl_scan_pick_details` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ScanedCode` varchar(100) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Ser_no` varchar(30) DEFAULT NULL,
  `ScanDatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Scan_by` varchar(30) DEFAULT NULL,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `QueueID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Scan_Pick` (`Sku`,`Ser_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_scan_pick_details` */

/*Table structure for table `tbl_scan_pick_details_archive` */

DROP TABLE IF EXISTS `tbl_scan_pick_details_archive`;

CREATE TABLE `tbl_scan_pick_details_archive` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ScanedCode` varchar(100) DEFAULT NULL,
  `Sku` varchar(30) DEFAULT NULL,
  `Ser_no` varchar(30) DEFAULT NULL,
  `ScanDatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Scan_by` varchar(30) DEFAULT NULL,
  `Carate_Code` varchar(30) DEFAULT NULL,
  `Invoice_No` varchar(30) DEFAULT NULL,
  `QueueID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `Scan_Pick` (`Sku`,`Ser_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_scan_pick_details_archive` */

/*Table structure for table `tbl_users` */

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `ID` bigint(11) NOT NULL AUTO_INCREMENT,
  `User_ID` varchar(30) DEFAULT NULL,
  `Password` varchar(100) DEFAULT NULL,
  `user_category` enum('OPERATOR','ADMIN','MANAGER','DEVELOPER') DEFAULT 'OPERATOR',
  `UserCode` varchar(30) DEFAULT NULL,
  `CreatedBy` varchar(30) DEFAULT NULL,
  `CreatedDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `_idx_User_name` (`User_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_users` */

insert  into `tbl_users`(`ID`,`User_ID`,`Password`,`user_category`,`UserCode`,`CreatedBy`,`CreatedDate`) values 
(1,'Mahesh','5a67b77cd0368a0fe5f54621318caa','ADMIN',NULL,NULL,'2018-10-12 12:55:44'),
(2,'OP1','5a67b77cd0368a0fe5f54621318caa','OPERATOR',NULL,NULL,'2018-10-12 12:55:44');

/*Table structure for table `tbl_weight_tolerance_config` */

DROP TABLE IF EXISTS `tbl_weight_tolerance_config`;

CREATE TABLE `tbl_weight_tolerance_config` (
  `Config_id` int(10) NOT NULL AUTO_INCREMENT,
  `From_Weight` decimal(10,3) DEFAULT NULL,
  `To_Weight` decimal(10,3) DEFAULT NULL,
  `Tolerance_per` decimal(10,1) DEFAULT NULL,
  `Static_Tolerance` decimal(10,3) DEFAULT NULL,
  `Activation_Date` datetime DEFAULT CURRENT_TIMESTAMP,
  `DeActivation_Date` datetime DEFAULT NULL,
  PRIMARY KEY (`Config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `tbl_weight_tolerance_config` */

insert  into `tbl_weight_tolerance_config`(`Config_id`,`From_Weight`,`To_Weight`,`Tolerance_per`,`Static_Tolerance`,`Activation_Date`,`DeActivation_Date`) values 
(1,0.000,1.500,10.0,0.025,'2018-01-19 14:12:39',NULL),
(2,1.501,2.500,6.0,0.025,'2018-01-19 14:12:59',NULL),
(3,2.501,5.000,2.5,0.025,'2018-01-19 14:13:08',NULL),
(4,5.001,10.000,1.5,0.025,'2018-01-19 14:13:19',NULL),
(5,10.010,50.000,1.5,0.025,'2018-01-20 11:58:52',NULL);

/*Table structure for table `tbl_zone_master` */

DROP TABLE IF EXISTS `tbl_zone_master`;

CREATE TABLE `tbl_zone_master` (
  `Id` int(5) NOT NULL AUTO_INCREMENT,
  `ZONE_ID` int(11) DEFAULT NULL,
  `Display_Name` varchar(10) DEFAULT NULL,
  `Zone_Name` varchar(10) DEFAULT NULL,
  `Zone_IPAddress` varchar(30) DEFAULT NULL,
  `LCD_IPAddress` varchar(30) DEFAULT NULL,
  `Scanner_IPAddress` varchar(30) DEFAULT NULL,
  `No_of_PTL` int(3) DEFAULT NULL,
  `Added_Datetime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `PTL_SendPort` bigint(5) DEFAULT NULL,
  `PTL_ReceivePort` bigint(5) DEFAULT NULL,
  `LCD_SendPort` bigint(5) DEFAULT NULL,
  `Scanner_ReceivePort` bigint(5) DEFAULT NULL,
  `Status` enum('ACTIVE','INACTIVE','LOGIN') DEFAULT 'INACTIVE',
  `Zone_Type` enum('NORMAL','SCANNABLE') DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Zone` (`Zone_IPAddress`),
  KEY `LCD` (`LCD_IPAddress`),
  KEY `Scanner` (`Scanner_IPAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_zone_master` */

insert  into `tbl_zone_master`(`Id`,`ZONE_ID`,`Display_Name`,`Zone_Name`,`Zone_IPAddress`,`LCD_IPAddress`,`Scanner_IPAddress`,`No_of_PTL`,`Added_Datetime`,`PTL_SendPort`,`PTL_ReceivePort`,`LCD_SendPort`,`Scanner_ReceivePort`,`Status`,`Zone_Type`) values 
(1,1,'ZONE-1','BOX-11','192.168.10.100','192.168.10.211','192.168.10.10',9,'2018-10-01 17:13:38',50100,50200,50001,50001,'INACTIVE',NULL),
(2,2,'ZONE-1','BOX-21','192.168.10.110','192.168.10.211','192.168.10.10',20,'2018-10-01 17:14:02',50101,50201,50001,50001,'INACTIVE',NULL),
(3,2,'ZONE-1','BOX-22','192.169.10.111','192.168.10.211','192.168.10.10',20,'2018-10-01 17:15:14',50102,50202,50001,50001,'INACTIVE',NULL),
(4,3,'ZONE-1','BOX-31','192.168.10.112','192.168.10.211','192.168.10.10',20,'2018-10-01 17:16:15',50103,50203,50001,50001,'INACTIVE',NULL),
(5,3,'ZONE-1','BOX-32','192.169.10.113','192.168.10.211','192.168.10.10',20,'2018-10-01 17:16:50',50104,50204,50001,50001,'INACTIVE',NULL),
(6,4,'ZONE-1','BOX-41','192.168.10.114','192.168.10.212','192.168.10.11',20,'2018-10-01 17:18:11',50105,50205,50001,50001,'INACTIVE',NULL),
(7,4,'ZONE-1','BOX-42','192.168.10.115','192.168.10.212','192.168.10.11',20,'2018-10-01 17:18:23',50106,50206,50001,50001,'INACTIVE',NULL),
(8,5,'ZONE-2','BOX-51','192.168.10.116','192.168.10.212','192.168.10.11',20,'2018-10-01 17:19:37',50107,50207,50001,50001,'INACTIVE',NULL),
(9,5,'ZONE-2','BOX-52','192.168.10.117','192.168.10.212','192.168.10.11',20,'2018-10-01 17:20:33',50108,50208,50001,50001,'INACTIVE',NULL),
(10,6,'ZONE-2','BOX-61','192.168.10.118','192.168.10.213','192.168.10.12',20,'2018-10-01 17:21:32',50109,50209,50001,50001,'INACTIVE',NULL),
(11,6,'ZONE-2','BOX-62','192.168.10.119','192.168.10.213','192.168.10.12',20,'2018-10-01 17:21:45',50110,50210,50001,50001,'INACTIVE',NULL),
(12,7,'ZONE-2','BOX-71','192.168.10.120','192.168.10.213','192.168.10.12',20,'2018-10-01 17:25:45',50111,50211,50001,50001,'INACTIVE',NULL),
(13,7,'ZONE-2','BOX-72','192.168.10.121','192.168.10.213','192.168.10.12',20,'2018-10-01 17:26:15',50112,50212,50001,50001,'INACTIVE',NULL),
(14,8,'ZONE-3','BOX-81','192.168.10.122','192.168.10.213','192.168.10.12',20,'2018-10-01 17:26:59',50113,50213,50001,50001,'INACTIVE',NULL),
(15,8,'ZONE-3','BOX-82','192.168.10.123','192.168.10.213','192.168.10.12',20,'2018-10-01 17:27:30',50114,50214,50001,50001,'INACTIVE',NULL),
(16,9,'ZONE-3','BOX-91','192.168.10.124','192.168.10.214','192.168.10.13',20,'2018-10-01 17:28:49',50115,50215,50001,50001,'INACTIVE',NULL),
(17,9,'ZONE-3','BOX-92','192.168.10.125','192.168.10.214','192.168.10.13',20,'2018-10-01 17:29:19',50116,50216,50001,50001,'INACTIVE',NULL),
(18,10,'ZONE-3','BOX-101','192.168.10.126','192.168.10.214','192.168.10.13',20,'2018-10-01 17:29:58',50117,50217,50001,50001,'INACTIVE',NULL),
(19,10,'ZONE-3','BOX-102','192.168.10.127','192.168.10.214','192.168.10.13',20,'2018-10-01 17:30:31',50118,50218,50001,50001,'INACTIVE',NULL),
(20,11,'ZONE-4','BOX-111','192.168.10.128','192.178.10.214','192.168.10.13',20,'2018-10-01 17:31:33',50119,50219,50001,50001,'INACTIVE',NULL),
(21,11,'ZONE-4','BOX-112','192.168.10.129','192.168.10.214','192.168.10.13',20,'2018-10-01 17:32:21',50120,50220,50001,50001,'INACTIVE',NULL),
(22,12,'ZONE-4','BOX-121','192.168.10.130','192.168.10.215','192.168.10.14',20,'2018-10-01 17:32:43',50121,50221,50001,50001,'INACTIVE',NULL),
(23,12,'ZONE-4','BOX-122','192.168.10.131','192.168.10.215','192.168.10.14',20,'2018-10-01 17:33:35',50122,50222,50001,50001,'INACTIVE',NULL),
(24,13,'ZONE-4','BOX-131','192.168.10.132','192.168.10.215','192.168.10.14',20,'2018-10-01 17:36:15',50123,50223,50001,50001,'INACTIVE',NULL),
(25,13,'ZONE-4','BOX-132','192.168.10.133','192.168.10.215','192.168.10.14',20,'2018-10-01 17:36:48',50124,50224,50001,50001,'INACTIVE',NULL),
(26,14,'ZONE-5','BOX-141','192.168.10.134','192.168.10.215','192.168.10.14',20,'2018-10-01 17:37:20',50215,50225,50001,50001,'INACTIVE',NULL),
(27,14,'ZONE-5','BOX-142','192.168.10.135','192.168.10.215','192.168.10.14',20,'2018-10-01 17:37:51',50216,50226,50001,50001,'INACTIVE',NULL),
(28,15,'ZONE-5','BOX-151','192.168.10.136','192.168.10.215','192.168.10.14',20,'2018-10-01 17:38:03',50217,50227,50001,50001,'INACTIVE',NULL),
(29,15,'ZONE-5','BOX-152','192.168.10.137','192.168.10.215','192.168.10.14',20,'2018-10-01 17:38:36',50218,50228,50001,50001,'INACTIVE',NULL);

/*Table structure for table `tbl_zone_ptl_mapping` */

DROP TABLE IF EXISTS `tbl_zone_ptl_mapping`;

CREATE TABLE `tbl_zone_ptl_mapping` (
  `Mapping_ID` int(10) NOT NULL AUTO_INCREMENT,
  `Zone_ID` int(11) DEFAULT NULL,
  `PTL` varchar(4) DEFAULT NULL,
  `Pick_Location` varchar(30) DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`Mapping_ID`),
  UNIQUE KEY `Zone_PTL` (`Zone_ID`,`PTL`),
  UNIQUE KEY `Pick_Location` (`Pick_Location`)
) ENGINE=InnoDB AUTO_INCREMENT=811 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_zone_ptl_mapping` */

insert  into `tbl_zone_ptl_mapping`(`Mapping_ID`,`Zone_ID`,`PTL`,`Pick_Location`,`isActive`) values 
(1,1,'1001','001',1),
(2,1,'1002','002',1),
(3,1,'1003','003',1),
(4,1,'1004','004',1),
(5,1,'1005','005',1),
(6,1,'1006','006',1),
(7,1,'1007','007',1),
(8,1,'1008','008',1),
(9,1,'1009','009',1),
(10,2,'1101','101',1),
(11,2,'1102','102',1),
(12,2,'1103','103',1),
(13,2,'1104','104',1),
(14,2,'1105','105',1),
(15,2,'1106','106',1),
(16,2,'1107','107',1),
(17,2,'1108','108',1),
(18,2,'1109','109',1),
(19,2,'1110','110',1),
(20,2,'1111','111',1),
(21,2,'1112','112',1),
(22,2,'1113','113',1),
(23,2,'1114','114',1),
(24,2,'1115','115',1),
(25,2,'1116','116',1),
(26,2,'1117','117',1),
(27,2,'1118','118',1),
(28,2,'1119','119',1),
(29,2,'1120','120',1),
(30,2,'1201','121',1),
(31,2,'1202','122',1),
(32,2,'1203','123',1),
(33,2,'1204','124',1),
(34,2,'1205','125',1),
(35,2,'1206','126',1),
(36,2,'1207','127',1),
(37,2,'1208','128',1),
(38,2,'1209','129',1),
(39,2,'1210','130',1),
(40,2,'1211','131',1),
(41,2,'1212','132',1),
(42,2,'1213','133',1),
(43,2,'1214','134',1),
(44,2,'1215','135',1),
(45,2,'1216','136',1),
(46,2,'1217','137',1),
(47,2,'1218','138',1),
(48,2,'1219','139',1),
(49,2,'1220','140',1),
(50,3,'1301','201',1),
(51,3,'1302','202',1),
(52,3,'1303','203',1),
(53,3,'1304','204',1),
(54,3,'1305','205',1),
(55,3,'1306','206',1),
(56,3,'1307','207',1),
(57,3,'1308','208',1),
(58,3,'1309','209',1),
(59,3,'1310','210',1),
(60,3,'1311','211',1),
(61,3,'1312','212',1),
(62,3,'1313','213',1),
(63,3,'1314','214',1),
(64,3,'1315','215',1),
(65,3,'1316','216',1),
(66,3,'1317','217',1),
(67,3,'1318','218',1),
(68,3,'1319','219',1),
(69,3,'1320','220',1),
(70,3,'1401','221',1),
(71,3,'1402','222',1),
(72,3,'1403','223',1),
(73,3,'1404','224',1),
(74,3,'1405','225',1),
(75,3,'1406','226',1),
(76,3,'1407','227',1),
(77,3,'1408','228',1),
(78,3,'1409','229',1),
(79,3,'1410','230',1),
(80,3,'1411','231',1),
(81,3,'1412','232',1),
(82,3,'1413','233',1),
(83,3,'1414','234',1),
(84,3,'1415','235',1),
(85,3,'1416','236',1),
(86,3,'1417','237',1),
(87,3,'1418','238',1),
(88,3,'1419','239',1),
(89,3,'1420','240',1),
(90,4,'1501','301',1),
(91,4,'1502','302',1),
(92,4,'1503','303',1),
(93,4,'1504','304',1),
(94,4,'1505','305',1),
(95,4,'1506','306',1),
(96,4,'1507','307',1),
(97,4,'1508','308',1),
(98,4,'1509','309',1),
(99,4,'1510','310',1),
(100,4,'1511','311',1),
(101,4,'1512','312',1),
(102,4,'1513','313',1),
(103,4,'1514','314',1),
(104,4,'1515','315',1),
(105,4,'1516','316',1),
(106,4,'1517','317',1),
(107,4,'1518','318',1),
(108,4,'1519','319',1),
(109,4,'1520','320',1),
(110,4,'1601','321',1),
(111,4,'1602','322',1),
(112,4,'1603','323',1),
(113,4,'1604','324',1),
(114,4,'1605','325',1),
(115,4,'1606','326',1),
(116,4,'1607','327',1),
(117,4,'1608','328',1),
(118,4,'1609','329',1),
(119,4,'1610','330',1),
(120,4,'1611','331',1),
(121,4,'1612','332',1),
(122,4,'1613','333',1),
(123,4,'1614','334',1),
(124,4,'1615','335',1),
(125,4,'1616','336',1),
(126,4,'1617','337',1),
(127,4,'1618','338',1),
(128,4,'1619','339',1),
(129,4,'1620','340',1),
(141,5,'1701','401',1),
(142,5,'1702','402',1),
(143,5,'1703','403',1),
(144,5,'1704','404',1),
(145,5,'1705','405',1),
(146,5,'1706','406',1),
(147,5,'1707','407',1),
(148,5,'1708','408',1),
(149,5,'1709','409',1),
(150,5,'1710','410',1),
(151,5,'1711','411',1),
(152,5,'1712','412',1),
(153,5,'1713','413',1),
(154,5,'1714','414',1),
(155,5,'1715','415',1),
(156,5,'1716','416',1),
(157,5,'1717','417',1),
(158,5,'1718','418',1),
(159,5,'1719','419',1),
(160,5,'1720','420',1),
(161,5,'1801','421',1),
(162,5,'1802','422',1),
(163,5,'1803','423',1),
(164,5,'1804','424',1),
(165,5,'1805','425',1),
(166,5,'1806','426',1),
(167,5,'1807','427',1),
(168,5,'1808','428',1),
(169,5,'1809','429',1),
(170,5,'1810','430',1),
(171,5,'1811','431',1),
(172,5,'1812','432',1),
(173,5,'1813','433',1),
(174,5,'1814','434',1),
(175,5,'1815','435',1),
(176,5,'1816','436',1),
(177,5,'1817','437',1),
(178,5,'1818','438',1),
(179,5,'1819','439',1),
(180,5,'1820','440',1),
(204,6,'1901','501',1),
(205,6,'1902','502',1),
(206,6,'1903','503',1),
(207,6,'1904','504',1),
(208,6,'1905','505',1),
(209,6,'1906','506',1),
(210,6,'1907','507',1),
(211,6,'1908','508',1),
(212,6,'1909','509',1),
(213,6,'1910','510',1),
(214,6,'1911','511',1),
(215,6,'1912','512',1),
(216,6,'1913','513',1),
(217,6,'1914','514',1),
(218,6,'1915','515',1),
(219,6,'1916','516',1),
(220,6,'1917','517',1),
(221,6,'1918','518',1),
(222,6,'1919','519',1),
(223,6,'1920','520',1),
(224,6,'2001','521',1),
(225,6,'2002','522',1),
(226,6,'2003','523',1),
(227,6,'2004','524',1),
(228,6,'2005','525',1),
(229,6,'2006','526',1),
(230,6,'2007','527',1),
(231,6,'2008','528',1),
(232,6,'2009','529',1),
(233,6,'2010','530',1),
(234,6,'2011','531',1),
(235,6,'2012','532',1),
(236,6,'2013','533',1),
(237,6,'2014','534',1),
(238,6,'2015','535',1),
(239,6,'2016','536',1),
(240,6,'2017','537',1),
(241,6,'2018','538',1),
(242,6,'2019','539',1),
(243,6,'2020','540',1),
(267,7,'2101','601',1),
(268,7,'2102','602',1),
(269,7,'2103','603',1),
(270,7,'2104','604',1),
(271,7,'2105','605',1),
(272,7,'2106','606',1),
(273,7,'2107','607',1),
(274,7,'2108','608',1),
(275,7,'2109','609',1),
(276,7,'2110','610',1),
(277,7,'2111','611',1),
(278,7,'2112','612',1),
(279,7,'2113','613',1),
(280,7,'2114','614',1),
(281,7,'2115','615',1),
(282,7,'2116','616',1),
(283,7,'2117','617',1),
(284,7,'2118','618',1),
(285,7,'2119','619',1),
(286,7,'2120','620',1),
(287,7,'2201','621',1),
(288,7,'2202','622',1),
(289,7,'2203','623',1),
(290,7,'2204','624',1),
(291,7,'2205','625',1),
(292,7,'2206','626',1),
(293,7,'2207','627',1),
(294,7,'2208','628',1),
(295,7,'2209','629',1),
(296,7,'2210','630',1),
(297,7,'2211','631',1),
(298,7,'2212','632',1),
(299,7,'2213','633',1),
(300,7,'2214','634',1),
(301,7,'2215','635',1),
(302,7,'2216','636',1),
(303,7,'2217','637',1),
(304,7,'2218','638',1),
(305,7,'2219','639',1),
(306,7,'2220','640',1),
(330,8,'2301','701',1),
(331,8,'2302','702',1),
(332,8,'2303','703',1),
(333,8,'2304','704',1),
(334,8,'2305','705',1),
(335,8,'2306','706',1),
(336,8,'2307','707',1),
(337,8,'2308','708',1),
(338,8,'2309','709',1),
(339,8,'2310','710',1),
(340,8,'2311','711',1),
(341,8,'2312','712',1),
(342,8,'2313','713',1),
(343,8,'2314','714',1),
(344,8,'2315','715',1),
(345,8,'2316','716',1),
(346,8,'2317','717',1),
(347,8,'2318','718',1),
(348,8,'2319','719',1),
(349,8,'2320','720',1),
(350,8,'2401','721',1),
(351,8,'2402','722',1),
(352,8,'2403','723',1),
(353,8,'2404','724',1),
(354,8,'2405','725',1),
(355,8,'2406','726',1),
(356,8,'2407','727',1),
(357,8,'2408','728',1),
(358,8,'2409','729',1),
(359,8,'2410','730',1),
(360,8,'2411','731',1),
(361,8,'2412','732',1),
(362,8,'2413','733',1),
(363,8,'2414','734',1),
(364,8,'2415','735',1),
(365,8,'2416','736',1),
(366,8,'2417','737',1),
(367,8,'2418','738',1),
(368,8,'2419','739',1),
(369,8,'2420','740',1),
(393,9,'2501','801',1),
(394,9,'2502','802',1),
(395,9,'2503','803',1),
(396,9,'2504','804',1),
(397,9,'2505','805',1),
(398,9,'2506','806',1),
(399,9,'2507','807',1),
(400,9,'2508','808',1),
(401,9,'2509','809',1),
(402,9,'2510','810',1),
(403,9,'2511','811',1),
(404,9,'2512','812',1),
(405,9,'2513','813',1),
(406,9,'2514','814',1),
(407,9,'2515','815',1),
(408,9,'2516','816',1),
(409,9,'2517','817',1),
(410,9,'2518','818',1),
(411,9,'2519','819',1),
(412,9,'2520','820',1),
(413,9,'2601','821',1),
(414,9,'2602','822',1),
(415,9,'2603','823',1),
(416,9,'2604','824',1),
(417,9,'2605','825',1),
(418,9,'2606','826',1),
(419,9,'2607','827',1),
(420,9,'2608','828',1),
(421,9,'2609','829',1),
(422,9,'2610','830',1),
(423,9,'2611','831',1),
(424,9,'2612','832',1),
(425,9,'2613','833',1),
(426,9,'2614','834',1),
(427,9,'2615','835',1),
(428,9,'2616','836',1),
(429,9,'2617','837',1),
(430,9,'2618','838',1),
(431,9,'2619','839',1),
(432,9,'2620','840',1),
(456,10,'2701','901',1),
(457,10,'2702','902',1),
(458,10,'2703','903',1),
(459,10,'2704','904',1),
(460,10,'2705','905',1),
(461,10,'2706','906',1),
(462,10,'2707','907',1),
(463,10,'2708','908',1),
(464,10,'2709','909',1),
(465,10,'2710','910',1),
(466,10,'2711','911',1),
(467,10,'2712','912',1),
(468,10,'2713','913',1),
(469,10,'2714','914',1),
(470,10,'2715','915',1),
(471,10,'2716','916',1),
(472,10,'2717','917',1),
(473,10,'2718','918',1),
(474,10,'2719','919',1),
(475,10,'2720','920',1),
(476,10,'2801','921',1),
(477,10,'2802','922',1),
(478,10,'2803','923',1),
(479,10,'2804','924',1),
(480,10,'2805','925',1),
(481,10,'2806','926',1),
(482,10,'2807','927',1),
(483,10,'2808','928',1),
(484,10,'2809','929',1),
(485,10,'2810','930',1),
(486,10,'2811','931',1),
(487,10,'2812','932',1),
(488,10,'2813','933',1),
(489,10,'2814','934',1),
(490,10,'2815','935',1),
(491,10,'2816','936',1),
(492,10,'2817','937',1),
(493,10,'2818','938',1),
(494,10,'2819','939',1),
(495,10,'2820','940',1),
(519,11,'2901','1001',1),
(520,11,'2902','1002',1),
(521,11,'2903','1003',1),
(522,11,'2904','1004',1),
(523,11,'2905','1005',1),
(524,11,'2906','1006',1),
(525,11,'2907','1007',1),
(526,11,'2908','1008',1),
(527,11,'2909','1009',1),
(528,11,'2910','1010',1),
(529,11,'2911','1011',1),
(530,11,'2912','1012',1),
(531,11,'2913','1013',1),
(532,11,'2914','1014',1),
(533,11,'2915','1015',1),
(534,11,'2916','1016',1),
(535,11,'2917','1017',1),
(536,11,'2918','1018',1),
(537,11,'2919','1019',1),
(538,11,'2920','1020',1),
(539,11,'3001','1021',1),
(540,11,'3002','1022',1),
(541,11,'3003','1023',1),
(542,11,'3004','1024',1),
(543,11,'3005','1025',1),
(544,11,'3006','1026',1),
(545,11,'3007','1027',1),
(546,11,'3008','1028',1),
(547,11,'3009','1029',1),
(548,11,'3010','1030',1),
(549,11,'3011','1031',1),
(550,11,'3012','1032',1),
(551,11,'3013','1033',1),
(552,11,'3014','1034',1),
(553,11,'3015','1035',1),
(554,11,'3016','1036',1),
(555,11,'3017','1037',1),
(556,11,'3018','1038',1),
(557,11,'3019','1039',1),
(558,11,'3020','1040',1),
(582,12,'3101','1101',1),
(583,12,'3102','1102',1),
(584,12,'3103','1103',1),
(585,12,'3104','1104',1),
(586,12,'3105','1105',1),
(587,12,'3106','1106',1),
(588,12,'3107','1107',1),
(589,12,'3108','1108',1),
(590,12,'3109','1109',1),
(591,12,'3110','1110',1),
(592,12,'3111','1111',1),
(593,12,'3112','1112',1),
(594,12,'3113','1113',1),
(595,12,'3114','1114',1),
(596,12,'3115','1115',1),
(597,12,'3116','1116',1),
(598,12,'3117','1117',1),
(599,12,'3118','1118',1),
(600,12,'3119','1119',1),
(601,12,'3120','1120',1),
(602,12,'3201','1121',1),
(603,12,'3202','1122',1),
(604,12,'3203','1123',1),
(605,12,'3204','1124',1),
(606,12,'3205','1125',1),
(607,12,'3206','1126',1),
(608,12,'3207','1127',1),
(609,12,'3208','1128',1),
(610,12,'3209','1129',1),
(611,12,'3210','1130',1),
(612,12,'3211','1131',1),
(613,12,'3212','1132',1),
(614,12,'3213','1133',1),
(615,12,'3214','1134',1),
(616,12,'3215','1135',1),
(617,12,'3216','1136',1),
(618,12,'3217','1137',1),
(619,12,'3218','1138',1),
(620,12,'3219','1139',1),
(621,12,'3220','1140',1),
(645,13,'3301','1201',1),
(646,13,'3302','1202',1),
(647,13,'3303','1203',1),
(648,13,'3304','1204',1),
(649,13,'3305','1205',1),
(650,13,'3306','1206',1),
(651,13,'3307','1207',1),
(652,13,'3308','1208',1),
(653,13,'3309','1209',1),
(654,13,'3310','1210',1),
(655,13,'3311','1211',1),
(656,13,'3312','1212',1),
(657,13,'3313','1213',1),
(658,13,'3314','1214',1),
(659,13,'3315','1215',1),
(660,13,'3316','1216',1),
(661,13,'3317','1217',1),
(662,13,'3318','1218',1),
(663,13,'3319','1219',1),
(664,13,'3320','1220',1),
(665,13,'3401','1221',1),
(666,13,'3402','1222',1),
(667,13,'3403','1223',1),
(668,13,'3404','1224',1),
(669,13,'3405','1225',1),
(670,13,'3406','1226',1),
(671,13,'3407','1227',1),
(672,13,'3408','1228',1),
(673,13,'3409','1229',1),
(674,13,'3410','1230',1),
(675,13,'3411','1231',1),
(676,13,'3412','1232',1),
(677,13,'3413','1233',1),
(678,13,'3414','1234',1),
(679,13,'3415','1235',1),
(680,13,'3416','1236',1),
(681,13,'3417','1237',1),
(682,13,'3418','1238',1),
(683,13,'3419','1239',1),
(684,13,'3420','1240',1),
(708,14,'3501','1301',1),
(709,14,'3502','1302',1),
(710,14,'3503','1303',1),
(711,14,'3504','1304',1),
(712,14,'3505','1305',1),
(713,14,'3506','1306',1),
(714,14,'3507','1307',1),
(715,14,'3508','1308',1),
(716,14,'3509','1309',1),
(717,14,'3510','1310',1),
(718,14,'3511','1311',1),
(719,14,'3512','1312',1),
(720,14,'3513','1313',1),
(721,14,'3514','1314',1),
(722,14,'3515','1315',1),
(723,14,'3516','1316',1),
(724,14,'3517','1317',1),
(725,14,'3518','1318',1),
(726,14,'3519','1319',1),
(727,14,'3520','1320',1),
(728,14,'3601','1321',1),
(729,14,'3602','1322',1),
(730,14,'3603','1323',1),
(731,14,'3604','1324',1),
(732,14,'3605','1325',1),
(733,14,'3606','1326',1),
(734,14,'3607','1327',1),
(735,14,'3608','1328',1),
(736,14,'3609','1329',1),
(737,14,'3610','1330',1),
(738,14,'3611','1331',1),
(739,14,'3612','1332',1),
(740,14,'3613','1333',1),
(741,14,'3614','1334',1),
(742,14,'3615','1335',1),
(743,14,'3616','1336',1),
(744,14,'3617','1337',1),
(745,14,'3618','1338',1),
(746,14,'3619','1339',1),
(747,14,'3620','1340',1),
(771,15,'3701','1401',1),
(772,15,'3702','1402',1),
(773,15,'3703','1403',1),
(774,15,'3704','1404',1),
(775,15,'3705','1405',1),
(776,15,'3706','1406',1),
(777,15,'3707','1407',1),
(778,15,'3708','1408',1),
(779,15,'3709','1409',1),
(780,15,'3710','1410',1),
(781,15,'3711','1411',1),
(782,15,'3712','1412',1),
(783,15,'3713','1413',1),
(784,15,'3714','1414',1),
(785,15,'3715','1415',1),
(786,15,'3716','1416',1),
(787,15,'3717','1417',1),
(788,15,'3718','1418',1),
(789,15,'3719','1419',1),
(790,15,'3720','1420',1),
(791,15,'3801','1421',1),
(792,15,'3802','1422',1),
(793,15,'3803','1423',1),
(794,15,'3804','1424',1),
(795,15,'3805','1425',1),
(796,15,'3806','1426',1),
(797,15,'3807','1427',1),
(798,15,'3808','1428',1),
(799,15,'3809','1429',1),
(800,15,'3810','1430',1),
(801,15,'3811','1431',1),
(802,15,'3812','1432',1),
(803,15,'3813','1433',1),
(804,15,'3814','1434',1),
(805,15,'3815','1435',1),
(806,15,'3816','1436',1),
(807,15,'3817','1437',1),
(808,15,'3818','1438',1),
(809,15,'3819','1439',1),
(810,15,'3820','1440',1);

/* Procedure structure for procedure `SP_AddNewSku` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_AddNewSku` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_AddNewSku`(_Sku_code varchar(30),_sku_type varchar(20),_Description varchar(100),_Pick_Location varchar(20))
BEGIN
              IF Exists(select `Sku` from `tbl_ptl_sku_mapping` where Sku=_Sku_code) then
		Update `tbl_ptl_sku_mapping` set `Sku_Type`= CONVERT(_sku_type , char) ,
		PICK_Location=_Pick_Location where Sku=_Sku_code;
	      else 
		insert into `tbl_ptl_sku_mapping`(`Pick_Location`,`Sku`,`Sku_Type`)
		values(_Pick_Location,_Sku_code,_sku_type);
	     End if;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_addScanner_PickData` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_addScanner_PickData` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_addScanner_PickData`(_invoice varchar(30),_scan_code varchar(200),_IPAddress varchar(30))
BEGIN
		INSERT IGNORE INTO tbl_scan_Pick_details(`Carate_Code`,`Invoice_No`, `Sku`,`Ser_no`,`Scan_by`,QueueID,ScanedCode)
		SELECT `Carate_Code`,_invoice,'',0,_IPAddress,`QueueID`,_scan_code FROM `tbl_request_queue_for_ptl` 
		WHERE  STATUS='INPROCESS' AND `Scanner_IPAddress`=_IPAddress limit 1 ;
			
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_AddweightStatustoCompletePick` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_AddweightStatustoCompletePick` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_AddweightStatustoCompletePick`(_Carate_code varchar(30),_iPaddress varchar(30),_weight decimal(10,3))
BEGIN		
		DECLARE _Carate_weight DECIMAL(10,3);
		DECLARE _weightstatus VARCHAR(10);
		DECLARE t_Per DECIMAL(10,2);
		DECLARE _Captured_Weight DECIMAL(10,3);
		DECLARE _s_Tolerance DECIMAL(10,3);
		SET _Captured_Weight=_weight;
		
		SELECT CM.`Actual_Weight` FROM  tbl_carate_status CM WHERE CM.Carate_Code=_Carate_code INTO _Carate_weight;	
				
		SELECT WC.Tolerance_Per,WC.Static_Tolerance FROM tbl_weight_tolerance_config WC
		WHERE _Carate_weight BETWEEN WC.From_weight AND WC.To_weight LIMIT 1 INTO t_per ,_s_Tolerance;

		SELECT  CASE WHEN (((100+t_per) *CAST(_Carate_weight AS DECIMAL(10,3)))/100)+_s_Tolerance >=CAST(_Captured_Weight AS DECIMAL(10,3))
		 AND  (((100-t_per) *CAST(_Carate_weight AS DECIMAL(10,3)))/100)-_s_Tolerance <=CAST(_Captured_Weight AS DECIMAL(10,3)) THEN 'COMPLETED' ELSE 'ERROR' END  AS STATUS
		INTO _weightstatus;
		
		SELECT IFNULL(_weightstatus,'ERROR') AS StatusforWeightMachine;		
		
		UPDATE `tbl_carate_status` SET Weiging=NOW(),`Capture_Weight`=_weight,Weighing_IP= _iPaddress,
		Duration=TIMESTAMPDIFF(MINUTE,`Inducted`, NOW())
		WHERE `Carate_Code`=_Carate_code AND `Status`=_weightstatus;
		
		Update `tbl_carate_master` set isActive=0 where `Crate_Code`=_Carate_code;

	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getCartonDataforBalanceWeight` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getCartonDataforBalanceWeight` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getCartonDataforBalanceWeight`(_Carton_Code varchar(30))
BEGIN
	Declare _weightinSystem decimal(10,3);
	Select case  when `Actual_Weight`>`Capture_Weight` then ' Under Weight' 
	when `Actual_Weight`<`Capture_Weight` then 'Over Weight' else 'Same Weigth' end as STATUS from `tbl_order_header` OH
	inner Join `tbl_carate_invoice_mapping` CM  on CM.`Invoice_Number`=OH.`Invoice_Number` 
	inner Join `tbl_carate_status` CS  on CS.`Carate_Code`=CM.`Carate_Barcode`  and CS.`Invoice_No`=CM.`Invoice_Number`
	where  CM.`Carate_Barcode`=_Carton_Code and CS.`Weiging` is not null limit 1;	
	
	Select CM.Carate_Barcode as Carton_Code, SM.Sku_Type, OH.Invoice_Number, OL.Sku,OL.Pick_Qty as Qty
	from `tbl_order_header` OH
	inner Join `tbl_order_line_header` OL  on OH.Order_id=OL.Order_id
	inner Join `tbl_ptl_sku_mapping` SM on SM.SKU=OL.Sku 
	inner Join `tbl_carate_invoice_mapping` CM  on CM.`Invoice_Number`=OH.`Invoice_Number`
	where CM.`Carate_Barcode` =_Carton_Code; -- CartonStatus='Error' AND 
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getDashboardGraphData` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getDashboardGraphData` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getDashboardGraphData`(_type varchar(2))
BEGIN
               IF _type='H'  Then
			Begin
				SELECT CONCAT(H.Value,'-',case when H.Value+1>23 then 00 else H.Value+1 end) AS HoursName,IFNULL(T.CompletCartons,0) CompletCartons , DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))  AS CartonCompletionDate 
				FROM tbl_hour_WeekDay_monthlist H  LEFT OUTER JOIN (
				SELECT HOUR(q.`FirstPick`)  AS Hours,COUNT(DISTINCT `Carate_Code`) CompletCartons,DATE(q.`FirstPick`) AS ScanTimeStamp FROM 
				`tbl_carate_status` q 
				WHERE q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -24 HOUR)  AND NOW()  and Q.`Status` not in ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`),HOUR(q.`FirstPick`) ) T  ON T.Hours=H.Value and Date(T.ScanTimeStamp) = DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))
				WHERE H.Type='H' 
				ORDER BY  DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR)),H.ID;
				
				SELECT CONCAT(H.Value,'-',CASE WHEN H.Value+1>23 THEN 00 ELSE H.Value+1 END) AS HoursName,IFNULL(T.CompletOrders,0) CompletOrders , DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))  AS OrderCompletionDate 
				FROM tbl_hour_WeekDay_monthlist H  LEFT OUTER JOIN (
				SELECT HOUR(q.`FirstPick`)  AS Hours,COUNT(DISTINCT OL.Order_ID) CompletOrders,Date(q.`FirstPick`) as ScanTimeStamp FROM 
				`tbl_carate_status` q 
				LEFT OUTER JOIN `tbl_order_header` OL ON OL.`Invoice_Number`=q.Invoice_No
				WHERE (q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -24 HOUR)  AND NOW()) AND Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`),HOUR(q.`FirstPick`) ) T  ON T.Hours=H.Value AND DATE(T.ScanTimeStamp) = DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))
				WHERE H.Type='H' 
				ORDER BY  DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR)),H.ID;
				
				SELECT CONCAT(H.Value,'-',CASE WHEN H.Value+1>23 THEN 00 ELSE H.Value+1 END) AS HoursName,IFNULL(T.CompletEaches,0) CompletEaches , DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))  AS EachesCompletionDate 
				FROM tbl_hour_WeekDay_monthlist H  LEFT OUTER JOIN (
				SELECT HOUR(q.FirstPick)  AS Hours,sum(oh.`Pick_Qty`) CompletEaches,DATE(q.FirstPick) AS ScanTimeStamp FROM 
				tbl_carate_status q 
				inner Join  `tbl_order_header` ol on OL.`Invoice_Number`=q.Invoice_No
				inner Join `tbl_order_line_header` OH  on OH.Order_id=OL.Order_id
				WHERE q.FirstPick BETWEEN DATE_ADD(NOW(),INTERVAL -24 HOUR)  AND NOW()  AND Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.FirstPick),HOUR(q.FirstPick) ) T  ON T.Hours=H.Value AND DATE(T.ScanTimeStamp) = DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR))
				WHERE H.Type='H' 
				ORDER BY  DATE(DATE_ADD(NOW(),INTERVAL -H.Value HOUR)),H.ID;
				
				SELECT ZM.Zone_Name  AS Bay, IFNULL(CompletEachesinBay,0) AS CompletEachesinBay ,DATE(IFNULL(ScanTimeStamp,NOW())) ScanTimeStamp FROM 
				tbl_Zone_Master ZM  LEFT OUTER JOIN (
                                SELECT Scanner_IPAddress,DATE(q.PickStart) ScanTimeStamp,SUM(`PickQty`) CompletEachesinBay
                                FROM `tbl_request_queue_for_ptl` q                                 
                                WHERE DATE(q.`PickStart`) between DATE(Date_Add(NOW(),interval -0 hour)) and DATE(NOW()) AND q.status='COMPLETED'
                                group by Scanner_IPAddress,DATE(q.PickStart)) Q
                                ON ZM.`Scanner_IPAddress`=q.`Scanner_IPAddress` ;
			END;
		End IF;
	      IF _type='W' then
			Begin
				SELECT CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),']') AS WeekName,
				COUNT(DISTINCT `Carate_Code`) CompletCartons ,
				CONCAT('Week of Month (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID DAY))) / 7) + 1,')') AS CartonCompletionDate,DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) daydate FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN  tbl_carate_status_archive q  ON Date(q.FirstPick)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) AND Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				AND  q.FirstPick BETWEEN DATE_ADD(Now(),INTERVAL -8 DAY)  AND DATE_ADD(NOW(), INTERVAL -1 DAY)
				WHERE W.Type='W'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),DATE(q.FirstPick),DAYNAME(q.FirstPick) 
				ORDER BY DATE_ADD(NOW(),INTERVAL -W.ID DAY);
				
				SELECT CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),']') AS WeekName,
				IFNULL(T.OrderCount,0) AS CompletOrders,CONCAT('Week of Month (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID DAY))) / 7) + 1,')') AS OrderCompletionDate ,DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) daydate
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS OrderCompletionDate,COUNT(DISTINCT OL.Order_ID) AS OrderCount 
				FROM tbl_carate_status_archive Q 
				INNER JOIN tbl_order_header_archive OL  ON OL.`Invoice_Number`=Q.`Invoice_No`
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -8 DAY)  AND DATE_ADD(NOW(), INTERVAL -1 DAY) and Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.OrderCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY))
				WHERE W.Type='W'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY))
				ORDER BY DATE_ADD(NOW(),INTERVAL -W.ID DAY);
				
				SELECT CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),']') AS WeekName,
				IFNULL(T.EachesCount,0) AS CompletEaches,CONCAT('Week of Month (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID DAY))) / 7) + 1,')') AS EachesCompletionDate ,DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) daydate
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS EachesCompletionDate,sum(oH.`Pick_Qty`) AS EachesCount 
				FROM tbl_carate_status_archive Q 
				INNER JOIN tbl_order_header_archive OL  ON OL.`Invoice_Number`=Q.`Invoice_No`
				INNER JOIN `tbl_order_line_header_archive` OH  ON OH.Order_id=OL.Order_id
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -8 DAY)  AND DATE_ADD(NOW(), INTERVAL -1 DAY) AND Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.EachesCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY))
				WHERE W.Type='W'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY))
				ORDER BY DATE_ADD(NOW(),INTERVAL -W.ID DAY);
				
			        SELECT ZM.Zone_Name  AS Bay, IFNULL(CompletEachesinBay,0) AS CompletEachesinBay ,DATE(IFNULL(ScanTimeStamp,NOW())) ScanTimeStamp FROM 
				tbl_Zone_Master ZM  LEFT OUTER JOIN (
                                SELECT Scanner_IPAddress,DATE(q.PickStart) ScanTimeStamp,SUM(`PickQty`) CompletEachesinBay
                                FROM `tbl_request_queue_for_ptl` q                                 
                                WHERE DATE(q.`PickStart`) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -0 HOUR)) AND DATE(NOW()) AND q.status='COMPLETED'
                                GROUP BY Scanner_IPAddress,DATE(q.PickStart)) Q
                                ON ZM.`Scanner_IPAddress`=q.`Scanner_IPAddress` ;
				
		        End;
	       End IF;
	       IF _type='D' then
			Begin
								
				SELECT Day_Name,COUNT(DISTINCT q.`Carate_Code`) CompletCartons,CartonCompletionDate,daydate  FROM (
				SELECT DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) AS Day_Name,CONCAT('Week (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID DAY))) / 7) + 1,')') AS CartonCompletionDate,DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) daydate  
				FROM tbl_hour_WeekDay_monthlist W WHERE W.Type='D'  
				ORDER BY DATE(DATE_ADD(NOW(),INTERVAL -W.ID DAY)) ) T
				LEFT OUTER JOIN  tbl_carate_status_archive q  ON DATE(q.FirstPick)=T.Day_Name 
				AND  q.FirstPick BETWEEN DATE_ADD(NoW(),INTERVAL -31 DAY)  AND DATE_ADD(NOW(),INTERVAL -1 DAY)
				GROUP BY Day_Name,CartonCompletionDate,daydate
				order by Day_Name;				
			
				SELECT DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) AS daydate,
				IFNULL(T.OrderCount,0) AS CompletOrders,CONCAT('Week (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS OrderCompletionDate ,DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) Day_Name
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS OrderCompletionDate,COUNT(DISTINCT OL.Order_ID) AS OrderCount 
				FROM tbl_carate_status_archive Q 
				INNER JOIN tbl_order_header_archive OL  ON OL.`Invoice_Number`=Q.`Invoice_No`
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY)  AND NOW() and Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.OrderCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				WHERE W.Type='D'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				ORDER BY DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY);
				
				SELECT DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) AS daydate,
				IFNULL(T.EachesCount,0) AS CompletEaches,CONCAT('Week (',FLOOR((DAYOFMONTH(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))) / 7) + 1,')') AS EachesCompletionDate ,DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)) Day_Name
				FROM tbl_hour_WeekDay_monthlist W
				LEFT OUTER JOIN (SELECT  DATE(q.`FirstPick`) AS EachesCompletionDate,SUM(oH.`Pick_Qty`) AS EachesCount 
				FROM tbl_carate_status_archive Q 
				INNER JOIN tbl_order_header_archive OL  ON OL.`Invoice_Number`=Q.`Invoice_No`
				INNER JOIN `tbl_order_line_header_archive` OH  ON OH.Order_id=OL.Order_id
				WHERE  q.`FirstPick` BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY)  AND NOW() AND Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				GROUP BY DATE(q.`FirstPick`)) T  ON  DATE(T.EachesCompletionDate)=DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				WHERE W.Type='D'  
				GROUP BY CONCAT(SUBSTRING(DAYNAME(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),1,3),'[',DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY)),']'),DATE(DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY))
				ORDER BY DATE_ADD(NOW(),INTERVAL -W.ID+1 DAY);
								
			       	SELECT ZM.Zone_Name  AS Bay, IFNULL(CompletEachesinBay,0) AS CompletEachesinBay ,DATE(IFNULL(ScanTimeStamp,NOW())) ScanTimeStamp FROM 
				tbl_Zone_Master ZM  LEFT OUTER JOIN (
                                SELECT Scanner_IPAddress,DATE(q.PickStart) ScanTimeStamp,SUM(`PickQty`) CompletEachesinBay
                                FROM `tbl_request_queue_for_ptl` q                                 
                                WHERE DATE(q.`PickStart`) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -0 HOUR)) AND DATE(NOW()) AND q.status='COMpLETED'
                                GROUP BY Scanner_IPAddress,DATE(q.PickStart)) Q
                                ON ZM.`Scanner_IPAddress`=q.`Scanner_IPAddress` ;
			End;
	      End IF;
	      IF _type='M' THEN
			BEGIN
				SELECT W.Value AS sMonthName,COUNT(DISTINCT `Carate_Code`) CompletCartons,YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) CartonCompletionDate
				,W.Quarter _Quarter FROM tbl_hour_WeekDay_monthlist W 
				LEFT OUTER JOIN  tbl_carate_status_archive q  ON MONTH(q.`FirstPick`)=W.ID 
				AND  q.FirstPick BETWEEN DATE_ADD(Now(),INTERVAL -12 MONTH)  AND Now() and Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				WHERE TYPE='M' 
				GROUP BY W.Value,W.Quarter,YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH))
				ORDER BY YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) , W.ID;
			
				SELECT W.Value AS sMonthName,IFNULL(T.CompletOrders,0) AS CompletOrders,YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) OrderCompletionDate
				,W.Quarter _Quarter FROM tbl_hour_WeekDay_monthlist W 
				LEFT OUTER JOIN (
				SELECT MONTHNAME(Q.FirstPick) AS sMonthName,YEAR(Q.FirstPick) AS sYear ,COUNT( DISTINCT ol.Order_id) AS CompletOrders  FROM
				tbl_order_header_archive OL  
				INNER JOIN   tbl_carate_status_archive q ON OL.`Invoice_Number`=Q.`Invoice_No`
				WHERE DATE(q.FirstPick) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -12 MONTH))  AND DATE(NOW()) 
				AND Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				GROUP BY MONTHNAME(Q.FirstPick)) T  ON CONVERT(T.sMonthName, CHAR)=CONVERT(W.Value,CHAR)
				WHERE TYPE='M' 
				ORDER BY YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) , W.ID;
				
				SELECT W.Value AS sMonthName,IFNULL(T.EachesCount,0) AS CompletEaches,YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) EachesCompletionDate
				,W.Quarter _Quarter FROM tbl_hour_WeekDay_monthlist W 
				LEFT OUTER JOIN (
				SELECT MONTHNAME(Q.FirstPick) AS sMonthName,YEAR(Q.FirstPick) AS sYear ,sum(oH.`Pick_Qty`) AS EachesCount  FROM
				tbl_order_header_archive OL  
				INNER JOIN   tbl_carate_status_archive q  ON OL.`Invoice_Number`=Q.`Invoice_No`
				INNER JOIN `tbl_order_line_header_archive` OH  ON OH.Order_id=OL.Order_id
				WHERE DATE(q.FirstPick) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -12 MONTH))  AND DATE(NOW())
				AND Q.`Status` NOT IN ('INDUCTED','INPROCESS')
				GROUP BY MONTHNAME(Q.FirstPick)) T  ON CONVERT(T.sMonthName, CHAR)=CONVERT(W.Value,CHAR)
				WHERE TYPE='M' 
				ORDER BY YEAR( DATE_ADD(NOW(),INTERVAL -W.ID+1 MONTH)) , W.ID;
				
			       	SELECT ZM.Zone_Name  AS Bay, IFNULL(CompletEachesinBay,0) AS CompletEachesinBay ,DATE(IFNULL(ScanTimeStamp,NOW())) ScanTimeStamp FROM 
				tbl_Zone_Master ZM  LEFT OUTER JOIN (
                                SELECT Scanner_IPAddress,DATE(q.PickStart) ScanTimeStamp,SUM(`PickQty`) CompletEachesinBay
                                FROM `tbl_request_queue_for_ptl` q                                 
                                WHERE DATE(q.`PickStart`) BETWEEN DATE(DATE_ADD(NOW(),INTERVAL -0 HOUR)) AND DATE(NOW()) AND q.status='COMpLETED'
                                GROUP BY Scanner_IPAddress,DATE(q.PickStart)) Q
                                ON ZM.`Scanner_IPAddress`=q.`Scanner_IPAddress` ;
				
		        END;
	       END IF;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_GetInvoiceStatus` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_GetInvoiceStatus` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GetInvoiceStatus`(_Invoice_No varchar(30))
BEGIN
             SELECT concat(IFNULL(CS.`Status`,'PENDING') ,'|',`Order_id`) as _Status
             FROM `tbl_order_header` OH 
             LEFT OUTER JOIN  `tbl_carate_status` CS ON OH.`Invoice_Number`=CS.Invoice_No 
             WHERE OH.`Invoice_Number`=_Invoice_No LIMIT 1;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getTableWiseDabataseSize` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getTableWiseDabataseSize` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getTableWiseDabataseSize`()
BEGIN
			SELECT
			  TABLE_NAME AS `Table`,
			  ROUND((DATA_LENGTH + INDEX_LENGTH) / 1024 / 1024) AS `Size (MB)`
			FROM
			  information_schema.TABLES
			WHERE
			  TABLE_SCHEMA = 'amway_ptl_540'
			ORDER BY
			  (DATA_LENGTH + INDEX_LENGTH)
			DESC;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_GetValidateInvoice` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_GetValidateInvoice` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_GetValidateInvoice`(_Invoice_No varchar(30))
BEGIN
                Select OH.`Invoice_Number` from `tbl_order_header` OH 
                Left outer Join `tbl_carate_invoice_mapping` CM  on CM.`Invoice_Number`=OH.Invoice_Number
                Left outer Join `tbl_carate_status` CS  on CS.`Invoice_No`=OH.OH.`Invoice_Number`
                where CS.`Status` is null;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_ArchiveData` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_ArchiveData` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ArchiveData`()
BEGIN
	if Hour(Now())>=23 then
		insert ignore into `tbl_carate_invoice_mapping_archive`(`Carate_Barcode`,`Source_IPAddress`,`Scanner_IP`,`Invoice_Number`,`Mapping_Date`,`Mapped_By`)
		select CM.`Carate_Barcode`,CM.`Source_IPAddress`,CM.`Scanner_IP`,CM.`Invoice_Number`,CM.`Mapping_Date`,CM.`Mapped_By` 
		from `tbl_carate_invoice_mapping` CM  
		inner Join `tbl_carate_status` CS  on CS.`Carate_Code`= CM.`Carate_Barcode` and CM.`Invoice_Number`=CS.`Invoice_No`
		where CS.`Status` in ('COMPLETED','ERROR') and Date(CS.`Weiging`)< Date(Date_Add(Now(),interval -31 day));
		
		Delete CM from `tbl_carate_invoice_mapping` CM  
		inner Join  `tbl_carate_status` CS  ON CS.`Carate_Code`= CM.`Carate_Barcode` AND CM.`Invoice_Number`= CS.`Invoice_No`
		WHERE CS.`Status` IN ('COMPLETED','ERROR') AND DATE(CS.`Weiging`)< DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));
		
		insert ignore `tbl_carate_status_Archive` (`Carate_Code`,`Invoice_No`,`Actual_Weight`,`Capture_Weight`,`Status`,`Inducted`,`FirstPick`,`Weiging`,`Weighing_IP`,`Duration`)
		select `Carate_Code`,`Invoice_No`,`Actual_Weight`,`Capture_Weight`,`Status`,`Inducted`,`FirstPick`,`Weiging`,`Weighing_IP`,`Duration` 
		from  tbl_carate_status CS WHERE CS.`Status` IN ('COMPLETED','ERROR') 
		AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));
		
		Delete from tbl_carate_status where `Status` IN ('COMPLETED','ERROR') 
		AND DATE(`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));
		
		insert IGNORE into `tbl_operator_login_Archive`(`Login_Id`,`Login_DateTime`,`Logout_Datetime`,`Duration`,`Login_By`)
		select `Login_Id`,`Login_DateTime`,`Logout_Datetime`,`Duration`,`Login_By` from `tbl_operator_login` 
		where Date(`Logout_Datetime`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));
		
		Delete from `tbl_operator_login` WHERE DATE(`Logout_Datetime`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));

		
		insert ignore into `tbl_order_line_header_Archive` (`Order_id`,`Line_Item_Seq_no`,`Sku`,`Pick_Qty`,`Pick_Location`,`Location`,`Added_DateTime`)
		select OL.`Order_id`,OL.`Line_Item_Seq_no`,OL.`Sku`,OL.`Pick_Qty`,OL.`Pick_Location`,OL.`Location`,OL.`Added_DateTime`
		from `tbl_order_line_header` OL  
		inner Join `tbl_order_header` OH  on OL.Order_id=OH.Order_id
		INNER JOIN `tbl_carate_status` CS  ON CS.`Invoice_No`=OH.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR') and DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));
		
		Delete OL from 	`tbl_order_line_header` OL  
		INNER JOIN `tbl_order_header` OH  ON OL.Order_id=OH.Order_id
		INNER JOIN `tbl_carate_status` CS  ON CS.`Invoice_No`=OH.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR') AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));	
		
		INSERT IGNORE INTO `tbl_order_header_Archive`(`Order_Number`,`Order_Datetime`,`Order_Weight`,`Order_Status`,`Invoice_Number`,`Invoice_Date`,`Warehouse_id`,`Line_Items`,`Sales_date`,`due_Date`,`Delivery_date`,`Added_Datetime`)
		SELECT OH.`Order_Number`,OH.`Order_Datetime`,OH.`Order_Weight`,OH.`Order_Status`,OH.`Invoice_Number`,OH.`Invoice_Date`,OH.`Warehouse_id`,OH.`Line_Items`,OH.`Sales_date`,OH.`due_Date`,OH.`Delivery_date`,OH.`Added_Datetime`
		FROM `tbl_order_header` OH  INNER JOIN `tbl_carate_status` CS  ON CS.`Invoice_No`=OH.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR')AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));
				
		DELETE OH  FROM `tbl_order_header` OH  INNER JOIN `tbl_carate_status` CS  ON CS.`Invoice_No`=OH.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR')AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));		
		
		insert ignore into `tbl_request_queue_for_ptl_archive` (`Carate_Code`,`Invoice_Number`,`Pick_Location`,`Sku`,`Sku_Type`,`Scanner_IPAddress`,`PTL_IPAddress`,`LCD_IPAddress`,`PTL_SendPort`,`LCD_SendPort`,`PickQty`,`Picked_Qty`,`Status`,`PickStart`,`PickConfirmation`,`PickDuration`,`DisplayMSG`)
		select Q.`Carate_Code`,Q.`Invoice_Number`,Q.`Pick_Location`,Q.`Sku`,Q.`Sku_Type`,Q.`Scanner_IPAddress`,Q.`PTL_IPAddress`,Q.`LCD_IPAddress`,Q.`PTL_SendPort`,Q.`LCD_SendPort`,Q.`PickQty`,Q.`Picked_Qty`,Q.`Status`,Q.`PickStart`,Q.`PickConfirmation`,Q.`PickDuration`,Q.`DisplayMSG`
		from `tbl_request_queue_for_ptl` Q 
		INNER JOIN `tbl_carate_status` CS  ON CS.`Invoice_No`=Q.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR') AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));
		
		Delete Q from tbl_request_queue_for_ptl Q 
		INNER JOIN `tbl_carate_status` CS  ON CS.`Invoice_No`=Q.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR')AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));
		
		insert into `tbl_scan_pick_details_archive`(`Sku`,`Ser_no`,`ScanDatetime`,`Scan_by`,`Carate_Code`,`Invoice_No`,`ScanedCode`,`QueueID`)
		select S.`Sku`,S.`Ser_no`,S.`ScanDatetime`,S.`Scan_by`,S.`Carate_Code`,S.`Invoice_No` ,`ScanedCode`,`QueueID`
		from `tbl_scan_pick_details` S
		inner Join `tbl_carate_status` CS  ON CS.`Invoice_No`=S.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR') AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));
		
		Delete S  from `tbl_scan_pick_details` S
		INNER JOIN `tbl_carate_status` CS  ON CS.`Invoice_No`=S.`Invoice_Number`
		WHERE CS.`Status` IN ('COMPLETED','ERROR')AND DATE(CS.`Weiging`)<DATE(DATE_ADD(NOW(),INTERVAL -31 DAY));	
	End if;	
		
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_createOperator` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_createOperator` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_createOperator`(_name varchar(20),_code varchar(30),_createdBy varchar(20),_userType varchar(10),_password varchar(32))
BEGIN
	 Declare _username varchar(20);
	 select count(1) from tbl_users where (USER_ID=_name or UserCode=_code) into _username;
	 IF _username>0 then
		Begin
			 select _name,_code;
		End;
	Else 
		Begin
			insert into tbl_users (USER_ID,PASSWORD, USER_CATEGORY,UserCode,CreatedBy,CreatedDate)
			values(_name,_password,Upper(_userType),_code,_createdBy,now());
		End;
	End IF;
    END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getBayConfigurationDetails` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getBayConfigurationDetails` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getBayConfigurationDetails`(_bayname varchar(5))
BEGIN
             IF _bayname!='' then
		Begin
		  Select Zone_Name,Zone_IPAddress as Zone_IP_Address,Display_Name,Scanner_IpAddress as Scanner_IP_Address ,
		  LCD_IpAddress as Display_IP_Address,'Admin' as CreatedBy
		  from tbl_zone_master where Zone_Name=_bayname limit 1;
		End;
	     Else
		Begin
		  SELECT Zone_Name,Zone_IPAddress as Zone_IP_Address,Display_Name,Scanner_IpAddress as Scanner_IP_Address,
		  LCD_IpAddress as Display_IP_Address,'Admin' AS CreatedBy
		  FROM tbl_zone_master ;
		End;
	    End IF;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getBaywisePTLConfigurations` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getBaywisePTLConfigurations` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getBaywisePTLConfigurations`(_bayname varchar(5))
BEGIN
             Select zpm.Zone_ID as Zone_name, PM.PICK_LOcation as PTLLocation,'' as Location,psm.Sku_Type,
             sm.SKU  as ActiveSku, '' as  Description,sm.SKU  as Alternate_Sku_code 
             from `tbl_zone_master` zpm 
			 inner Join `tbl_zone_ptl_mapping` PM on PM.ZONE_ID=zpm.ZONE_ID
			 inner Join `tbl_ptl_sku_mapping` psm on psm.PICK_LOcation=PM.PICK_LOcation
             inner Join sku_master sm on sm.Sku_ID=psm.Sku_ID
             where zpm.Zone_Name=_bayname order by PM.PICK_Location;
             
END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_DetailReport` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_DetailReport` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_DetailReport`(_frequency VARCHAR(1))
BEGIN
		IF _frequency='D' THEN
			SELECT DATE(OH.`Order_Datetime`) AS Order_Date,COUNT(OH.`Order_Number`) OrderCount,COUNT(CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', (Q.Crates/COUNT(CM.`Carate_Barcode`)) '%Crate'
			FROM `tbl_order_header` OH 
			INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header` OLH ON OLH.`Order_id`=OH.order_id
			LEFT OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,
					COUNT(`Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers FROM`tbl_request_queue_for_ptl` Q
					WHERE DATE(Q.`PickStart`)=DATE(NOW())
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(OH.`Order_Datetime`)
			WHERE DATE(OH.`Order_Datetime`)=DATE(NOW())
			GROUP BY DATE(OH.`Order_Datetime`);
		END IF;
		IF _frequency='W' THEN
			SELECT DATE(OH.`Order_Datetime`) AS Order_Date,COUNT(OH.`Order_Number`) OrderCount,COUNT(CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', (Q.Crates/COUNT(CM.`Carate_Barcode`)) '%Crate'
			FROM `tbl_order_header` OH 
			INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header` OLH ON OLH.`Order_id`=OH.order_id
			LEFT OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,
					COUNT(`Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers FROM`tbl_request_queue_for_ptl` Q
					WHERE DATE(Q.`PickStart`)BETWEEN DATE_ADD(NOW(),INTERVAL -7 DAY) AND DATE(NOW())
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(OH.`Order_Datetime`)
			WHERE DATE(OH.`Order_Datetime`) between DATE_Add(NOW(),interval -7 day) and Date(Now())
			GROUP BY DATE(OH.`Order_Datetime`);
		END IF;
		if _frequency='M' then
			SELECT DATE(OH.`Order_Datetime`) AS Order_Date,COUNT(OH.`Order_Number`) OrderCount,COUNT(CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', (Q.Crates/COUNT(CM.`Carate_Barcode`)) '%Crate'
			FROM `tbl_order_header` OH 
			INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header` OLH ON OLH.`Order_id`=OH.order_id
			LEFT OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,
					COUNT(`Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers FROM`tbl_request_queue_for_ptl` Q
					WHERE DATE(Q.`PickStart`)BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY) AND DATE(NOW())
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(OH.`Order_Datetime`)
			WHERE DATE(OH.`Order_Datetime`) BETWEEN DATE_ADD(NOW(),INTERVAL -31 DAY) AND DATE(NOW())
			GROUP BY DATE(OH.`Order_Datetime`);
		End if;
		if _frequency='Y' then
			SELECT DATE(OH.`Order_Datetime`) AS Order_Date,COUNT(OH.`Order_Number`) OrderCount,COUNT(CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', (Q.Crates/COUNT(CM.`Carate_Barcode`)) '%Crate'
			FROM `tbl_order_header` OH 
			INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header` OLH ON OLH.`Order_id`=OH.order_id
			LEFT OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,
					COUNT(`Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers FROM`tbl_request_queue_for_ptl` Q
					WHERE DATE(Q.`PickStart`)BETWEEN DATE_ADD(NOW(),INTERVAL -12 MONTH) AND DATE(NOW())
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(OH.`Order_Datetime`)
			WHERE DATE(OH.`Order_Datetime`) BETWEEN DATE_ADD(NOW(),INTERVAL -12 Month) AND DATE(NOW())
			GROUP BY DATE(OH.`Order_Datetime`)
			union
			SELECT DATE(OH.`Order_Datetime`) AS Order_Date,COUNT(OH.`Order_Number`) OrderCount,COUNT(CM.`Carate_Barcode`) CrateCount,
			COUNT(OLH.`Sku`) LineItems,SUM(OLH.`Pick_Qty`) AS Eaches,Q.StartPick,Q.EndPick,
			Q.PickDuration,Q.Crates PickCrates,Q.LineItems AS PickLines,Q.Eaches AS PickEaches, ROUND((Q.Eaches/HOUR(Q.PickDuration))/Pickers,0) AS 'Moderated Rate', (Q.Crates/COUNT(CM.`Carate_Barcode`)) '%Crate'
			FROM `tbl_order_header_Archive` OH 
			INNER JOIN `tbl_carate_invoice_mapping_Archive` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
			INNER JOIN `tbl_order_line_header_Archive` OLH ON OLH.`Order_id`=OH.order_id
			LEFT OUTER JOIN 
				(
					SELECT DATE(Q.`PickStart`) AS PickDate, COUNT(`Sku`) AS LineItems,SUM(`PickQty`) AS Eaches,MIN(`PickStart`) AS StartPick, 
					MAX(`PickConfirmation`) AS EndPick, TIMEDIFF(MAX(`PickConfirmation`) ,MIN(`PickStart`)) AS PickDuration,
					COUNT(`Carate_Code`) Crates,COUNT(DISTINCT `Scanner_IPAddress`) AS Pickers 
					FROM`tbl_request_queue_for_ptl_Archive` Q
					WHERE DATE(Q.`PickStart`)BETWEEN DATE_ADD(NOW(),INTERVAL -12 MONTH) AND DATE(NOW())
					GROUP BY DATE(Q.`PickStart`)
				) Q
			ON Q.PickDate= DATE(OH.`Order_Datetime`)
			WHERE DATE(OH.`Order_Datetime`) BETWEEN DATE_ADD(NOW(),INTERVAL -12 MONTH) AND DATE(NOW())
			GROUP BY DATE(OH.`Order_Datetime`);
		End if;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_SearchMapping` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_SearchMapping` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_SearchMapping`(_condition varchar(1000))
BEGIN
		/*SET @t1 =CONCAT("SELECT zpm.Zone_ID AS BayName,pm.PTLLocation,sm.sku_code,sm.Description,sm.Alternate_Sku_Code FROM zone_ptl_mapping zpm 
				INNER JOIN ptl_sku_mapping psm ON psm.PTL_code=zpm.PTL_Code
				INNER JOIN sku_master sm ON sm.SKU_ID=psm.SKU_ID 
				INNER JOIN ptl_master pm ON pm.PTL_code=psm.PTL_code WHERE " , _condition);*/
		 SET @t1=_condition;
		 PREPARE stmt3 FROM @t1;
		 EXECUTE stmt3;
		 DEALLOCATE PREPARE stmt3;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getCartonHistory` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getCartonHistory` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getCartonHistory`(_carton_code varchar(30),_CodeType varchar(10))
BEGIN
	IF _CodeType='CRT' then
		IF Not Exists (Select Carate_Barcode from tbl_carate_invoice_mapping_Archive where Carate_Barcode=_carton_code limit 1) then
			Begin
				Select OH.Order_Number as Order_No,OH.Invoice_Number ,'' as Record_Type,OH.`Sales_Date` as Order_Date,
				OL.Sku,OL.Pick_Qty as Qty,OH.Order_weight as Weight,OL.PICK_LOCATION 
				from tbl_order_line_header_archive OL 
				inner join tbl_order_header_archive OH on  OL.Order_Id=OH.Order_Id 
				inner Join `tbl_carate_invoice_mapping_Archive` CM  on CM.`Invoice_Number`=OH.`Invoice_Number`
				where CM.`Carate_Barcode`=_carton_code;
			End;
		Else
			Begin
				SELECT OH.Order_Number AS Order_No,OH.Invoice_Number ,'' AS Record_Type,OH.`Sales_Date` AS Order_Date,
				OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION 
				FROM tbl_order_line_header OL 
				INNER JOIN tbl_order_header OH ON  OL.Order_Id=OH.Order_Id 
				INNER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
				WHERE CM.`Carate_Barcode`=_carton_code;		
			End;
		End IF;
	End if;
	if _CodeType='INV' then
		IF NOT EXISTS (SELECT Invoice_Number FROM tbl_order_header WHERE Invoice_Number=_carton_code LIMIT 1) THEN
			BEGIN
				SELECT OH.Order_Number AS Order_No,OH.Invoice_Number,'' AS Record_Type,OH.`Sales_date` AS Order_Date,
				OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION 
				FROM tbl_order_line_header_archive OL 
				INNER JOIN tbl_order_header_archive OH ON  OL.Order_Id=OH.Order_Id 
				Left outer JOIN `tbl_carate_invoice_mapping_Archive` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
				WHERE OH.Invoice_Number=_carton_code;
			END;
		ELSE
			BEGIN
				SELECT OH.Order_Number AS Order_No,OH.Invoice_Number,'' AS Record_Type,OH.`Sales_date` AS Order_Date,
				OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION 
				FROM tbl_order_line_header OL 
				INNER JOIN tbl_order_header OH ON  OL.Order_Id=OH.Order_Id 
				LEFT OUTER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
				WHERE OH.Invoice_Number=_carton_code;
			END;
		END IF;
	End if;
	if _CodeType='' then
		SELECT OH.Order_Number AS Order_No,OH.Invoice_Number,'' AS Record_Type,OH.`Sales_date` AS Order_Date,
		OL.Sku,OL.Pick_Qty AS Qty,OH.Order_weight AS Weight,OL.PICK_LOCATION 
		FROM tbl_order_line_header_archive OL 
		INNER JOIN tbl_order_header_archive OH ON  OL.Order_Id=OH.Order_Id 
		LEFT OUTER JOIN `tbl_carate_invoice_mapping_Archive` CM  ON CM.`Invoice_Number`=OH.`Invoice_Number`
		WHERE Date(OH.`Added_Datetime`)=Date(Now());
	End IF;	    
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_getDataforDisplayonLED` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_getDataforDisplayonLED` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_getDataforDisplayonLED`(_LCD_IP varchar(30))
BEGIN
		Declare _Scanner_IP VARCHAR(30)	;
		select `Scanner_IPAddress` from `tbl_request_queue_for_ptl` 
		where `LCD_IPAddress`=_LCD_IP and `Status`='INPROCESS' limit 1  into _Scanner_IP;
			
		select  Concat('INV  :',repeat(' ',(14- Length(Q.`Invoice_Number`))),Q.`Invoice_Number`,
			       'CAR  :',REPEAT(' ',(14- Length(Q.`Carate_Code`))),Q.`Carate_Code`,
			       'PICK :',REPEAT(' ',(14-Length(COUNT(Q.`QueueID`)))),count(Q.`QueueID`)
			     ) as Message
		from `tbl_request_queue_for_ptl` Q
		where Q.`Scanner_IPAddress`=_Scanner_IP and Q.`Status`='INPROCESS' and Q.DisplayMSG=0;
		
		Update  `tbl_request_queue_for_ptl` set DisplayMSG=1 
		where DisplayMSG=0 and `Scanner_IPAddress`=_Scanner_IP and `Status`='INPROCESS';-- and `Status`='INPROCESS';
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_getIPList` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_getIPList` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_getIPList`()
BEGIN
		Select distinct `Zone_IPAddress` as IPAddress,`PTL_SendPort` as SendPort,`PTL_ReceivePort`as ReceivedPort,'PTL' as _Type ,Zone_Type
		from `tbl_zone_master` 
		union all
		select distinct `LCD_IPAddress` as IPAddress,`LCD_SendPort` as SendPort,`LCD_SendPort` AS ReceivedPort,'LED' as _Type ,Zone_Type
		from `tbl_zone_master`
		union all
		select distinct `Scanner_IPAddress` as IPAddress,`Scanner_ReceivePort` AS SendPort,`Scanner_ReceivePort` AS ReceivedPort,'Scanner' AS _Type ,Zone_Type
		FROM `tbl_zone_master`
		union all
		Select '192.168.10.20' as IPAddress,'50001' as SendPort,'50001' as ReceivedPort,'Carate' as _Type,Zone_Type FROM `tbl_zone_master`
		union all
	        SELECT '192.168.250.1' AS IPAddress,'50001' AS SendPort,'50001' AS ReceivedPort,'Weight' AS _Type,Zone_Type FROM `tbl_zone_master`;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_ValidateCarton_code` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_ValidateCarton_code` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_ValidateCarton_code`(_Carton_code varchar(100),_IPAddress varchar(30))
BEGIN
	     DEclare _ccode varchar(30);
	     Declare _ip varchar(30);
	     Declare _Zone_Type varchar(100);
            Select Carton_code from tbl_Order_Header OL  where `Invoice_Number`=_Carton_code limit 1 into _ccode;
            Select `Zone_IPAddress`,`Zone_Type` from tbl_Zone_Master  where Scanner_IPAddress=_IPAddress limit 1 into _ip,_Zone_Type;
            select IFNULL(_ccode,'') as Invoice_Number,_ip as Zone_IP_Address,_Zone_Type as Zone_Type;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_WeightTolerance` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_WeightTolerance` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_WeightTolerance`(_ipaddress varchar(30),_tolerance decimal(10,3),_flag bit)
BEGIN
           IF _flag=0 then
		Begin
			Select `From_Weight`,`To_Weight`,`Tolerance_per`,`Static_Tolerance` from `tbl_weight_tolerance_config`;
		End;
	   Else
		Begin
		       Update weight_config set isActive=0 ,DeActivation_Date=now() where isActive=1;
		       
		       insert into weight_config(IPAddress,Min_Percent,Max_Percent)
		       values(_ipaddress,_tolerance,_tolerance);
		       
		       SELECT IPAddress,Min_Percent FROM weight_config WHERE isActive=1;
		 End;
	 End IF;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getPickerProductivity_Report` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getPickerProductivity_Report` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getPickerProductivity_Report`(_fromdatetime DATETIME,_todatetime DATETIME)
BEGIN

	select OL.`Login_Id`,Q.`Invoice_Number`,OH.`Invoice_Date`,OLH.`Sku`,PLH.`Pick_Qty`,Q.`PickDuration`,
	ZM.`Display_Name` as Zone_ID,Q.`Pick_Location`,OH.`Order_Weight`,CS.`Capture_Weight`,time(`Weiging`)
	from `tbl_operator_login` OL
	inner Join `tbl_request_queue_for_ptl` Q  on `Login_By`=`Scanner_IPAddress`
	inner Join `tbl_order_header` OH  on OH.`Invoice_Number`=Q.Invoice_Number
	inner Join `tbl_order_line_header` OLH on OLH.Order_id=OH.Order_id
	inner Join `tbl_zone_master` ZM  on ZM.`Scanner_IPAddress`=Q.`Scanner_IPAddress`
	inner Join  `tbl_carate_status` CS  on CS.`Invoice_No`=OH.Invoice_Number
	where OL.`Login_DateTime` between _fromdatetime and _todatetime;
	
END */$$
DELIMITER ;

/* Procedure structure for procedure `sp_getScannedData_Report` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_getScannedData_Report` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_getScannedData_Report`(_fromdatetime DATETIME,_todatetime DATETIME,_Code varchar(100),_ReportType varchar(20))
BEGIN
		
		if _ReportType='Date' then
			SELECT S.`Invoice_No` as Order_No, S.`Carate_Code` as Carton_Code,concat(Sku,'.',`Ser_no`) as Sku_Code ,
			Q.`Pick_Location` ,ScanDatetime ,OH.`Invoice_Date`,
			COUNT(S.Sku) Qty,OL.`Login_Id`,ZM.`Display_Name` as Zone_ID
			FROM `tbl_scan_pick_details` S
			inner Join `tbl_operator_login` OL on Date(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) and S.`Scan_by`=OL.`Login_By`
			inner Join `tbl_zone_master` ZM  on ZM.`Zone_IPAddress`=OL.`Login_By`
			inner Join `tbl_request_queue_for_ptl` Q on Q.`QueueID`=S.`QueueID`
			inner Join `tbl_order_line_header` OH  on OH.`Invoice_Number`=Q.`Invoice_Number`
			WHERE DATE(S.`ScanDatetime`) BETWEEN DATE(_fromdatetime) AND DATE(_todatetime)
			GROUP BY S.Invoice_No,S.Carate_Code,S.Sku;
		end if;
		if _ReportType='Invoice' then
			SELECT S.`Invoice_No` AS Order_No, S.`Carate_Code` AS Carton_Code,CONCAT(S.Sku,'.',S.`Ser_no`) AS Sku_Code ,
			Q.`Pick_Location` ,TIME(ScanDatetime) AS ScanTime,OH.`Invoice_Date`,
			COUNT(S.Sku) Qty,OL.`Login_Id`,ZM.`Display_Name` AS Zone_ID
			FROM `tbl_scan_pick_details` S
			INNER JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By`
			INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=OL.`Login_By`
			INNER JOIN `tbl_request_queue_for_ptl` Q ON Q.`QueueID`=S.`QueueID`
			INNER JOIN `tbl_order_line_header` OH  ON OH.`Invoice_Number`=Q.`Invoice_Number`
			WHERE OH.Invoice_Number=_Code
			GROUP BY S.Invoice_No,S.Carate_Code,S.Sku;
		End if;
		if _ReportType ='Sku' then
			if Locate('.',_Code)>0 and Locate('=',_Code)>0 then
				SELECT S.`Invoice_No` AS Order_No, S.`Carate_Code` AS Carton_Code,ScanedCode AS Sku_Code ,
				'NONE' as Pick_Location ,TIME(ScanDatetime) AS ScanTime,OH.`Invoice_Date`,
				COUNT(S.Sku) Qty,OL.`Login_Id`,ZM.`Display_Name` AS Zone_ID
				FROM `tbl_scan_pick_details` S
				INNER JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By`
				INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=OL.`Login_By`
				INNER JOIN `tbl_order_line_header` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
				WHERE S.`ScanedCode`=_Code
				GROUP BY S.Invoice_No,S.Carate_Code,S.ScanedCode;
			else 
				SELECT S.`Invoice_No` AS Order_No, S.`Carate_Code` AS Carton_Code,CONCAT(S.Sku,'.',S.`Ser_no`) AS Sku_Code ,
				Q.`Pick_Location` ,TIME(ScanDatetime) AS ScanTime,OH.`Invoice_Date`,
				COUNT(S.Sku) Qty,OL.`Login_Id`,ZM.`Display_Name` AS Zone_ID
				FROM `tbl_scan_pick_details` S
				INNER JOIN `tbl_operator_login` OL ON DATE(OL.`Login_DateTime`)=DATE(S.`ScanDatetime`) AND S.`Scan_by`=OL.`Login_By`
				INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=OL.`Login_By`
				INNER JOIN `tbl_request_queue_for_ptl` Q ON Q.`QueueID`=S.`QueueID`
				INNER JOIN `tbl_order_line_header` OH  ON OH.`Invoice_Number`=S.`Invoice_No`
				WHERE CONCAT(S.Sku,'.',S.`Ser_no`)=_Code
				GROUP BY S.Invoice_No,S.Carate_Code,S.Sku;
			end if;
		end if;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_Request_for_PTL` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_Request_for_PTL` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Request_for_PTL`(_ScanCode varchar(30),_scanner_ip varchar(30),_mode int)
BEGIN
/* TRIM(TRAILING '\r\n' FROM Scanner_IPAddress)*/
 /* _mode :=> 0--> Login,
	      1--> Logout,
	      2--> Pick */
	      Declare _Crate_code varchar(100);
		IF _mode=0 then
			if exists (select `User_ID` from `tbl_users` where `User_ID`=_ScanCode and `user_category`='OPERATOR') then
				IF  not  exists(select `Login_Id` from tbl_operator_login where `Login_Id`=_ScanCode and Date(`Login_DateTime`)=Date(Now())) then
					insert into `tbl_operator_login` (`Login_Id`,`Login_DateTime`,`Login_By`)
					values(_ScanCode,now(),_scanner_ip);
					
					Update `tbl_zone_master` set `Status`='ACTIVE'  where `Scanner_IPAddress`=_scanner_ip;
					
					Select distinct `Zone_IPAddress`,`LCD_IPAddress`,`PTL_SendPort`,`LCD_SendPort` from `tbl_zone_master` ZM 
					inner Join `tbl_zone_ptl_mapping` ZPM  on ZPM.`Zone_ID`=ZM.`Zone_Id`
					where Scanner_IPAddress=_scanner_ip ;
				end if;
			end if;
		End if;
		if _mode=1 then
			if exists (select `Login_Id` from `tbl_operator_login` where `Login_By`=_scanner_ip and `Logout_Datetime` is null) then
				update tbl_operator_login set `Logout_Datetime`=now(),`Duration`=Timestampdiff(minute,NOw(),Login_DateTime)
				where `Login_By`=_scanner_ip;
				
				UPDATE `tbl_zone_master` SET `Status`='INACTIVE'  WHERE `Scanner_IPAddress`=_scanner_ip;
				
				SELECT distinct `Zone_IPAddress`,`LCD_IPAddress`,`PTL_SendPort`,`LCD_SendPort` FROM `tbl_zone_master` ZM 
				INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.`Zone_ID`=ZM.`Zone_Id`
				WHERE Scanner_IPAddress=_scanner_ip;
			End if;
		end if;
		if _mode=2 then			
			   Select Q.`Carate_Code`, Q.`PTL_IPAddress`,Q.`Pick_Location`,Q.`PTL_SendPort`,
			   CONCAT(REPEAT('0', 3- LENGTH((`PickQty`-`Picked_Qty`))),(`PickQty`-`Picked_Qty`))  as PickQty,Q.`Sku_Type`,Q.`LCD_IPAddress`,Q.`LCD_SendPort`
			   from `tbl_request_queue_for_ptl` Q 
			   inner Join `tbl_zone_master` ZM  on ZM.`Zone_IPAddress`=Q.PTL_IPAddress
			   where Q.`Scanner_IPAddress`=_scanner_ip  and Q.`Invoice_Number`=_ScanCode 
			   and (Q.Status='PENDING' or `PickConfirmation` is null)
			   and ZM.Status='ACTIVE';
			   
			   Update tbl_request_queue_for_ptl Q
			   INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.PTL_IPAddress
			   set Q.STATUS='INPROCESS' ,Q.`PickStart`=now() 
			   where Q.`Scanner_IPAddress`=_scanner_ip AND Q.`Invoice_Number`=_ScanCode  
			   AND STATUS='PENDING' AND ZM.Status='ACTIVE';	
			   
                           select `Carate_Code` from `tbl_request_queue_for_ptl` Q where  `Invoice_Number`=_ScanCode limit 1 into _Crate_code;			   
			   
			   update tbl_carate_status   set `Status`='INPROCESS',`FirstPick`=now() where `Carate_Code`= _Crate_code
			   and Status='INDUCTED';	   
		End if;
		if _mode=3 then
			update `tbl_zone_master` set `Status`='LOGIN' 
			where `Scanner_IPAddress`=_scanner_ip and Status !='ACTIVE';
		end if;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_Map_CaratetoInvoiceandPrepareList` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_Map_CaratetoInvoiceandPrepareList` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_Map_CaratetoInvoiceandPrepareList`(_Carate_code varchar(30),_system_IP varchar(30),_invoice_no varchar(30),_user varchar(30))
BEGIN
	Declare _scanner_IP VARCHAR(30);
	Declare _MEssageinfo varchar(1000);
	Declare _Crate_Weight decimal(10,2);
	SET _scanner_IP='192.168.10.220';
	IF Not Exists (Select `Invoice_Number` from `tbl_order_header` where `Invoice_Number`=	_invoice_no) then
		set _MEssageinfo='Invalid Invoice Number Used';
		select _MEssageinfo as ErrorStatus;
	Else
	
	 IF not Exists(select `Crate_Code` from `tbl_carate_master` where `Crate_Code`= _Carate_code and `isActive`=0) then
		
		select `Crate_Weight` from `tbl_carate_master` where `Crate_Code`=_Carate_code into _Crate_Weight;
		insert IGNORE into tbl_carate_invoice_mapping(`Carate_Barcode`,`Source_IPAddress`,`Scanner_IP`,`Invoice_Number`,`Mapping_Date`,`Mapped_By`)
		values(_Carate_code,_system_IP,_scanner_IP,_invoice_no,now(),_user);

		insert IGNORE into `tbl_request_queue_for_ptl`(`Carate_Code`,`Invoice_Number`,`Pick_Location`,`Sku`,`Sku_Type`,`Scanner_IPAddress`,`PickQty`,
		`Picked_Qty`,`Status`,`PTL_IPAddress`,`LCD_IPAddress`,`PTL_SendPort`,`LCD_SendPort`)
		SELECT _Carate_code, _invoice_no,ZPM.`PTL`,PSM.`Sku`,ZM.`Zone_Type`,ZM.`Scanner_IPAddress`,OL.`Pick_Qty`,0,'PENDING',
		ZM.`Zone_IPAddress`,ZM.`LCD_IPAddress`,ZM.`PTL_SendPort`,ZM.`LCD_SendPort` 
		FROM tbl_zone_master ZM  
		INNER JOIN `tbl_zone_ptl_mapping` ZPM  ON ZPM.Zone_Id=ZM.Zone_Id
		INNER JOIN `tbl_order_line_header` OL ON OL.`Pick_Location`=ZPM.`Pick_Location`
		INNER JOIN `tbl_order_header` OH  ON OH.`Order_id`=OL.`Order_id`
		INNER JOIN `tbl_carate_invoice_mapping` CIM ON CIM.`Invoice_Number`=OH.`Invoice_Number`
		-- INNER JOIN `tbl_ptl_sku_mapping` PSM  ON PSM.`Pick_Location`=ZPM.Pick_Location
		WHERE CIM.`Carate_Barcode`=_Carate_code and OH.`Invoice_Number`=_invoice_no;
		
		insert IGNORE into `tbl_carate_status` (`Carate_Code`,Invoice_No,`Actual_Weight`,`Status`,Inducted)
		select _Carate_code,_invoice_no,(`Order_Weight`+_Crate_Weight) as Actual_Weight,'INDUCTED',now() from `tbl_order_header` OH 
		where `Invoice_Number`=_invoice_no;
		
		Select Concat(EmptyCount, ' Absolute Sku''s picking is required First' ) as EmptyLocation from
		(select COUNT(`Line_Item_Seq_no`) EmptyCount from  `tbl_order_line_header` OLH  
		inner Join `tbl_order_header` OH  on OH.`Order_id`=OLH.`Order_id`
		inner Join `tbl_carate_invoice_mapping` CR on CR.`Invoice_Number`=OH.`Invoice_Number`
		where OLH.`Pick_Location`='' and CR.`Carate_Barcode`=_Carate_code) T where T.EmptyCount>0  limit 1 into _MEssageinfo;
		select _MEssageinfo as  EmptyLocation;
		
		update`tbl_carate_master` set isActive=1 WHERE `Crate_Code`= _Carate_code AND `isActive`=0;
		
	 Else 
		SELECT CASE WHEN CS.Status IS NULL THEN 'INVALID INVOICE NUMBER' ELSE IFNULL(OH.`Invoice_Number`,'INVALID INVOICE NUMBER') END  AS ErrorStatus FROM `tbl_order_header` OH 
		LEFT OUTER JOIN `tbl_carate_invoice_mapping` CM  ON CM.`Invoice_Number`=OH.Invoice_Number
		LEFT OUTER JOIN `tbl_carate_status` CS  ON CS.`Invoice_No`=OH.`Invoice_Number`
		where OH.`Invoice_Number`=_invoice_no  LIMIT 1 into  _MEssageinfo; 
		IF Length(_MEssageinfo)=0 then
			select Concat('Crate :',`Carate_Barcode`,' Already inprocess For Invoice :',`Invoice_Number` ,' ,Mapped By ',`Mapped_By`,'on ',`Mapping_Date`) as ErrorStatus 
			from `tbl_carate_invoice_mapping` CM 
			inner Join `tbl_carate_status` CS  on CS.`Carate_Code`=CM.Carate_Barcode  where CM.`Carate_Barcode`=_Carate_code 
			and CS.`Status` not in ('COMPLETED','ERROR') limit 1 into _MEssageinfo;
		End IF;
		SELECT _MEssageinfo AS  ErrorStatus;
	 End if;
	END IF;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `SP_PickConfirmation` */

/*!50003 DROP PROCEDURE IF EXISTS  `SP_PickConfirmation` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_PickConfirmation`(_mode varchar(10),_scan_code varchar(30),_ip varchar(30))
BEGIN
		Declare _sku_code varchar(30);
		declare _ser_no varchar(30);
		Declare _Carate_Code varchar(30);
		Declare _BayScanner_IP varchar(30);
		Declare _MSG  varchar(3);
		IF _mode='B1' then
			Update `tbl_request_queue_for_ptl` Q 
			inner Join `tbl_zone_master` ZM  on ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
			set Q.`Status`='COMPLETED',Q.`PickConfirmation`=now(),Q.`Picked_Qty`=Q.`PickQty`,
			Duration=TIMESTAMPDIFF(MINUTE,NOW(),`PickStart`)
			where Q.`Pick_Location`=_scan_code and Q.Status='INPROCESS' and ZM.`Zone_IPAddress`=_ip and ZM.Status='ACTIVE';
			
			Select IFNULL(A.`Scanner_IPAddress`,'0') from  `tbl_request_queue_for_ptl` Q
			INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
			where Q.`Pick_Location`=_scan_code AND ZM.`Zone_IPAddress`=_ip AND ZM.Status='ACTIVE' limit 1 into _BayScanner_IP;
			
			SELECT IFNULL(Q.`Carate_Code`,'0') FROM  `tbl_request_queue_for_ptl` Q
			INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
			WHERE Q.`Pick_Location`=_scan_code AND ZM.`Zone_IPAddress`=_ip AND ZM.Status='ACTIVE' LIMIT 1 INTO _Carate_Code;
			 			
			IF not Exists (select Q.`QueueID`  from `tbl_request_queue_for_ptl` Q
			where Q.`Scanner_IPAddress`=_BayScanner_IP and Carate_Code=_Carate_Code and `Status`='INPROCESS') Then
				IF Exists (Select `QueueID` from `tbl_request_queue_for_ptl` 
					where `Carate_Code`=_Carate_Code and `Status`='PENDING') then
						SELECT  Q.`PTL_IPAddress`,Q.`Pick_Location`,Q.`PTL_SendPort`,'PAS' AS PickQty,'GREEN' as `Sku_Type`,Q.`LCD_IPAddress`,Q.`LCD_SendPort`
						FROM `tbl_request_queue_for_ptl` Q
						INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
						WHERE Q.`Pick_Location` =_scan_code and Q.`Status`='COMPLETED' AND Q.`Scanner_IPAddress`=_BayScanner_IP and ZM.Status='ACTIVE' limit 10;
				else
						SELECT  Q.`PTL_IPAddress`,Q.`Pick_Location`,Q.`PTL_SendPort`,'EXT' AS PickQty,'GREEN' AS `Sku_Type`,Q.`LCD_IPAddress`,Q.`LCD_SendPort`
						FROM `tbl_request_queue_for_ptl` Q
						INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
						WHERE Q.`Pick_Location` =_scan_code AND Q.`Status`='COMPLETED' AND Q.`Scanner_IPAddress`=_BayScanner_IP AND ZM.Status='ACTIVE' LIMIT 10;
				end if;
			end if;
			
		else 
			if locate('.',_scan_code)>0 and  LOCATE('=',_scan_code) =0 then
				set _sku_code=LEFT(_scan_code,LOCATE('.',_scan_code)-1); 
				set _ser_no=REVERSE(LEFT(REVERSE(_scan_code),LOCATE('.',REVERSE(_scan_code))-1));
				
				INSERT IGNORE INTO tbl_scan_Pick_details(`Carate_Code`,`Invoice_No`, `Sku`,`Ser_no`,`Scan_by`,QueueID,ScanedCode)
				SELECT `Carate_Code`,`Invoice_Number`,_sku_code,_ser_no,_ip,`QueueID`,_scan_code FROM `tbl_request_queue_for_ptl` 
				WHERE `Sku` =_sku_code AND STATUS='INPROCESS' AND `Scanner_IPAddress`=_ip ;
				
				UPDATE `tbl_request_queue_for_ptl` SET `Picked_Qty`= (CASE WHEN `PickQty`>`Picked_Qty` THEN `Picked_Qty`+1 ELSE Picked_Qty END)
				WHERE `Sku` =_sku_code AND STATUS='INPROCESS' AND `Scanner_IPAddress`=_ip;
			
			else 
				set _sku_code='';
				set _ser_no=0;
				
				INSERT IGNORE INTO tbl_scan_Pick_details(`Carate_Code`,`Invoice_No`, `Sku`,`Ser_no`,`Scan_by`,QueueID,ScanedCode)
				SELECT `Carate_Code`,`Invoice_Number`,_sku_code,_ser_no,_ip,0,_scan_code FROM `tbl_request_queue_for_ptl` 
				WHERE  STATUS='INPROCESS' AND `Scanner_IPAddress`=_ip limit 1 ;
				
				/*UPDATE `tbl_request_queue_for_ptl` SET `Picked_Qty`= (CASE WHEN `PickQty`>`Picked_Qty` THEN `Picked_Qty`+1 ELSE Picked_Qty END)
				WHERE  STATUS='INPROCESS' AND `Scanner_IPAddress`=_ip limit 1;*/
			
			end if;
			
			IF Exists (Select `Carate_Code` from `tbl_request_queue_for_ptl` 
					where `Sku` =_scan_code  and (`PickQty`-`Picked_Qty`)>0 AND STATUS='INPROCESS') then
				SELECT  `PTL_IPAddress`,`Pick_Location`,`PTL_SendPort`,
				CONCAT(REPEAT('0', 3- LENGTH((`PickQty`-`Picked_Qty`))),(`PickQty`-`Picked_Qty`))  AS PickQty,`Sku_Type`,`LCD_IPAddress`,`LCD_SendPort`
				FROM `tbl_request_queue_for_ptl` 
				WHERE `Sku` =_scan_code AND STATUS='INPROCESS' and `Scanner_IPAddress`=_ip;
			else
				UPDATE `tbl_request_queue_for_ptl` SET `Status`='COMPLETED',`PickConfirmation`=NOW(),
				Duration=TIMESTAMPDIFF(MINUTE,NOW(),`PickStart`)
				WHERE `Sku` =_scan_code  AND STATUS='INPROCESS';				
			
				SELECT `Carate_Code` FROM  `tbl_request_queue_for_ptl` 
				WHERE Scanner_IPAddress=_ip and `Status`='COMPLETED' order by QueueID desc LIMIT 1 INTO _Carate_Code;
				
				IF NOT EXISTS (SELECT `QueueID`  FROM `tbl_request_queue_for_ptl` 
				WHERE `Scanner_IPAddress`=_ip AND Carate_Code= _Carate_Code and  `Status`='INPROCESS') THEN
					IF EXISTS (SELECT `QueueID` FROM `tbl_request_queue_for_ptl` 
						WHERE `Carate_Code`=_Carate_Code AND `Status`='PENDING') THEN
							SELECT  Q.q`PTL_IPAddress`,Q.`Pick_Location`,Q.`PTL_SendPort`,'PAS' AS PickQty,'GREEN' AS `Sku_Type`,Q.`LCD_IPAddress`,Q.`LCD_SendPort`
							FROM `tbl_request_queue_for_ptl` Q
							INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
							WHERE Q.`Sku` =_scan_code AND Q.`Status`='COMPLETED' AND Q.`Scanner_IPAddress`=_ip and ZM.Status='ACTIVE' LIMIT 10;
					ELSE
							SELECT  `PTL_IPAddress`,`Pick_Location`,`PTL_SendPort`,'EXT' AS PickQty,'RED' AS `Sku_Type`,`LCD_IPAddress`,`LCD_SendPort`
							FROM `tbl_request_queue_for_ptl` 
							INNER JOIN `tbl_zone_master` ZM  ON ZM.`Zone_IPAddress`=Q.`PTL_IPAddress`
							WHERE Q.`Sku` =_scan_code AND Q.`Status`='COMPLETED' AND Q.`Scanner_IPAddress`=_ip AND ZM.Status='ACTIVE' LIMIT 10;
					END IF;
				END IF;
			End if;
			  
		End IF;
	END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
