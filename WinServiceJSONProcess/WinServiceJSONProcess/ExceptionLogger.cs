﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinServiceJSONProcess
{
    public static class ExceptionLogger
    {
        //Log Exception
        public static void LogAllError(Exception x, string _invoice, string sku)
        {
            try
            {
                string errorCode = x.GetType().Name;

                var st = new StackTrace(x, true);
                var frames = st.GetFrames();
                string query = @"insert into tbl_order_error_details(Invoice_Number,Sku,Error_Message) values(?error_code,?sku,?message)";

                MySqlParameter[] param = new MySqlParameter[] {
                new MySqlParameter("?error_code",_invoice),
                new MySqlParameter("?message",x.ToString()),
               new MySqlParameter("?sku",sku)
            };

                General.DMLWithParameter(query, param);
            }
            catch (Exception e)
            {
            }

        }
        public static void LogAllError(Exception x)
        {


            try
            {
                string errorCode = x.GetType().Name;

                var st = new StackTrace(x, true);
                var frames = st.GetFrames();
                var traceString = "";
                //foreach (var frame in frames)
                //{
                //    if (frame.GetFileLineNumber() < 1)
                //        continue;

                //    string[] filePathArray = frame.GetFileName().Split('\\');

                //    string fileName = filePathArray.Last();
                //    string methode = frame.GetMethod().Name;
                //    int lineNumber = frame.GetFileLineNumber();

                //    traceString = "File Name =>" + fileName + System.Environment.NewLine + ", Methode => " + methode + System.Environment.NewLine + ", Line =>" + lineNumber;


                string query = @"insert into error_log(source,message) values(?error_code,?message)";

                MySqlParameter[] param = new MySqlParameter[] {
                new MySqlParameter("?error_code","JSONService"),
                new MySqlParameter("?message",x.ToString())
            };

                General.DMLWithParameter(query, param);
                //}
            }
            catch (Exception e)
            {

                //string query = @"insert into error_log(error_code,error_desc,error_timestamp) values('ExceptionLogger','ExceptionLogger','" + System.DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "')";
                //General.DML(query, true);
            }

        }
    }
}
