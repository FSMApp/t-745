﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinServiceJSONProcess
{
    public class General
    {
        public static string MySqlConnectionString = ConfigurationManager.ConnectionStrings["Constr"].ConnectionString;

        static string fileMysqlQuery = "MysqlQuery.txt";
        public static bool SetConnection()
        {
            try
            {
                MySqlConnectionString = ConfigurationManager.ConnectionStrings["Constr"].ConnectionString;
            }
            catch (Exception e)
            {
                //WriteToLogFile(e.StackTrace, "Error.txt");
                return false;

            }
            return true;
        }

        public static DataTable SelectQuery(string query)
        {
            DataTable dt = new DataTable();
            try
            {
                using (MySqlConnection conn = new MySqlConnection(MySqlConnectionString))
                {
                    conn.Open();
                    MySqlCommand command = new MySqlCommand(query, conn);
                    MySqlDataAdapter adapter = new MySqlDataAdapter(command);
                    adapter.Fill(dt);
                }
            }
            catch (Exception e)
            {
                //WriteToLogFile(e.StackTrace, "Error.txt");
                ExceptionLogger.LogAllError(e);
            }
            return dt;
        }
        public static void DMLWithParameter(string query, MySqlParameter[] parameter = null)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(MySqlConnectionString))
                {
                    using (MySqlCommand cmd = new MySqlCommand(query, connection))
                    {
                        connection.Open();
                        if (parameter != null)
                        {
                            cmd.Parameters.AddRange(parameter);
                        }
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                ExceptionLogger.LogAllError(e);
                throw;
            }
        }
        public static void DML(string query)
        {
            try
            {
                using (MySqlConnection connection = new MySqlConnection(MySqlConnectionString))
                {
                    using (MySqlCommand cmd = new MySqlCommand(query, connection))
                    {
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf("DUPLICATE entry") == -1)
                    ExceptionLogger.LogAllError(e);
                throw;
            }
        }
        public static Int64 DML(string query, bool orderid)
        {
            Int64 rownumber = 0;
            try
            {
                using (MySqlConnection connection = new MySqlConnection(MySqlConnectionString))
                {
                    using (MySqlCommand cmd = new MySqlCommand(query, connection))
                    {
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        rownumber = cmd.LastInsertedId;
                    }
                }
            }
            catch (Exception e)
            {
                ExceptionLogger.LogAllError(e);
                rownumber = 0;
            }
            return rownumber;
        }
        public static string SelectScaler(string query)
        {
            string value = "";
            try
            {
                using (MySqlConnection connection = new MySqlConnection(MySqlConnectionString))
                {
                    using (MySqlCommand cmd = new MySqlCommand(query, connection))
                    {
                        connection.Open();
                        value = Convert.ToString(cmd.ExecuteScalar());
                    }
                }
            }
            catch (Exception e)
            {
                //WriteToLogFile(e.StackTrace, "Error.txt");
                ExceptionLogger.LogAllError(e);
            }
            return value;

        }
        public static void TransactionalQuery(string query, MySqlTransaction mysqlTrans = null, MySqlConnection mysqlCon = null)
        {
            try
            {
                using (MySqlCommand cmd = new MySqlCommand(query, mysqlCon, mysqlTrans))
                {
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                //WriteToLogFile(e.Message+"--"+ e.StackTrace, "Error.txt");
                throw;
            }
        }
        public static string TransactionalQueryScaler(string query, MySqlTransaction mysqlTrans = null, MySqlConnection mysqlCon = null)
        {
            try
            {
                using (MySqlCommand cmd = new MySqlCommand(query, mysqlCon, mysqlTrans))
                {

                    return Convert.ToString(cmd.ExecuteScalar());
                }
            }
            catch (Exception e)
            {
                //WriteToLogFile(e.Message + "--" + e.StackTrace, "Error.txt");
                throw;
            }

        }
        public static DataTable TransactionalQueryData(string query, MySqlTransaction mysqlTrans = null, MySqlConnection mysqlCon = null)
        {
            DataTable dt = new DataTable();
            try
            {
                using (MySqlCommand cmd = new MySqlCommand(query, mysqlCon, mysqlTrans))
                {
                    MySqlDataAdapter da = new MySqlDataAdapter(cmd);
                    da.Fill(dt);
                    return dt;

                }
            }
            catch (Exception e)
            {
                //WriteToLogFile(e.Message + "--" + e.StackTrace, "Error.txt");

                throw;
            }
        }
    }
}
