﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Web.Script.Serialization;
using System.Timers;
using System.Configuration;
using System.IO;

namespace WinServiceJSONProcess
{
    public class LineItem
    {
        public string line_number { get; set; }
        public string product_code { get; set; }
        public string order_quantity { get; set; }
        public string location { get; set; }
        public string station { get; set; }
    }
    public class Datum
    {
        public string invoice_number { get; set; }
        public string order_number { get; set; }
        public string warehouse_id { get; set; }
        public List<LineItem> line_items { get; set; }
        public string order_date { get; set; }
        public string invoice_date { get; set; }
        public string sales_date { get; set; }
        public string due_date { get; set; }
        public string delivery_date { get; set; }
        public string order_status { get; set; }
        public double order_weight { get; set; }
    }
    public class RootObject
    {
        public string batch_number { get; set; }
        public List<Datum> data { get; set; }
    }
    public partial class WinServiceJSONProcess : ServiceBase
    {
        public WinServiceJSONProcess()
        {
            InitializeComponent();
        }
        Timer timerJson = null;
        string _constr = string.Empty;
        protected override void OnStart(string[] args)
        {
            _constr = ConfigurationManager.ConnectionStrings["Constr"].ConnectionString;
            timerJson = new Timer();
            timerJson.Interval = 1000;
            timerJson.Elapsed += TimerJson_Elapsed;
            timerJson.Enabled = true;
            timerJson.Start();
        }

        private void TimerJson_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (timerJson.Enabled)
            {
                timerJson.Enabled = false;
                DataTable dtFiles = getAllUnprocessedFiles();
                if (dtFiles != null && dtFiles.Rows.Count > 0)
                {
                    foreach (DataRow drfile in dtFiles.Rows)
                    {
                        DateTime File_ProcessStarted = DateTime.Now;
                        if (ProcessFile(Convert.ToString(drfile["File_Name"])))
                        {
                            string _updateQuery = "Update tbl_File_Master set file_Status=1,File_Processed=Now(),File_ProcessStarted='" + File_ProcessStarted.ToString("yyyy-MM-dd HH:mm:ss") + "' where File_Name='" + Convert.ToString(drfile["File_Name"]) + "';";
                            General.DML(_updateQuery);
                        }
                    }
                }
                timerJson.Enabled = true;
            }
        }
        bool ProcessFile(string _fileName)
        {
            string myjson = File.ReadAllText(Path.Combine(ConfigurationManager.AppSettings["JsonFilePath"], _fileName + ".txt"));
            try
            {
                JavaScriptSerializer objDel = new JavaScriptSerializer();
                List<RootObject> ORiData = objDel.Deserialize<List<RootObject>>("[" + myjson + "]");
                foreach (var batch in ORiData)
                {
                    Parallel.ForEach(batch.data, new ParallelOptions() { MaxDegreeOfParallelism = 5 }, invoice =>
                    {
                        {
                            Int64 Orderid = 0;
                            try
                            {
                                string _status = General.SelectScaler("Call SP_GetInvoiceStatus('" + invoice.invoice_number + "');");
                                if (_status == string.Empty)
                                {
                                    string _sale_date = Convert.ToDateTime(invoice.sales_date).ToString("yyyy-MM-dd HH:mm:ss");
                                    string strquery = string.Format("Insert into tbl_order_header (Order_Number, Order_Weight, Invoice_Number, Warehouse_id, Line_Items, Sales_date,File_ID) Values('{0}','{1}',{2},'{3}','{4}','{5}','{6}')",
                                        invoice.order_number, invoice.order_weight, invoice.invoice_number, invoice.warehouse_id, invoice.line_items.Count, _sale_date, _fileName);
                                    Orderid = General.DML(strquery, true);
                                    strquery = string.Empty;
                                    if (Orderid > 0)
                                    {
                                        foreach (var lines in invoice.line_items)
                                        {
                                            try
                                            {
                                                if (lines.station.Trim() == string.Empty)
                                                {
                                                    ExceptionLogger.LogAllError(new Exception("Pick Location is Empty"), invoice.invoice_number, lines.product_code);
                                                }
                                                else
                                                {
                                                    strquery = string.Format("Insert ignore into tbl_order_line_header(Order_id,Line_Item_Seq_no,Sku,Pick_Qty,Pick_Location,Location) Values({0},{1},'{2}',{3},'{4}','{5}')",
                                                        Orderid, lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location);
                                                    General.DML(strquery);
                                                }
                                            }
                                            catch (Exception erLine)
                                            {
                                                ExceptionLogger.LogAllError(erLine, invoice.invoice_number, lines.product_code);
                                            }
                                        }
                                    }
                                }
                                else if (_status.Split('|')[0].ToUpper() == "PENDING")
                                {
                                    Int64 _orderid = Convert.ToInt64(_status.Split('|')[1]);
                                    ManupulateData(invoice, _orderid, _fileName);
                                }
                            }
                            catch (Exception innerex)
                            {
                                ExceptionLogger.LogAllError(innerex, invoice.invoice_number, string.Empty);
                                //ExceptionLogger.LogAllError(innerex);
                            }
                        }
                    });
                }
                return true;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogAllError(ex);
                return false;
            }
        }
        static void ManupulateData(dynamic invoice, Int64 _orderid, string fileid)
        {
            try
            {
                string _sale_date = Convert.ToDateTime(invoice.sales_date).ToString("yyyy-MM-dd HH:mm:ss");
                string strquery = string.Format("Update tbl_order_header  set File_ID='{6}', Order_Number='{0}',Update_DateTime=NOW(), Order_Weight={1}, Warehouse_id={3}, Line_Items={4}, Sales_date='{5}' where Invoice_Number='{2}'",
                    invoice.order_number, invoice.order_weight, invoice.invoice_number, invoice.warehouse_id, invoice.line_items.Count, _sale_date, fileid);
                General.DML(strquery);
                strquery = string.Empty;
                foreach (var lines in invoice.line_items)
                {
                    if (lines.station.Trim() == string.Empty)
                    {
                        ExceptionLogger.LogAllError(new Exception("Pick Location is Empty"), invoice.invoice_number, lines.product_code);
                    }
                    else
                    {
                        try
                        {
                            try
                            {
                                strquery = string.Format("Insert into tbl_order_line_header(Order_id,Line_Item_Seq_no,Sku,Pick_Qty,Pick_Location,Location) Values({0},{1},'{2}',{3},'{4}','{5}')",
                                _orderid, lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location);
                                General.DML(strquery);
                            }
                            catch
                            {
                                strquery = string.Format("Update tbl_order_line_header set Line_Item_Seq_no={0},Pick_Qty={2},Pick_Location='{3}',Location='{4}' where Order_id={5} and Sku={1};",
                                    lines.line_number, lines.product_code, lines.order_quantity, lines.station, lines.location, _orderid);
                                General.DML(strquery);
                            }
                        }
                        catch (Exception inner)
                        {
                            if (inner.Message.IndexOf("DUPLICATE entry") == -1)
                                ExceptionLogger.LogAllError(inner, invoice.invoice_number, lines.product_code);
                        }
                    }
                }
            }
            catch (Exception oinner)
            {
                ExceptionLogger.LogAllError(oinner, invoice.invoice_number, string.Empty);
            }
        }
        DataTable getAllUnprocessedFiles()
        {
            DataTable dtFiles = null;
            try
            {
                using (MySqlConnection mycon = new MySqlConnection(_constr))
                {
                    using (MySqlCommand mycmd = new MySqlCommand("select File_Name from tbl_file_Master  where file_Status=0;", mycon))
                    {
                        mycmd.CommandType = CommandType.Text;
                        using (MySqlDataAdapter myadp = new MySqlDataAdapter(mycmd))
                        {
                            dtFiles = new DataTable();
                            myadp.Fill(dtFiles);
                        }
                    }
                }
                return dtFiles;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogAllError(ex);
                return null;
            }
            finally
            {
                if (dtFiles != null)
                {
                    dtFiles.Dispose();
                }
            }
        }
        protected override void OnStop()
        {
            timerJson.Stop();
            timerJson.Enabled = false;
        }
    }
}
